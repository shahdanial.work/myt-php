<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class L_mesej {

  public function __construct(){
    $this->CI =& get_instance();
    $this->CI->load->dbforge();
  }
  # FROM SYSTEM TO USER
  public function system_user($id, $content){
    # insert new table.
    $fields = array(
      'dari' => array(
        'type' =>'VARCHAR',
        'constraint' => '9'
      ),
      'status' => array(
        'type' =>'VARCHAR',
        'constraint' => '20'
      ),
      'waktu' => array(
        'type' =>'VARCHAR',
        'constraint' => '35'
      ),
      'mesej' => array(
        'type' =>'VARCHAR',
        'constraint' => '900'
      )
    );
    $this->CI->dbforge->add_field('id');
    $this->CI->dbforge->add_field($fields);
    $attributes = array('ENGINE' => 'InnoDB');
    $this->CI->dbforge->create_table('zz_mesej_' . $id, TRUE, $attributes); // TRUE if table name not exist
    #then insert data to table
    date_default_timezone_set("Asia/Kuala_Lumpur");
    $masa = date("g:iA j M Y"); // 11:01PM 1 Jan 2017
    $data = array();
    foreach ($content as $key => $mesej) {
      $data[] = array(
        'mesej' => $mesej,
        'waktu' => $masa,
        'status' => 'not_seen',
        'dari' => 'A' #A is id of Malayatimes System
      );
    }
    $this->CI->db->insert_batch('zz_mesej_' . $id, $data);
  } // END OG SYSTEM_USER

}
