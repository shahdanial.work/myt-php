<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class L_login {

  // SET SUPER GLOBAL
  var $CI = NULL;

  /**
  * Class constructor
  *
  * @return   void
  */
  public function __construct() {
    $this->CI =& get_instance();
    $this->CI->load->library('encrypt');
    $this->CI->load->helper(array('h_function_helper'));
  }

  public function login($email, $password,$hangpimana) {
    #cek email dan password, get id of this email and password entered
    //$query = $this->CI->db->get_where('users',array('email'=>$email,'password' => tapuk($password)));
    $password = tapuk($password);
    $query = $this->CI->db->conn_id->prepare("SELECT id_user FROM users WHERE (email = :email AND password = :password)");
    $query->execute(
      array(
        ':email'         => $email,
        ':password'          => $password
        )
    );

    if($query->rowCount() > 0) {
      //foreach ($query->fetch(PDO::FETCH_ASSOC) as $row){
        $id_user = $query->fetch(PDO::FETCH_ASSOC)['id_user'];
        print_r($id_user);
        $this->CI->session->set_userdata('email', $email);

        $session_id = uniqid(rand());

        $hehe_id = $id_user * 79346;

        $user_id = $hehe_id  .''. $session_id;

        $this->CI->session->set_userdata('id_login', $session_id);
        $this->CI->session->set_userdata('id', $this->CI->encrypt->encode($user_id));//base64_encode($user_id));

        redirect($hangpimana);

      //} //end foreach

    } else {
      #jika tidak ada, set notifikasi dalam flashdata.
      $this->CI->session->set_flashdata('sukses','Email atau Password salah');

      #redirect ke halaman login
      redirect(site_url('login?redirect='. urlencode($hangpimana)));
    }
    //return false;
  }

  public function xhr_login($email, $password,$hangpimana) {
    #cek email dan password, get id of this email and password entered
    //$query = $this->CI->db->get_where('users',array('email'=>$email,'password' => tapuk($password)));
    $password = tapuk($password);
    $query = $this->CI->db->conn_id->prepare("SELECT id_user FROM users WHERE (email = :email AND password = :password)");
    $query->execute(
      array(
        ':email'         => $email,
        ':password'          => $password
        )
    );

    if($query->rowCount() > 0) {
      //foreach ($query->fetch(PDO::FETCH_ASSOC) as $row){
        $id_user = $query->fetch(PDO::FETCH_ASSOC)['id_user'];

        $this->CI->session->set_userdata('email', $email);

        $session_id = uniqid(rand());

        $hehe_id = $id_user * 79346;

        $user_id = $hehe_id  .''. $session_id;

        $this->CI->session->set_userdata('id_login', $session_id);
        $this->CI->session->set_userdata('id', $this->CI->encrypt->encode($user_id));//base64_encode($user_id));

        redirect($hangpimana); #if xhr.. go to after_social_login. then other go to id(refferer)

      //} //end foreach

    } else {
      #jika tidak ada, set notifikasi dalam flashdata.
      print_r('Email atau Password salah..');
    }
    //return false;
  }

  public function login_google($gplus_id, $redirect){

    $query = $this->CI->db->conn_id->prepare("SELECT id_user FROM users WHERE (gplus_id = :gplus_id)");
    $query->execute(
      array(
        ':gplus_id'         => $gplus_id
        )
    );

    if($query->rowCount() > 0) {

        $id_user = $query->fetch(PDO::FETCH_ASSOC)['id_user'];

        $session_id = uniqid(rand());

        $hehe_id = $id_user * 79346;

        $user_id = $hehe_id  .''. $session_id;

        $this->CI->session->set_userdata('id_login', $session_id);
        $this->CI->session->set_userdata('id', $this->CI->encrypt->encode($user_id));//base64_encode($user_id));

        redirect($redirect);

    } else {
      redirect(site_url('login?redirect='.urlencode($redirect)));
    }
  }

  public function cek_login($hangpimana) {
    $this->CI->output->set_header('X-Robots-Tag: noindex');
    //cek session email
    $session_id = $this->CI->session->userdata('id_login');
    if(!$session_id) {
    # tell google this for user only.
    set_status_header('403');
      //set notifikasi
      $this->CI->session->set_flashdata('sukses', 'Email atau Password salah');

      //alihkan ke halaman login
      if(isset($hangpimana)){
        redirect(site_url('login?redirect='.urlencode($hangpimana)));
      } else {
        redirect(site_url('login'));
      }
    } else {
      set_status_header('200');
    }

  }

  public function logout($hangpimana) {
    $this->CI->session->unset_userdata('id_login');
    $this->CI->session->unset_userdata('id');
    $this->CI->session->unset_userdata('mantakedijolo');
    $this->CI->session->set_flashdata('sukses','Anda telah logout');
    redirect(site_url('login?redirect='.urlencode($hangpimana)));
  }
}
