<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_img extends CI_Model{

   /* PENGGUNA:
   contoller: Submit.php
   FUNGSI: Record penggunaan gambar dari post tertentu yang x di upload sendiri (di upload di post lain). record agar tidak di dielete kelak.
   */
   function record($filename, $from_tbl, $from_id){
      #check dulu ada x sesiap record.
      $check_manager = $this->db->conn_id->prepare(
         "SELECT id FROM file_manager WHERE filename = :filename AND from_tbl = :from_tbl AND from_id = :from_id"
      );
      $check_manager->execute(array(
         ':filename' => $filename,
         ':from_tbl' => $from_tbl,
         ':from_id' => $from_id
      ));
      if( !$check_manager->rowCount() > 0 ){

         #mmg baru dia guna ni. mari record.
         if(file_exists('./gambar/'.$filename)){ #file_wujud

            $record = $this->db->conn_id->prepare(
               "INSERT INTO file_manager
               (filename, from_tbl, from_id)
               VALUES (:filename, :from_tbl, :from_id)"
            );
            $record->execute(array(
               ':filename' => $filename,
               ':from_tbl' => $from_tbl,
               ':from_id' => $from_id
            ));
            return true;

         } else {

            $query = $this->db->conn_id->prepare(
               "DELETE FROM file_manager WHERE filename = :filename AND from_tbl = :from_tbl AND from_id = :from_id"
            );
            $query->execute(array(
               ':filename' => $filename,
               ':from_tbl' => $from_tbl,
               ':from_id' => $from_id
            ));
            return false;
         }
      }
   }

   /* PENGGUNA:
   contoller: Submit.php
   FUNGSI: delete file yg di upload dari satu post tapi x guna IF TAKDE orang lain guna. Jika ada orang lain guna, delete record je.
   */
   function delete($filename, $from_tbl, $from_id){

      $check_manager = $this->db->conn_id->prepare(
         "SELECT id FROM file_manager WHERE filename = :filename AND from_tbl != :from_tbl AND from_id != :from_id" #check ada x post lain guna.
      );
      $check_manager->execute(array(
         ':filename' => $filename,
         ':from_tbl' => $from_tbl,
         ':from_id' => $from_id
      ));

      if($check_manager->rowCount() > 0){# ada post orang lain guna. delete record ko je kat manager.

         $delete_manager = $this->db->conn_id->prepare(
            "DELETE FROM file_manager WHERE filename = :filename AND from_tbl = :from_tbl AND from_id = :from_id"
         );
         $delete_manager->execute(array(
            ':filename' => $filename,
            ':from_tbl' => $from_tbl,
            ':from_id' => $from_id
         ));

      } else { #ko sorang je guna. delete tbl and manager sekali.

         #get_id file before detete ($file_tbl_id)
         $get = $this->db->conn_id->prepare(
            "SELECT id FROM file_tbl WHERE filename = :filename"
         );
         $get->execute(array(
            ':filename' => $filename
         ));

         $file_tbl_id = $get->fetch(PDO::FETCH_COLUMN);

         $delete_manager = $this->db->conn_id->prepare(
            "DELETE FROM file_manager WHERE filename = :filename"
         );
         $delete_manager->execute(array(
            ':filename' => $filename
         ));

         $delete_tbl = $this->db->conn_id->prepare(
            "DELETE FROM file_tbl WHERE filename = :filename"
         );
         $delete_tbl->execute(array(
            ':filename' => $filename
         ));

         #delete tag_manager
         if($file_tbl_id){
            $delete_img_tag = $this->db->conn_id->prepare(
               "DELETE FROM tag_manager WHERE from_tbl = :from_tbl AND from_id = :from_id"
            );
            $delete_img_tag->execute(array(
               ':from_tbl' => 'file_tbl',
               ':from_id' => $file_tbl_id
            ));
         }

         #delete file!
         $folder = array('./gambar/','./picker/','./foto/','./photo/','./picture/','./image/','./thumbnail/');
         foreach ($folder as $dir) {
            if( file_exists($dir.$filename) && !is_dir($dir.$filename) ){
               unlink($dir.$filename);
            }
         }

      }

   }



}
