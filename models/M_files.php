<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_files extends CI_Model{

   public function __construct(){
      parent::__construct();
      $this->load->helper(array('h_user_helper', 'h_query_helper'));
   }

   function delete($data){
       
        #what we do here? we only delete record of post_id and from_tbl from file_manager.
        $query = $this->db->conn_id->prepare(
            'DELETE FROM file_manager WHERE from_id = :from_id AND from_tbl = :from_tbl'
         );
         $query->execute(array(
            ':from_id' => $data['from_id'],
            ':from_tbl' => $data['from_tbl']
         ));

   }

   function insert($data){
    
         if($data['img_uploaded']){
            foreach ($data['img_uploaded'] as $filename) {
                
               $query = $this->db->conn_id->prepare(
                   'INSERT INTO file_manager (from_tbl, from_id, filename)
                   VALUES (:from_tbl, :from_id, :filename)'
                );
                $query->execute(array(
                   ':from_tbl'=>$data['from_tbl'],
                   ':from_id'=>$data['from_id'],
                   ':filename'=>$filename
                ));
       
            }
         }
    
    }

}#end class