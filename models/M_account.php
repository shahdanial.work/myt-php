<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_account extends CI_Model{

     public function __construct() {
          parent::__construct();
          // $this->load->helper(array('user_helper'));
          $this->load->model(array('m_mail'));
     }

     function daftar($data){
          date_default_timezone_set("Asia/Kuala_Lumpur");
          $masa = date("Y-m-d");
          $query = $this->db->conn_id->prepare( "INSERT INTO users ( nama, namapena, email, password, reg_time )
          VALUES ( :nama, :namapena, :email, :password, :reg_time )" );
          $query->execute( array(
               ':nama' =>$data['nama'],
               ':namapena' =>$data['namapena'],
               ':email' =>$data['email'],
               ':password' =>$data['password'],
               ':reg_time' =>$masa
          ) );
          if($this->session->flashdata('redirect')){
               redirect($this->session->flashdata('redirect'));
          } elseif(isset($data['redirect'])){
               redirect($data['redirect']);
          } else {
               redirect(site_url('profile'));
          }
     }

     function check_daftar($data){
          $err = array();
          #check email.
          $check_email = $this->db->conn_id->prepare('SELECT id_user FROM users WHERE LOWER(email) = :email');
          $check_email->execute(array(':email' => strtolower($data['email'])));
          $row_email = $check_email->fetch(PDO::FETCH_ASSOC);
          if($row_email){
               $err['email'] = 'Email ini telah telah sedia ada';
          } else {
               #check email domain
               $prefix = str_replace('/','', explode('@', $data['email']));
               $domain = new Phois\Whois\Whois($prefix[1]);
               if ($domain->isAvailable()) {
                    $err['email'] = '<b>'. $data['email'] .'</b> tidak valid';
               }
          }
          #check namapena.
          $check_namapena = $this->db->conn_id->prepare('SELECT id_user FROM users WHERE LOWER(namapena) = :namapena');
          $check_namapena->execute(array(':namapena' => strtolower($data['namapena'])));
          $row_namapena = $check_namapena->fetch(PDO::FETCH_ASSOC);
          if($row_namapena){
               $err['namapena'] = 'Nama Pena ini telah telah sedia ada';
          }

          # ------------------------------- SET UP EMAIL -----------------------------------------
          if(!sizeof($err)){ # only run this code if there's no error

               #HANTAR PASSWORD DAN EMAIL PENDAFTARAN.

               if( !$this->m_mail->pendaftaran_manual($data) ){
                    $err['email'] = '<b>'. $data['email'] .'</b> tidak valid. Gagal menghantar email.';
               }

          } #end if isset($err)
          # ------------------------------- SET UP EMAIL -----------------------------------------

          #FINAL.
          if(sizeof($err)){
               #there's error, so recheck back.
               $this->session->set_flashdata('err', $err);
               $this->session->set_flashdata('data', $data);
               redirect(site_url('register'));
          } else {
               #there's no error, so proceed to register this email and user data.
               $data['password'] = tapuk($data['password']); // cyrpt password to md5
               $this->daftar($data);
               #set inbox.
               $check_id = $this->db->conn_id->prepare('SELECT id_user FROM users WHERE namapena = :namapena');
               $check_id->execute(array(':namapena' => $data['namapena']));
               $id = $check_id->fetch(PDO::FETCH_ASSOC)['id_user'];
               $content = array(
                    'Pendaftaran berjaya. Anda boleh login bila-bila masa melalui Form Login menggunakan '. $data['email'] .' dan dan Password yang anda tetapkan.',
                    'Untuk kes terlupa Password; kami telah menghantar Password ke email '. $data['email'] .'(sila check SPAM box jika tiada di inbox. Please mark as NOT SPAM)',
                    'Tapi, sila pastikan anda verify akaun ini dengan klik <b>Link Verify</b> yang telah kami hantar ke '. $data['email'] .'. Ini bagi mengesahkan anda mencipta akaun menggunakan email sendiri.'
               );
               $this->load->library(array('l_mesej'));
               $this->l_mesej->system_user($id, $content);
          }
     }

     function daftar_google($data, $redirect){
          #hantar data ke email.
          $this->load->library(array('l_hantar_email'));
          $this->l_hantar_email->register_user_google($your_email = $data['email'],$your_name = $data['nama'],$your_password = $data['password']);
          $data['password'] = tapuk($data['password']);
          #insert user
          $query = $this->db->conn_id->prepare( "INSERT INTO users ( nama, namapena, email, password, gplus_id, jantina, verify, reg_time )
          VALUES ( :nama,:namapena,:email,:password,:gplus_id,:jantina,:verify,:reg_time )" );
          $query->execute( array(
               ':nama' =>$data['nama'],
               ':namapena' =>$data['namapena'],
               ':email' =>$data['email'],
               ':password' =>$data['password'],
               ':gplus_id' =>$data['gplus_id'],
               ':jantina' =>$data['jantina'],
               ':verify' =>$data['verify'],
               ':reg_time' =>$data['reg_time']
          ) );
          #get 'id_user' after insert them; to send mesej
          $stmt = $this->db->conn_id->prepare('SELECT id_user FROM users WHERE gplus_id = :gplus_id');
          $stmt->execute(array(':gplus_id' => $data['gplus_id']));
          $row = $stmt->fetch(PDO::FETCH_ASSOC);
          if($row) {
               $id = $row['id_user'];
               $content = array(
                    'Pendaftaran berjaya. Anda boleh login bila-bila masa melalui Google Login menggunakan email '. $data['email'] .' kelak.',
                    'Anda juga boleh login menggunakan '. $data['email'] .' dan password yang telah kami hantar ke email '. $data['email'] .'(sila check SPAM box jika tiada di inbox. Please mark as NOT SPAM)',
                    'Jika anda mahukan Password yang anda mudah ingat, sila edit <a href="#password">EDIT PASSWORD</a>'
               );
               $this->load->library(array('l_mesej'));
               $this->l_mesej->system_user($id, $content);
          }
          if($redirect == site_url('dashboard')){
               $redirect = site_url('profile#password');
          }
          #let's login with gplus_id.
          $gplus_id = $data['gplus_id'];
          $this->l_login->login_google($gplus_id, $redirect);
     }

     function up_reputation($post_user_id, $jumlah_up){
          // u need to continue this thing here
          $this->db->where('id_user', $post_user_id);
          $this->db->update('users', $jumlah_up);
     }

     function update_profile($data){

          if( isset($data['password']) ){
               #password pon nak tukar
               $statement = $this->db->conn_id->prepare(
                    // "UPDATE `users` SET `nama` = :nama, `email` = :email, `namapena` = :namapena, `soalan` = :soalan, `jawapan` = :jawapan, `password` = :password WHERE `id_user` = :id_user"
                    "UPDATE users SET nama = :nama, namapena = :namapena, soalan = :soalan, jawapan = :jawapan WHERE id_user = :id_user"
               );
               $statement->execute(array(
                    ':nama'=>$data['nama'],
                    // ':email'=>$data['email'],
                    ':namapena'=>$data['namapena'],
                    ':soalan'=>$data['soalan'],
                    ':jawapan'=>$data['jawapan'],
                    // ':password'=>$data['password'],
                    ':id_user'=>current_user_id()
               ));
          } else {
               #password xyah
               $statement = $this->db->conn_id->prepare(
                    //"UPDATE `users` SET `nama` = :nama, `email` = :email, `namapena` = :namapena, `soalan` = :soalan, `jawapan` = :jawapan WHERE `id_user` = :id_user"
                     "UPDATE users SET nama = :nama, namapena = :namapena, soalan = :soalan, jawapan = :jawapan WHERE id_user = :id_user"
               );
               $statement->execute(array(
                    ':nama'=>$data['nama'],
                    //':email'=>$data['email'],
                    ':namapena'=>$data['namapena'],
                    ':soalan'=>$data['soalan'],
                    ':jawapan'=>$data['jawapan'],
                    ':id_user'=>current_user_id()
               ));
          }

          // if( user_info(current_user_id())['verify'] == 'yes' && user_info(current_user_id())['email'] != $data['email']){
          //
          //      $query = $this->db->conn_id->prepare(
          //           "UPDATE users SET verify = :no WHERE id_user = :id_user"
          //      );
          //      $query->execute(array(
          //           ':no' => 'no',
          //           ':id_user' => current_user_id()
          //      ));
          //
          // }

          return $statement->rowCount();

     }

     function check_update_profile($data){

          $nak_inform = 0;
          //
          // if(isset($data['password'])){
          //      if( user_info(current_user_id())['password'] != tapuk($data['password']) ){
          //
          //           #waah password pon tukar haaa..
          //           $nak_inform += 1;
          //
          //      }
          // }
          // #check email.
          $err = array();
          // if($data['email'] != user_info(current_user_id())['email']){ #dia nak tukar email
          //      $check_email = $this->db->conn_id->prepare('SELECT id_user FROM users WHERE LOWER(email) = :email');
          //      $check_email->execute(array(':email' => strtolower($data['email'])));
          //      $row_email = $check_email->fetch(PDO::FETCH_ASSOC);
          //      if($row_email){
          //           $err['email'] = 'Email ini telah telah sedia ada';
          //      } else {
          //           #check email domain
          //           $prefix = str_replace('/','', explode('@', $data['email']));
          //           $domain = new Phois\Whois\Whois($prefix[1]);
          //           if ($domain->isAvailable()) {
          //                $err['email'] = '<b>'. $data['email'] .'</b> tidak valid';
          //           }
          //      }
          // }

          #check namapena.
          if($data['namapena'] != user_info(current_user_id())['namapena']){ #dia nak namapena
             $data['namapena'] = strtolower($data['namapena']);
               $check_namapena = $this->db->conn_id->prepare('SELECT id_user FROM users WHERE LOWER(namapena) = :namapena');
               $check_namapena->execute(array(':namapena' => $data['namapena']));
               $row_namapena = $check_namapena->fetch(PDO::FETCH_ASSOC);
               if($row_namapena){
                    $err['namapena'] = 'Nama Pena ini telah telah sedia ada';
               }
          }

          # ------------------------------- SET UP EMAIL TO NEW EMAIL -----------------------------------------
          // if( !sizeof($err) && $data['email'] != user_info(current_user_id())['email']){ # only run this code if there's no error
          //
          //      $nak_inform += 1;
          //
          //      if( !$this->m_mail->tukar_email($data) ){
          //           $err['email'] = '<b>'. $data['email'] .'</b> tidak valid. Gagal menghantar email.';
          //      }
          //
          // }
          # ------------------------------- SET UP EMAIL TO NEW EMAIL -----------------------------------------

          #FINAL.
          if(sizeof($err)){
               #there's error, so recheck back.
               $this->session->set_flashdata('err', $err);
               $this->session->set_flashdata('data', $data);
               redirect(site_url('profile#error'));
          } else {
               #there's no error, so proceed to register this email and user data.
               #before update let's use old data to inform them this changed.

               // $your_email = user_info(current_user_id()['email']);
               // $your_name = user_info(current_user_id()['nama']);
               // $new_email = $data['email'];
               // #$new_password = $data['password']; //xnak password, nnt tujuan tukar sbb email hacked.
               //
               // #kemaskini profile!
               // if(isset($data['password'])){
               //      $data['password'] = tapuk($data['password']);
               // }

               if($this->update_profile($data)){ #condition ni update profile terus..

                    // print_r($nak_inform);
                    // print_r(array($your_email,$your_name, $new_email));
                    // print_r('<hr>');
                    // if($nak_inform > 0){
                    //      #dah updated. mari inform.
                    //      if($this->m_mail->gtau_update_profile($your_email,$your_name, $new_email)){
                    //           print_r('berjaya hantar email');
                    //      } else {
                    //           print_r('gagal hantar email');
                    //      }
                    //      print_r("<hr>");
                    //      print_r(user_info(current_user_id()));
                    //      print_r('<hr>');
                    //      print_r(array($your_email,$your_name, $new_email));
                    // }

                    #set inbox.
                    $id = current_user_id();
                    $content = array(
                         'Anda telah berjaya mengemaskini details Profile anda.'
                    );
                    $this->load->library(array('l_mesej'));
                    $this->l_mesej->system_user($id, $content);
                    redirect(site_url('profile#success'));

               } else {

                    redirect(site_url('profile#failed'));

               }

          }

     }

     function email_verify($data){
          $statement = $this->db->conn_id->prepare("UPDATE `users` SET `jantina` = :jantina, `gplus_id` = :gplus_id, `nama` = :nama, `verify` = :verify WHERE `id_user` = :id_user");
          $statement->execute(array(':id_user' => $data['id_user'],':jantina' => $data['jantina'],':gplus_id' => $data['gplus_id'],':nama' => $data['nama'],':verify' => 'yes'));
          $gplus_id = $data['gplus_id'];
          $redirect = $data['redirect'];
          $this->l_login->login_google($gplus_id, $redirect);
     }

     function remove_user($data){
          if(is_numeric($data['id_user'])){
               #delete user.
               $stmt = $this->db->conn_id->prepare("DELETE FROM users WHERE id_user = ?");
               $stmt->execute(array($data['id_user']));
               #update user post
               $statement = $this->db->conn_id->prepare("UPDATE `post_tbl` SET `author_id` = :admin_id WHERE `author_id` = :id_user");
               $statement->execute(array(':id_user' => $data['id_user'],':admin_id' => '1'));
               #drop mesej table. using CI dbforge; PDO didn't support quering binding for TABLE NAME.
               $table_name = 'zz_mesej_'. $data['id_user'];
               $this->load->dbforge();
               $this->dbforge->drop_table($table_name,TRUE);
               // $wtf = "DROP TABLE ' . $table_name . '";
               // $sql = $this->db->conn_id->prepare($wtf);
               // $sql->execute();
          } else {
               print_r('Terdapat Masalah Meneruskan Process pada m_account->remove_user(), mohon screenshoot/copy mesej ini dan report kepada shahdanial.work@gmail.com');
          }
          #new registration Process.

          $this->session->set_flashdata('redirect', $data['redirect']); //login/google will use it to set $redirect
          redirect(site_url('login/google'));
     }

     function update_gplus($data){

          if( isset($data['nama']) && isset($data['jantina']) ){
               #nama dan jantina
               $statement = $this->db->conn_id->prepare("UPDATE `users` SET `gplus_id` = :gplus_id, `nama` = :nama, `jantina` = :jantina WHERE `email` = :email");
               $statement->execute(array(':email' => $data['email'],':gplus_id' => $data['gplus_id'],':nama' => $data['nama'],':jantina' => $data['jantina']));
          } elseif( isset($data['jantina']) ){
               #jantina n gplus_id sahaja
               $statement = $this->db->conn_id->prepare("UPDATE `users` SET `gplus_id` = :gplus_id, `jantina` = :jantina WHERE `email` = :email");
               $statement->execute(array(':email' => $data['email'],':gplus_id' => $data['gplus_id'],':jantina' => $data['jantina']));
          } elseif( isset($data['name']) ){
               #nama n gplus_id sahaja
               $statement = $this->db->conn_id->prepare("UPDATE `users` SET `gplus_id` = :gplus_id, `nama` = :nama WHERE `email` = :email");
               $statement->execute(array(':email' => $data['email'],':gplus_id' => $data['gplus_id'],':nama' => $data['nama']));
          } else {
               #gplus_id sahaja
               $statement = $this->db->conn_id->prepare("UPDATE `users` SET `gplus_id` = :gplus_id WHERE `email` = :email");
               $statement->execute(array(':email' => $data['email'], ':gplus_id' => $data['gplus_id']));
          }
          #send mesej tell them your email verified.
          $stmt = $this->db->conn_id->prepare('SELECT id_user FROM users WHERE gplus_id = :gplus_id');
          $stmt->execute(array(':gplus_id' => $data['gplus_id']));
          $row = $stmt->fetch(PDO::FETCH_ASSOC);
          if($row) {
               $id = $row['id_user'];
               $content = array(
                    'Akaun ' . $data['email'] . ' telah di verify.',
                    'Kini anda juga bila-bila masa boleh login menggunakan <a href="' . site_url('login') . '" target="_blank">Google Login</a> button dengan sekali klik.'
               );
               $this->load->library(array('l_mesej'));
               $this->l_mesej->system_user($id, $content);
          }
          $gplus_id = $data['gplus_id'];
          $redirect = $data['redirect'];
          $this->l_login->login_google($gplus_id, $redirect);
     }

     function moderate_user($action, $selected_emails){
          foreach ($selected_emails as $selected_email) {
               $this->db->where('email', $selected_email);
               $this->db->update('users', $action);
          }
          $this->load->helper('h_user_helper');
          $admin_gang = array('special admin', 'admin');
          if(in_array(user_info(current_user_id())['user_role'], $admin_gang)){
               redirect(site_url('moderateuser'));
          }else{
               redirect(site_url('dashboard'));
          }
     }

}
