<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_post extends CI_Model{

   function submit($data){

      $current_role = user_info(current_user_id())['user_role'];
      $admin_gang = array('special admin', 'special staff');

      if(in_array($current_role, $admin_gang)){
         #how admin save draft. (different. set status publish, no need validiate auhtor_id for post_tbl).
         $statement = $this->db->conn_id->prepare(
            "UPDATE post_tbl SET title = :title, content = :content, meta_title = :meta_title, meta_description = :meta_description, first_img = :first_img, google_thumb = :google_thumb, viral_title = :viral_title, viral_description = :viral_description, viral_thumb = :viral_thumb, status = :status, masa = :masa WHERE id = :post_id"
         );
         $statement->execute(array(
            ':title' => $data['title'],
            ':content' => $data['content'],
            ':meta_title' => $data['meta_title'],
            ':meta_description' => $data['meta_description'],
            ':first_img' => $data['first_img'],

            ':google_thumb' => $data['google_thumb'],
            ':viral_title' => $data['viral_title'],
            ':viral_description' => $data['viral_description'],
            ':viral_thumb' => $data['viral_thumb'],

            ':status' => 'publish', ###
            ':masa' => $data['masa'],
            ':post_id' => $data['post_id']
         ));
      } else {
         #user, post must valiadiate by author_id. status pending
         $statement = $this->db->conn_id->prepare(
            "UPDATE post_tbl SET title = :title, content = :content, meta_title = :meta_title, meta_description = :meta_description, first_img = :first_img, google_thumb = :google_thumb, viral_title = :viral_title, viral_description = :viral_description, viral_thumb = :viral_thumb, status = :status, masa = :masa WHERE id = :post_id AND author_id = :user_id"
         );
         $statement->execute(array(
            ':title' => $data['title'],
            ':content' => $data['content'],
            ':meta_title' => $data['meta_title'],
            ':meta_description' => $data['meta_description'],
            ':first_img' => $data['first_img'],

            ':google_thumb' => $data['google_thumb'],
            ':viral_title' => $data['viral_title'],
            ':viral_description' => $data['viral_description'],
            ':viral_thumb' => $data['viral_thumb'],

            ':status' => 'pending', ###
            ':masa' => $data['masa'],
            ':post_id' => $data['post_id'],
            ':user_id' => current_user_id()
         ));
      }

      $this->category_process($data);

      $this->remove_old_tag($data);

      $this->tag_process($data);

      if($data['response_msg']){

         echo $data['response_msg'];

      } elseif ($data['redirect']){

         redirect($data['redirect']);

      }

   } #end function

   function update_draft($data){

      $current_role = user_info(current_user_id())['user_role'];
      $admin_gang = array('special admin', 'special staff');

      if(in_array($current_role, $admin_gang)){
         #how admin save draft.
         $statement = $this->db->conn_id->prepare(
            'UPDATE '.$data['from_tbl'].' SET title = :title, content = :content, meta_title = :meta_title, meta_description = :meta_description, first_img = :first_img, google_thumb = :google_thumb, viral_title = :viral_title, viral_description = :viral_description, viral_thumb = :viral_thumb WHERE id = :post_id'
         );
         $statement->execute(array(
            ':title' => $data['title'],
            ':content' => $data['content'],
            ':meta_title' => $data['meta_title'],
            ':meta_description' => $data['meta_description'],
            ':first_img' => $data['first_img'],

            ':google_thumb' => $data['google_thumb'],
            ':viral_title' => $data['viral_title'],
            ':viral_description' => $data['viral_description'],
            ':viral_thumb' => $data['viral_thumb'],

            ':post_id' => $data['post_id']
         ));
      } else {
         #user, post must valiadiate by author_id.
         $statement = $this->db->conn_id->prepare(
            'UPDATE '.$data['from_tbl'].' SET title = :title, content = :content, meta_title = :meta_title, meta_description = :meta_description, first_img = :first_img, google_thumb = :google_thumb, viral_title = :viral_title, viral_description = :viral_description, viral_thumb = :viral_thumb WHERE id = :post_id AND author_id = :user_id'
         );
         $statement->execute(array(
            ':title' => $data['title'],
            ':content' => $data['content'],
            ':meta_title' => $data['meta_title'],
            ':meta_description' => $data['meta_description'],
            ':first_img' => $data['first_img'],

            ':google_thumb' => $data['google_thumb'],
            ':viral_title' => $data['viral_title'],
            ':viral_description' => $data['viral_description'],
            ':viral_thumb' => $data['viral_thumb'],

            ':post_id' => $data['post_id'],
            ':user_id' => current_user_id()
         ));
      }

      $this->category_process($data);

      $this->remove_old_tag($data);

      $this->tag_process($data);

      if($data['response_msg']){

         echo $data['response_msg'];

      } elseif ($data['redirect']){

         redirect($data['redirect']);

      }

   } #end function

   function drafting_title($data){
      $current_role = user_info(current_user_id())['user_role'];
      $admin_gang = array('special admin', 'special staff');

      if(in_array($current_role, $admin_gang)){
         #how admin save draft (no need user_id)
         $statement = $this->db->conn_id->prepare(
           'UPDATE post_tbl SET title = :title WHERE id = :post_id'
         );
         $statement->execute(array(
           ':title' => $data['title'],
           ':post_id' => $data['post_id']
         ));

      } else {
         #how user save draft.. must with user id.
         $statement = $this->db->conn_id->prepare(
           "UPDATE post_tbl SET title = :title WHERE id = :post_id  AND author_id = :user_id"
         );
         $statement->execute(array(
           ':title' => $data['title'],
           ':post_id' => $data['post_id'],
           ':user_id' => current_user_id()
         ));

      }

      echo 'Tajuk post telah di kemasikini kepada \'' . $data['title'] . '\'';
   } #end  function

   function drafting_content($data){
      $current_role = user_info(current_user_id())['user_role'];
      $admin_gang = array('special admin', 'special staff');

      if(in_array($current_role, $admin_gang)){
         #how admin save draft (no need user_id)
         $statement = $this->db->conn_id->prepare(
           "UPDATE post_tbl SET content = :content WHERE id = :post_id"
         );
         $statement->execute(array(
           ':content' => $data['content'],
           ':post_id' => $data['post_id']
         ));

      } else {
         #how user save draft.. must with user id.
         $statement = $this->db->conn_id->prepare(
           "UPDATE post_tbl SET content = :content WHERE id = :post_id  AND author_id = :user_id"
         );
         $statement->execute(array(
           ':content' => $data['content'],
           ':post_id' => $data['post_id'],
           ':user_id' => current_user_id()
         ));

      }

      echo 'Isi artikel telah di kemaskini';
   } #end  function


   function category_process($data){
      #CATEFORY PROCESS.
      #1. category:: apa-apa hal delete dulu category lama.
      $del_cat = $this->db->conn_id->prepare(
         "DELETE FROM category_manager WHERE from_tbl = :from_tbl AND from_id = :from_id"
      );
      $del_cat->execute(array(
         ':from_tbl'=>$data['from_tbl'],
         ':from_id'=>$data['post_id']
      ));

      if($data['category']){
         #2. category:: check and get id of category.
         $get_cat_id = $this->db->conn_id->prepare(
            "SELECT id FROM category_tbl WHERE domain = :domain"
         );
         $get_cat_id->execute(array(
            ':domain'=>$data['category']
         ));
         $cat_id = $get_cat_id->fetch(PDO::FETCH_COLUMN);

         if(is_numeric($cat_id)){
            #3. category:: availabel! let's record.
            $record_cat = $this->db->conn_id->prepare(
               "INSERT INTO category_manager (from_tbl, from_id, category_id)
               VALUES (:from_tbl, :from_id, :category_id)"
            );
            $record_cat->execute(array(
               ':from_tbl'=>$data['from_tbl'],
               ':from_id'=>$data['post_id'],
               ':category_id'=>$cat_id
            ));

            if(!$record_cat){
               print_r('failed to save your category.');
            }

         }

      }

   }#end function

   function remove_old_tag($data){
      if($data['tag']){
         $this->remove_old_tag_compare($data);
      } else {
         $this->remove_all_old($data);
      }
   } #end function

   function remove_all_old($data){
      #get id by title.
      $get_tag_id = $this->db->conn_id->prepare(
         "SELECT tag_id FROM tag_manager WHERE from_tbl = :from_tbl AND from_id = :from_id"
      );
      $get_tag_id->execute(array(
         ':from_tbl'=>$data['from_tbl'],
         ':from_id'=>$data['post_id']
      ));
      $ids = $get_tag_id->fetchAll(PDO::FETCH_COLUMN); #array('7','9',12)

      if($ids){

         foreach ($ids as $id) {

            $kira = $this->db->conn_id->prepare(
               "SELECT id FROM tag_manager WHERE tag_id = :tag_id"
            );#sekadar kira id tu asbab je.
            $kira->execute(array(
               ':tag_id'=>$id
            ));

            if($kira->rowCount() < 2 ){#1 or kebawah = boleh delete.

               $delete_manager = $this->db->conn_id->prepare(
                  "DELETE FROM tag_manager WHERE tag_id = :tag_id"
               );
               $delete_manager->execute(array(
                  ':tag_id'=>$id
               ));

               $delete_tbl = $this->db->conn_id->prepare(
                  "DELETE FROM tag_tbl WHERE id = :id"
               );
               $delete_tbl->execute(array(
                  ':id'=>$id
               ));

            } else { #ada org guna tag ni, remove record je la.
               #x perlu, akan dibuat dibawah.
            }

         }

      }

   }


   function remove_old_tag_compare($data){

      // foreach ($data['tag'] as $tagtitle) {
      //
      //    $ck_tag = $this->db->conn_id->prepare(
      //       "SELECT tag_id FROM tag_manager WHERE from_tbl = :from_tbl AND from_id = :from_id"
      //    );
      //    $ck_tag->execute(array(
      //       ':from_tbl'=>$data['from_tbl'],
      //       ':from_id'=>$data['post_id']
      //    ));
      //    $tag_id = $ck_tag->fetchAll(PDO::FETCH_COLUMN); #ex array (1,2,3,4);
      //
      //    if($tag_id){
      //
      //       foreach($tag_id as $id){
      //          #get title to caompare.
      //          $get_title = $this->db->conn_id->prepare(
      //             "SELECT title FROM tag_tbl WHERE id = :id"
      //          );
      //          $get_title->execute(array(
      //             ':id'=>$id
      //          ));
      //          $title = $get_title->fetch(PDO::FETCH_COLUMN);
      //
      //          if($title){
      //
      //             if( !in_array( strtolower($title), array_map('strtolower', $data['tag']) ) ){ #ex $tgtitle['title'] = 'awie'. $data['tag'] = array('erra', 'amy');
      //                #remove tag lama kalo xde org guna.
      //
      //                $kira = $this->db->conn_id->prepare(
      //                   "SELECT id FROM tag_manager WHERE tag_id = :tag_id"
      //                ); #kira je. id tu asbab.
      //                $kira->execute(array(
      //                   ':tag_id'=>$id
      //                ));
      //
      //                if($kira->rowCount() < 2){ #1 or kebawah, bermakna tiada org lain guna.
      //
      //                   $delete_manager = $this->db->conn_id->prepare(
      //                      "DELETE FROM tag_manager WHERE tag_id = :tag_id"
      //                   );
      //                   $delete_manager->execute(array(
      //                      ':tag_id'=>$id
      //                   ));
      //
      //                   $delete_tbl = $this->db->conn_id->prepare(
      //                      "DELETE FROM tag_tbl WHERE id = :id"
      //                   );
      //                   $delete_tbl->execute(array(
      //                      ':id'=>$id
      //                   ));
      //
      //                } #kira.
      //
      //             } #in_array
      //
      //          }#$title.
      //
      //       }#foreach.
      //
      //    }#$tag_id.
      //
      // }#forech.

   }#end function


   function tag_process($data){

      if($data['tag']){

         #0. tag:: delete dulu semua tag sedia ada.
         $del_tag = $this->db->conn_id->prepare(
            "DELETE FROM tag_manager WHERE from_tbl = :from_tbl AND from_id = :from_id"
         );
         $del_tag->execute(array(
            ':from_tbl'=>$data['from_tbl'],
            ':from_id'=>$data['post_id']
         ));

         foreach ($data['tag'] as $tagname) {
            #1. tag:: check tag weater available or not.
            $check_tag = $this->db->conn_id->prepare(
               "SELECT id FROM tag_tbl WHERE LOWER(title) = :title"
            );
            $check_tag->execute(array(
               ':title'=>strtolower($tagname)
            ));
            $tag_id = $check_tag->fetch(PDO::FETCH_COLUMN);

            if(is_numeric($tag_id)){ #now always use $tag_id for id. and $tagname for title.

               #2. tag:: tag wujud, record je.
               $record_tag = $this->db->conn_id->prepare(
                  "INSERT INTO tag_manager (from_tbl, from_id, tag_id)
                  VALUES (:from_tbl, :from_id, :tag_id)"
               );
               $record_tag->execute(array(
                  ':from_tbl'=>$data['from_tbl'],
                  ':from_id'=>$data['post_id'],
                  ':tag_id'=>$tag_id
               ));

            } else {

               #1. tag:: tag tidak wujud di tag_tbl, insert insert lu baru record (tag baru bhai).
               $new_tag = $this->db->conn_id->prepare(
                  "INSERT INTO tag_tbl (title, author_id)
                  VALUES (:title, :author_id)"
               );
               $new_tag->execute(array(
                  ':title'=>$tagname,
                  ':author_id'=>current_user_id()
               ));

               if($new_tag){
                  #2. tag:: jika berjaya insert, dapatkan semula id tag dimasukkan tu.
                  $check_tags = $this->db->conn_id->prepare(
                     "SELECT id FROM tag_tbl WHERE LOWER(title) = :title AND author_id = :author_id
                     ");
                     $check_tags->execute(array(
                        ':title'=>strtolower($tagname),
                        ':author_id'=>current_user_id()
                     ));
                     $tag_ids = $check_tags->fetch(PDO::FETCH_COLUMN);

                     #3. tag:: dapatkan id tag berjaya, mari record di tag_manager.
                     if(is_numeric($tag_ids)){
                        $record_tags = $this->db->conn_id->prepare(
                           "INSERT INTO tag_manager (from_tbl, from_id, tag_id)
                           VALUES (:from_tbl, :from_id, :tag_id)"
                        );
                        $record_tags->execute(array(
                           ':from_tbl'=>$data['from_tbl'],
                           ':from_id'=>$data['post_id'],
                           ':tag_id'=>$tag_ids
                        ));
                     }

                  }#new_tag.

               }#numeric.

            }#foreach.

         } #end if $data['tag']

      } #end function.

} # END CLASS
