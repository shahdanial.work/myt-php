<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tags extends CI_Model{

   public function __construct(){
      parent::__construct();
      $this->load->helper(array('h_user_helper', 'h_query_helper'));
   }

   function delete($from_tbl, $from_id){ #delete record

        #what we do here? we only delete tag of from_id and from_tbl from tag_manager.
        $query = $this->db->conn_id->prepare(
            'DELETE FROM tag_manager WHERE from_id = :from_id AND from_tbl = :from_tbl'
         );
         $query->execute(array(
            ':from_id' => $from_id,
            ':from_tbl' => $from_tbl
         ));

   }

   function delete_tag_tbl($tag_path){

    $where = array(
        'tag_path )( =' => $tag_path
      );

     #if(!check_table($tag_path, 'tag_manager', 'path')){

     if( ! query_select('tag_path', 'tag_manager', $where) ){

        #pastikan jgn delete kalo status = publish (dah index)

        $query = $this->db->conn_id->prepare(
            'DELETE FROM tag_tbl WHERE tag_path = :tag_path AND status <> :publish'
         );
         $query->execute(array(
            ':tag_path' => $tag_path,
            ':publish' => 'publish'
         ));

     }

   }

   function insert($data, $from_tbl, $from_id){

    #record tag to tag manager..
    $data['tag'] = array_unique(array_filter($data['tag']));

     if($data['tag']){
        foreach ($data['tag'] as $title) {

           $path = build_link(trim($title), 'tag_tbl', 'tag_path');
           //$path = build_unique_link($title, 'tag_tbl', 'tag_path');// takleh pakai ni sbb nak check kalo tag dah ada xyah buat tag baru kat `tag_tbl`

            #INSERT to tag_tbl
            if(!check_table($path, 'tag_tbl', 'path')){

                $insert_tag = $this->db->conn_id->prepare(
                    'INSERT INTO tag_tbl (title, author_id, tag_path)
                    VALUES (:title, :author_id, :tag_path)'
                );
                $insert_tag->execute(array(
                    ':title'=> trim($title),
                    ':author_id'=> current_user_id(), // $data['author_id'], // mesti current_user_id(), sbb current user / admin yg create tag ni
                    ':tag_path'=>  $path
                ));
            }
            #record
            $where = array(
                'tag_path )( =' => $path,
                'from_tbl )( =' => $from_tbl,
                'from_id )( =' => $from_id
            );

            if( ! query_select('tag_path', 'tag_manager', $where) ){
                $query = $this->db->conn_id->prepare(
                    'INSERT INTO tag_manager (from_tbl, from_id, tag_path)
                    VALUES (:from_tbl, :from_id, :tag_path)'
                );
            }

            $query->execute(array(
            ':from_tbl'=>$from_tbl,
            ':from_id'=>$from_id,
            ':tag_path'=>$path
            ));



        } #end foreach
     } #end if data

   } #end function insert

}#end class
