<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_keyword extends CI_Model{

   public function __construct(){
      parent::__construct();
      $this->load->helper(array('h_user_helper', 'h_query_helper'));
   }

   function delete($from_tbl, $from_id){ #delete record

        #what we do here? we only delete tag of from_id and from_tbl from tag_manager.
        $query = $this->db->conn_id->prepare(
            'DELETE FROM keyword_manager WHERE from_id = :from_id AND from_tbl = :from_tbl'
         );
         $query->execute(array(
            ':from_id' => $data['from_id'],
            ':from_tbl' => $data['from_tbl']
         ));

   }


   function insert($data, $from_tbl, $from_id){

      if(isset($data['gender'])){

         #RECORD GENDER
         $data['gender'] = array_unique($data['gender']);

          if($data['gender']){
             foreach ($data['gender'] as $value) {

                $value = trim($value);

                 #record
                 $where = array(
                     'mykey )( =' => 'gender',
                     'myvalue )( =' => $value,
                     'from_tbl )( =' => $from_tbl,
                     'from_id )( =' => $from_id
                 );

                 // var_dump(query_select('myvalue', 'keyword_manager', $where));

                 if( ! query_select('myvalue', 'keyword_manager', $where) ){
                     $query = $this->db->conn_id->prepare(
                        'INSERT INTO keyword_manager (from_tbl, from_id, mykey, myvalue)
                        VALUES (:from_tbl, :from_id, :key, :value)'
                     );
                     $query->execute(array(
                     ':from_tbl' => $from_tbl,
                     ':from_id' => $from_id,
                     ':key' => 'gender',
                     ':value' => $value
                     ));
                 }



             } #end foreach
          } #end if data

      }

      if(isset($data['interest'])){

         #RECORD INTEREST
         $data['interest'] = array_unique($data['interest']);

          if($data['interest']){
             foreach ($data['interest'] as $value) {

                $value = trim($value);

                 #record
                 $where = array(
                     'mykey )( =' => 'interest',
                     'myvalue )( =' => $value,
                     'from_tbl )( =' => $from_tbl,
                     'from_id )( =' => $from_id
                 );

                 if( ! query_select('myvalue', 'keyword_manager', $where) ){
                     $query = $this->db->conn_id->prepare(
                         'INSERT INTO keyword_manager (from_tbl, from_id, mykey, myvalue)
                         VALUES (:from_tbl, :from_id, :key, :value)'
                     );
                     $query->execute(array(
                     ':from_tbl' => $from_tbl,
                     ':from_id' => $from_id,
                     ':key' => 'interest',
                     ':value' => $value
                     ));
                 }



             } #end foreach
          } #end if data

      }

   } #end function insert

}#end class
