<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mshare extends CI_Model{

  function up_share($page_url, $post_detail, $post_user_id, $post_detail1){
    $this->db->where('blogger_post_url', $page_url);
    $this->db->update('post_tbl', $post_detail);

    $this->db->where('id_user', $post_user_id);
    $this->db->update('users', $post_detail1);
  }

}
