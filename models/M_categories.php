<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_categories extends CI_Model{

   public function __construct(){
      parent::__construct();
      $this->load->helper(array('h_user_helper', 'h_query_helper'));
   }

   function delete($from_tbl, $from_id){

     #what we do here? we only delete tag of from_id and from_tbl from tag_manager.
     $query = $this->db->conn_id->prepare(
         'DELETE FROM category_manager WHERE from_id = :from_id AND from_tbl = :from_tbl'
      );
      $query->execute(array(
         ':from_id' => $from_id,
         ':from_tbl' => $from_tbl
      ));

   }

   function insert($category, $from_tbl, $from_id){

    $query = $this->db->conn_id->prepare(
        'INSERT INTO category_manager (from_tbl, from_id, domain)
        VALUES (:from_tbl, :from_id, :domain)'
     );
     $query->execute(array(
        ':from_tbl'=>$from_tbl,
        ':from_id'=>$from_id,
        ':domain'=>$category
     ));

   }


}#end class
