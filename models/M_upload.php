<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_upload extends CI_Model{

   public function __construct()
   {
      parent::__construct();
      $this->load->model(array('m_tags'));
   }

   function post_image($res){
      #echo '<pre>';
      foreach ($res as $key => $data) {
         if($data['security'] == 'safe'){
            $data['text'] = trim(preg_replace('!\s+!', ' ', preg_replace('/[^\da-z0-9 ]/i', ' ', $data['text'])));
            $data['path'] = trim(preg_replace("/[\s_]/", "-", preg_replace("/[\s-]+/", " ", preg_replace("/[^a-z0-9_\s-]/", "",  strtolower($data['text']) ) ) ) );

            if($data['ext'] == 'webp') $data['ext'] = 'png';

            if(!file_exists('./gambar/' . $data['path'] . '.' . $data['ext'])){
               #jika takde file bla-bla.jpg.
               $data['filename'] = $data['path'] . '.' . $data['ext'];
               #path dan ext akan guna diatas. xyah setup apa-apa.
            } else {
               #file dah ada let's play.
               //~~~~~~~~~~~~~~~~~~~~~~~~~~
               #SETUP.
               date_default_timezone_set("Asia/Kuala_Lumpur");
               $year = date("Y");
               $path = $data['path'];
               $ext = $data['ext'];
               $final = array();

               //~~~~~~~~~~~~~~~~~~~~~~~~~~
               #JPG.
               if($ext == 'jpg'){
                  #tukar ext
                  $push = array(
                     'path' => $path,
                     'ext' => 'jpeg'
                  );
                  $push['filename'] = $push['path'] . '.' . $push['ext'];
                  array_push($final,$push);

                  if( file_exists('./gambar/' . $push['filename']) ){
                     $push_1 = array(
                        'path' => $path,
                        'ext' => 'png'
                     );
                     $push_1['filename'] = $push_1['path'] . '.' . $push_1['ext'];
                     array_push($final,$push_1);
                  }

                  if(isset($push_1)){
                     if( file_exists('./gambar/' . $push_1['filename']) ){
                        $push_2 = array(
                           'path' => $path,
                           'ext' => 'gif'
                        );
                        $push_2['filename'] = $push_2['path'] . '.' . $push_2['ext'];
                        array_push($final,$push_2);
                     }
                  }

                  #main years lak.
                  if(isset($push_2)){
                     if( file_exists('./gambar/' . $push_2['filename']) ){
                        $push_3 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'jpeg'
                        );
                        $push_3['filename'] = $push_3['path'] . '.' . $push_3['ext'];
                        array_push($final,$push_3);
                     }
                  }

                  if(isset($push_3)){
                     if( file_exists('./gambar/' . $push_3['filename']) ){
                        $push_4 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'jpg'
                        );
                        $push_4['filename'] = $push_4['path'] . '.' . $push_4['ext'];
                        array_push($final,$push_4);
                     }
                  }

                  if(isset($push_4)){
                     if( file_exists('./gambar/' . $push_4['filename']) ){
                        $push_5 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'png'
                        );
                        $push_5['filename'] = $push_5['path'] . '.' . $push_5['ext'];
                        array_push($final,$push_5);
                     }
                  }

                  if(isset($push_5)){
                     if( file_exists('./gambar/' . $push_5['filename']) ){
                        $push_6 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'gif'
                        );
                        $push_6['filename'] = $push_6['path'] . '.' . $push_6['ext'];
                        array_push($final,$push_6);
                     }
                  }

                  #play with namapena time.
                  if(isset($push_6)){
                     if( file_exists('./gambar/' . $push_6['filename']) ){
                        $push_7 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'jpg'
                        );
                        $push_7['filename'] = $push_7['path'] . '.' . $push_7['ext'];
                        array_push($final,$push_7);
                     }
                  }

                  if(isset($push_7)){
                     if( file_exists('./gambar/' . $push_7['filename']) ){
                        $push_8 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'jpeg'
                        );
                        $push_8['filename'] = $push_8['path'] . '.' . $push_8['ext'];
                        array_push($final,$push_8);
                     }
                  }

                  if(isset($push_8)){
                     if( file_exists('./gambar/' . $push_8['filename']) ){
                        $push_9 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'png'
                        );
                        $push_9['filename'] = $push_9['path'] . '.' . $push_9['ext'];
                        array_push($final,$push_9);
                     }
                  }

                  if(isset($push_9)){
                     if( file_exists('./gambar/' . $push_9['filename']) ){
                        $push_10 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'gif'
                        );
                        $push_10['filename'] = $push_10['path'] . '.' . $push_10['ext'];
                        array_push($final,$push_10);
                     }
                  }
               }
               //~~~~~~~~~~~~~~~~~~~~~~~~~~
               #JPEG.
               if($ext == 'jpeg'){
                  #tukar ext
                  $push = array(
                     'path' => $path,
                     'ext' => 'jpg'
                  );
                  $push['filename'] = $push['path'] . '.' . $push['ext'];
                  array_push($final,$push);

                  if( file_exists('./gambar/' . $push['filename']) ){
                     $push_1 = array(
                        'path' => $path,
                        'ext' => 'png'
                     );
                     $push_1['filename'] = $push_1['path'] . '.' . $push_1['ext'];
                     array_push($final,$push_1);
                  }

                  if(isset($push_1)){
                     if( file_exists('./gambar/' . $push_1['filename']) ){
                        $push_2 = array(
                           'path' => $path,
                           'ext' => 'gif'
                        );
                        $push_2['filename'] = $push_2['path'] . '.' . $push_2['ext'];
                        array_push($final,$push_2);
                     }
                  }

                  #main years lak.
                  if(isset($push_2)){
                     if( file_exists('./gambar/' . $push_2['filename']) ){
                        $push_3 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'jpeg'
                        );
                        $push_3['filename'] = $push_3['path'] . '.' . $push_3['ext'];
                        array_push($final,$push_3);
                     }
                  }

                  if(isset($push_3)){
                     if( file_exists('./gambar/' . $push_3['filename']) ){
                        $push_4 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'jpg'
                        );
                        $push_4['filename'] = $push_4['path'] . '.' . $push_4['ext'];
                        array_push($final,$push_4);
                     }
                  }

                  if(isset($push_4)){
                     if( file_exists('./gambar/' . $push_4['filename']) ){
                        $push_5 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'png'
                        );
                        $push_5['filename'] = $push_5['path'] . '.' . $push_5['ext'];
                        array_push($final,$push_5);
                     }
                  }

                  if(isset($push_5)){
                     if( file_exists('./gambar/' . $push_5['filename']) ){
                        $push_6 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'gif'
                        );
                        $push_6['filename'] = $push_6['path'] . '.' . $push_6['ext'];
                        array_push($final,$push_6);
                     }
                  }

                  #play with namapena time.
                  if(isset($push_6)){
                     if( file_exists('./gambar/' . $push_6['filename']) ){
                        $push_7 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'jpeg'
                        );
                        $push_7['filename'] = $push_7['path'] . '.' . $push_7['ext'];
                        array_push($final,$push_7);
                     }
                  }

                  if(isset($push_7)){
                     if( file_exists('./gambar/' . $push_7['filename']) ){
                        $push_8 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'jpg'
                        );
                        $push_8['filename'] = $push_8['path'] . '.' . $push_8['ext'];
                        array_push($final,$push_8);
                     }
                  }

                  if(isset($push_8)){
                     if( file_exists('./gambar/' . $push_8['filename']) ){
                        $push_9 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'png'
                        );
                        $push_9['filename'] = $push_9['path'] . '.' . $push_9['ext'];
                        array_push($final,$push_9);
                     }
                  }

                  if(isset($push_9)){
                     if( file_exists('./gambar/' . $push_9['filename']) ){
                        $push_10 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'gif'
                        );
                        $push_10['filename'] = $push_10['path'] . '.' . $push_10['ext'];
                        array_push($final,$push_10);
                     }
                  }
               }
               //~~~~~~~~~~~~~~~~~~~~~~~~~~
               #PNG.
               if($ext == 'png'){
                  #tukar ext
                  $push = array(
                     'path' => $path,
                     'ext' => 'gif'
                  );
                  $push['filename'] = $push['path'] . '.' . $push['ext'];
                  array_push($final,$push);

                  if( file_exists('./gambar/' . $push['filename']) ){
                     $push_1 = array(
                        'path' => $path,
                        'ext' => 'jpg'
                     );
                     $push_1['filename'] = $push_1['path'] . '.' . $push_1['ext'];
                     array_push($final,$push_1);
                  }

                  if(isset($push_1)){
                     if( file_exists('./gambar/' . $push_1['filename']) ){
                        $push_2 = array(
                           'path' => $path,
                           'ext' => 'jpeg'
                        );
                        $push_2['filename'] = $push_2['path'] . '.' . $push_2['ext'];
                        array_push($final,$push_2);
                     }
                  }

                  #main years lak.
                  if(isset($push_2)){
                     if( file_exists('./gambar/' . $push_2['filename']) ){
                        $push_3 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'png'
                        );
                        $push_3['filename'] = $push_3['path'] . '.' . $push_3['ext'];
                        array_push($final,$push_3);
                     }
                  }

                  if(isset($push_3)){
                     if( file_exists('./gambar/' . $push_3['filename']) ){
                        $push_4 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'gif'
                        );
                        $push_4['filename'] = $push_4['path'] . '.' . $push_4['ext'];
                        array_push($final,$push_4);
                     }
                  }

                  if(isset($push_4)){
                     if( file_exists('./gambar/' . $push_4['filename']) ){
                        $push_5 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'jpg'
                        );
                        $push_5['filename'] = $push_5['path'] . '.' . $push_5['ext'];
                        array_push($final,$push_5);
                     }
                  }

                  if(isset($push_5)){
                     if( file_exists('./gambar/' . $push_5['filename']) ){
                        $push_6 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'jpeg'
                        );
                        $push_6['filename'] = $push_6['path'] . '.' . $push_6['ext'];
                        array_push($final,$push_6);
                     }
                  }

                  #play with namapena time.
                  if(isset($push_6)){
                     if( file_exists('./gambar/' . $push_6['filename']) ){
                        $push_7 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'png'
                        );
                        $push_7['filename'] = $push_7['path'] . '.' . $push_7['ext'];
                        array_push($final,$push_7);
                     }
                  }

                  if(isset($push_7)){
                     if( file_exists('./gambar/' . $push_7['filename']) ){
                        $push_8 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'gif'
                        );
                        $push_8['filename'] = $push_8['path'] . '.' . $push_8['ext'];
                        array_push($final,$push_8);
                     }
                  }

                  if(isset($push_8)){
                     if( file_exists('./gambar/' . $push_8['filename']) ){
                        $push_9 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'jpg'
                        );
                        $push_9['filename'] = $push_9['path'] . '.' . $push_9['ext'];
                        array_push($final,$push_9);
                     }
                  }

                  if(isset($push_9)){
                     if( file_exists('./gambar/' . $push_9['filename']) ){
                        $push_10 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'png'
                        );
                        $push_10['filename'] = $push_10['path'] . '.' . $push_10['ext'];
                        array_push($final,$push_10);
                     }
                  }
               }
               //~~~~~~~~~~~~~~~~~~~~~~~~~~
               #GIF.
               if($ext == 'gif'){
                  #tukar ext
                  $push = array(
                     'path' => $path,
                     'ext' => 'png'
                  );
                  $push['filename'] = $push['path'] . '.' . $push['ext'];
                  array_push($final,$push);

                  if( file_exists('./gambar/' . $push['filename']) ){
                     $push_1 = array(
                        'path' => $path,
                        'ext' => 'jpg'
                     );
                     $push_1['filename'] = $push_1['path'] . '.' . $push_1['ext'];
                     array_push($final,$push_1);
                  }

                  if(isset($push_1)){
                     if( file_exists('./gambar/' . $push_1['filename']) ){
                        $push_2 = array(
                           'path' => $path,
                           'ext' => 'jpeg'
                        );
                        $push_2['filename'] = $push_2['path'] . '.' . $push_2['ext'];
                        array_push($final,$push_2);
                     }
                  }

                  #main years lak.
                  if(isset($push_2)){
                     if( file_exists('./gambar/' . $push_2['filename']) ){
                        $push_3 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'gif'
                        );
                        $push_3['filename'] = $push_3['path'] . '.' . $push_3['ext'];
                        array_push($final,$push_3);
                     }
                  }

                  if(isset($push_3)){
                     if( file_exists('./gambar/' . $push_3['filename']) ){
                        $push_4 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'png'
                        );
                        $push_4['filename'] = $push_4['path'] . '.' . $push_4['ext'];
                        array_push($final,$push_4);
                     }
                  }

                  if(isset($push_4)){
                     if( file_exists('./gambar/' . $push_4['filename']) ){
                        $push_5 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'jpg'
                        );
                        $push_5['filename'] = $push_5['path'] . '.' . $push_5['ext'];
                        array_push($final,$push_5);
                     }
                  }

                  if(isset($push_5)){
                     if( file_exists('./gambar/' . $push_5['filename']) ){
                        $push_6 = array(
                           'path' => $path . '-' . $year,
                           'ext' => 'jpeg'
                        );
                        $push_6['filename'] = $push_6['path'] . '.' . $push_6['ext'];
                        array_push($final,$push_6);
                     }
                  }

                  #play with namapena time.
                  if(isset($push_6)){
                     if( file_exists('./gambar/' . $push_6['filename']) ){
                        $push_7 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'gif'
                        );
                        $push_7['filename'] = $push_7['path'] . '.' . $push_7['ext'];
                        array_push($final,$push_7);
                     }
                  }

                  if(isset($push_7)){
                     if( file_exists('./gambar/' . $push_7['filename']) ){
                        $push_8 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'png'
                        );
                        $push_8['filename'] = $push_8['path'] . '.' . $push_8['ext'];
                        array_push($final,$push_8);
                     }
                  }

                  if(isset($push_8)){
                     if( file_exists('./gambar/' . $push_8['filename']) ){
                        $push_9 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'jpg'
                        );
                        $push_9['filename'] = $push_9['path'] . '.' . $push_9['ext'];
                        array_push($final,$push_9);
                     }
                  }

                  if(isset($push_9)){
                     if( file_exists('./gambar/' . $push_9['filename']) ){
                        $push_10 = array(
                           'path' => $path . '-' . uniqid(rand()) . '-' . date("YmdHis"),
                           'ext' => 'png'
                        );
                        $push_10['filename'] = $push_10['path'] . '.' . $push_10['ext'];
                        array_push($final,$push_10);
                     }
                  }
               }
               //~~~~~~~~~~~~~~~~~~~~~~~~~~

               $data['path'] = array_values(array_slice($final, -1))[0]['path'];
               $data['ext'] = array_values(array_slice($final, -1))[0]['ext'];
               $data['filename'] = array_values(array_slice($final, -1))[0]['filename'];

            } #end proccess else if filename exists.

            $this->original($data);
         }//if data['security']
      } // end foreach.
      #echo '</pre>';
   }


   function original($data){
      #upload first time with orginal size!
      if($data['security'] == 'safe'){
         move_uploaded_file($data['tmp_name'], './A/' . $data['filename']);
         if(file_exists('./A/' . $data['filename'])){

            $d_url = 'http://localhost:8013/A/' . $data['filename'];
            $api_url = 'http://localhost:5000/res-param/imgsize?'. $d_url;
            $api_data = array();

            // use key 'http' even if you send the request to https://...
            $api_options = array(
                 'https' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($api_data)
                 )
            );
            $api_context  = stream_context_create($api_options);
            $json = file_get_contents($api_url, false, $api_context);

            if ($json === FALSE) {
               $data['security'] = 'warning';
            } else {
               $json = json_decode($json, true);
               $data['width'] = $json['width'];
               $data['height'] = $json['height'];
               $data['type'] = $json['type'];

               if($data['type'] == 'webp'){

                  $im = imagecreatefromwebp('./A/' . $data['filename']);

                  if($data['ext'] == 'jpg' || $data['ext'] == 'jpeg')
                  imagejpeg($im, './A/' . $data['filename'], 100);

                  if($data['ext'] == 'png')
                  imagepng($im, './A/' . $data['filename'], 100);

                  if($data['ext'] == 'gif')
                  imagegif($im, './A/' . $data['filename'], 100);

                  imagedestroy($im);

               }

               // var_dump($data);

               if($data['width'] < 150 && $data['height'] < 150){
                  $data['security'] = 'warning';
                  unlink('./A/' . $data['filename']);
               }
            }


         } else {
            #Awal-awal lagi error upload ke dir site_url('A/' . $filename)
            $data['security'] = 'warning';
         }

         // var_dump($data);

         if($data['security'] == 'safe') $this->thumbnail_gua($data);

      }
   } # end function()


   function thumbnail_gua($data){

      $filename = $data['filename'];


      $width = $data['width'];
      $height = $data['height'];

      //------------------------------------- 664xauto gambar post  ----------------------------------------
      if($width > 644){
         //lebar dikecilkan, so reduce quality
         $gambarwidth = 644;
         $gambarheight = $gambarwidth / ($width / $height);
         $gambar_math = 100 - (644 / $width * 100);
         if($gambar_math < 65){
            $gambarquality = '65%';
         } else {
            $gambarquality = (int) $gambar_math . '%';
         }
         $gambarsharp = array( 'sharpen', (int) $gambar_math );
      } else {
         //lebar maintain auto sebab gambar memang kecil, so kekalkan 100%
         $gambarwidth = $width;
         $gambarheight = $height;
         $gambarquality = '100%';
         $gambarsharp = array( 'sharpen', 0 );
      }

      // var_dump($gambarwidth, $gambarheight, $gambarwidth > 350, $gambarheight > 200);

      if($gambarwidth > 350 && $gambarheight > 200 ){ #layak ada watermark.

         $config_A = array(
            'filters' => $gambarsharp,
            'source_image' => './A/' . $filename,
            'maintain_ratio' => true,
            'width' => (int) $gambarwidth,
            'quality' => $gambarquality,
            'new_image' => './gambar_A/'
         );
         $this->load->library('image_lib', $config_A);
         $this->image_lib->initialize($config_A);
         $this->image_lib->resize();
         $this->image_lib->clear();
         $the_config = array(
            'quality' => '100%',
            'source_image' => './gambar_A/'.$filename,
            'image_library' => 'gd2',
            'wm_type' => 'overlay',
            'wm_overlay_path' => 'watermark/watermark.png',
            //'wm_opacity' => '12',
            //'wm_padding' => 8,
            'wm_vrt_alignment' => 'bottom',
            'wm_hor_alignment' => 'right',
            'new_image' => './gambar/'
         );
         $this->load->library('image_lib', $the_config);
         $this->image_lib->initialize($the_config);
         $this->image_lib->watermark();
         $this->image_lib->clear();
         $this->image_lib->display_errors();
      } else {
         $config_A = array(
            'filters' => $gambarsharp,
            'source_image' => './A/' . $filename,
            'maintain_ratio' => true,
            'width' => (int) $gambarwidth,
            'quality' => $gambarquality,
            'new_image' => './gambar/'
         );
         $this->load->library('image_lib', $config_A);
         $this->image_lib->initialize($config_A);
         $this->image_lib->resize();
         $this->image_lib->clear();
      }
      if ( file_exists('./gambar_A/'.$filename) ){
         unlink('./gambar_A/'.$filename);
      }



      $thumbnails = array(
         array(
            'fake' => './A_fake/',
            'folder' => './picker/',
            'tw' => 110,
            'th' => 110
         ),
         array(
            'fake' => './B_fake/',
            'folder' => './foto/',
            // 'tw' => 658,
            // 'th' => 309
            'tw' => 652,
            'th' => 312
         ),
         array(
            'fake' => './C_fake/',
            'folder' => './photo/',
            // 'tw' => 300,
            // 'th' => 194
            'tw' => 322,
            'th' => 179
         ),
         array(
            'fake' => './D_fake/',
            'folder' => './picture/',
            'tw' => 100,
            'th' => 75
         ),
         array(
            'fake' => './E_fake/',
            'folder' => './image/',
            'tw' => 206,
            'th' => 206
         ),
         array(
            'fake' => './F_fake/',
            'folder' => './thumbnail/',
            'tw' => 72,
            'th' => 72
         )
      );

      foreach ($thumbnails as $potret) {
         #(GAMBAR ASAL).
         $gw = $width;
         $gh = $height;
         #(TARGET).
         $tw = $potret['tw'];
         $th = $potret['th'];
         #BUAT COMPARISON.
         $rw = $gw - $tw;
         $rh = $gh - $th;
         if($rw < 0 || $rh < 0){ #PEMBESARAN START. apabila ada je yang kecl, ianya mesti pembesaran.
            $pw = str_replace('-', '', $rw) / $gw * 100;
            $ph = str_replace('-', '', $rh) / $gh * 100;
            if($pw > $ph){
               $fpw = ($pw + 100) / 100 * $gw ;
               $fph = ($pw + 100) / 100 * $gh ;
            } else {
               $fpw = ($ph + 100) / 100 * $gw ;
               $fph = ($ph + 100) / 100 * $gh ;
            }
            $quality = '100%'; #always 100% sbb gambar dibesarkan
         } else { #PENGECILAN START.
            $pw = str_replace('-', '', $rw) / $gw * 100;
            $ph = str_replace('-', '', $rh) / $gh * 100;
            if($pw < $ph){
               $fpw = (100 - $pw) / 100 * $gw;
               $fph = (100 - $pw) / 100 * $gh;
               $quality = 100 - $pw;
            } else {
               $fpw = (100 - $ph) / 100 * $gw;
               $fph = (100 - $ph) / 100 * $gh;
               $quality = 100 - $ph;
            }
            if($quality < 66){ #overide
               $quality = 66;
            }
         }
         #LAIN CERITA. lepas kita resize diatas. baru lah nak crop. meh cari position atas dan tepi.
         $posisi_atas = ($fph - $th) / 2;
         $posisi_kiri = ($fpw - $tw) / 2;

         #JOM RESIZE IMAGE (kecilkan or besarkan, upload di fake folder).
         $resize_config = array(
            'quality' => $quality,
            'source_image' => './A/' . $filename, #<< first time upload, amek full image url
            'new_image' => $potret['fake'],
            'maintain_ratio' => false,
            'width' => (int) $fpw,
            'height' => (int) $fph
         );
         $this->load->library('image_lib', $resize_config);
         $this->image_lib->initialize($resize_config);
         $this->image_lib->resize();
         $this->image_lib->clear();
         #JOM CROP IMAGE pulak.
         $crop_config = array(
            'quality' => '100%',
            'source_image' => $potret['fake'] . $filename,
            'new_image' => $potret['folder'],
            'maintain_ratio' => false,
            'x_axis' => (int) $posisi_kiri,
            'y_axis' => (int) $posisi_atas,
            'width' => (int) $tw,
            'height' => (int) $th
         );
         $this->load->library('image_lib', $crop_config);
         $this->image_lib->initialize($crop_config);
         $this->image_lib->crop();
         $this->image_lib->clear();
         #DELETE FAKE.
         if(file_exists($potret['fake'] . $filename) && !is_dir($potret['fake'] . $filename)){
            unlink($potret['fake'] . $filename);
         }
      } #end foreach.

      #settle semua, unlink gambar ori.
      if(file_exists('./A/' .$filename) && !is_dir('./A/' .$filename)){
         unlink('./A/' .$filename);
      }

      //  print_r('<br/><img alt="' . $data['text'] . '" src="' . base_url() . 'gambar/' . $filename . '" />');
      //  print_r('<br/><img alt="' . $data['text'] . '" src="' . base_url() . 'foto/' . $filename . '" />');
      //  print_r('<br/><img alt="' . $data['text'] . '" src="' . base_url() . 'photo/' . $filename . '" />');
      //  print_r('<br/><img alt="' . $data['text'] . '" src="' . base_url() . 'picture/' . $filename . '" />');
      //  print_r('<br/><img alt="' . $data['text'] . '" src="' . base_url() . 'image/' . $filename . '" />');
      //  print_r('<br/><img alt="' . $data['text'] . '" src="' . base_url() . 'thumbnail/' . $filename . '" />');
      $the_gambar = './gambar/' .$filename;
      $A_file = './picker/' .$filename;
      $B_file = './foto/' .$filename;
      $C_file = './photo/' .$filename;
      $D_file = './picture/' .$filename;
      $E_file = './image/' .$filename;
      $F_file = './thumbnail/' .$filename;
      #let's set true if files exist in dir and success insert files data to db.

      // var_dump( $this->update_db($data) , file_exists($the_gambar) , file_exists($A_file) , file_exists($B_file) , file_exists($C_file) , file_exists($D_file) , file_exists($E_file) , file_exists($F_file) );

      if ( $this->update_db($data) && file_exists($the_gambar) && file_exists($A_file) && file_exists($B_file) && file_exists($C_file) && file_exists($D_file) && file_exists($E_file) && file_exists($F_file) ) {
         $this->output->set_header('Content-type: text/plain; charset=utf-8');
         print_r('<div class="border"><img alt="' . $data['text'] . '" src="' . base_url() . 'picker/' . $filename . '" /></div>');

         #db $this->update_db($data) kan dah insert kat file_tbl, mari tag gambar ni kt file_manager.
         if($data['img_tag']){

            $stmt = $this->db->conn_id->prepare("SELECT id FROM file_tbl WHERE filename = :filename");
            $stmt->execute(array(
               ':filename'=>$data['filename']
            ));
            $img_id = $stmt->fetch(PDO::FETCH_COLUMN);
            if(is_numeric($img_id)){
               #1. kita arraykan by comma tag input ni.
               $tag['tag'] = explode(',',$data['img_tag']);
               $this->m_tags->insert($tag, 'file_tbl', $img_id);
            }

            // $stmt = $this->db->conn_id->prepare("SELECT id FROM file_tbl WHERE filename = :filename");
            // $stmt->execute(array(
            //    ':filename'=>$data['filename']
            // ));
            // $img_id = $stmt->fetch(PDO::FETCH_COLUMN);
            // if(numeric($img_id)){
            //    $msk = $this->db->conn_id->prepare("INSERT INTO tag_manager SET(filename, from_tbl, from_id) VALUES(:filename, :from_tbl, :from_id)");
            //    $msk->execute(array(
            //       ':filename'=>$data['filename'],
            //       ':from_tbl'=>$data['from_tbl'],
            //       ':from_id'=>$data['from_id']
            //    ));
            // }
         }

      } else {

         // var_dump(
         //    file_exists($the_gambar) , file_exists($A_file) , file_exists($B_file) , file_exists($C_file) , file_exists($D_file) , file_exists($E_file) , file_exists($F_file)
         // );
         #ITEM FAILED, DELETE ALL
         if(file_exists($the_gambar) && !is_dir($the_gambar)){
              unlink($the_gambar);
         }
         if(file_exists($A_file) && !is_dir($A_file)){
              unlink($A_file);
         }
         if(file_exists($B_file) && !is_dir($B_file)){
              unlink($B_file);
         }
         if(file_exists($C_file) && !is_dir($C_file)){
              unlink($C_file);
         }
         if(file_exists($D_file) && !is_dir($D_file)){
              unlink($D_file);
         }
         if(file_exists($E_file) && !is_dir($E_file)){
              unlink($E_file);
         }
         if(file_exists($F_file) && !is_dir($F_file)){
              unlink($F_file);
         }
         clearstatcache();

         ////////////////////////////// DELETE DB //////////////////////
         $this->m_img->delete($data['filename'], $data['from_tbl'], $data['from_id']);
         ////////////////////////////// DELETE DB //////////////////////
      }
   }



   function update_db($data){

      date_default_timezone_set("Asia/Kuala_Lumpur");
      $masa = date("Y-m-d");

      #insert to file_tbl.
      $query = $this->db->conn_id->prepare( "INSERT INTO file_tbl ( filename, caption, uploader )
      VALUES ( :filename, :caption, :uploader )" );
      $check_q = $query->execute( array(
         ':filename'=>$data['filename'],
         ':caption'=>$data['text'],
         ':uploader'=>current_user_id()
      ) );
      if($check_q){ #berjaya masukkan data ke file_tbl.
         #let's tell our system, that this images used in where. so later if there's action from_tbl from_id, if this images no used at another post, we can delete it.
         $M_query = $this->db->conn_id->prepare( "INSERT INTO file_manager ( from_tbl, from_id, filename )
         VALUES ( :from_tbl, :from_id, :filename )" );
         $check_m = $M_query->execute( array(
            ':from_tbl' =>$data['from_tbl'],
            ':from_id' =>$data['from_id'],
            ':filename' =>$data['filename']
         ) );
         if($check_m){ #berjaya masukkan record file_manager.
            return TRUE;
         } else { #gagal masukkan record ke file_manager.
            #delete balik apa yg berjaya di masukkan di $query.
            $stmt = $this->db->conn_id->prepare("DELETE FROM file_tbl WHERE filename = :filename");
            $stmt->execute(array(
               ':filename'=>$data['filename']
            ));
            return FALSE;
         }
      } else { #gagal masukkan file ke file ke file_tbl.
         return FALSE;
         #return false ini akan membuatkan function diatas delete semula file yg cuba di upload.

      }
   } #end of function.









}#end class
