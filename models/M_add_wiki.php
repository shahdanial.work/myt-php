<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_add_wiki extends CI_Model{


     function new_wiki(){

          date_default_timezone_set("Asia/Kuala_Lumpur");
          $data['masa'] = date("Y-m-d H:i:s");
          $this->load->helper('h_user_helper');
          $data['author_id'] = current_user_id();

          $ck = $this->db->conn_id->prepare("SELECT id from tag_tbl WHERE title = :title AND content = :content AND author_id = :author_id AND status = :status");
          $ck->execute(array(
               ':title'=> '',// content kosong...
               ':content'=>'',// content kosong...
               ':author_id'=>current_user_id(),
               ':status'=>'pending'
          ));
          $id = $ck->fetch(PDO::FETCH_COLUMN);
          if($id){
               #ada draft kosong.
               redirect(site_url('wiki?id='.$id.'&status=pending'));

          } else {

               #takde yg kosong.
               // $this->db->conn_id->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
               $query = $this->db->conn_id->prepare( "INSERT INTO tag_tbl ( masa, author_id )
               VALUES ( :masa, :author_id )" );
               $query->execute(array(
                    ':masa' =>$data['masa'],
                    ':author_id' =>current_user_id()
               ));
               // print_r($query->errorInfo());
               if($query){

                    #set up url query redirect to draft
                    $get_id = $this->db->conn_id->prepare('SELECT id FROM tag_tbl WHERE author_id = :author_id ORDER BY id ASC LIMIT 0,1');
                    $get_id->execute(array(
                         ':author_id'=>current_user_id()
                    ));
                    $result = $get_id->fetch(PDO::FETCH_COLUMN);
                    if(is_numeric($result)){
                         redirect(site_url('wiki?id='.$result.'&status=pending'));
                    } else {
                         #failed to get id.
                         print_r('Ops.. There\'s some error. Please infrom to shahdanial.work@gmail.com');
                         // print_r('<textarea style="width:100%;height:700px;">');
                         // print_r($data);
                         // print_r('</textarea>');
                    }

               } else { // else $query

                    #failed to insert new draft, so let's retry by load this url, it will automatically retry.
                    print_r('There\'s some error. Please infrom to shahdanial.work@gmail.com');

               } // end $query.

          } // if $id


     } // end function



}
