<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_posts extends CI_Model{

   public function __construct(){
      parent::__construct();
      $this->load->helper(array('h_user_helper', 'h_query_helper', 'h_file_helper','h_tag_helper', 'h_link_helper'));
      $this->load->model(array('m_tags', 'm_keyword', 'm_categories', 'm_files'));
   }

   function post_proccess($data){

       date_default_timezone_set("Asia/Kuala_Lumpur");
       $data['masa'] = date("Y-m-d H:i:s");
       $data['author_id'] = author_post_id($data['from_id'], 'post_tbl');

       #setup img uploaded
       $data['img_uploaded'] = img_uploaded('post_tbl', $data['from_id']);
       $data['img_used'] = img_used($data);
       $img_org = img_org($data);
       $img_unused = img_unused($data);

       if(isset($data['tag'])) $old_tags = tag_recorded('post_tbl', $data['from_id']);


       #whatever to_tbl and from_tbl. we always record penggunaan img org..
       if($img_org){
            foreach ($img_org as $filename) {
                $this->m_img->record($filename, 'post_tbl', $data['from_id']);
            }
        }

        if( $data['to_tbl'] != $data['from_tbl'] || isset($data['reset_gambar'])){// status bertukar
            #ex: draft to pending, pending to publish etc. (dengan ini draft tidak menerima effect)
            if($img_unused){
                foreach ($img_unused as $filename) {
                    $this->m_img->delete($filename, 'post_tbl', $data['from_id']);
                }
            }
        }

        /*
        diatas ni kan kita delete img_unused. so skang kita ada record image digunakan yang baru
        */
        // $data['img_uploaded'] = img_uploaded($data);

        if(sizeof($data['tag'])<1){
           #tag xde
            $data['tag'] = array();
        }

        $data['status'] = $data['to_tbl'];

        /* --------------- UPDATE ---------------- */

        $this->m_tags->delete('post_tbl', $data['from_id']);
        $this->m_keyword->delete('post_tbl', $data['from_id']);
        $this->m_categories->delete('post_tbl', $data['from_id']);
        // $this->m_img->delete($data);



        if(isset($data['tag'])){
           if(sizeof($data['tag']) > 0){
               $this->m_tags->insert($data, 'post_tbl', $data['from_id']);
           }
        }


        if(isset($old_tags)){
           if(sizeof($old_tags) > 0 ){
             #tag ada, mari delete tag lama di tag_tbl yg ko create dari post ini, tp x guna di post ini.

             foreach ($old_tags as $path) {

                if( !in_array($path, $data['tag']) ){
                  $this->m_tags->delete_tag_tbl($path);
                }

             }

           }
        }


        if(isset($data['gender'])){
           if(sizeof($data['gender']) > 0){
               $this->m_keyword->insert($data, 'post_tbl', $data['from_id']);
           }
        } else if(isset($data['interest'])){
           if(sizeof($data['interest']) > 0){
               $this->m_keyword->insert($data, 'post_tbl', $data['from_id']);
           }
        }

        if(isset($data['category'])){
           if(!empty($data['category'])){
                if(allowed_domain('http://'.$data['category'])){
                   $this->m_categories->insert($data['category'], 'post_tbl', $data['from_id']);
                }
           }
        }



        #5. if you want to set status as 'publish', you need to set the unique url first and this post dont have path.
        if($data['from_tbl'] != $data['to_tbl'] && $data['status'] == 'publish'){
             if(!empty($data['meta_title'])){
                  $data['path'] = build_unique_link($data['meta_title'], 'post_tbl', 'path');
             } else {
                  $data['path'] = build_unique_link($data['title'], 'post_tbl', 'path');
             }
        }

        #finally
        $this->update($data);

        /* --------------- UPDATE ---------------- */


      if($data['redirect']){
          if(!empty($data['redirect'])){
           $this->session->set_flashdata('from_tbl', $data['to_tbl']);
           redirect($data['redirect']);
          }
      }

   }#end post_proccess

   function update($data){

      $data['selected_key'] = array('title','content','first_img','meta_title','meta_description','google_thumb','viral_title','viral_description','viral_thumb','status');

      if($data['from_tbl'] != 'publish' && $data['from_tbl'] != 'banned') array_push($data['selected_key'], 'masa');

      if(isset($data['path'])) array_push($data['selected_key'], 'path');

      $query = $this->db->conn_id->prepare(
         'UPDATE post_tbl SET '.query_update($data)['prepare'].' WHERE id = :from_id'
      );

      $getter = query_update($data)['execute'];
      $getter[':from_id'] = $data['from_id'];

      $query->execute($getter);

   }



} #end class
