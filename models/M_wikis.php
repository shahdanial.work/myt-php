<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_wikis extends CI_Model{

   public function __construct(){
      parent::__construct();
      $this->load->helper(array('h_user_helper', 'h_query_helper', 'h_file_helper','h_tag_helper', 'h_link_helper'));
      $this->load->model(array('m_tags', 'm_categories', 'm_files', 'm_keyword'));
   }

   function post_proccess($data){

       date_default_timezone_set("Asia/Kuala_Lumpur");
       $data['masa'] = date("Y-m-d H:i:s");
       $data['from_tbl'] = 'tag_tbl';
       $data['author_id'] = author_post_id($data['from_id'], $data['from_tbl']);

       #setup img uploaded
       $data['img_uploaded'] = img_uploaded($data['from_tbl'], $data['from_id']);
       $data['img_used'] = img_used($data);
       $img_org = img_org($data);
       $img_unused = img_unused($data);

       if(isset($data['tag'])) $old_tags = tag_recorded('tag_tbl', $data['from_id']);


       # record penggunaan img org..
       if($img_org){
            foreach ($img_org as $filename) {
                $this->m_img->record($filename, $data['from_tbl'], $data['from_id']);
            }
        }



        if(isset($data['status'])){

           /* status row, this used if request to change row from pending. */
           if($data['status'] == 'publish'){
               if($img_unused){
                   foreach ($img_unused as $filename) {
                       $this->m_img->delete($filename, $data['from_tbl'], $data['from_id']);
                   }
               }
           }

        }





      $this->m_tags->delete('tag_tbl', $data['from_id']);
      $this->m_keyword->delete('tag_tbl', $data['from_id']);
      $this->m_categories->delete('tag_tbl', $data['from_id']);
      // $this->m_img->delete($data);



      if(isset($data['tag'])){
          if(sizeof($data['tag']) > 0){
             $this->m_tags->insert($data, 'tag_tbl', $data['from_id']);
          }
      }

      if(isset($old_tags)){
         if(sizeof($old_tags) > 0 ){
            #tag ada, mari delete tag lama di tag_tbl yg ko create dari post ini, tp x guna di post ini.

            foreach ($old_tags as $path) {

              if( !in_array($path, $data['tag']) ){
                $this->m_tags->delete_tag_tbl($path);
              }

            }

         }
      }

      if(isset($data['gender'])){
          if(sizeof($data['gender']) > 0){
             $this->m_keyword->insert($data, 'tag_tbl', $data['from_id']);
          }
      } else if(isset($data['interest'])){
          if(sizeof($data['interest']) > 0){
             $this->m_keyword->insert($data, 'tag_tbl', $data['from_id']);
          }
      }





        $this->update($data);



      if($data['redirect']){
          if(!empty($data['redirect'])){
           $this->session->set_flashdata('from_tbl', $data['to_tbl']);
           redirect($data['redirect']);
          }
      }

    //    print_r('<textarea style="width:100%;height:100%">');
    //    print_r($data);
    //    print_r('</textarea>');

   }#end post_proccess

   function update($data){

      $data['selected_key'] = array('masa','type','title','content','first_img','meta_title','meta_description','google_thumb','viral_title','viral_description','viral_thumb','status');

      $query = $this->db->conn_id->prepare(
         'UPDATE '.$data['from_tbl'].' SET '.query_update($data)['prepare'].' WHERE id = :from_id'
      );

      $getter = query_update($data)['execute'];
      $getter[':from_id'] = $data['from_id'];

      $query->execute($getter);

   }



   //
   //
   // function type_manager($data){
   //
   //          $data['selected'] = array('from_tbl', 'from_id', 'type_path');
   //
   //          $query = $this->db->conn_id->prepare(
   //              'INSERT INTO type_manager '.query_insert($data)['prepare']
   //          );
   //          $query->execute(
   //              query_insert($data)['execute']
   //          );
   //
   // }#end insert..
   //
   //
   // function delete_type_manager($data){
   //
   //
   //      $query = $this->db->conn_id->prepare(
   //          'DELETE FROM type_manager WHERE from_id = :from_id AND from_tbl = :from_tbl'
   //      );
   //      $query->execute(array(
   //          ':from_id'=>$data['from_id'],
   //          ':from_tbl' => $data['from_tbl']
   //      ));
   //
   // }#end delete.



} #end class
