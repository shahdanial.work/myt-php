<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_add_post extends CI_Model{




   function new_draft(){

        date_default_timezone_set("Asia/Kuala_Lumpur");
        $data['masa'] = date("Y-m-d H:i:s");
        $this->load->helper('h_user_helper');
        $data['author_id'] = current_user_id();

        $ck = $this->db->conn_id->prepare("SELECT id from post_tbl WHERE title = :title AND content = :content AND author_id = :author_id AND  status = :status");
        $ck->execute(array(
             ':title'=> '',// content kosong...
             ':content'=>'',// content kosong...
             ':author_id'=>current_user_id(), //user id lu
             ':status' => 'draft'
        ));
        $id = $ck->fetch(PDO::FETCH_COLUMN);
        if(is_numeric($id)){
             #ada draft kosong.
             redirect(site_url('post?id='.$id.'&status=draft'));

        } else {

             #takde yg kosong.
             // $this->db->conn_id->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
             $query = $this->db->conn_id->prepare( "INSERT INTO post_tbl ( author_id, status )
             VALUES ( :author_id, :status )" );
             $query->execute(array(
                  ':author_id' =>current_user_id(),
                  ':status' => 'draft'
             ));
             // print_r($query->errorInfo());
             if($query){

                  #set up url query redirect to draft
                  $get_id = $this->db->conn_id->prepare('SELECT id FROM post_tbl WHERE author_id = :author_id AND status = :status ORDER BY id DESC LIMIT 0,1');
                  $get_id->execute(array(
                       ':author_id'=>current_user_id(),
                       ':status' => 'draft'
                  ));
                  $result = $get_id->fetch(PDO::FETCH_COLUMN);
                  if(is_numeric($result)){
                       redirect(site_url('post?id='.$result.'&status=draft'));
                  } else {
                       #failed to get id.
                       print_r('Ops.. There\'s some error. Please infrom to shahdanial.work@gmail.com');
                       // print_r('<textarea style="width:100%;height:700px;">');
                       // print_r($data);
                       // print_r('</textarea>');
                  }

             } else { // else $query

                  #failed to insert new draft, so let's retry by load this url, it will automatically retry.
                  print_r('There\'s some error. Please infrom to shahdanial.work@gmail.com');

             } // end $query.

        } // if $id





     /*
     function new_draft(){

          date_default_timezone_set("Asia/Kuala_Lumpur");
          $data['masa'] = date("Y-m-d H:i:s");
          $this->load->helper('h_user_helper');
          $data['author_id'] = current_user_id();

          $ck = $this->db->conn_id->prepare("SELECT id from draft_tbl WHERE title = :title AND content = :content AND author_id = :author_id");
          $ck->execute(array(
               ':title'=> '',// content kosong...
               ':content'=>'',// content kosong...
               ':author_id'=>current_user_id()
          ));
          $id = $ck->fetch(PDO::FETCH_COLUMN);
          if($id){
               #ada draft kosong.
               redirect(site_url('post?id='.$id.'&status=draft'));

          } else {

               #takde yg kosong.
               // $this->db->conn_id->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
               $query = $this->db->conn_id->prepare( "INSERT INTO draft_tbl ( masa, author_id )
               VALUES ( :masa, :author_id )" );
               $query->execute(array(
                    ':masa' =>$data['masa'],
                    ':author_id' =>current_user_id()
               ));
               // print_r($query->errorInfo());
               if($query){

                    #set up url query redirect to draft
                    $get_id = $this->db->conn_id->prepare('SELECT id FROM draft_tbl WHERE author_id = :author_id ORDER BY id ASC LIMIT 0,1');
                    $get_id->execute(array(
                         ':author_id'=>current_user_id()
                    ));
                    $result = $get_id->fetch(PDO::FETCH_COLUMN);
                    if(is_numeric($result)){
                         redirect(site_url('post?id='.$result.'&status=draft'));
                    } else {
                         #failed to get id.
                         print_r('Ops.. There\'s some error. Please infrom to shahdanial.work@gmail.com');
                         // print_r('<textarea style="width:100%;height:700px;">');
                         // print_r($data);
                         // print_r('</textarea>');
                    }

               } else { // else $query

                    #failed to insert new draft, so let's retry by load this url, it will automatically retry.
                    print_r('There\'s some error. Please infrom to shahdanial.work@gmail.com');

               } // end $query.

          } // if $id
     */


     } // end function



}
