<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_mail extends CI_Model{

     public function __construct() {
          parent::__construct();
          // $this->load->helper(array('user_helper'));
          $this->load->library('email');
     }

     function pendaftaran_manual($data){

          if($data['email'] && $data['password']){

               $my_messages = '';

               if($data['nama']){
                    $my_messages .= 'Hi '.$data['nama'].',<br/><br/>';
               }

               $my_messages .= '
               Anda kini adalah sebahagian dari team Malaya Times. Untuk menjana pendapatan, anda boleh login di <a href="http://media.malayatimes.com">MEDIA MALAYATIMES</a> dengan klik button <b>Google Login</b>.
               <br/>
               Namun sekiranya berlaku sebarang masalah kelak, anda juga boleh login menggunakan details dibawah.
               <br/><br/>
               <hr>
               <br/>
               email: <i>'.$data['email'].'</i><br/>
               password: <b>'.$data['password'].'</b><br/>
               Login di: http://media.malayatimes.com<br/><br/>
               NOTA: Sebaiknya anda tukar password kepada yang mudah anda ingati di <a href="http://media.malayatimes.com/profile#password">http://media.malayatimes.com/profile#password</a>.
               <br/><br/>
               <hr>
               <br/><br/>
               <center>
               <a title="Berita Semasa Malaysia" href="http://www.malayatimes.com" target="_blank"><img alt="Berita Semasa Malaysia" src="https://3.bp.blogspot.com/-hrz9tfwwpzU/V0RjW9CulkI/AAAAAAAAA3w/IWbGZmdFD7kjvX_qnDJcdndz_N9q3mnvQCLcB/s1600/Malaya-Times-News.jpg"/></a>
               </center>
               <br/><br/>
               <h3>Benefit menjadi penulis di MALAYATIMES.COM</h3>
               <ol>
               <li>Menjana pendapatan lumayan dengan menerbitkan posts/entri-entri.</li>
               <li>Dibayar income untuk segala usaha anda sendiri. Selagi post-post anda ada pembaca.</li>
               <li>Anda menulis di sebuah portal terbesar di Malaysia yang mempunyai trafik tinggi, lebih mudah boost income berbanding website sendiri.</li>
               <li>Anda tidak perlu pandai atau membuang masa untuk pengaturcaraan sebuah Website yang rumit untuk menjana pendapatan yang baik melali website.</li>
               <li>Setiap artikel anda mampu menjanakan pendapatan setiap bulan! selagi ada pembacanya (pasif income).</li>
               <li>Sekiranya anda menulis bagus dan berkualiti serta rajin, anda akan menerima pelbagai tawaran dari syarikat (antaranya: kenaikan percent Share, bonus dan sebagainya).</li>
               <li>Kami menyediakan pelbagai kategori pilihan mengikut kesesuaian dan minat anda. Anda boleh menulis berkaitan Resepi, Tutorial, Info, Tips, Iklan, Soalan, Artis, Pendidikan, Seram, Kerjaya, Rumah Tangga dan pelbagai lagi.</li>
               <li>Inilah kerjaya part time atau full time lumayan yang paling berpotensi untuk dilaksanakan di mana jua tanpa kekangan masa dan tekanan!</li>
               </ol>
               <br/><br/>
               <h3>Kesimpulan</h3>
               <ol>
               <li>Sila menulis dengan banyak. Sekali anda menulis anda menambah satu pasif income. Bayangkan dalam setahun anda berjaya menulis beribu Artikel, berapa pasif income anda? 1 Artikel mampu menjana Ratusan/Ribuan ringgit SEBULAN mengikut jumlah pembaca dan populariti.</li>
               <li>Tulis artikel yang menjadi minat natizen (viral).</li>
               <li>Tulis artikel yang berkaitan dengan kehangatan dan isu semasa (terkini).</li>
               <li>Tulis artikel yang berinformasi dan <b>sentiasa</b> di cari di Google, misalnya <i>Tempat menarik di Selangor</i>.</li>
               <li>Share artikel anda dibanyak group dan pelbagai social media.</li>
               <li>Tulis apa jua yang bersifat baik untuk pembaca.</li>
               </ol>
               <br/><br/>
               <h3>Terma dan syarat</h3>
               <ol>
               <li>Setiap artikel yang anda submit, <b>kadangkala</b> akan disaring dahulu sebelum diterbitkan.</li>
               <li>Jangan membuat artikel yang mempunyai keyword dan usur 18sx, 18sg, fitnah dan sampah.</li>
               <li>Sila olah dan gunakan bahasa yang formal.</li>
               <li>Income dari artikel anda dibayar dalam frekunsi bulan per request.</li>
               </ol>
               ';

               $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.gmail.com',
                    'smtp_port' => '465',
                    'smtp_timeout' => '7',
                    'smtp_user' => 'malayatimes.com@gmail.com',
                    'smtp_pass' => 'antasyahh',
                    'wordwrap' => TRUE,
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'newline' => "\r\n"
               );
               $this->email->initialize($config);
               $this->email->from('malayatimes.com@gmail.com', 'Malaya Times Management');
               $this->email->to($data['email']);
               $this->email->subject('Details Login Malaya Times');
               $this->email->message($my_messages);

               return $this->email->send();

          }

     }




     function tukar_email($data){

          if($data['email']){

               $my_messages = 'Hi '.$data['nama'].',<br/><br/>
               Anda baru sahaja menukar <b>Details Profile untuk Login</b> di <a href="http://media.malayatimes.com">MEDIA MALAYATIMES</a>. Dibawah berikut adalah Details  login baru anda.
               <br/><br/>
               <hr>
               <br/>
               email: <i>'.$data['email'].'</i><br/>
               password: <b>'.$data['password'].'</b><br/>
               Login di: http://media.malayatimes.com<br/><br/>
               <br/><br/>
               <hr>
               <br/><br/>
               <center>
               <a title="Berita Semasa Malaysia" href="http://www.malayatimes.com" target="_blank"><img alt="Berita Semasa Malaysia" src="https://3.bp.blogspot.com/-hrz9tfwwpzU/V0RjW9CulkI/AAAAAAAAA3w/IWbGZmdFD7kjvX_qnDJcdndz_N9q3mnvQCLcB/s1600/Malaya-Times-News.jpg"/></a>
               </center>
               ';

               $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.gmail.com',
                    'smtp_port' => '465',
                    'smtp_timeout' => '7',
                    'smtp_user' => 'malayatimes.com@gmail.com',
                    'smtp_pass' => 'antasyahh',
                    'wordwrap' => TRUE,
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'newline' => "\r\n"
               );
               $this->email->initialize($config);
               $this->email->from('malayatimes.com@gmail.com', 'Malaya Times Management');
               $this->email->to($data['email']);
               $this->email->subject('Profile anda telah dikemaskini');
               $this->email->message($my_messages);

               return $this->email->send();

          }

     }




     function gtau_update_profile($your_email,$your_name, $new_email){

          $my_messages = 'Hi '.$your_name.',<br/><br/>
          Anda baru sahaja menukar <b>Details Profile untuk Login</b> di <a href="http://media.malayatimes.com">MEDIA MALAYATIMES</a>. Dibawah berikut adalah Email baru yang dikemaskini untuk profile anda.
          <br/><br/>
          <hr>
          <br/>
          email: <i>'.$new_email.'</i><br/>
          Login di: http://media.malayatimes.com<br/><br/>
          <br/><br/>
          <b>(jika kemaskini datails profile anda ini bukan dilakukan oleh anda sendiri. Mungkin komputer atau email anda telah di godam. <a href="http://media.malayatimes.com/profile#password">Sila tukar Password Malayatimes</a> dan Details login Email anda segera. Atau untuk bantuan; sila hubungi shahdanial.work@gmail.com)</b>.
          <br/><br/>
          NOTA: Email ini dihantar sebagai makluman, kerana sistem kami sangat mengambil berat tentang data, privasi dan sekuriti Team kami.';

          $config = Array(
               'protocol' => "smtp",
               'smtp_host' => "ssl://smtp.gmail.com",
               'smtp_port' => "465",
               'smtp_timeout' => "30",
               'smtp_user' => "malayatimes.com@gmail.com",
               'smtp_pass' => "antasyahh",
               'wordwrap' => TRUE,
               'mailtype' => "html",
               'charset' => "utf-8",
               'newline' => "\r\n"
          );
          $this->email->initialize($config);
          $this->email->from('<malayatimes.com@gmail.com>', 'Malaya Times Management');
          $this->email->to($your_email . ' <' . $your_email . '>');
          $this->email->subject('Kemasikini Login details');
          $this->email->message($my_messages);

          #return $this->email->send();

          if($this->email->send()){
               return true;
          } else {
               show_error($this->email->print_debugger());
               return false;
          }

     }




}
