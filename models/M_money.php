<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_money extends CI_Model{

  function update_income($data){
    // $this->db->insert('keluar_masuk',$data);
    // var_dump($data);
    $query = $this->db->conn_id->prepare(
      "INSERT INTO keluar_masuk
             (currency, value, penerangan, added_time, added_by_user_id, income_cost)
      VALUES(:currency, :value, :penerangan, :added_time, :added_by_user_id, :income_cost)"
    );

    $query->execute(
      array(
          ':currency' => $data['currency'],
          ':value' => $data['value'],
          ':penerangan' => $data['penerangan'],
          ':added_time' => $data['added_time'],
          ':added_by_user_id' => $data['added_by_user_id'],
          ':income_cost' => $data['income_cost']
      )
    );
    // $result->$query->fetch(PDO::FETCH_COLUMN);
    // var_dump($result);

    redirect(site_url('income_cost'));
  }

  function edit_income($data){
     $query = $this->db->conn_id->prepare(
       "UPDATE keluar_masuk SET added_by_user_id = :added_by_user_id, added_time = :added_time, currency = :currency, value = :value, penerangan = :penerangan WHERE id = :id"
     );

     $query->execute(
        array(
           ':added_by_user_id' => current_user_id(),
           ':added_time' => $data['added_time'],
           ':currency' => $data['currency'],
           ':value' => $data['value'],
           ':penerangan' => $data['penerangan'],

           ':id' => $data['id']
        )
     );
     // $result->$query->fetch(PDO::FETCH_COLUMN);
     redirect($data['redirect']);
  }

}
