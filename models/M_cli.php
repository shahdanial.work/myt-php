<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_cli extends CI_Model{

  function clear_ip_post_data($data, $site_data){
    $this->db->update('post_tbl',$data);

    $this->db->where('what', 'time_clear_ip');
    $this->db->update('site_data',$site_data);
  }

}
