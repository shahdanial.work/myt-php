<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_view extends CI_Model{

  function up_visitor_online($tambah_online){ // on visit anything
    $this->db->where('what', 'visitor_online');
    $this->db->update('site_data', $tambah_online);
  }

  function up_post_view($page_url, $post_detail, $post_user_id, $post_detail1, $post_detail2){ // on visit post page
    $this->db->where('blogger_post_url', $page_url);
    $this->db->update('post_tbl', $post_detail);

    $this->db->where('id_user', $post_user_id);
    $this->db->update('users', $post_detail1);

    $this->db->where('what', 'visitor_online');
    $this->db->update('site_data', $post_detail2);
  }

  function up_tag_view($current_id, $label_data, $tambah_online){ //on visit tag page
    $this->db->where('id', $current_id);
    $this->db->update('tag_tbl', $label_data);

    $this->db->where('what', 'visitor_online');
    $this->db->update('site_data', $tambah_online);
  }




  #reserve

  function leavepage($what, $site_detail){
    $this->db->where('what', $what);
    $this->db->update('site_data', $site_detail);
  }

  function new_label($label_data1, $tambah_online){ // if label not exist when viewing label page
    $this->db->insert('tag_tbl',$label_data1);
    $this->db->update('site_data', $site_detail);

    $this->db->where('what', 'visitor_online');
    $this->db->update('site_data', $tambah_online);
  }

  function convert_post_to_admin($post_data, $user_data, $shahdanial_id){ // if post exist in table without user_id
    $this->db->insert('post_tbl',$post_data);

    $this->db->where('id_user', $shahdanial_id);
    $this->db->update('users', $user_data);
  }



}
