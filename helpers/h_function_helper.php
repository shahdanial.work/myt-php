<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('gambar_page')) {
  function gambar_page($pageurl, $blogspot_size, $malayatimes_size) {
    //$pageurl=$dari_canonical.'?m=1';
    $myhtml = file_get_contents($pageurl);
    $mydoc = new DOMDocument();
    @$mydoc->loadHTML($myhtml);
    $hmm = $mydoc->getElementById('entry-content')->getElementsByTagName('img');
    $kira = 0;
    foreach ($hmm as $hm) {
      $kira = $kira+1;
      $imglink = $hm->getAttribute('src'); // before this the variable is $hhh
      /*if($hhh){//if($kira == 1 && $hhh){
      $semiimg = str_replace('/s1600/', '/w658-h309-c/', $hhh);
      $img = str_replace('/gambar/', '/foto/', $semiimg);
    } else {
    $img = 'link default image here';
  } */

}
$linkgambar = parse_url($imglink);
$domain = explode('.', $linkgambar['host']);
$tld = $domain[sizeof($domain)-2] . '.' . $domain[sizeof($domain)-1];
if($tld == 'malayatimes.com'){
  $linkgambarpath = explode('/', $linkgambar['path']);
  return str_replace( '/' . $linkgambarpath[sizeof($linkgambarpath)-2] . '/', '/' . $malayatimes_size . '/', $imglink );
} else {
  $linkgambarpath = explode('/', $linkgambar['path']);
  return str_replace( '/' . $linkgambarpath[sizeof($linkgambarpath)-2] . '/', '/' . $blogspot_size . '/', $imglink );
}

}
}

//############

if ( ! function_exists('title_page')) {
  function title_page($pageurl) {
    //$pageurl=$dari_canonical.'?m=1';
    $myhtml1 = file_get_contents($pageurl);
    $mydoc1 = new DOMDocument();
    @$mydoc1->loadHTML($myhtml1);
    $title = $mydoc1->getElementsByTagName('h1')->item('0')->textContent;
    return $title;
  }
}

//############

if ( ! function_exists('entry_page')) {
  function entry_page($pageurl) {
    $content = file_get_contents($pageurl);
    $first_step = explode( '<section>' , $content );
    $second_step = explode("</section>" , $first_step[1] );

    return $second_step[0];
  }
}

//############

if ( ! function_exists('label_page')) {
  function label_page($pageurl) {
    $myhtml2 = file_get_contents($pageurl);
    $mydoc2 = new DOMDocument();
    @$mydoc2->loadHTML($myhtml2);
    $hmm2 = $mydoc2->getElementById('headline_meta')->getElementsByTagName('strong');
    $kira2 = 0;
    foreach ($hmm2 as $hm2) {
      $kira2 = $kira2+1;
      $labels = $hm2; // before this the variable is $hhh
      return $labels->textContent;
    }
  }
}

//############

if ( ! function_exists('postid_page')) {
  function postid_page($pageurl) {
    $myhtml3 = file_get_contents($pageurl);
    $mydoc3 = new DOMDocument();
    @$mydoc3->loadHTML($myhtml3);
    $hmm3 = $mydoc3->getElementById('headline_meta')->getElementsByTagName('b');
    foreach ($hmm3 as $hm3) {
      $postid = $hm3; // before this the variable is $hhh
      return $postid->textContent;
    }
  }
}
