<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_tld')) {
  function get_tld($param){
     $param = 'http://'. str_replace('//', '', str_replace('https://', '', str_replace('http://', '', $param)));
    $dari= parse_url($param);
    if($dari['host']){
      $dari_domain = $dari['host'];
      //$dari_canonical = $dari['scheme'] . '://' . $dari['host'] . $dari['path'];
      $fromdomain = explode('.', $dari_domain);
      $fromtld = $fromdomain[sizeof($fromdomain)-2] . '.' . $fromdomain[sizeof($fromdomain)-1];
      return $fromtld;
    } else {
      return FALSE;
    }
  }
}

if ( ! function_exists('allowed_domain')){
  function allowed_domain($param){
    $allowed_domain = array('malayatimes.com','malayatimes.com');
    $str = get_tld($param);
    if(in_array($str, $allowed_domain)){
      return TRUE;
    } else  {
      return FALSE;
    }
  }
}

if( !function_exists('build_link')){
   function build_link($param){
      return strtolower(
        preg_replace('/[^A-Za-z0-9\- ]/', '',
        str_replace(' ', '-',
        preg_replace('!\s+!', ' ', trim($param))
        )
        )
      );
   }
}


if( !function_exists('build_unique_link')){
  # build_unique_link($link_anda, $table_to_check, $row_link_name)
  function build_unique_link($link, $table_name, $link_row){

    $link = build_link($link);

    $ci =& get_instance();
    $ci->load->helper('h_query_helper');

    $num = 0;

    while(check_table($link, $table_name, $link_row)){
      $num ++;
      $link = $link . '-' . $num;
    }

    return $link;

  }
}
