<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('check_html')) {
  function check_html($str){
    return preg_match( "/\/[a-z]*>/i", $str ) != 0;
  }
}

if(! function_exists('build_js_id')){
   function build_js_id($string) {
      $string = str_replace(' ', '-', trim($string)); // Replaces all spaces with hyphens.

      return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
   }
}

if ( ! function_exists('format_content_as_editor')) {
  function format_content_as_editor($str){
    /*if($str){
      return
      preg_replace('/(\r\n|\n|\r){3,}/', "$1$1",
      str_replace('<br/>', '',
      str_replace('<br>', '',
      //preg_replace('/<p [^<]*?class="([^<]*?para.*?)">(.*?)<\/p>/','<div class="$1" contenteditable="true" autocomplete="off">$2</div>',
      str_replace('<p class="para"', '<div class="para" contenteditable="true" autocomplete="off"',
      str_replace('</p>', '</div>',
      $str
      )
      )
      //)
      )
      )
      );
   } else {
      return  '<div class="para" contenteditable="true" autocomplete="off">Sila berkarya dengan kemas dan teratur...</div>';
   } */
   if($str){
      return
      preg_replace('/<div [^<]*?class="([^<]*?message.*?)">(.*?)<\/div>/s','<div class="$1" contenteditable="true" autocomplete="off">$2</div>',
      str_replace('<blockquote', '<blockquote contenteditable="true" autocomplete="true"',
      str_replace('<tr', '<tr contenteditable="true" autocomplete="true"',
      str_replace('<td', '<td contenteditable="true" autocomplete="true"',
      str_replace('<br/>', '',
      str_replace('<br>', '',
      preg_replace('/<p [^<]*?class="([^<]*?para.*?)">(.*?)<\/p>/s','<div class="$1" contenteditable="true" autocomplete="off">$2</div>',
      $str
      )
      )
      )
      )
      )
      )
      );
   } else {
      return  '<div class="para" contenteditable="true" autocomplete="off">Start writing..</div>';
   }
  }
}
