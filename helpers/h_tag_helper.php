<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('tag_recorded')) {
  function tag_recorded($from_tbl, $from_id){

      //if($data['tag']){

        $path_sql = array();

          //foreach ($data['tag'] as $title) {

            $ci =& get_instance();

            #SETUP UPLOADED IMAGE.
            $tag_path = $ci->db->conn_id->prepare(
                "SELECT tag_path FROM tag_manager WHERE (from_tbl = :from_tbl AND from_id = :from_id)"
            );
            $tag_path->execute(array(
                ':from_tbl' => $from_tbl,
                ':from_id' => $from_id
            ));

            if($tag_path){
                $path_sql = $tag_path->fetchAll(PDO::FETCH_COLUMN, 0); #tags yg upload di post ini. as array
            }

          //} # end foreach

          return $path_sql;

      //} #$data['tag']

  }
}