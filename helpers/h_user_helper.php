<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//###########
if ( ! function_exists('author_post_id')) {
    function author_post_id($post_id, $table_name){
        $ci =& get_instance();
        $query = $ci->db->conn_id->prepare(
            'SELECT author_id FROM '.$table_name.' WHERE id = :post_id'
        );
        $query->execute(array(
            ':post_id'=>$post_id
        ));
        $author_id = $query->fetch(PDO::FETCH_COLUMN);
        return $author_id;

    }
}

//###########

if ( ! function_exists('convert_money')) {
   function convert_money($amount, $from, $to){ // example convert_money("1", "USD", "MYR");
      $data = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$from&to=$to");
      preg_match("/<span class=bld>(.*)<\/span>/",$data, $converted);
      $converted = preg_replace("/[^0-9.]/", "", $converted[1]);
      return $converted; //number_format((float)$converted, 2, '.', '')
   }
}

if ( ! function_exists('myr_to_usd')) {
   function myr_to_usd($amount){ // example convert_money("1", "USD", "MYR");
      $ci =& get_instance();
      $stmt = $ci->db->conn_id->prepare('SELECT value FROM site_data WHERE what = :usd_to_myr');
      $stmt->execute(array(':usd_to_myr'=>'usd_to_myr'));
      $row = $stmt->fetch(PDO::FETCH_ASSOC)['value'];
      if (strpos($row, '+') !== false) {#kena darab.
         $row = str_replace('+', '', $row);
         return $amount * $row;
      }elseif(strpos($row, '/') !== false){#kena bahagi.
         $row = str_replace('/', '', $row);
         return $amount / $row;
      }
   }
}

//###########

if ( ! function_exists('current_user_id')) {
   function current_user_id(){
      $ci =& get_instance();
      $ci->load->library('encrypt');
      $param = $ci->encrypt->decode($ci->session->userdata('id'));//base64_decode($ci->session->userdata('id'));
      $id_login = $ci->session->userdata('id_login');
      $semi_id = str_replace($id_login, '', $param);
      $id = $semi_id / 79346;
      return $id;
   }
}

//###########

if ( ! function_exists('user_info')) {
   function user_info($id){
      $ci =& get_instance();
      $stmt = $ci->db->conn_id->prepare('SELECT * FROM users WHERE id_user = :id_user');
      $stmt->execute(array(':id_user'=>$id));
      $my_array = $stmt->fetch(PDO::FETCH_ASSOC);

      //overide reputation..
      $query = $ci->db->conn_id->prepare(
         "SELECT
            COALESCE(( SELECT COUNT(id) * 15 FROM reputation_manager WHERE author_id = :user_id AND type = 'view' AND MONTH(masa) = MONTH(CURRENT_DATE()) AND YEAR(masa) = YEAR(CURRENT_DATE()) ), 0)
            +
            COALESCE(( SELECT COUNT(post_tbl.id) * 1000 FROM post_tbl WHERE  post_tbl.author_id = :user_id AND status = 'publish' AND MONTH(post_tbl.masa) = MONTH(CURRENT_DATE()) AND YEAR(post_tbl.masa) = YEAR(CURRENT_DATE()) ), 0)"
      );
      $query->execute(
         array(
            ':user_id' => $id
         )
      );
      $my_array['reputation'] = $query->fetch(PDO::FETCH_COLUMN);

      // if(!$my_array['reputation']) $my_array['reputation'] = 0;

      return $my_array;

   }
}

//###########
if ( ! function_exists('site_income')) {
  function site_income(){

     $ci =& get_instance();

     $query = $ci->db->conn_id->prepare(
     "SELECT
          #INCOME
          COALESCE(( SELECT SUM(value) FROM keluar_masuk WHERE income_cost = 'income' AND currency = 'MYR' AND MONTH(added_time) = MONTH(CURRENT_DATE()) AND YEAR(added_time) = YEAR(CURRENT_DATE()) ), 0)
          +
          (
             COALESCE(( SELECT SUM(value) FROM keluar_masuk WHERE income_cost = 'income' AND currency = 'USD'  AND MONTH(added_time) = MONTH(CURRENT_DATE()) AND YEAR(added_time) = YEAR(CURRENT_DATE()) ), 0)
             *
             COALESCE(( SELECT value FROM site_data WHERE what = 'usd_to_myr' ), 0)
          )
          as income,

          #COST
           COALESCE((SELECT SUM(value) FROM keluar_masuk WHERE income_cost = 'cost' AND currency = 'MYR'  AND MONTH(added_time) = MONTH(CURRENT_DATE()) AND YEAR(added_time) = YEAR(CURRENT_DATE()) ), 0)
          +
          (
             COALESCE(( SELECT SUM(value) FROM keluar_masuk WHERE income_cost = 'cost' AND currency = 'USD'  AND MONTH(added_time) = MONTH(CURRENT_DATE()) AND YEAR(added_time) = YEAR(CURRENT_DATE()) ), 0)
             *
             COALESCE(( SELECT value FROM site_data WHERE what = 'usd_to_myr' ), 0)
          )
          as cost"
   );
   $query->execute();

   $result = $query->fetch(PDO::FETCH_ASSOC);
   // var_dump($result);
   if($result){

      if(!array_key_exists('income', $result)) $result['income'] = 0;
      if(!array_key_exists('cost', $result)) $result['cost'] = 0;

   } else {

     $result = array(
         'income' => 0,
         'cost' => 0
     );

   }
      if($result['income'] > $result['cost']){
         return $result['income'] - $result['cost'];
      }
      return 0;

   }
}

//###########

if ( ! function_exists('all_reputation')) {
  function all_reputation(){
    $ci =& get_instance();
    $all_rep_query = $ci->db->conn_id->prepare(
      "SELECT
      COALESCE(( SELECT COUNT(id) * 15 FROM reputation_manager WHERE type = 'view' AND MONTH(masa) = MONTH(CURRENT_DATE()) AND YEAR(masa) = YEAR(CURRENT_DATE()) ), 0)
      +
      COALESCE(( SELECT COUNT(post_tbl.id) * 1000 FROM post_tbl WHERE MONTH(post_tbl.masa) = MONTH(CURRENT_DATE()) AND YEAR(post_tbl.masa) = YEAR(CURRENT_DATE()) ), 0)"
    );
    $all_rep_query->execute();
    $all_reputation = $all_rep_query->fetch(PDO::FETCH_COLUMN);
    // if($all_reputation){
      return $all_reputation;
   // } else {
   //    return 0;
   // }
  }
}

//###########

if ( ! function_exists('author_income')) {
   function author_income($user_id) {

      $ci =& get_instance();

      $current_reputation = user_info($user_id)['reputation'];
      $current_role = user_info($user_id)['user_role'];

      $final_site_income = site_income();


      if($final_site_income < 1 || $current_reputation < 1 || all_reputation() < 1){
         $user_income = 0;
      } else {
         $user_income = $final_site_income * ($current_reputation / all_reputation());
      }

      // var_dump($final_site_income);
      // var_dump($current_reputation);
      // var_dump(all_reputation());

      $user_istimewa = array('special admin', 'admin', 'staff', 'special staff', 'special author');

      if(in_array($current_role, $user_istimewa)){
         $taraf_user = 'istimewa';
      } else {
         $taraf_user = 'biasa';
      }

      if($current_reputation >= 60000 || in_array($current_role, $user_istimewa)){//15% fake bonus @ tolak 0%
         $real_percent_tolak = 0;
         $fake_bonus_percent = 15;
         $reputation_level = 'high';
         $max_rp = $current_reputation;
      } else {
         if($current_reputation >= 34200){//10% fake bonus @ tolak 5%
            $real_percent_tolak = 5;
            $fake_bonus_percent = 10;
            $reputation_level = 'medium';
            $max_rp = 60000;
         } else {
            if($current_reputation >= 16800){//5% fake bonus @ tolak 10
               $real_percent_tolak = 10;
               $fake_bonus_percent = 5;
               $reputation_level = 'low';
               $max_rp = 34200;
            } else {//0% fake bonus @ tolak 15
               $real_percent_tolak = 15;
               $fake_bonus_percent = 0;
               $reputation_level = 'lowest';
               $max_rp = 16800;
            }
         }
      }
      if($current_reputation > 0){
         $bar_percent = ($current_reputation / $max_rp) * 100;
      } else {
         $bar_percent =0;
      }


      $unfixed_deduction = $real_percent_tolak/100 * $user_income;
      $fake_income_total = number_format((float)$user_income - $unfixed_deduction, 2, '.', '');
      $total_percent = 100+$fake_bonus_percent;
      $fake_income_amount = number_format((float)100/$total_percent * $fake_income_total, 2, '.', '');
      $fake_bonus_amount = number_format((float)$fake_bonus_percent/$total_percent * $fake_income_total, 2, '.', '');

      if($fake_income_total > 0){
         $result_fake_income_amount = $fake_income_amount;
         $result_fake_bonus_amount = $fake_bonus_amount;
         $result_fake_income_total = $fake_income_total;
      } else {
         $result_fake_income_amount = 0;
         $result_fake_bonus_amount = 0;
         $result_fake_income_total = 0;
      }

      return array(
         'bonus_percent' => $fake_bonus_percent,
         'total_income' => $result_fake_income_total,
         'income_amount' => $result_fake_income_amount,
         'bonus_amount' => $result_fake_bonus_amount,
         'reputation_level' => $reputation_level,
         'bar_percent' => $bar_percent,
         'current_reputation' => $current_reputation,
         'taraf_user' => $taraf_user//,
         //'last_added_time' => $last_added_time
      );

   }
}
