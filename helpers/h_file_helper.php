<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('img_uploaded')) {
  function img_uploaded($from_tbl, $from_id){

    $ci =& get_instance();

    #SETUP UPLOADED IMAGE.
    $filename = $ci->db->conn_id->prepare(
        "SELECT filename FROM file_manager WHERE (from_tbl = :from_tbl AND from_id = :from_id)"
     );
     $filename->execute(array(
        ':from_tbl' => $from_tbl,
        ':from_id' => $from_id
     ));
     $img_upload = array();
     if($filename){
        $img_upload = $filename->fetchAll(PDO::FETCH_COLUMN, 0); #image yg upload di post ini.
     }

     return $img_upload;

  }
}


if ( ! function_exists('img_used')) {
    function img_used($data){


        if($data['content']){

        #required $data['content']
        $ci =& get_instance();
        $ci->load->helper('h_link_helper');

         #SETUP USED IMAGES.
         $img_used = array(); #image yang ada dalam post.

         $meta_img = array($data['first_img'], $data['google_thumb'], $data['viral_thumb']);
         $meta_img = array_filter($meta_img);

         foreach ($meta_img as $img_url) {
            if($img_url){
               array_push($img_used, $img_url);
            }
         }


         $dom = new domDocument;
         libxml_use_internal_errors(true);
         $dom->loadHTML($data['content']);
         libxml_clear_errors();
         $dom->preserveWhiteSpace = false;
         $images = $dom->getElementsByTagName('img');

         foreach($images as $img){

            $url = $img->getAttribute('src');

            if(allowed_domain($url)){
               $filename = pathinfo($url)['basename'];#ex my-image-path.png
               if(!in_array($filename, $img_used)){
                  array_push($img_used, trim($filename));
               }
            }

         }

         return array_filter($img_used);

        } else {
            return array();
        }

    }
  }


  if ( ! function_exists('img_org')) {
    function img_org($data){

        #required $data['img_used'], $data['img_uploaded]
        $img_org = array();
        foreach ($data['img_used'] as $filename) {
            if(!in_array($filename, $data['img_uploaded'])){
                array_push($img_org, $filename);
            }
        }

        return array_filter($img_org);

    }
}


if ( ! function_exists('img_unused')) {
    function img_unused($data){

        #required $data['img_uploaded'], $data['img_used']
        $img_unused = array();
        foreach ($data['img_uploaded'] as $filename) {
            if(!in_array($filename, $data['img_used'])){
                array_push($img_unused, $filename);
            }
        }

        return array_filter($img_unused);

    }
}
