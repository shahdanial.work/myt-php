<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('query_update')) {
  function query_update($data){
     $final = array();
     $first = array();
     $prepare = '';
     $execute = array();
     $selected = $data['selected_key'];
     foreach ($data as $key => $value) {
      if(in_array($key, $selected)){
          array_push($first,$key);
          $execute[':'.$key] = $value;
      }
     }

     foreach ($first as $key => $value) {
       if($key < sizeof($first)-1){
          $comma = ', ';
       } else {
          $comma = '';
       }
       $damn = $value.' = :'.$value.$comma;
       $prepare = $prepare . $damn;
     }

     $final['prepare'] = $prepare;
     $final['execute'] = $execute;

     return $final;

  }
}

if ( ! function_exists('query_insert')) {
  function query_insert($data){

    $final = array();

    $execute = array();
    $selected = $data['selected'];
    $first = array();
    $execute = array();

    foreach ($data as $key => $value) {
      if(in_array($key, $selected)){
        array_push($first, $key);
        $execute[':'.$key] = $value;
      }
    }

    $set_prepare = '';
    $value_prepare = '';
    foreach($first as $key => $value){
      if($key < sizeof($first)-1){
        $comma = ', ';
      } else {
        $comma = '';
      }
      $set_prepare = $set_prepare.$value.$comma;
      $value_prepare = $value_prepare.':'.$value.$comma;
    }

    $final['prepare'] = '('.$set_prepare.') VALUES ('.$value_prepare.')';
    $final['execute'] = $execute;


    return $final;

  }
}


if( !function_exists('check_table')){
  function check_table($value, $table_name, $row){

    $ci =& get_instance();
    $query = $ci->db->conn_id->prepare(
      'SELECT '.$row.' FROM '.$table_name.' WHERE LOWER('.$row.') = :value'
    );
    $query->execute(array(
        ':value'=>trim(strtolower($value))
    ));
    $result = $query->fetchAll(PDO::FETCH_COLUMN);
    if($result){
      return $result;
    } else {
      return false;
    }

  }
}





/* EXAMPLE:
$where = array(
  'tag_path )( =' => $path,
  'from_tbl )( =' => $data['from_tbl'],
  'from_id )( =' => $data['from_id']
);
if( ! query_select('tag_path', 'tag_manager', $where) ){
 #do something
}

$get_somthing = query_select('tag_path', 'tag_manager', $where); #get tag_path from tag_manager. (ALWAYS RETRUN ARRAY if TRUE);
*/


function query_select($select, $from_tbl, $where){ #string, #string, #array..

    $ci =& get_instance();
    $prepare = 'SELECT '. $select .' FROM '. $from_tbl .' WHERE ';
    $execute = array();

    $i = 0;
    foreach ($where as $keyy => $value) {
      $format_key = explode(')(', $keyy);
      $key = trim($format_key[0]);
      $condition = trim($format_key[1]);
      $i = $i + 1;
      if($i < sizeof($where)){
        $and = ' AND ';
      } else {
        $and = ' ';
      }

      $execute[':'.$key] = strtolower($value);
      $prepare = $prepare . 'LOWER(' . $key . ') ' .$condition. ' :'. $key . $and;
    }
    $query = $ci->db->conn_id->prepare($prepare);
    $query->execute($execute);
    $query_result = $query->fetchAll(PDO::FETCH_ASSOC);

    if(!$query_result || sizeof(array_filter($query_result)) < 1){
      return false;
    } else {
      return $query_result;
    }
}
