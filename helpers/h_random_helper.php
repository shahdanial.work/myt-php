<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

#Undycrypted
if ( ! function_exists('tapuk')) {
  function tapuk($string){
    $str = $string;//preg_replace('/\s+/', '', $string);
    $a= wordwrap($str, 4, 'e' , true );
    $b = wordwrap($a, '3', 'a', true);
    $c = wordwrap($b, '2', 'c', true);
    $d = wordwrap($c, '1', 'l', true);
    $e = wordwrap($d, '2', '_', true);
    return md5($e);
  }
}

#from Shah Danial to shahdanil01
if ( ! function_exists('random_generate')) {
  function random_generate($string){
    $pattern = " ";
    $firstPart = strstr(strtolower($string), $pattern, true);
    $secondPart = substr(strstr(strtolower($string), $pattern, false), 0,3);
    $nrRand = rand(0, 100);
    $random = trim($firstPart).trim($secondPart).trim($nrRand);
    return strtolower( preg_replace('/\s+/', '', $random) );
  }
}

if ( ! function_exists('random_password')) {
  function random_password() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
      $n = rand(0, $alphaLength);
      $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
  }
}
