<div class='wysiwyg-controls'>
<a unselectable='on'  class='a_image editor_control' data-show='a_a_image' href='#' data-tooltip='Insert Images' id='image_control'><i aria-hidden='true' class='fa fa-image'></i></a>
<a unselectable='on'  class='a_video editor_control' data-show='a_a_video' href='#' data-tooltip='Embed Youtube Video' id='video_control'><i aria-hidden='true' class='fa fa-video-camera'></i></a>
<a unselectable='on'  class='a_link editor_control' data-show='a_a_link' href='#' data-tooltip='Insert Text Link' id='link_control'><i aria-hidden='true' class='fa fa-link'></i></a>
<a unselectable="on" class="a_heading editor_control" data-show="a_a_heading" href="#" data-tooltip="Masukkan heading" id="heading_control"><i aria-hidden="true" class="fa fa-bold"></i></a>
<a unselectable='on'  class='a_related editor_control' data-show='a_a_related' href='#' data-tooltip='Insert Related Article of malayatimes.com' id='related_control'><i aria-hidden='true' class='fa fa-newspaper-o'></i></a>
<a unselectable='on'  class='a_button editor_control' data-show='a_a_button' href='#' data-tooltip='Insert Text Link Button' id='button_control'><i aria-hidden='true' class='fa fa-chevron-circle-right'></i></a>
<a unselectable='on'  class='a_table editor_control' data-show='a_a_table' href='#' data-tooltip='Insert simple Table' id='table_control'><i aria-hidden='true' class='fa fa-table'></i></a>
<a unselectable='on'  class='a_message editor_control' data-show='a_a_message' href='#' data-tooltip='Insert Alert Messages' id='message_control'><i aria-hidden='true' class='fa fa-info-circle'></i></a>
<a unselectable='on'  class='a_quote editor_control' data-show='a_a_qoute' href='#' data-tooltip='Insert Blocqoute (box kata-kata seseorang)' id='qoute_control'><i aria-hidden='true' class='fa fa-quote-left'></i></a>

<!-- menu sidebar -->

             <a unselectable='on' href="#" class="sidebar_control" id="close-sidebar" style="display: none;"><img src="<?php echo site_url('assets/image/close.png') ?>"></a>
             <a unselectable='on' href="#" class="sidebar_control" id="open-sidebar" style="display: inline;"><img src="<?php echo site_url('assets/image/menu.png') ?>"></a>

<!-- end menu -->

</div>
