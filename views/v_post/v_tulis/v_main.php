<!DOCTYPE html>
<html><head><?php $this->load->view('v_post/v_tulis/v_head', $table); ?>
</head><body id="main-blog" class="tulis">
  <div class="hfeed section" id="blog-content"><div class="widget Blog" data-version="1" id="Blog1">
    <div class="wysiwyg-editor">

      <?php $this->load->view('v_post/v_tulis/v_control', $table); ?>
      <?php /* OPEN FORM */
        $attributes = array('class' => 'main-form');
      echo form_open_multipart('submit', $attributes);
      ?>

      <table class="left-right">
        <tbody>
          <tr>
            <td class="left">
              <?php $this->load->view('v_post/v_tulis/v_left', $table); ?>
            </td>
            <td class="right" unselectable="on">
              <?php $this->load->view('v_post/v_tulis/v_right', $table); ?>
            </td>
          </tr>
        </tbody>
      </table>
      <?php /* CLOSE FORM */ echo form_close();  ?>
      <?php /* OPEN FORM */
        $attributes = array('class' => 'draft-form', 'id' => 'draft-form');
      echo form_open_multipart('save-draft', $attributes);
      ?>
        <div id="draft_hidden" class="content_wtf">
          <?php
          #interest.
          if(sizeof($table['keyword']['gender']) > 0){
             if(in_array('male', $table['keyword']['gender'])){
                echo '<input class="hidden_gender" type="hidden" name="gender[]" value="male">';
             }
             if(in_array('female', $table['keyword']['gender'])){
                echo '<input class="hidden_gender" type="hidden" name="gender[]" value="female">';
             }
          } else {
             echo '<input class="hidden_gender" type="hidden" name="gender[]" value="male">';
             echo '<input class="hidden_gender" type="hidden" name="gender[]" value="female">';
          }

          if($table['keyword']['interest']){
             foreach ($table['keyword']['interest'] as $tt) {
                echo '<input value="'.$tt.'" name="interest[]" id="draft_interest_value_'.build_js_id($tt).'_id" readonly="true" type="hidden">';
             }
          }
          #tag
          if($table['tag']){
            foreach ($table['tag'] as $tag) {
                  $tag_id = 'draft_tag_value_'. str_replace(' ', '_', $tag) .'_id';
                  //[&\/\\#,+()$~%.!\@\^\_\=\-\[\]\"\;\`\'\|:*?<>{}]
                  $symbol = array(
                     '[','&','/','#','+','(',')','$','~','%','.','!','@','^','_','=','-',
                     '"',
                     ';',
                     ',',
                     '`',
                     '\'',
                     '|',
                     ':',
                     '*','?','<','>','{','}',']'
                  );
                  $tag_class = str_replace($symbol, '', $tag);
                  $tag_class = str_replace(' ', '_', $tag_class);
                  echo '<input class="'. $tag_class.'" value="'.$tag.'" name="tag[]" id="'. $tag_id .'" readonly="true" type="hidden">';
            }
         }
          #category.
          $cat_name = '';
          if(!empty($table['category'])){
            if(allowed_domain($table['category'])){
              $cat_name = $table['category'];
            }
          }
          echo '<input id="category_draft" name="category" type="hidden" value="'. $cat_name .'" />';
          #title.
          echo '<input id="title_draft" name="title" type="hidden" value="'.$table['title'].'" />';
          #content.
          echo '<textarea id="content_draft" name="content">'.$table['content'].'</textarea>';
          #meta_title.
          echo '<input id="meta_title_draft" name="meta_title" type="hidden" value="'.$table['meta_title'].'" />';
          #meta_description.
          echo '<input id="meta_description_draft" name="meta_description" type="hidden" value="'.$table['meta_description'].'" />';
          #viral_title
          echo '<input name="viral_title" id="viral_title_draft"  type="text" value="'.$table['viral_title'].'">';
          #viral_meta_description.
          echo '<input id="viral_description_draft" name="viral_meta_description" type="hidden" value="'.$table['viral_description'].'" />';
          #first_img
          $first_img = '';
          if(strlen($table['first_img'])>0){
            $first_img = site_url('picker/'). $table['first_img'];
          }
          echo '<input id="first_img_draft" name="post-thumbnails-path" type="hidden" value="'.$first_img.'" />';
          #google_thumb
          $google_thumb = '';
          if(strlen($table['google_thumb'])>0){
            $google_thumb = site_url('picker/').$table['google_thumb'];
          }
          echo '<input id="seo-thumbnails-path_draft" name="seo-thumbnails-path" type="hidden" value="'.$google_thumb.'"/>';
          #viral_thumb
          $viral_thumb = '';
          if(strlen($table['viral_thumb'])>0){
            $viral_thumb = site_url('picker/').$table['viral_thumb'];
          }
          echo '<input id="viral-thumbnails-path_draft" name="viral-thumbnails-path" type="hidden" value="'.$viral_thumb.'"/>';

           echo '<input type="hidden" name="id" value="'.$table['id'].'"/>';
           echo '<input type="hidden" name="status" value="'.$_GET['status'].'"/>';
          #button? lol.
          echo '<button type="sumbit">submit</button>';
           ?>
        </div>


      <?php /* CLOSE FORM */ echo form_close();  ?>
      <?php echo '</div>'; #end wysiwyg-editor ?>
      <?php $this->load->view('v_post/v_tulis/v_preview', $table); ?>

     <?php $this->load->view('v_element/v_sidebar'); ?>


      <div class="modal" unselectable="on" <?php
      if($table['error_flash']){ echo ' style="display:block;"' ;
      }
      ?>><div class="popup">
        <!--<div class="popup-header"><a class="popup-close">&#215;</a><h4></h4></div>-->
        <?php $this->load->view('v_post/v_tulis/v_popup/v_image', $table); ?>
        <?php $this->load->view('v_post/v_tulis/v_popup/v_video', $table); ?>
        <?php $this->load->view('v_post/v_tulis/v_popup/v_link', $table); ?>
        <?php $this->load->view('v_post/v_tulis/v_popup/v_heading', $table); ?>
        <?php $this->load->view('v_post/v_tulis/v_popup/v_error_msg', $table); ?>
        <?php $this->load->view('v_post/v_tulis/v_popup/v_related', $table); ?>
        <?php $this->load->view('v_post/v_tulis/v_popup/v_button', $table); ?>
        <?php $this->load->view('v_post/v_tulis/v_popup/v_table', $table); ?>
        <?php $this->load->view('v_post/v_tulis/v_popup/v_message', $table); ?>
        <?php $this->load->view('v_post/v_tulis/v_popup/v_qoute', $table); ?>
        <?php $this->load->view('v_post/v_tulis/v_popup/v_login', $table); ?>
      </div>
    </div>

  </div></div>

  <div id="wait-full"><?php $this->load->view('v_post/v_tulis/v_popup/v_loader', $table); ?></div>

<div id="meteor">
   <?php
   $my_js = site_url('js/meteor.js');
   if(current_user_id() === 81 || current_user_id() === 1) $my_js = site_url('js/meteor_1.js');
    ?>
<script type="text/javascript" src="<?php echo $my_js ?>"></script>
</div>

</body></html>
