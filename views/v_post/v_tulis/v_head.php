<meta content='text/html; charset=UTF-8' http-equiv='Content-Type'/><meta content='width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no' name='viewport'/>
<title><?php
if($table['status'] == 'draft'){
  echo 'Write general article';
} else {
  if($table['is_admin']){
    echo 'Review Submitted Article';
  } else {
    echo 'Reedit Submitted Article';
  }
}
?></title>

<!--[if lte IE 8 ]><script> (function() { var html5element = ("main").split(','); for (var i = 0; i < html5element.length; i++) { document.createElement(html5element[i]); } try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {} })(); </script> <![endif]--><!--[if IE]><script type="text/javascript" src="https://www.blogger.com/static/v1/jsbin/579771828-ieretrofit.js"></script><![endif]--><!--[if IE]> <script> (function() { var html5 = ("abbr,article,aside,audio,canvas,datalist,details," + "figure,footer,header,hgroup,mark,menu,meter,nav,output," + "progress,section,time,video").split(','); for (var i = 0; i < html5.length; i++) { document.createElement(html5[i]); } try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {} })(); </script> <![endif]-->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/tulis.css" ?>">

<link href='<?php echo site_url('css/font-awesome.min.css'); ?>' rel='stylesheet'/>
