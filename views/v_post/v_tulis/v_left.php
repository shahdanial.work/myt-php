<?php

#interest.
if($table['keyword']['interest']){
   foreach ($table['keyword']['interest'] as $tt) {
      echo '<input value="'.$tt.'" name="interest[]" id="interest_value_'.build_js_id($tt).'_id" readonly="true" type="hidden">';
   }
}

#tag
if($table['tag']){
   foreach ($table['tag'] as $tag) {
         $tag_id = str_replace(' ', '_', $tag);
         $symbol = array(
            '[','&','/','#','+','(',')','$','~','%','.','!','@','^','_','=','-',
            '"',
            ';',
            ',',
            '`',
            '\'',
            '|',
            ':',
            '*','?','<','>','{','}',']'
         );
         $tag_class = str_replace($symbol, '', $tag);
         $tag_class = str_replace(' ', '_', $tag_class);
         echo '<input class="'. $tag_class .'" value="'.$tag.'" name="tag[]" id="tag_value_'. $tag_id .'_id" readonly="true" type="hidden">';
   }
}

?>
<input maxlength="100" name="title" value="<?php if(isset($table['title'])) echo $table['title']; ?>" autocomplete="off"  id="post-title" placeholder="Type your title" data-tooltip="contoh yang mesra carian google (apa yang orang selalu taip dengan tepat untuk cari di Google): Gambar perkahwinan Yusry KRU dan Lisa Surihani / Perlawanan Malaysia vs Vietnam Jun 2016 / Keputusan perlawanan Malaysia vs Singapura Julai 2016 / Video perlawanan Chong Wei vs Lin Dan Terbuka London 2016 / dan banyak lagi. Ianya bergantung kepada artikel anda. Ada jenis artikel yang sangat dicari² oleh pengguna Google Search Engine. Manakala ada juga yang tidak tidak dicari-cari di Search Engine; hanya memerlukan kita share hebahkan, jemput pembaca baca dan viralkan ianya (teknik Viral); dalam hal ini Tajuk dan gambar PERTAMA memainkan peranan"/>
   <?php
   // if(isset($table['is_admin'])){
      // echo '<!--';
      // print_r($table);
      // echo '-->';
   // }
   ?>
   <div class="wysiwyg-content" id="post-editor"><?php
      echo format_content_as_editor($table['content']);
   ?></div>
   <div style="display:none !important"><textarea name="content" id="content_wtf"><?php
   if(isset($table['content'])) echo $table['content'];
   ?></textarea></div>


   <?php $query = $this->db->conn_id->prepare("SELECT filename FROM file_manager WHERE from_id = :from_id AND from_tbl = :from_tbl");
   $query->execute( array(
      ':from_id' => $table['id'],
      ':from_tbl' => 'post_tbl'
   ) );
   $result = $query->fetchAll(PDO::FETCH_COLUMN);

   #open tag section...
   echo '<section class="the-box tethumbnail" id="post-thumbnail" unselectable="on">
   <h4>Post Thumbnail:</h4>
   <div class="thebox_isi">
   <h3>Select main page Thumbnail:</h3>
   <div id="post-thumnails" class="v_s_thumb">';

   if($result){
      foreach ($result as $path) {
         if($path == $table['first_img']){
            $selected = ' selected';
         } else {
            $selected = '';
         }
         $img = '<div class="post_pics'.$selected.'"><img alt="try gambar with caption" src="' . site_url('picker/') . $path . '"></div>';
         echo $img;
      }
   }

   #Close tag section..
   echo '</div>';
   $first_img_link = '';
   if($table['first_img'] && !empty($table['first_img'])){
    $first_img_link = site_url('picker/' ) . $table['first_img'];
   }
   echo '<input  id="post-thumbnails-path" name="post-thumbnails-path" type="hidden" value="'.$first_img_link.'" />';
   echo '</div>
   <div class="thebox_footer">
      <div class="pick-thumb-button" id="pick-post-thumb" data-tooltip="klik untuk mencari/upload thumbnail sesuai"><i class="fa fa-wrench" aria-hidden="true"></i> add thumbnail</div>
    </div>
   </section>';
   ?>

   <section class="the-box" id="seo" unselectable="on">
      <h4>Google Section:</h4>
      <div class="thebox_isi">
         <h3>SEO Title:</h3>
         <div class="wrap-box"><input name="meta_title" id="seo-title" placeholder="Tajuk di Search Engine" data-tooltip="Apakah keyword carian yang tepat mungkin paling dicari di Google?" type="text" value="<?php echo $table['meta_title'] ?>" maxlength="60"></div>
         <div class="input-meta"><b id="title-count">0</b> of <span class="red">max 60</span>.</div>
         <h3>SEO Description:</h3>
         <textarea data-tooltip="Sebaiknya mempunyai 2x ulangan keyword carian yang anda tetapkan pada <span>SEO Title</span>" name="meta_description" id="meta-description" placeholder="Description di Search Engine.. Jika tidak mahir, lebih baik abaikan ruangan ini.." maxlength="150"><?php echo $table['meta_description'] ?></textarea>
         <div class="input-meta"><b id="description-count">0</b> of <span class="red">max 150</span>.</div>
         <h3 style="display:none"  class="tethumbnail">Select SEO Thumbnail:</h3>
         <?php

         if($result){
            echo '<div id="seo-thumbnails" class="tethumbnail v_s_thumb">';
            foreach ($result as  $path) {
               if($path == $table['google_thumb']){
                  $selected = ' selected';
               } else {
                  $selected = '';
               }
               $img = '<div class="seo_pics'.$selected.'"><img alt="try gambar with caption" src="' . site_url('picker/') . $path . '"></div>';
               echo $img;
            }
         }  else {
            echo '<div id="seo-thumbnails" style="display:none" class="tethumbnail v_s_thumb">';
         }
         echo '</div>';

         $google_thumb_link = '';
         if($table['google_thumb'] && !empty($table['google_thumb'])){
          $google_thumb_link = site_url('picker/' ) . $table['google_thumb'];
         }
         echo '<input id="seo-thumbnails-path" name="seo-thumbnails-path" type="hidden" value="'.$google_thumb_link.'" />';
         ?>

      </div>
      <div class="thebox_footer">
         <div class="pick-thumb-button" id="pick-post-thumb" data-tooltip="klik untuk mencari/upload thumbnail sesuai"><i class="fa fa-wrench" aria-hidden="true"></i> add thumbnail</div>
       </div>
   </section>

   <textarea style="display:none" id="pass-thumbnail"></textarea>

   <section class="the-box" id="viral" unselectable="on">
      <h4>Facebook Section:</h4>
      <div class="thebox_isi">
         <h3>Viral Title:</h3>
         <div class="wrap-box"><input data-tooltip="apakah Title yang menarik minat orang untuk respon link yang di share di Facebook?" name="viral_title" id="viral-title" placeholder="Tajuk untuk netizen di Facebook" type="text" value="<?php echo $table['viral_title'] ?>" maxlength="60"></div>
         <div class="input-meta"><b id="viral-title-count">0</b> of <span class="red">max 60</span>.</div>
         <h3>Viral Description:</h3>
         <textarea data-tooltip="Masukkan juga <span>ringkasan text yang mampu menarik minat</span> user di Facebook memberi respon" name="viral_meta_description" id="viral-meta-description" placeholder="Ringasan content yang akan kelihatan untuk Netizen" maxlength="150"><?php echo $table['viral_description'] ?></textarea>
         <div class="input-meta"><b id="viral-description-count">0</b> of <span class="red">max 150</span>.</div>
         <h3 style="display:none"  class="tethumbnail">Select Viral  Thumbnail:</h3>
         <?php
         if($result){
            echo '<div id="viral-thumbnails" class="tethumbnail v_s_thumb">';
            foreach ($result as  $path) {
               if($path == $table['viral_thumb']){
                  $selected = ' selected';
               } else {
                  $selected = '';
               }
               $img = '<div class="viral_pics'.$selected.'"><img alt="try gambar with caption" src="' . site_url('picker/') . $path . '"></div>';
               echo $img;
            }
         }  else {
            echo '<div id="viral-thumbnails" style="display:none" class="tethumbnail v_s_thumb">';
         }
         echo '</div>';

         $viral_thumb_link = '';
         if($table['viral_thumb'] && !empty($table['viral_thumb'])){
           $viral_thumb_link = site_url('picker/') . $table['viral_thumb'];
         }
         ?>

         <input id="viral-thumbnails-path" name="viral-thumbnails-path" type="hidden" value="<?php echo $viral_thumb_link; ?>" />
      </div>
      <div class="thebox_footer">
         <div class="pick-thumb-button" id="pick-post-thumb" data-tooltip="klik untuk mencari/upload thumbnail sesuai"><i class="fa fa-wrench" aria-hidden="true"></i> add thumbnail</div>
       </div>
<?php
       echo '<input type="hidden" name="id" value="'.$table['id'].'"/>';
       echo '<input type="hidden" name="status" value="'.$table['status'].'"/>';
       ?>
 </section>
