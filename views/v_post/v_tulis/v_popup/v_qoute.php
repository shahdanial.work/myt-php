<section class="popup-content" id="a_a_qoute">

   <div class="popup-header">
      <h4>Insert Blockqoute</h4>
      <a class="popup-close">&#215;</a>
   </div>

   <div class="popup-body" id="a_a_quote">
      <div class="popup-tips"><button class="button" id="a_link_tips"><i aria-hidden="true" class="fa fa-info-circle"></i> tips</button></div><h3>Text: </h3><textarea class="insert-quote textarea field" placeholder="Masukkan kata-kata seseorang"></textarea>
      <div class="popup-footer">
         <button class="insert button" disabled="disabled">Insert</button>
      </div>
      <div class="shah_tips">
         <button class="button">close</button>
         <ol>
            <li>Khas untuk box kata-kata sesorang.</li>
            <li>Masukkan tanpa tanda simbol open Quote dan close Quote</li>
            <li><strong>Contoh Text:</strong> Saya sebagai Ketua Polis Negara, perlu merahsiakan perkara ini dari pihak-pihak tertentu untuk menjaga senseviti kaum di negara ini.</li>
         </ol>
      </div>
   </div>
</section>
