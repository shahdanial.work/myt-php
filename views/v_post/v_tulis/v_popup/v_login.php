<section id="login-again">

   <div class="popup-header">
      <h4>Please Login</h4>
   </div>

<div id="login-again-heading"><div id="login-again-message">Please login again..</div></div>

   <div class="login-again-form">

    <?php
//     echo '<span id="google_login" class="social_login google-login">
//         <span class="button_logo">
// <i aria-hidden="true" class="fa fa-google-plus"></i>
//         </span>
//         <span class="button_text">
//     GOOGLE LOGIN
//         </span>
//     </span>';

echo '<span class="login-with-google" id="google_login"><svg class="ico-google-login" viewBox="0 0 18 18" width="18" height="18" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><path d="M0 0h18v18H0z"></path><path fill="#EA4335" fill-rule="nonzero" d="M9 4.167c1.18 0 2.237.406 3.07 1.2l2.283-2.284C12.967 1.793 11.157 1 9 1a7.996 7.996 0 0 0-7.147 4.407l2.66 2.063c.63-1.897 2.4-3.303 4.487-3.303z"></path><path fill="#4285F4" fill-rule="nonzero" d="M16.66 9.183c0-.523-.05-1.03-.127-1.516H9v3.006h4.313a3.718 3.718 0 0 1-1.593 2.394l2.577 2c1.503-1.394 2.363-3.454 2.363-5.884z"></path><path fill="#FBBC05" fill-rule="nonzero" d="M4.51 10.53A4.86 4.86 0 0 1 4.257 9c0-.533.09-1.047.253-1.53L1.85 5.407A7.971 7.971 0 0 0 1 9a7.93 7.93 0 0 0 .853 3.593L4.51 10.53z"></path><path fill="#34A853" fill-rule="nonzero" d="M9 17c2.16 0 3.977-.71 5.297-1.937l-2.577-2c-.717.484-1.64.767-2.72.767-2.087 0-3.857-1.407-4.49-3.303L1.85 12.59A8.002 8.002 0 0 0 9 17z"></path><path d="M1 1h16v16H1z"></path></g></svg><span class="text">Sign in with Google</span></span>';
    ?>

    <?php #echo '<div class="or"></div>'; ?>
	<!--form login-->

   <?php #echo form_open_multipart(site_url('login'), 'id="login-again-form"'); ?>

      <?php
          // echo '<p>
          //     <input id="social-login-email" class="email_input" type="text" name="email" value="" placeholder="yourmail@gmail.com">
          // </p>
          //
          //
          // <p>
          //     <input class="password_input" type="password" name="password" value="" placeholder="Your password" autocomplete="off">
          // </p>
          //
          // <input id="from" type="hidden" name="redirect" value="http://media.malayatimes.com/check-login-api/">
          // <p>
          //      <input class="enter-button" type="submit" name="btnSubmit" value="ENTER">
          // </p>';
       ?>

      <?php #echo form_close(); ?>


    <!--form login-->
   </div>
</section>
