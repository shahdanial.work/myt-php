<section class="popup-content" id="a_a_message">

   <div class="popup-header">
      <h4>Message Box</h4>
      <a class="popup-close">&#215;</a>
   </div>

   <div class="popup-body">
      <div class="popup-tips"><button class="button" id="a_message_tips"><i aria-hidden="true" class="fa fa-info-circle"></i> tips</button></div>
      <h3>Text Mesej:</h3>
      <textarea class="insert-message textarea field" placeholder="Taip mesej (perhatian) anda"></textarea>
      <div class="message-box"></div>
      <div class="popup-footer"><button class="insert button" disabled="disabled">Insert</button></div>
      <div class="shah_tips"><button class="button">close</button>
         <ol>
            <li>Mesej box ini untuk adalah sebuah box bbagi sesebuah mesej text.</li>
            <li>Misalnya anda hendak mengambil perhatian pembaca</li>
            <li><b>Contoh text:</b> Sila pastikan anda memasukkan No Kad Pengenalan betul</li>
            <li><b>Contoh text lain:</b> Tarikh tutup telah tamat.</li>
         </ol>
      </div>
   </div>
</section>
