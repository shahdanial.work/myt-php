<section class="popup-content" id="a_a_image">

   <div class="image-header">
      <h4>Images manager</h4>
      <a class="popup-close">&#215;</a>
   </div>

   <div id="pilihan-option" class="image-select">
      <span id="pilih-cari-gambar" class="select-search selected" data-show="search-gambar" data-tooltip="cari gambar yang sudah diupload user lain">Search</span>
      <span id="pilih-upload" class="select-upload" data-show="upload-gambar" data-tooltip="upload gambar sendiri">Upload</span>
      <span id="pilih-masuk-gambar" class="select-url" data-show="masuk-gambar" data-tooltip="masukkan link gambar dari website lain">Insert link</span>
   </div>

   <div class="image-option" id="image-option">

      <div id="search-gambar" class="search-select">

         <div class="wtf" style="display:none"><div class="warning-search"><div class="search-warning"><i class="fa fa-warning"></i> kotak input tak boleh kosong</div></div></div>

         <div id="upload-gambar" style="display:none;" class="uploading"><span>SILA TUNGGU . . .</span><br/><div class="meloading"></div></div>

         <div id="cari-gambar" class="kotak">
            <input class="cari-gambar" type="text" placeholder="taip keyword carian gambar yang simple" />
         </div>

         <div id="cari-gambar-result" class="picca" style="display:none">
         </div>

      </div>

      <div id="masuk-gambar" class="picca" style="display:none">
         <br/>
         <i>Upload gambar melalui link gambar dari internet akan datang..</i>
         <br/>
      </div>

      <div id="upload-gambar" class="upload-select">


         <div style="display:none;" class="uploading"><span>SILA TUNGGU . . .</span><br/>
            <div class="meloading"></div>
         </div>

         <!-- UPLOAD -->
         <?php echo form_open_multipart('upload', 'id="ajaxupload"'); ?>
         <div class="form-gruops wadepak">

            <?php if (isset($error)) { echo '<span class="text-danger">'.$error.'</span>'; } ?>
            <div class="form-group aduhai">
               <input id="insert-file" name="usr_files[]" type="file" multiple="" />
               <label for="insert-file" class="label-insert-file">
                  <i class="fa fa-image"></i> <span id="jumlah-file">0</span> ready to upload
               </label>
            </div>

            <?php
            echo '<input class="static_input" type="hidden" name="id" value="'.$table['id'].'"/>';
            echo '<input class="static_input" type="hidden" name="status" value="'.$table['status'].'"/>';
           ?>

            <input type="submit" value="UPLOAD" class="upload-button button" disabled="disabled"/>

            <br/><div id="before-upload"></div>

         </div>
         <?php echo form_close(); ?>

      </div>

   </div>

   <div class="bucketine thumbnail_off"></div>
   <div class="bucketheaderwrap thumbnail_off">
      <div class="bucketheader">
         <span class="bukettitle">BUCKET</span>
         <span class="bucketdescription">select then insert</span>
      </div>
   </div>

   <div class="pics thumbnail_off">
      <?php
         $query = $this->db->conn_id->prepare(
            #"SELECT filename FROM file_manager WHERE from_id = :from_id AND from_tbl = :from_tbl"
            "SELECT file_tbl.caption, file_tbl.filename
            FROM file_tbl
            LEFT JOIN file_manager
            ON file_manager.filename = file_tbl.filename
            WHERE file_manager.from_tbl = 'post_tbl' AND file_manager.from_id = :from_id"
         );
         $query->execute( array(
            ':from_id' => $table['id']
         ) );
         $result = $query->fetchAll(PDO::FETCH_ASSOC);
         if($result){
            //var_dump($result);
            foreach ($result as $gmbr) {
                  $img = '<div class="border"><img alt="'.$gmbr['caption'].'" src="' . site_url('picker/') . $gmbr['filename'] . '"></div>';
                  echo $img;
            }
         }
      ?>
   </div>

   <div class="popup-footer thumbnail_off">
      <button class="insert button" id="img-insert-btn" disabled="disabled">INSERT</button>
   </div>

</section>
