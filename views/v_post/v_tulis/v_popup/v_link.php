<section class="popup-content" id="a_a_link">

   <div class="popup-header">
      <h4>Insert Link</h4>
      <a class="popup-close">&#215;</a>
   </div>

   <div class="popup-body" id="a_a_link">
      <div class="popup-tips"><button class="button" id="a_link_tips"><i aria-hidden="true" class="fa fa-info-circle"></i> tips</button></div><h3>Text: </h3><input class="insert-text input field" placeholder="Text for this link"/><h3>Link: </h3><input data-tooltip="sebaik anda memasukkan link, system akan memberikan suggestion text yang terbaik untuk ranking post anda" class="insert-link input field" placeholder="insert URL/link" type="url">
      <div class="popup-footer">
         <button class="insert button" disabled="disabled">Insert</button>
      </div>
      <div class="shah_tips">
         <button class="button">close</button>
         <ol>
            <li>Tetapan <b>Text</b> yang baik, adalah berdasarkan Tajuk pada link page tersebut. Sekurangnya <b>Text</b> yang anda tetapkan tersebut, keywordnya juga terdapat pada Tajuk pada link page yang anda masukkan</li>
            <li>Misalnya <a href="http://thesisforblogspot.blogspot.my/2015/06/thesis-blogger-template-v3.html" target="_blank" title="click untuk check title page">link ini</a> yang anda masukkan. Tajuk page di <b>Link</b> tersebut adalah <u>Thesis Blogger Template V3</u>, so anda boleh menetapkan <b>Text</b> sebagai <i>Thesis</i> atau <i>Blogger</i> atau <i>Template</i> atau <i>Thesis Blogger</i> atau <i>Thesis Blogger Template</i> atau <i>Thesis Template</i> (tidak kisah, asalkan <b>Text</b> yang anda tetapkan ada terdapat pada Tajuk <b>Link</b> page).</li>
            <li>Sebaiknya linkkan <b>Text</b> ke mana-mana label, artikel atau page Malaya Times sendiri. Caranya <b><a class="keywordada" href="https://www.google.com/search?q=GANTI+DENGAN+KEYWORD+ANDA+site:malayatimes.com" target="_blank" title="Lepas klik tukar keyword kepada keyword Text anda">klik sini</a></b> untuk lihat hasil carian keyword <span class="dakeyword field"></span>tersebut dari Malaya Times di Google.</li>
            <li>Jika tiada Tajuk page Malaya Times yang sesuai dengan <b>Text</b> anda, maka Search Google keyword <b>Text</b>
               <span class="adakeyword field"></span>yang anda nak linkkan, dan pilih yang paling top pada result dengan keyword tersebut, linkkan kesitu. Caranya <b><a class="keywordada" href="https://www.google.com/search?q=GANTI+DENGAN+KEYWORD+ANDA" target="_blank" title="Lepas klik tukar keyword kepada keyword Text anda">Klik sini</a></b> untuk lihat hasil carian keyword <span class="adakeyword field"></span>tersebut dari semua web di Google.
            </li>
         </ol>
      </div>
   </div>
</section>
