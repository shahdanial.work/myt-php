<?php
if($table['error_flash']){
   $add_class = 'popup-content popup_content_show';
} else {
   $add_class = 'popup-content';
}
echo '<section class="' . $add_class .'" id="a_a_publish">';
?>
   <div class="popup-header">
      <h4><i aria-hidden="true" class="fa fa-warning"></i> Kriteria wajib</h4>
      <a class="popup-close">×</a>
   </div>
   <div class="popup-body">
      <div id="error-msg">
         <?php
            if($table['error_flash']){
               echo '<ol>';
               foreach ($table['error_flash'] as $key => $error_msgs) {
                  echo $error_msgs;
               }
               echo '</ol>';
            }
            echo '</div>';
            ?>
         <div class="popup-footer"><button class="button close_all_popup">Okay, faham</button></div>
      </div>
   </section>
