<div class="kolum-kiri">
  <?php #title
  if($this->session->flashdata('from_tbl')){
    $prom_tbl = str_replace('_tbl','',$this->session->flashdata('from_tbl'));
    } else {
    $prom_tbl = 'pending';
    }

  if(!$title){
    $title = 'Tiada tajuk..';
  }
  echo '<a class="post-title basetooltip" title="edit this article" href="'.site_url('post?id=' . $id .'&status='.$prom_tbl) .'">'.$title.'</a>';
  ?>
  <?php #category.
  $ct = $this->db->conn_id->prepare("SELECT domain FROM category_manager WHERE from_tbl = :from_tbl AND from_id = :from_id");
  $ct->execute(array(
    ':from_tbl' => 'post_tbl',
    ':from_id' => $id
  ));
  $cat_domain = $ct->fetch(PDO::FETCH_COLUMN);
  if($cat_domain){
    $cat=$this->db->conn_id->prepare("SELECT title FROM category_tbl WHERE domain = :domain");
    $cat->execute(array(
      ':domain'=>$cat_domain
    ));
    $category_title = $cat->fetch(PDO::FETCH_COLUMN);
    if($category_title){
      echo '<a class="post-category basetooltip" title="visit category page"  href="http://'.$cat_domain.'" target="_blank">'.$category_title.'</a>,';
    }
  }
  ?>
  <?php #tag
  $tg = $this->db->conn_id->prepare(
    'SELECT tag_tbl.title
    FROM tag_tbl
    RIGHT JOIN tag_manager ON tag_manager.from_id = :satu AND tag_manager.from_tbl = :post_tbl AND tag_manager.tag_path = tag_tbl.tag_path'
  );
  $tg->execute(array(
    ':satu' => $id,
    ':post_tbl' => 'post_tbl'
  ));
  $tg_title = $tg->fetchAll(PDO::FETCH_COLUMN);
  $tg_title = array_filter($tg_title);
  if($tg_title){
    echo '<div class="post-tags">'; #tag ada.

    $tags = array();
    foreach ($tg_title as $title_tag) {

      $tq = $this->db->conn_id->prepare("SELECT type FROM tag_tbl WHERE title = :title");
      $tq->execute(array(
        ':title'=> $title_tag
      ));

      $tag_type = $tq->fetch(PDO::FETCH_COLUMN);

        if($tag_type){
          $type = strtolower(str_replace(' ','-',$tag_type));
        } else {
          $type ='noindex';
        }
        $markup = '<a class="basetooltip" title="visit tag page" href="http://wiki.malayatimes.com/'. $type .'/'. strtolower(str_replace(' ','-',$title_tag)) .'" target="_blank">'. $title_tag .'</a>';
        array_push($tags,$markup);

    }
    echo implode(', ', $tags);
    echo '</div>';#end-post_tag
  }
  ?>
</div>

<div class="kolum-kanan">
  <a class="post-edit basetooltip" title="edit this article" href="<?php echo site_url('post?id=' . $id.'&status='.$prom_tbl)?>"><i class="fa fa-pencil" aria-hidden="true"></i> edit</a>
</div>
