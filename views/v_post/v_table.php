<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  if($this->session->flashdata('from_tbl')){
    $prom_tbl = $this->session->flashdata('from_tbl');
    } else {
    $prom_tbl = 'pending';
    }

    $meta_title = ucfirst(str_replace('post', 'publish', $prom_tbl) . ' posts index');

    $author_id = current_user_id();
    $user_role = user_info(current_user_id())['user_role'];

    $admin_gang = array('special admin', 'admin', 'special staff');

?><!DOCTYPE html>
<head>
  <meta content='width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no' name='viewport'/>
  <title><?php echo $meta_title ?></title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/tulis.css" ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/element.css" ?>">
  <link href='<?php echo site_url('css/font-awesome.min.css') ?>' rel='stylesheet'/>
  <script src='<?php echo base_url() ?>js/jquery183.js' type='text/javascript'></script>
  <script src='<?php echo base_url() ?>js/ajax310.js' type='text/javascript'></script>
  <script type="text/javascript" src="<?php echo base_url(). "js/element.js" ?>"></script>
</head>
<body class="dashboard">
  <?php $this->load->view('v_element/v_nav'); ?>

  <div id="the-wrapper"><div id="wrapper-the">

     <div class="header-action">
      <div class="kolum-kiri">
         <a href="/add-post" class="seselect" data-title="Tulis artikel baru">Write Post</a>
      </div>
      <div class="kolum-kanan">
         <div class="filter-role">
             <a href="/draft" <?php if($prom_tbl == 'draft') echo 'class="active"'; ?> data-title="Show all Draft posts">draft</a>
             <a href="/pending"  <?php if($prom_tbl == 'pending') echo 'class="active"'; ?>  data-title="Show all Pending posts">pending</a>
             <a href="/published"  <?php if($prom_tbl == 'post') echo 'class="active"'; ?>  data-title="show all Published posts">published</a>
         </div>
      </div>
     </div>

    <div class="post-table pending">

      <?php

      if(in_array($user_role, $admin_gang) && $prom_tbl != 'draft'){
        //$this->db->where('author_id', $author_id);
             $query = $this->db->conn_id->prepare('SELECT id, title FROM post_tbl WHERE status = :status ORDER BY masa DESC LIMIT 0,30');
             $query->execute(array(
                ':status' => $prom_tbl
             ));

      } else {
        $query = $this->db->conn_id->prepare('SELECT id, title FROM post_tbl WHERE author_id = :author_id AND status = :status ORDER BY masa DESC LIMIT 0,30');
        $query->execute(array(
          ':author_id' => current_user_id(),
          ':status' => $prom_tbl
        ));
      }
        $ROWS = $query->fetchAll(PDO::FETCH_ASSOC);
      if($ROWS){
          foreach ($ROWS as $row) {
            echo '<div class="rows">';
            $this->load->view('v_post/v_row', $row);
            echo '</div>';
          }
      } else {

        if(!in_array($user_role, $admin_gang)){
          $mesej_tiada = 'Anda tiada artikel yang belum di review, sila tulis aritkel!.';
        } else {
          $mesej_tiada = 'Tiada artikel yang dihantar untuk review anda sebelum anda publish.';
        }
        echo '<div class="rows">';
        echo $mesej_tiada;
        echo '</div>';
      }

      ?>

    </div><!--end post table -->
  </div></div>

  <?php $this->load->view('v_element/v_sidebar'); ?>

</body>

</html>
