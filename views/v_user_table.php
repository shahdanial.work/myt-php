<?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 $current_user_role = user_info(current_user_id())['user_role'];

 ?><!DOCTYPE html>
 <head>
   <meta content='width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no' name='viewport'/>
   <title>Moderate Users Role</title>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/tulis.css" ?>">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/element.css" ?>">
   <link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet'/>

   <script src='<?php echo base_url() ?>js/jquery183.js' type='text/javascript'></script>

  <script src='<?php echo base_url() ?>js/ajax310.js' type='text/javascript'></script>

   <script type="text/javascript" src="<?php echo base_url(). "js/element.js" ?>"></script>
 </head>
 <body class="dashboard moderateuser">

<?php $this->load->view('v_element/v_nav'); ?>

<div id="the-wrapper"><div id="wrapper-the">

  <?php

  $currentlink = $_SERVER['REQUEST_URI'];

  $soal = explode('?', $currentlink);


  if(strpos( $currentlink, '?' ) !== false){ // 1
    $filterby = $soal[1];
  } else {
    $filterby = false;
  }


if($current_user_role == 'special admin'){ //shah
$checkkeyword = array('special-admin', 'admin', 'special-staff', 'staff', 'special-author', 'author', 'banned');
}

if($current_user_role == 'admin'){ // adam
$checkkeyword = array('special-staff', 'staff', 'special-author', 'author', 'banned');
}

if($current_user_role == 'special staff'){ // shah and adam staff
$checkkeyword = array('staff', 'special-author', 'author', 'banned');
}

if($filterby === false){//if(strpos( $currentlink, '?' ) !== true){ // ada ? ke x
  #xde ?
  ?>

<?php  $attributes = array('class' => 'role-form');
  echo form_open_multipart('moderateuser/changerole', $attributes);
  ?>

<div class="header-action">
  <div class="kolum-kiri">
    <span class="seselect select-all" title="Select All Users">Select All</span>
    <span class="seselect unselect-all" title="Select All Users" style="display:none">Unselect All</span>
  </div>
  <div class="kolum-kanan">
    <div class="filter-role">
        <a href="<?php echo site_url('moderateuser') ?>" class="nak-role basetooltip" title="show all users">All</a>
        <?php foreach ($checkkeyword as $rolecan) {
             $linkfilter = $rolecan;
             $rolecan = str_replace('-', ' ', $rolecan);
             echo '<a href="'. site_url('moderateuser?'. $linkfilter) .'" class="nak-role basetooltip" title="show '. $rolecan .' only">'. $rolecan .'</a>';
        } ?>

    </div>
  </div>
</div>

    <div class="user-table">

    <?php
     $query = $this->db->get('users');
      if($query->result()){
      foreach ($query->result() as $row) {
        $user_role = $row->user_role;
        $email = $row->email;

        $html = '<div class="rows">
            <div class="kolum-kiri">
              <input type="checkbox" name="user-email[]" value="'. $email .'"> <span class="user-email">'. $email .'</span>
            </div>
            <div class="kolum-kanan">
              <span class="current-role">'. $user_role .'</span>
            </div>
                </div>';

          if(in_array($user_role, str_replace('-', ' ', $checkkeyword))){
        echo $html;
          }


     } // foreach
     } //if
     ?>
    </div>

    <div class="submit-role">
      Set role to: <div class="wrap-box">
        <input name="role-to" id="role-search" placeholder="Select one" readonly="true" type="text"/>
<ul class="role-list" style="display: none;">
  <?php foreach ($checkkeyword as $rolecan) {
       $rolecan = str_replace('-', ' ', $rolecan);
       echo '<li class="select-role">' . $rolecan . '</li>';
  } ?>
</ul>
                   </div>

<button class="button" type="submit" disabled="disabled">Save</button>

    </div>

    <?php echo form_close(); ?>

    <?php } else { //ada ? ke x ?>

    <?php if(in_array($filterby, $checkkeyword)){ //WTF start ?>

      <?php  $attributes = array('class' => 'role-form');
        echo form_open_multipart('moderateuser/changerole', $attributes);
        ?>

      <div class="header-action">
        <div class="kolum-kiri">
          <span class="seselect select-all" title="Select All Users">Select All</span>
          <span class="seselect unselect-all" title="Select All Users" style="display:none">Unselect All</span>
        </div>
        <div class="kolum-kanan">
          <div class="filter-role">
            <a href="<?php echo site_url('moderateuser') ?>" class="nak-role basetooltip" title="show all users">All</a>
            <?php foreach ($checkkeyword as $rolecan) {
                 $linkfilter = $rolecan;
                 $rolecan = str_replace('-', ' ', $rolecan);
                 echo '<a href="'. site_url('moderateuser?'. $linkfilter) .'" class="nak-role basetooltip" title="show '. $rolecan .' only">'. $rolecan .'</a>';
            } ?>
          </div>
        </div>
      </div>

          <div class="user-table">

          <?php
           $query = $this->db->get('users');
            if($query->result()){
            foreach ($query->result() as $row) {
              $user_role = $row->user_role;
              $email = $row->email;

              $html = '<div class="rows">
                  <div class="kolum-kiri">
                    <input type="checkbox" name="user-email[]" value="'. $email .'"> <span class="user-email">'. $email .'</span>
                  </div>
                  <div class="kolum-kanan">
                    <span class="current-role">'. $user_role .'</span>
                  </div>
                      </div>';

                if($user_role == str_replace('-', ' ', $filterby) && in_array($user_role, str_replace('-', ' ', $checkkeyword))){
              echo $html;
                }


           } // foreach
           } //if
           ?>
          </div>

          <div class="submit-role">
            Set role to: <div class="wrap-box">
              <input name="role-to" id="role-search" placeholder="Select one" readonly="true" type="text"/>
      <ul class="role-list" style="display: none;">
      <?php foreach ($checkkeyword as $rolecan) {
           $rolecan = str_replace('-', ' ', $rolecan);
           echo '<li class="select-role">' . $rolecan . '</li>';
      } ?>
      </ul>
                         </div>



      <button class="button" type="submit" disabled="disabled">Save</button>

          </div>

          <?php echo form_close(); ?>

    <?php } else { //WTF else ?>
      Anda tersalah halaman, sila <a href="<?php echo site_url('moderateuser') ?>">kembali</a>
    <?php } //WTF end ?>

    <?php } // ada ? ke x end ?>


</div></div>
<?php $this->load->view('v_element/v_sidebar'); ?>
 </body>
 </html>
