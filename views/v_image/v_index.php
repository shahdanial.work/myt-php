<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$masa = explode(' ', $data['masa_insert']);
$y = explode('-', $masa[0]);
$year = $y[0];
$month = date('F', mktime(0, 0, 0, $y[1], 10));
$masa = $masa[0].'T'.$masa[1].'.000Z';
#setting alt..
if(strrpos(lcfirst( $data['caption']), 'gambar') > 0){
   $data['alt'] = 'foto '.$data['caption'] .' ok? berikut foto terbaru '. $data['caption'] . ' di upload pada '.$month.' '.$year;
} else {
   $data['alt'] = 'gambar '.$data['caption'] .' ok? berikut gambar terbaru '. $data['caption'] .' di upload pada '.$month.' '.$year;
}

#setting related_html
$data['related_html'] = '';
if($data['related']){
   $data['related_html'] .= '<section id="related">
      <div class="related_wrapper slider">
         <h3>RELATED</h3>
         <span class="button slide_left"></span>';
         $c = 0;
         $i = 0;
         foreach ($data['related'] as $ll) {
            $data['related_html'] .= '<div id="related-'.$i.'" class="related_post big sliding" style="left:'.$c.'px"><div class="related_inner"><a href="'.str_replace('.', '--', $ll['filename']).'" title="'.$ll['caption'].'" class="related-thumb"><img src="https://media.malayatimes.com/image/'.$ll['filename'].'" alt="'.$ll['caption'].'"></a></div></div>';
            $c += 206;
            $i++;
         }

         $data['related_html'] .= '<div id="related-'.$i.'" class="related_post big see_more_related sliding" style="left:'.$c.'px">
            <div class="related_inner">
               <form action="https://hub.malayatimes.com/cari"><input name="keyword" type="hidden" value="'.$data['caption'].'" autocomplete="off" placeholder="mulakan carian..">
               <button type="submit">SEE MORE</button>
               </form>
            </div>
         </div>
         <span class="button slide_right"></span>
      </div>
   </section>';
}

echo '<!DOCTYPE html>
';

$html = '
<html class="main-blog">

<head>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type"/><meta content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
    <title>'.$data['caption'].'</title>
    <meta property="og:description" name="twitter:description" content="media: '.$data['caption'].'">
    <meta rel="canonical" content="'.current_url().'" property="og:url">
    <meta name="twitter:site" content="@malayatimes">
    <meta property="og:title" name="twitter:title" content="Malaya Times">
    <meta property="og:type" content="article">
    <meta name="twitter:card" content="summary_large_image">
    <link href="https://media.malayatimes.com/foto/'.$data['filename'].'" rel="image_src">
    <meta property="og:image" name="twitter:image" content="https://media.malayatimes.com/foto/'.$data['filename'].'">
    <meta property="fb:app_id" content="1829602560385819">
    <script src="https://media.malayatimes.com/js/es5.js"></script>
    <link rel="stylesheet" type="text/css" href="https://media.malayatimes.com/css/hub-cari.css">
</head>

<body id="image-page" class="cols_2">
    <aside id="top">
        <div class="width">
            <form id="form" action="https://hub.malayatimes.com/cari">
                <a href="https://hub.malayatimes.com" id="logo"><img src="https://3.bp.blogspot.com/-oP3dOJzTGVg/W2TCpuSanHI/AAAAAAAAC5Y/cvM0nSx77oUW6KE1Omum7KT_5JPGhC3OACLcBGAs/s1600/m-white.png"></a>
                <div id="search-box">
                    <input name="keyword" id="search-input" type="text" value="" autocomplete="off" placeholder="mulakan carian.." required="required">
                    <button type="submit" id="search-button" class="cari"></button>
                </div>
                <div id="search-suggest">
                </div>
                <input type="hidden" id="nak_filter" name="filtering">
            </form>
            <div class="aside">
                <ul>
                    <li class="button"><a href="https://media.malayatimes.com/add-wiki">Tulis wiki</a></li>
                </ul>
            </div>
        </div>
    </aside>
    <div class="option">
        <div class="width">
            <div class="section">
                <ul class="option-list">
                  <li class="menu-item"><a href="//www.malayatimes.com" title="Berita Semasa Malaysia">Utama</a></li>
                  <li class="menu-item"><a href="//politik.malayatimes.com" title="Berita Terkini Politik Malaysia">Politik</a></li>
                  <li class="menu-item"><a href="//artis.malayatimes.com" title="Gosip Artis Malaysia">Artis</a></li>
                  <li class="menu-item berita"><a href="//berita.malayatimes.com" title="Berita Terkini Malaysia">Berita</a></li>
                  <li class="menu-item"><a href="//viral.malayatimes.com" title="Viral Malaysia">Viral</a></li>
                  <li class="menu-item"><a href="//sukan.malayatimes.com" title="Berita Sukan Malaysia">Sukan</a></li>
                  <li class="menu-item"><a href="//pendidikan.malayatimes.com" title="Info pendidikan Malaysia">Pendidikan</a></li>
                </ul>
                <button id="mobile-menu-toggle">☰</button>
            </div>
        </div>
    </div>
    <div id="main">
        <div class="width">
           <main class="main inline inline_left">
              <article id="post" class="post hentry">
              <h1 id="title" class="entry-title">'.$data['caption'].'</h1>
              <div id="content" class="entry-content">
              <figure class="figure">
              <a href="'.current_url().'" title="'.$data['alt'].' " target="_blank" class="img_wrapper"><img src="https://media.malayatimes.com/gambar/'.$data['filename'].'" alt=" '.$data['alt'].' "></a>
              <p class="legend">'.$data['caption'].'<span class="entry-meta"> — <span class="vcard author"><span class="fn" title="'.$data['namapena'].'"><a href="//www.malayatimes.com/'.$data['namapena'].'" rel="author" title="author profile">@'.$data['namapena'].'</a></span></span>, <time datetime="'.$masa.'"><abbr class="published" title="'.$masa.'">'.$month.' '.$year.'</abbr></time></span></p>
              </figure></div>
              </article>
              '.$data['related_html'].'
           </main>
           <aside id="sidebar" class="inline inline_right"><div class="widget"><div class="premium"><img src="https://media.malayatimes.com/assets/image/iklan-square.png"></div></div></aside>
        </div>
    </div>
    <footer>
        <div class="footer-content">
        <div class="option">
           <div class="width">
               <div class="section">
                   <ul class="option-list">
                     <li class="menu-item"><a href="https://blog.malayatimes.com/dasar-privasi" >Dasar privasi</a></li>
                   </ul>
               </div>
           </div>
       </div>
        </div>
        <aside id="noti-important" class="not-important">
            <script type="text/javascript" src="https://media.malayatimes.com/js/media-image/1.js"></script>
        </aside>
    </footer>
</body>

</html>
';

echo preg_replace("/[\n\r]/","",$html);

// if(current_user_id() == 1) echo '<hr><br><br>';var_dump($data);
?>
