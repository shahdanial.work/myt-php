<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$level_percent = author_income(current_user_id())['reputation_level'];
$current_reputation = author_income(current_user_id())['current_reputation'];
$bar_percent = author_income(current_user_id())['bar_percent'];
$bonus_percent = author_income(current_user_id())['bonus_percent'];
$bonus_amount = author_income(current_user_id())['bonus_amount'];
$income_amount = author_income(current_user_id())['income_amount'];
$taraf_user = author_income(current_user_id())['taraf_user'];
$total_income = author_income(current_user_id())['total_income'];

?><!DOCTYPE html>
<head>
  <meta content='width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no' name='viewport'/>
  <title>Bonus and Revenue</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/element.css" ?>" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/circle-bar.css" ?>" />
  <link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet'/>

  <script src='<?php echo base_url() ?>js/jquery183.js' type='text/javascript'></script>

  <script src='<?php echo base_url() ?>js/ajax310.js' type='text/javascript'></script>

  <script type="text/javascript" src="<?php echo base_url(). "js/element.js" ?>"></script>

</head>
<body class="dashboard level">
  <?php $this->load->view('v_element/v_nav'); ?>

  <div id="the-wrapper"><div id="wrapper-the">
    <div class="isi <?Php echo $level_percent ?>-level">
      <div class="bhg-wrapper">

        <div class="bhg-left">
          <?php if($level_percent == 'lowest'){?>

            <div class="bhg first basetooltip" title="Jika Reputation anda bulan ini diatas 60,000, anda layak menerima 15% bonus tambahan dari income anda bulan ini.">
              <div class="c100 p0 big green">
                <span>15%</span>
                <div class="slice">
                  <div class="bar"></div>
                  <div class="fill"></div>
                </div>
              </div>
            </div>

            <div class="bhg basetooltip" title="Jika Reputation anda bulan ini diatas 34,200, anda layak menerima 10% bonus tambahan dari income anda bulan ini.">
              <div class="c100 p0 big">
                <span>10%</span>
                <div class="slice">
                  <div class="bar"></div>
                  <div class="fill"></div>
                </div>
              </div>
            </div>

            <div class="bhg basetooltip" title="Jika Reputation anda bulan ini diatas 16,800, anda layak menerima 5% bonus tambahan dari income anda bulan ini.">
              <div class="c100 p0 big yellow">
                <span>5%</span>
                <div class="slice">
                  <div class="bar"></div>
                  <div class="fill"></div>
                </div>
              </div>
            </div>

            <div class="bhg last active basetooltip" title="Buat masa ini, Reputation anda yang terkumpul bagi bulan ini adalah <?php echo  $current_reputation ?>, untuk Reputation dibawah 16,800 tidak akan mendapat bonus.">
              <div class="c100 p<?php echo number_format($bar_percent) ?> big orange dark">
                <span><?php echo  $bonus_amount ?>%</span>
                <div class="slice">
                  <div class="bar"></div>
                  <div class="fill"></div>
                </div>
              </div>
            </div>


            <?php } ?>

            <?php if($level_percent == 'low'){?>

              <div class="bhg first basetooltip" title="Jika Reputation anda bulan ini diatas 60,000, anda layak menerima 15% bonus tambahan dari income anda bulan ini.">
                <div class="c100 p0 big green">
                  <span>15%</span>
                  <div class="slice">
                    <div class="bar"></div>
                    <div class="fill"></div>
                  </div>
                </div>
              </div>

              <div class="bhg basetooltip" title="Jika Reputation anda bulan ini diatas 34,200, anda layak menerima 10% bonus tambahan dari income anda bulan ini.">
                <div class="c100 p0 big">
                  <span>10%</span>
                  <div class="slice">
                    <div class="bar"></div>
                    <div class="fill"></div>
                  </div>
                </div>
              </div>

              <div class="bhg active basetooltip" title="Buat masa ini, Reputation anda yang terkumpul bagi bulan ini adalah <?php echo  $current_reputation ?>, 5% bonus tambahan jika Reputation anda diatas 16,800">
                <div class="c100 p<?php echo number_format($bar_percent) ?> big yellow dark">
                  <span>5%</span>
                  <div class="slice">
                    <div class="bar"></div>
                    <div class="fill"></div>
                  </div>
                </div>
              </div>

              <div class="bhg last basetooltip" title="misi disini done 100%">
                <div class="c100 p0 big orange dark">
                  <span>0%</span>
                  <div class="slice">
                    <div class="bar"></div>
                    <div class="fill"></div>
                  </div>
                </div>
              </div>


              <?php } ?>
              <?php if($level_percent == 'medium'){?>

                <div class="bhg first basetooltip" title="Jika Reputation anda bulan ini diatas 60,000, anda layak menerima 15% bonus tambahan dari income anda bulan ini.">
                  <div class="c100 p0 big green">
                    <span>15%</span>
                    <div class="slice">
                      <div class="bar"></div>
                      <div class="fill"></div>
                    </div>
                  </div>
                </div>

                <div class="bhg active basetooltip" title="Buat masa ini, Reputation anda yang terkumpul bagi bulan ini adalah <?php echo  $current_reputation ?>, 10% bonus tambahan jika Reputation anda diatas 34,200">
                  <div class="c100 p<?php echo number_format($bar_percent) ?> big dark">
                    <span>10%</span>
                    <div class="slice">
                      <div class="bar"></div>
                      <div class="fill"></div>
                    </div>
                  </div>
                </div>

                <div class="bhg basetooltip" title="misi disini done 100%">
                  <div class="c100 p0 big yellow dark">
                    <span>5%</span>
                    <div class="slice">
                      <div class="bar"></div>
                      <div class="fill"></div>
                    </div>
                  </div>
                </div>

                <div class="bhg last basetooltip" title="misi disini done 100%">
                  <div class="c100 p0 big orange dark">
                    <span>0%</span>
                    <div class="slice">
                      <div class="bar"></div>
                      <div class="fill"></div>
                    </div>
                  </div>
                </div>


                <?php } ?>
                <?php if($level_percent == 'high'){?>

                  <div class="bhg active first basetooltip" title="Buat masa ini, Reputation anda yang terkumpul bagi bulan ini adalah <?php echo  $current_reputation ?>, anda sudah layak menerima 15% bonus tambahan.">
                    <div class="c100 p<?php echo number_format($bar_percent) ?> big green dark">
                      <span>15%</span>
                      <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                      </div>
                    </div>
                  </div>

                  <div class="bhg basetooltip" title="misi disini done 100%">
                    <div class="c100 p0 big dark">
                      <span>10%</span>
                      <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                      </div>
                    </div>
                  </div>

                  <div class="bhg basetooltip" title="misi disini done 100%">
                    <div class="c100 p0 big yellow dark">
                      <span>5%</span>
                      <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                      </div>
                    </div>
                  </div>

                  <div class="bhg last basetooltip" title="misi disini done 100%">
                    <div class="c100 p0 big orange dark">
                      <span>0%</span>
                      <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                      </div>
                    </div>
                  </div>


                  <?php } ?>
          </div>

          <div class="bhg-right">
            <?php if($taraf_user == 'istimewa'){ ?><span class="alert">Anda adalah user yang diberi keistimewaan; sentiasa menerima 15% bonus dari jumlah income bulanan, tanpa mengira Reputation point bulanan.</span><?php } ?>
            <div class="level-table">

              <div class="income-amount">
                <span class="label">revenue</span><span class="value">RM<?php echo $income_amount ?></span>
              </div>

              <div class="bonus-amount">
                <span class="label"><?php echo $bonus_percent ?>% bonus</span><span class="value">RM<?php echo $bonus_amount ?></span>
              </div>

              <div class="total-amount">
                <span class="label">total income</span><span class="value">RM<?php echo $total_income ?></span>
              </div>

            </div>
          </div>

        </div>
      </div>
    </div></div>
    <?php $this->load->view('v_element/v_sidebar'); ?>
  </body>
  </html>
