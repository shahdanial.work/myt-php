<div class="kolum-kiri">
  <?php #title
  if($this->session->flashdata('status')){
    $status = $this->session->flashdata('status');
    } else {
    $status = 'pending';
    }

  if(!$title){
    $title = 'Tiada tajuk..';
  }
  echo '<a class="post-title" data-title="edit this wiki" href="'.site_url('wiki?id=' . $id .'&status='.$status) .'">'.$title.'</a>';
  ?>
  <?php #category.
  $ct = $this->db->conn_id->prepare("SELECT type_path FROM type_manager WHERE from_tbl = 'tag_tbl' AND from_id = :from_id");
  $ct->execute(array(
    ':from_id' => $id
  ));
  $type_path = $ct->fetch(PDO::FETCH_COLUMN);
  if($type_path){
    $cat=$this->db->conn_id->prepare("SELECT title FROM type_tbl WHERE type_path = :type_path");
    $cat->execute(array(
      ':type_path'=>$type_path
    ));
    $type_title = $cat->fetch(PDO::FETCH_COLUMN);
    if($type_title){
      echo '<span class="post-category basetooltip" title="visit category page" >'.$type_title.'</span>,';
    }
  }
  ?>
  <?php #tag
  // $tg = $this->db->conn_id->prepare(
  //   'SELECT tag_tbl.title FROM tag_tbl RIGHT JOIN tag_manager ON tag_manager.from_id = :satu AND tag_manager.from_tbl = :post_tbl AND tag_manager.tag_path = tag_tbl.tag_path'
  // );
  // $tg->execute(array(
  //   ':satu' => $id,
  //   ':post_tbl' => $status .'_tbl'
  // ));
  // $tg_title = $tg->fetchAll(PDO::FETCH_COLUMN);
  // $tg_title = array_filter($tg_title);
  // if($tg_title){
  //   echo '<div class="post-tags">'; #tag ada.
  //
  //   $tags = array();
  //   foreach ($tg_title as $title_tag) {
  //
  //     $tq = $this->db->conn_id->prepare("SELECT type FROM tag_tbl WHERE title = :title");
  //     $tq->execute(array(
  //       ':title'=> $title_tag
  //     ));
  //
  //     $tag_type = $tq->fetch(PDO::FETCH_COLUMN);
  //
  //       if($tag_type){
  //         $type = strtolower(str_replace(' ','-',$tag_type));
  //       } else {
  //         $type ='noindex';
  //       }
  //       $markup = '<a class="basetooltip" title="visit tag page" href="http://wiki.malayatimes.com/'. $type .'/'. strtolower(str_replace(' ','-',$title_tag)) .'" target="_blank">'. $title_tag .'</a>';
  //       array_push($tags,$markup);
  //
  //   }
  //   echo implode(', ', $tags);
  //   echo '</div>';#end-post_tag
  // }
  ?>
</div>

<div class="kolum-kanan">
  <a class="post-edit" data-title="edit this article" href="<?php echo site_url('wiki?id=' . $id.'&status='.$status)?>"><i class="fa fa-pencil" aria-hidden="true"></i> edit</a>
</div>
