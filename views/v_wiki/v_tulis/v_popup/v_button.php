<section class="popup-content" id="a_a_button">

   <div class="popup-header">
      <h4>Insert Button</h4>
      <a class="popup-close">&#215;</a>
   </div>

   <div class="popup-body" id="a_a_button">
<div class="popup-tips"><button class="button" id="a_button_tips"><i aria-hidden="true" class="fa fa-info-circle"></i> tips</button></div><h3>Text: </h3><input class="insert-text-button input field" placeholder="Tajuk page yang nak di Linkkan"/><h3>Link: </h3><input class="insert-link-button input field" placeholder="insert URL/link" type="url"/>
<div class="popup-footer">
<button class="insert button" disabled="disabled">Insert</button>
</div>
<div class="shah_tips">
<button class="button">close</button>
<ol>
<li>Masukkan sahaja link dan text untuk button anda.</li>
<li>Untuk tetapan <b>Text</b>, sila gunakan tajuk page pada link anda (lain dan ringkas pun tidak mengapa, asal text-nya ada persamaan sedikit dengan Tajuk page link tersebut.</li>
<li><b>contoh Text yang baik:</b> Keputusan dan pemenang ABPBH (<i>ianya berdasarkan Tajuk page Link</i>)</li>
<li><b>contoh Text boleh diterima:</b> Keputusan ABPBH (<i>masih relevan dengan Tajuk page Link</i>)</li>
<li><b>contoh yang tidak boleh diterima</b>: Gambar Artis (<i>walaupun mungkin page/post link tersebut banyak gambar artis, tapi Tajuknya tiada terdapat keyword 'Gambar' mahupun keyword 'Artis'</i>)</li>
<li>dibawah berikut gambarajah contoh tampilan Button ini</li>
<li style="text-align:center"><img src="<?php echo  site_url('assets/image/button.jpg') ?>"/></li>
</ol>
</div>
</div>
</section>
