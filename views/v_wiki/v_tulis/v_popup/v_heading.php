<section class="popup-content" id="a_a_heading">

   <div class="popup-header">
      <h4>Insert sub-title</h4>
      <a class="popup-close">&#215;</a>
   </div>

   <div class="popup-body" id="a_a_heading">
      <div class="popup-tips"><button class="button" id="a_heading_tips"><i aria-hidden="true" class="fa fa-info-circle"></i> tips</button></div><h3>Text: </h3><input class="insert-text input field" placeholder="Text for this sub title"/><h3>Link: </h3><input data-tooltip="Masukkan link jika nak jadikan sub-title ini sebagai link" id="link_heading" class="input field" placeholder="addtional: link" type="url">
      <div class="popup-footer">
         <button class="insert button" disabled="disabled">Insert</button>
      </div>
   </div>
</section>
