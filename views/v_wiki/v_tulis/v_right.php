
<section class="the-box" id="category-box">
   <h4>Wiki type:</h4>
      <div class="wrap-box">
            <?php

            $mylist = '';
            $current_title = '';

            if($table['type_list']){
               foreach ($table['type_list'] as  $row) {
                  $mylist .= '<li class="select-label" data-domain="'. $row['title'] .'" data-path="'. $row['type_path'] .'">'. $row['title'] .'</li>';
                  if($row['type_path'] == $table['type']) $current_title = $row['title'];
               }
            }

            echo '<input id="category-search" placeholder="Select one" readonly="true" type="text" value="'.$table['type'].'"/>';

            echo ' <input id="the_category" name="category" type="hidden" value="'.$table['type'].'"/>';

            echo '<ul class="category-list">';

            echo $mylist;

            echo '</ul>';

            ?>
      </div>
</section>

<section class="the-box" id="label-box">
   <h4>Tag other Wiki:</h4>
   <div class="thebox_isi">
      <div class="wrap-search">
         <div class="wrap-box">
            <input  autocomplete="off" id="label-search" placeholder="Search or add new Wiki" data-tooltip="<b>Search and pick</b> or <b>Add new Wiki</b>. <b>warning</b>: untuk <b>tag dan add new wiki</b>, pastikan ejaan anda dengan format yang betul dan ianya berpotensi digunakan berkali-kali sepanjang masa. <br/><br/><span>contoh betul</span>: <i>Anwar Ibrahim, Tun Mahathir, Najib Razak, Awie, ABPBH, Proton</i>.<br/><br/><span>contoh salah</span>: <i>Proton perdana 2017, ABPBH 2015</i> (boleh digunakan sekali dua sahaja).<br/><br/>Ianya akan digunakan sebagai wiki rujukan bagi Figura, Produk, Event, Peristiwa popular kelak." type="text"/><span data-input="label-search" data-target="labelresult" class="go">+</span>
         </div>
         <h3>Selected Wiki</h3>
         <ul class="label-list" id="label-list">
         </ul>
      </div>
      <div id="labelresult" class="selected-group" data-tooltip="click selected Wiki here to remove 1 by 1">
         <?php if($table['tag']){
            // foreach ($table['tag'] as $tag_path) {
            //   $ch = $this->db->conn_id->prepare(
            //     'SELECT title FROM tag_tbl WHERE tag_path = :tag_path'
            //   );
            //   $ch->execute(array(
            //       ':tag_path'=>$tag_path
            //   ));
            //   $tag = $ch->fetch(PDO::FETCH_COLUMN, 0);
            //   if($tag){
              foreach($table['tag'] as $tag){
                  echo '<span class="selected-label" title="click to remove" data-val="'.$tag.'">'.$tag.'<i class="fa fa-times" aria-hidden="true"></i></span>';
              }
            //   }
            // }
          } ?>
         </div>
      </div>
   </section>

   <?php if($table['is_admin'] > 0){ ?>

   <section class="the-box" id="targetting">
      <h4>Ads targetting (in-english):</h4>
      <div class="thebox_isi">
         <h3>Gender</h3>
         <?php
         $checked_male = ' checked="checked"';
         $checked_female = ' checked="checked"';
         if(sizeof($table['keyword']['gender']) > 0){
            if(in_array('male', $table['keyword']['gender'])){
               $checked_male = ' checked="checked"';
            } else {
               $checked_male = ' ';
            }
            if(in_array('female', $table['keyword']['gender'])){
               $checked_female = ' checked="checked"';
            } else {
               $checked_female = ' ';
            }
         }
         echo '<p class="inline"><input id="select-male" type="checkbox" name="gender[]" value="male" class="gender_input"'.$checked_male.'><label for="select-male">Male</label></p>';
         echo '<p class="inline"><input id="select-female" type="checkbox" name="gender[]" value="female" class="gender_input"'.$checked_female.'><label for="select-female">Female</label></p>';
          ?>
         <div class="divider"></div>
         <h3>Interest</h3>
         <div class="wrap-box">
            <input autocomplete="off" id="ads-search" placeholder="Topik berkaitan tentang?" data-tooltip="Masukkan keywords yang berkaitan post ini sahaja. Misalnya:<br><br><span class='red'>cth:</span> <i>fashion, handbag, Tshirt, jeans, lipstick, eye shadow</i><br><br><span class='red'>cth:</span> <i>news, sport</i><br><br><span class='red'>cth:</span> <i>programming, coding, laptop, server, computer, computer hardware</i><br><br>(gunakan/terjemah keywords kepada in-english)" type="text"><span data-input="ads-search" data-target="adsresult" class="go">+</span>
            <br><br>
            <div id="adsresult" class="selected-group" data-tooltip="click selected item to remove"><?php
            if($table['keyword']['interest']){
               foreach ($table['keyword']['interest'] as $tt) {
                  echo '<span class="selected-interest interest_value_Sport_id" title="click to remove" data-val="'.$tt.'">'.$tt.'<i class="fa fa-times" aria-hidden="true"></i></span>';
               }
            }
             ?></div>
         </div>
      </div>
   </section>

<?php } ?>

   <section id="buttons">

      <div id="draft_saved"></div>
      <?php

      if($table['status']  != 'draft'){
         #dalam editing.
         if($table['is_admin']>0){
            $button_title = 'PUBLISH ARTICLE INI?';
            $button_text = 'PUBLISH';
         } else {
            $button_title = 'Resubmit artikel yang telah diedit.';
            $button_text = 'SUMBIT';
         }
         echo '<button type="submit" class="publish button button_large" id="save" data-tooltip="'.$button_title.'">'.$button_text.'</button>';
      } else {
         #dalam draft.
         echo '<button type="button" class="draft button button_large" id="draft" data-tooltip="Save as Draft, anda boleh edit kelak sebelum Sumbit">Save Draft</button> ';

         if($table['is_admin']>0){
            echo '<button type="submit" class="save button_dark button_large" id="save" data-tooltip="Publish terus ke blog Kategori yang dipilih">Publish</button>';
         } else {
            echo '<button type="submit" class="save button_dark button_large" id="save" data-tooltip="Kualiti post akan diperiksa oleh Department dahulu sebelum diterbitkan">Submit to Review</button>';
         }
      }
      ?>

   </section>
