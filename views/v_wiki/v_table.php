<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  if($this->session->flashdata('status')){
    $status = $this->session->flashdata('status');
    } else {
    $status = 'pending';
    }

    $meta_title = ucfirst($status) . ' wiki index';

    $author_id = current_user_id();
    $user_role = user_info(current_user_id())['user_role'];

    $admin_gang = array('special admin', 'admin', 'special staff');

?><!DOCTYPE html>
<head>
  <meta content='width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no' name='viewport'/>
  <title><?php echo $meta_title ?></title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/tulis.css" ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/element.css" ?>">
  <link href='<?php echo site_url('css/font-awesome.min.css') ?> rel='stylesheet'/>
  <script src='<?php echo base_url() ?>js/jquery183.js' type='text/javascript'></script>
  <script src='<?php echo base_url() ?>js/ajax310.js' type='text/javascript'></script>
  <script type="text/javascript" src="<?php echo base_url(). "js/element.js" ?>"></script>
</head>
<body class="dashboard">
  <?php $this->load->view('v_element/v_nav'); ?>

  <div id="the-wrapper"><div id="wrapper-the">

     <div class="header-action">
      <div class="kolum-kiri">
         <a href="/add-wiki" class="seselect" data-title="Tulis Wiki baru">Write wiki</a>
      </div>
      <div class="kolum-kanan">
         <div class="filter-role">
             <a href="wiki/draft" <?php if($status == 'pending') echo 'class="active"'; ?> data-title="Show all Draft Wiki">draft</a>
             <a href="wiki/pending"  <?php if($status == 'submit') echo 'class="active"'; ?>  data-title="Show all Pending Wiki">pending</a>
             <a href="wiki/published"  <?php if($status == 'publish') echo 'class="active"'; ?>  data-title="show all Published Wiki">published</a>
         </div>
      </div>
     </div>

    <div class="post-table pending">

      <?php

      if(in_array($user_role, $admin_gang) && $status != 'pending'){
        //$this->db->where('author_id', $author_id);
             $query = $this->db->conn_id->prepare('SELECT id, title FROM tag_tbl WHERE status = :status ORDER BY masa DESC LIMIT 0,30');
             $query->execute(array(
                ':status' => $status
             ));

      } else {
        $query = $this->db->conn_id->prepare('SELECT id, title FROM tag_tbl WHERE author_id = :author_id AND status = :status ORDER BY masa DESC LIMIT 0,30');
        $query->execute(array(
          ':author_id' => current_user_id(),
          ':status' => $status
        ));
      }
        $ROWS = $query->fetchAll(PDO::FETCH_ASSOC);
      if($ROWS){
          foreach ($ROWS as $row) {
            echo '<div class="rows">';
            $this->load->view('v_wiki/v_row', $row);
            echo '</div>';
          }
      } else {

        if(!in_array($user_role, $admin_gang)){
          $mesej_tiada = 'Anda tiada wiki yang belum di review, sila tulis wiki baru!.';
        } else {
          $mesej_tiada = 'Tiada wiki yang dihantar untuk review anda sebelum anda publish.';
        }
        echo '<div class="rows">';
        echo $mesej_tiada;
        echo '</div>';
      }

      ?>

    </div><!--end post table -->
  </div></div>

  <?php $this->load->view('v_element/v_sidebar'); ?>

</body>

</html>
