<?php
$array = array();
$search = trim(urldecode($cari));


#cari category
$cari_category = 'tiada';
$cari_tag = false;
$pdo = $this->db->conn_id->prepare("SELECT domain FROM category_tbl WHERE LOWER(title) REGEXP :cari OR LOWER(meta_title) REGEXP :cari");
$pdo->execute(array(
   ':cari' =>strtolower($search)
));
$result = $pdo->fetch(PDO::FETCH_COLUMN);
if($result){
   echo 'http://'. $result . '/';
   $cari_category = 'ada';
}

#cari tag.
if($cari_category == 'tiada'){
   $cari_tag = 'tiada';
   $pdo1 = $this->db->conn_id->prepare("SELECT type, title FROM tag_tbl WHERE (LOWER(title) REGEXP :cari OR LOWER(meta_title) REGEXP :cari) AND status = :status");
   $pdo1->execute(array(
      ':cari' =>strtolower($search),
      ':status'=>'publish'
   ));
   $hasilnya = $pdo1->fetch(PDO::FETCH_ASSOC);
   if($hasilnya){
      echo 'http://'. 'wiki.malayatimes.com/type/'. $hasilnya['type'] .'/'. str_replace(' ', '-', build_link($hasilnya['title']));
      $cari_tag = 'ada';
   }
}

#cari post.
if($cari_tag == 'tiada'){
   $cari_post = 'tiada';
   $pdo2 = $this->db->conn_id->prepare("SELECT id, title FROM post_tbl WHERE (LOWER(title) REGEXP :cari OR LOWER(meta_title) REGEXP :cari) AND status = :status");
   $pdo2->execute(array(
      ':cari' =>strtolower($search),
      ':status'=>'publish'
   ));
   $post = $pdo2->fetch(PDO::FETCH_ASSOC);
   if($post){
      //print_r($post);
      $cat_id = $this->db->conn_id->prepare("SELECT category_id FROM category_manager WHERE from_tbl = :from_tbl AND from_id = :from_id");
      $cat_id->execute(array(
         ':from_tbl'=>'post_tbl',
         ':from_id'=>$post['id']
      ));
      $category_id = $cat_id->fetch(PDO::FETCH_COLUMN);
      if(is_numeric($category_id)){
         $cat_domain = $this->db->conn_id->prepare("SELECT domain FROM category_tbl WHERE id = :id");
         $cat_domain->execute(array(
            ':id'=>$category_id
         ));
         $category_domain = $cat_domain->fetch(PDO::FETCH_COLUMN);
         if($category_domain){
            echo 'http://'. $category_domain . '/'. build_link($post['title']);
            $cari_post = 'ada';
         }
      }
   }
}

if($cari_category == 'tiada' && $cari_tag == 'tiada' && $cari_post == 'tiada'){
    #echo 'tiada link ditemui';
}
?>
