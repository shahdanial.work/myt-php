<?php $this->load->helper('h_user_helper');
$user_role = user_info(current_user_id())['user_role'];//$this->session->userdata('user_role');
$canmoderate = array('special admin', 'admin', 'special staff') ?>
<ul class="sidebar" unselectable="on">
<li><a href="<?php echo site_url('dashboard') ?>"><i aria-hidden="true" class="fa fa-home"></i> Dashboard</a></li>
<li><a href="<?php echo site_url('add-post') ?>"><i aria-hidden="true" class="fa fa-pencil"></i> Write Post</a></li>
<li><a href="<?php echo site_url('pending') ?>"><?php if($user_role == 'super admin'){ echo 'Moderate Post'; } else { echo 'Pending Post'; } ?></a></li>
<li><a href="<?php echo site_url('draft') ?>">Draft Post</a></li>
<li><a href="<?php echo site_url('published') ?>">Published Post</a></li>
<li><a href="<?php echo site_url('add-wiki') ?>"><i aria-hidden="true" class="fa fa-pencil"></i> Write Wiki</a></li>
<li><a href="<?php echo site_url('wiki/pending') ?>"><?php if($user_role == 'super admin'){ echo 'Moderate Wiki'; } else { echo 'Pending Wiki'; } ?></a></li>
<li><a href="<?php echo site_url('wiki/draft') ?>">Draft Wiki</a></li>
<li><a href="<?php echo site_url('wiki/published') ?>">Published Wiki</a></li>
<li><a href="<?php echo site_url('profile') ?>">Profile</a></li>
<?php if(in_array($user_role, $canmoderate)){ ?><li><a href="<?php echo site_url('moderateuser') ?>">Moderate Users</a></li><?php } ?>
<li><a href="<?php echo site_url('income') ?>">Income & Bonus</a></li>
<?php if($user_role == 'special admin'){ ?>
<li><a href="<?php echo site_url('income-cost') ?>">Update Income</a></li>
<?php } ?>
<li><a href="<?php echo base_url() ?>logout"><i aria-hidden="true" class="fa fa-sign-out"></i>  Logout</a></li>
</ul>
