<nav class="navigation">
     <table>
       <tbody>
         <tr>
           <td class="site-menu left">
             <!-- menu will begin here -->
             <a href="#" class="notification">
             <i aria-hidden="true" class="fa fa-inbox"></i>
           </a>
           </td>
           <td class="site-header center">
             <a href="<?php echo base_url() ?>dashboard" rel="home"><img src="<?php echo site_url('assets/image/malayatimes-bar.png') ?>" alt="Media Malayatimes"></a>
           </td>
           <td class="site-menu right">
             <a href="#" class="hidden-button" id="close-sidebar"><img src="<?php echo site_url('assets/image/close.png') ?>" /></a>
             <a href="#" id="open-sidebar"><img src="<?php echo site_url('assets/image/menu.png') ?>" /></a>
           </td>
         </tr>
       </tbody>
     </table>
   </nav>
