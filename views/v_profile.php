<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 ?><!DOCTYPE html>
 <head>
   <meta content='width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no' name='viewport'/>
   <title>Edit Profile</title>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/tulis.css" ?>">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/element.css" ?>">
   <link href='<?php echo site_url('css/font-awesome.min.css') ?>' rel='stylesheet'/>

   <script src='<?php echo base_url() ?>js/jquery183.js' type='text/javascript'></script>

  <script src='<?php echo base_url() ?>js/ajax310.js' type='text/javascript'></script>

   <script type="text/javascript" src="<?php echo base_url(). "js/element.js" ?>"></script>
 </head>
 <body class="dashboard profile">
<?php $this->load->view('v_element/v_nav'); ?>

<div id="the-wrapper"><div id="wrapper-the">

  <?php
  $attributes = array('id' => 'edited-profile');
  echo form_open_multipart('profile', $attributes);
  ?>

  <?php
   //echo '<input type="hidden" name="current_user_id" value="' . current_user_id() * 79 - 37; .'" /><!-- get row name id -->';
   ?>

<h2 class="table_heading">Account Details</h2>
<table class="table_form">
<tbody>

<tr>
<th><label for="nama">Nama</label></th>
<td>
  <?php echo form_error('nama', '<small class="warning">', '</small>'); ?>
  <input value="<?php if(set_value('nama')){ echo set_value('nama'); } else { echo user_info(current_user_id())['nama']; } ?>" name="nama" class="nama input"/>
</td>
</tr>

<?php
// echo '<tr>
// <th><label for="email">Email</label></th>
// <td>';

  // if( isset($this->session->flashdata('err')['email']) ){echo $this->session->flashdata('err')['email'];}else{echo form_error('email', '<small class="warning">', '</small>');}

  // echo '<input value="';
  // if( isset($this->session->flashdata('data')['email']) ){ $email = $this->session->flashdata('data')['email']; }elseif(set_value('email')){ $email = set_value('email'); } else { $email = user_info(current_user_id())['email']; } echo $email;
  // echo '" name="email" class="email input" disabled/>
  //      </td>
  //      </tr>';
 ?>

<tr>
<th><label for="namapena">Nama Pena</label></th>
<td>
  <?php if( isset($this->session->flashdata('err')['namapena']) ){echo $this->session->flashdata('err')['namapena'];}else{echo form_error('namapena', '<small class="warning">', '</small>');} ?>
  <input value="<?php if( isset($this->session->flashdata('data')['namapena']) ){ $namapena = $this->session->flashdata('data')['namapena']; }elseif(set_value('namapena')){ $namapena = set_value('namapena'); } else { $namapena = user_info(current_user_id())['namapena']; } echo $namapena; ?>" name="namapena" class="namapena input"/>
</td>
</tr>

</tbody>
</table>


<h2 class="table_heading">Sequrity Question</h2>
<table class="table_form">
<tbody>

<tr>
<th><label for="soalan">Soalan</label></th>
<td>
  <?php echo form_error('soalan', '<small class="warning">', '</small>'); ?>
  <input placeholder="ex: App game yang paling anda suka?" value="<?php if(set_value('soalan')){ echo set_value('soalan'); } else { echo user_info(current_user_id())['soalan']; } ?>" name="soalan" class="soalan input"/>
</td>
</tr>

<tr>
<th><label for="jawapan">Jawapan</label></th>
<td>
  <?php echo form_error('jawapan', '<small class="warning">', '</small>'); ?>
  <input placeholder="ex: FIFA" value="<?php if(set_value('jawapan')){ echo set_value('jawapan'); } else { echo user_info(current_user_id())['jawapan']; } ?>" name="jawapan" class="jawapan input"/>
</td>
</tr>

</tbody>
</table>

<?php

echo '<br><br><button type="submit" class="button">Update</button>';
/* echo '<h2 class="table_heading" id="password">Change Password?</h2>
<small class="warning">abaikan jika tidak mahu tukar password</small><br/>
<table class="table_form">
<tbody>

<tr>
<th><label for="password">Password baru</label></th>
<td>
  <?php echo form_error('password', '<small class="warning">', '</small>'); ?>
  <input type="password" autocomplete="on"  placeholder="Taip password baru" name="password" class="newpassword input" value="<?php if(set_value('password')){ echo set_value('password'); } ?>"/>
</td>
</tr>

<tr>
<th><label for="password_conf">Ulang Password Baru</label></th>
<td>
  <?php echo form_error('password_conf', '<small class="warning">', '</small>'); ?>
  <input autocomplete="on" type="password" placeholder="Taip semula password baru" name="password_conf" class="ulangpassword input" value="<?php if(set_value('password_conf')){ echo set_value('password_conf'); } ?>"/>
</td>
</tr>

<tr>
  <th></th>
  <td class="alignright"><button type="submit" class="button">Update</button></td>
</tr>

</tbody>
</table>'; */
?>

<?php echo form_close(); ?>


</div></div>
<?php $this->load->view('v_element/v_sidebar'); ?>
 </body>
 </html>
