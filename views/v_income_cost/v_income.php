<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<head>
  <meta content='width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no' name='viewport'/>
  <title>Update Income</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/tulis.css" ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/element.css" ?>">
  <link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet'/>

  <script src='<?php echo base_url() ?>js/jquery183.js' type='text/javascript'></script>

  <script src='<?php echo base_url() ?>js/ajax310.js' type='text/javascript'></script>

  <script type="text/javascript" src="<?php echo base_url(). "js/element.js" ?>"></script>
  <link rel="stylesheet" type="text/css" href="https://media.malayatimes.com/css/tulis.css"/>
</head>
<body class="dashboard income">
  <?php $this->load->view('v_element/v_nav'); ?>

  <div id="the-wrapper"><div id="wrapper-the">
     <?php
     $attributes = array('class' => 'income-form');
     if(isset($data)){
        echo form_open_multipart('income_cost/edit_'.$data['income_cost'], $attributes);
        echo '<input type="hidden" name="id" value="'.$data['id'].'">';
     } else {
        echo form_open_multipart('income_cost/update_income', $attributes);
     }
     ?>


      <h1><?php if(isset($data)){ echo 'Edit'; }else{ echo 'New'; } ?> Income Record</h1>

      <br><br>

      <section class="the-box">

         <h4 class="input-heading">Matawang</h4>

         <div class="thebox_isi">
            <select name="currency">
              <option value="MYR" <?php if(isset($data)){ if($data['currency'] == 'MYR') echo 'selected'; } ?>>MYR</option>
              <option value="USD" <?php if(isset($data)){ if($data['currency'] == 'USD') echo 'selected'; } ?>>USD</option>
            </select>
         </div>

      </section>



      <section class="the-box">

         <h4 class="input-heading">Jumlah Wang</h4>
         <div class="thebox_isi">

            <div class="wrap-box">
               <input value="<?php if(isset($data)){ if($data['value']) echo $data['value']; } ?>" placeholder="109.00" type="number" name="value" class="value">
            </div>

         </div>

      </section>

      <section class="the-box">

         <h4 class="input-heading">Penerangan</h4>
         <div class="thebox_isi">

            <textarea placeholder="Bayaran dari Media Prima by cash/online transaction ke akaun Mr Adam/Shah untuk beriklan di semua site selama setahun" type="number" name="penerangan" class="penerangan" cols="100" rows="7"><?php if(isset($data)){ if($data['penerangan']) echo $data['penerangan']; } ?></textarea>

         </div>

      </section>



      <button type="submit" class='updateincome basetooltip button' id='updateincome' title='Pastikan jumlah betul dan tidak terlebih'>Update Income</button>

  <?php echo form_close(); ?>
  </div></div>
  <?php $this->load->view('v_element/v_sidebar'); ?>
</body>
</html>
