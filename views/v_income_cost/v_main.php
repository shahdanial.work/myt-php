<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?><!DOCTYPE html>
<head>
  <meta content='width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no' name='viewport'/>
  <title>Update Income</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/tulis.css" ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/element.css" ?>">
  <link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet'/>

  <script src='<?php echo base_url() ?>js/jquery183.js' type='text/javascript'></script>

 <script src='<?php echo base_url() ?>js/ajax310.js' type='text/javascript'></script>

  <script type="text/javascript" src="<?php echo base_url(). "js/element.js" ?>"></script>
</head>
<body class="dashboard income">
<?php $this->load->view('v_element/v_nav'); ?>

<div id="the-wrapper"><div id="wrapper-the">

<ol>
  <li><a href="<?php echo site_url('income_cost/income') ?>">ADD INCOME RECORD</a></li>
  <li><a href="<?php echo site_url('income_cost/cost') ?>">ADD COST RECORD</a></li>
</ol>

<?php

if($data){
   // if(current_user_id() == '1') var_dump($data);

   #add edit html in array data.
   foreach ($data as $item => $array) {
      $data[$item]['edit'] = '<a href="'. site_url('income_cost/edit?type=' . $array['income_cost'] . '&id=' . $array['id']) .'">EDIT</a>';
   }
   echo '<div class="elementary"><div class="table"><table>';
   #thead..
   if($data['0']){
      echo '<thead>';
      echo '<tr>';
      foreach ($data['0'] as $key => $value) {
         if($key != 'id'){
            if($key == 'income_cost') $key = 'type';
            if($key == 'penerangan') $key = 'description';
            if($key == 'added_time') $key = 'time';
            if($key == 'added_by_user_id') $key = 'by';
            echo '<td>'. $key .'</td>';
         }
      }
      echo '</tr>';
      echo '</thead>';
   }

   #tbody..
   $c = 0;
   $last = count($data) - 1;
   foreach ($data as $row) {
      if($c == '0') echo '<tbody>';
      echo '<tr>';
      foreach ($row as $key => $value) {
         if($key != 'id'){
            if($key == 'added_by_user_id') $value = user_info($value)['email'];
            echo '<td class="tr_'.$key.'">'. $value .'</td>';
         }
      }
      echo '</tr>';
      if($c == $last) echo '</tbody><!--hihi-->';
      $c++;
   }
   echo '</table></div></div>';
}

 ?>

</div></div>
<?php $this->load->view('v_element/v_sidebar'); ?>
</body>
</html>
