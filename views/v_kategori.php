<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 ?><!DOCTYPE html>
 <head>
   <meta content='width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no' name='viewport'/>
   <title>Pilih Kategori</title>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/tulis.css" ?>">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/element.css" ?>">
   <link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet'/>

   <script src='<?php echo base_url() ?>js/jquery183.js' type='text/javascript'></script>

  <script src='<?php echo base_url() ?>js/ajax310.js' type='text/javascript'></script>

   <script type="text/javascript" src="<?php echo base_url(). "js/element.js" ?>"></script>
 </head>
 <body class="dashboard">
<?php $this->load->view('v_element/v_nav'); ?>

<div id="the-wrapper"><div id="wrapper-the">

<div class="alert_messages">Terdapat pelbagai platform kategori untuk anda menerbitkan pelbagai jenis penulisan seperti Blog, Forum, Pertanyaan dan lain-lain untuk potensi anda mejana pendapatan mengikut idea semasa dan kepakaran anda. Sila pilih mana-mana kategori dibawah</div>

<div class="tags">
<a href="<?php echo site_url('tulis?berita') ?>">berita</a>

<a href="<?php echo site_url('tulis?artis') ?>">artis</a>

<a href="<?php echo site_url('tulis?viral') ?>">viral</a>

<a href="<?php echo site_url('tulis?sukan') ?>">sukan</a>

<a href="<?php echo site_url('tulis?pendidikan') ?>">pendidikan</a>

<a href="<?php echo site_url('tulis?review') ?>">review</a>

<a href="<?php echo site_url('tulis?kerjaya') ?>">kerjaya</a>

<a href="<?php echo site_url('tulis?resepi') ?>">resepi</a>

<a href="<?php echo site_url('tulis?iklan') ?>">iklan</a>

</div>

<!--
      <h3><?php echo $this->session->userdata('namapena') ?><?php echo $this->session->userdata('id_login') ?></h3>

      <p>Selamat datang di halaman dashboard, <?php echo $this->session->userdata('id') ?>! Untuk logout dari sistem, silakan klik <?php echo anchor('login/logout','di sini...'); ?></p>
-->

</div></div>
<?php $this->load->view('v_element/v_sidebar'); ?>
 </body>
 </html>
