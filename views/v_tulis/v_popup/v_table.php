<section class='popup-content' id='a_a_table'>
<div class='popup-tips'><button class='button' id='a_button_tips'><i aria-hidden='true' class='fa fa-info-circle'></i> tips</button></div>
<div class='table-content'><table class='content-table'><tbody>
<thead>
<tr>
<th contenteditable='true'>EDIT ME</th>
<th contenteditable='true'>EDIT ME</th>
</tr>
</thead>
<tr>
<td contenteditable='true'>-</td>
<td contenteditable='true'>-</td>
</tr>
<tr>
<td contenteditable='true'>-</td>
<td contenteditable='true'>-</td>
</tr>
</tbody></table></div>
<div class='box'><button class='add-rows'><i aria-hidden='true' class='fa fa-plus'></i> Tambah Rows</button><button class='remove-rows'><i aria-hidden='true' class='fa fa-minus'></i> Buang last rows</button>
</div>
<div class='boxs'>
<input class='tablebg' name='check' type='checkbox'/>
<label>Background Column fist?</label>
<br/><br/>
<input class='tablebelang' name='check' type='checkbox'/>
<label>Background Rows selang seli?</label>
</div>
<div class='popup-footer'>
<span class='insert button' disabled='false'>Insert</span>
</div>
<div class='shah_tips'>
<button class='button'>close</button>
<ol>
<li>Kami hanya benarkan 2 Column sahaja</li>
<li>Manakala Rows link, anda boleh tambah sendiri dengan klik button <b>+ Add Rows</b> yang disediakan</li>
<li>Sila isi/edit butiran pada Table ini dengan simple, kerana jika panjang ianya sukar untuk mobile browser bersaiz kecil</li>
</ol>
</div>
</section>
