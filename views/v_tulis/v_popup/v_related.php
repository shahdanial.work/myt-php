<section class='popup-content' id='a_a_related'>
<div class='popup-tips'><button class='button' id='a_related_tips'><i aria-hidden='true' class='fa fa-info-circle'></i> tips</button></div>
<h3>Link:</h3><input class='insert-related-link input field' placeholder='Paste URL of related Post/Page'/>
<h3>Title:</h3><input class='insert-related-text input field' placeholder='Paste Title of related Post/Page'/>
<div class='popup-footer'><button class='insert button' disabled='disabled'>Insert</button></div>
<div class='shah_tips'><button class='button'>close</button>
<p class='search'><input class='search-related input field' placeholder='type simple keyword yang related'/><a class='search-button' href='https://www.google.com/#q=GANTI+DENGAN+KEYWORD+RELATED+ANDA+site:malayatimes.com' target='_blank'><i aria-hidden='true' class='fa fa-search'></i></a></p>
<small>contoh: <i>Mahathir</i> atau <i>Awie</i></small>
<ol>
<li>Anda hanya boleh masukkan link posts / tag pages / homepage blog Malaya Times sahaja.</li>
<li>Tetapan Title/text hendaklah tepat sama dengan posts / tag pages / hompepages link yang anda pilih.</li>
<li>Jika artikel yang anda tulis ini terdapat banyak perenggan, anda disarankan buat dua Related Articles pada perenngan lain pual.</li>
</ol>
</div>
</section>
