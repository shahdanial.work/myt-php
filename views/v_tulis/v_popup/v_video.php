<section class='popup-content' id='a_a_video'>
<div class='popup-tips'><button class='button' id='a_video_tips'><i aria-hidden='true' class='fa fa-info-circle'></i> tips</button></div>
<h3>Alamat Video:</h3>
<input class='insert-youtube input field' placeholder='Paste video Youtube URL'/>
<div class='embed-video field'></div>
<div class='popup-footer'><button class='insert button' disabled='disabled'>Insert</button></div>
<div class='shah_tips'><button class='button'>close</button>
<ol>
<li>Copy alamat (url/link) video Youtube anda nak Embed di post ini pada kotak <b>Embed Youtube Video</b></li>
<li>Contoh <i>alamat video Youtube</i>, boleh rujuk gambar rajah dibawah:</li>
<li><img alt='URL Youtube' src='<?php echo site_url('assets/image/Youtube-Deskop.jpg') ?>'/></li>
<li>Anda juga boleh copy <i>alamat video Youtube</i> dari Mobile browser. System kami turut support <i>alamat video Youtube</i> dari Mobile browser.</li>
</ol>
</div>
</section>
