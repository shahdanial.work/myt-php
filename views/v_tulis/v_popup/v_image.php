<section class='popup-content' id='a_a_image'>

  <div class="image-header">
    <h4>Images manager</h4>
    <a class='image-close'>&#215;</a>
  </div>

  <div class="image-select">
    <span class="select-search selected">Search</span>
    <span class="select-upload">Upload</span>
    <span class="select-url">Insert link</span>
  </div>

  <div class="image-option">

    <div class="search-select">

      <div class="wtf" style="display:none"><div class="warning-search"><div class="search-warning"><i class="fa fa-warning"></i> kotak input tak boleh kosong</div></div></div>

      <div style="display:none;" class="uploading"><span>SILA TUNGGU . . .</span><br/><div class="meloading"></div></div>

     <div class="kotak">
      <input class="cari-gambar" type="text" placeholder="taip keyword yang simple" />
      <button class="cari-gambar-button button">CARI GAMBAR</button>
     </div>

      <div class="picca"></div>

    </div>

    <div class="upload-select">


      <div style="display:none;" class="uploading"><span>SILA TUNGGU . . .</span><br/><div class="meloading"></div></div>

    <!-- UPLOAD -->
<?php echo form_open_multipart('upload');?>
  <div class="form-gruops wadepak">

    <?php if (isset($error)) { echo '<span class="text-danger">'.$error.'</span>'; } ?>
  <div class="form-group aduhai">
    <input id="insert-file" name="usr_files[]" type="file" multiple="" />
    <label for="insert-file" class="label-insert-file">
      <i class="fa fa-image"></i> <span>0</span> ready to upload
    </label>
  </div>

  <input type="submit" value="UPLOAD" class="upload-button button" disabled="disabled"/>

  <br/><div id="before-upload"></div>

  </div>
<?php echo form_close(); ?>

  </div>

   </div>

    <div class="bucketine"></div>
    <div class="bucketheaderwrap">
    <div class="bucketheader">
      <span class="bukettitle">BUCKET</span>
      <span class="bucketdescription">select then insert</span>
    </div>
    </div>

    <div class='pics'></div>

  <div class="popup-footer">
    <button class="insert button" id="img-insert-btn" disabled="disabled">INSERT</button>
  </div>

</section>
