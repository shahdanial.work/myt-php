<div class='wysiwyg-controls'>
<a class='a_image basetooltip' href='#' title='Insert Images'><i aria-hidden='true' class='fa fa-image'></i></a>
<a class='a_video basetooltip' href='#' title='Embed Youtube Video'><i aria-hidden='true' class='fa fa-video-camera'></i></a>
<a class='a_link basetooltip' href='#' title='Insert Text Link'><i aria-hidden='true' class='fa fa-link'></i></a>
<a class='a_related basetooltip' href='#' title='Insert Related Article of malayatimes.com'><i aria-hidden='true' class='fa fa-newspaper-o'></i></a>
<a class='a_button basetooltip' href='#' title='Insert Text Link Button'><i aria-hidden='true' class='fa fa-chevron-circle-right'></i></a>
<a class='a_table basetooltip' href='#' title='Insert simple Table'><i aria-hidden='true' class='fa fa-table'></i></a>
<a class='a_message basetooltip' href='#' title='Insert Alert Messages'><i aria-hidden='true' class='fa fa-info-circle'></i></a>
<a class='a_quote basetooltip' href='#' title='Insert Blocqoute (box kata-kata seseorang)'><i aria-hidden='true' class='fa fa-quote-left'></i></a>
</div>
