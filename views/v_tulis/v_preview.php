<div class='body'>
<div id='my-content'>
<h1 class='entry-title'><?php
if($table['title']){
  echo $table['title'];
} else {
  echo 'Tajuk haruslah mesra carian pengguna Google';
} ?></h1>
<div class='my-content'><?php
  if($table['content']){
    $content = str_replace('--&gt;', '-->', str_replace('&lt;!--', '<!--', $table['content']) );
    echo
    $content
     ;
  } else {
    echo "Ciri sesebuah artikel yang baik adalah artikel yang berinformasi dengan menggunakan ayat yang formal serta mampu menarik minat pembaca untuk kongsikan artikel tersebut; ianya bergantung kepada kreativiti anda, pembaca tidak mungkin share artikel anda tanpa sebarang sebab.";
  }
   ?></div>
</div>
</div>
