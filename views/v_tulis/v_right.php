<section class='the-box' id='category-box'>
  <h4>Select Category:</h4>
  <div class='wrap-box'>
    <input required="required" name='category' id='category-search' placeholder='Select Category' readonly='true' type='text' value="<?php
    if(is_numeric($table['category_id'])){
      $curr_cat = $this->db->conn_id->prepare("SELECT domain FROM category_tbl WHERE id = :id");
      $curr_cat->execute(array(':id'=>$table['category_id']));
      if($curr_cat){
        echo $curr_cat->fetch(PDO::FETCH_COLUMN);
      }
    } ?>"/>
    <ul class='category-list'>
      <?php
      if(user_info(current_user_id())['user_role'] == 'special admin'){
        $cat_query = $this->db->conn_id->prepare("SELECT id, domain, title FROM category_tbl");
      } else {
        $cat_query = $this->db->conn_id->prepare("SELECT id, domain, title FROM category_tbl WHERE (status = :status)");
      }
      $cat_query->execute(array(
        ':status' => 'publish'
      ));
      if($cat_query){
        $category = $cat_query->fetchAll(PDO::FETCH_ASSOC);
        //print_r($category);
        foreach ($category as  $row) {
           if($row['domain'] == 'blog.malayatimes.com'){

             if(user_info(current_user_id())['role'] == 'special admin') echo "<li onclick='auto_draft(this);' class='select-label' data-domain='". $row['domain'] ."' data-id='". $row['id'] ."'>". $row['title'] ."</li>";

           } else {
             echo "<li onclick='auto_draft(this);' class='select-label' data-domain='". $row['domain'] ."' data-id='". $row['id'] ."'>". $row['title'] ."</li>";
           }

        }
      }
      ?>
    </ul>
  </div>
</section>
<section class='the-box' id='label-box'>
  <h4>Select label:</h4>
  <div class='wrap-search'>
    <div class='wrap-box'>
      <input  autocomplete="off" class='basetooltip' id='label-search' placeholder='Add new or search/pick already label' title='Pick or add new label. PERINGATAN: untuk add new Label, pastikan ejaan anda dengan format yang betul dan ianya berpotensi digunakan berkali-kali sepanjang masa. Example: ' type='text'/><span class='go'>add</span>
    </div>
    <h3>Selected Label</h3>
    <ul class='label-list'>
    </ul>
  </div>
  <div class='basetooltip' id='labelresult' title='click selected label here to remove 1 by 1'>
    <?php if($table['tag_id']){
      foreach ($table['tag_id'] as $tag_id) {
        $tg = $this->db->conn_id->prepare("SELECT title FROM tag_tbl WHERE id = :id");
        $tg->execute(array(':id'=>$tag_id));
        if($tg){
          $cat_name = $tg->fetch(PDO::FETCH_COLUMN);
          echo '<span class="selected-label" title="click to remove" data-val="'.$cat_name.'">'.$cat_name.'<i class="fa fa-times" aria-hidden="true"></i></span>';}
      }
    } ?>
  </div>
</section>
<section id='buttons'>

  <div id="draft_saved"></div>
  <?php


  if($table['status']  != 'draft'){
    #dalam editing.
    if($table['is_admin']>0){
      $button_title = 'PUBLISH ARTICLE INI?';
      $button_text = 'PUBLISH';
    } else {
      $button_title = 'Resubmit artikel yang telah diedit.';
      $button_text = 'SUMBIT';
    }
    echo "<button type='submit' class='publish button basetooltip' id='save' title='".$button_title."'>".$button_text."</button>";
  } else {
    #dalam draft.
    echo "<button onclick='manual_draft();' type='button' class='draft button basetooltip' id='draft' data-title='Save as Draft, anda boleh edit kelak sebelum Sumbit'>Save Draft</button> ";

    if($table['is_admin']>0){
      echo "<button type='submit' class='save button_dark' id='save' data-title='Publish terus ke blog Kategori yang dipilih'>Publish</button>";
    } else {
      echo "<button type='submit' class='save button_dark' id='save' data-title='Kualiti post akan diperiksa oleh Department dahulu sebelum diterbitkan'>Submit to Review</button>";
    }
  }
  ?>

</section>
