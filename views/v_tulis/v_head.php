<meta content='text/html; charset=UTF-8' http-equiv='Content-Type'/><meta content='width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no' name='viewport'/>
<title><?php
if($table['status'] == 'draft'){
  echo 'Write general article';
} else {
  if($table['is_admin']){
    echo 'Review Submitted Article';
  } else {
    echo 'Reedit Submitted Article';
  }
}
?></title>
<link href='<?php echo site_url('favicon.ico') ?>' rel='icon' type='image/x-icon'/>
<script src='<?php echo base_url(). "js/jquery183.js" ?>' type='text/javascript'></script>
<script src='<?php echo base_url(). "js/ajax310.js" ?>' type='text/javascript'></script>
<script type="text/javascript" src="<?php echo base_url(). "js/function.js" ?>"></script>
<script type="text/javascript" src="<?php echo base_url(). "js/before-upload.js" ?>"></script>
<script src='<?php echo site_url('js/paras.js') ?>' type='text/javascript'></script>
 <style type="text/css">
 .faces {
    content: '';
    position: absolute;
    background: rgba(0,0,0,0);
    border: 2px solid #ddd;
 }
 </style>
<script type="text/javascript">
$(document).ready(function(e) {
    $(".upload-select form").on('submit',(function(e) {
        e.preventDefault();

        $.ajax({
            url: "example.com/upload-select",
            type: "POST",
            data:  new FormData(this),
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
              if($('#insert-file')[0].files.length){
            $(".pics").prepend(data);
            $('.label-insert-file span').text('0');
            $('.upload-button').attr('disabled', 'disabled');
            $('#wings').html('<script type=\"text/javascript\" src=\"<?php echo base_url(). "js/tulis.js" ?>\"><\/script>');
              } else {
               $('.upload-select .wadepak').prepend('<div class="warning-gambar"><div class="gambar-warning"><i class="fa fa-warning"></i> awas, anda ada select file gambar?</div></div>');
              }
            $('.upload-select .wadepak').show();
            $('.upload-select .uploading').hide();
            $('.img_wrpr').remove();
            $('#img-insert-btn').removeAttr('disabled');
            },
            error: function(){
            }
       });
    }));
});
</script>

<!--other head content -->
<!--[if lte IE 8 ]><script> (function() { var html5element = ("main").split(','); for (var i = 0; i < html5element.length; i++) { document.createElement(html5element[i]); } try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {} })(); </script> <![endif]--><!--[if IE]><script type="text/javascript" src="https://www.blogger.com/static/v1/jsbin/579771828-ieretrofit.js"></script><![endif]--><!--[if IE]> <script> (function() { var html5 = ("abbr,article,aside,audio,canvas,datalist,details," + "figure,footer,header,hgroup,mark,menu,meter,nav,output," + "progress,section,time,video").split(','); for (var i = 0; i < html5.length; i++) { document.createElement(html5[i]); } try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {} })(); </script> <![endif]-->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/tulis.css" ?>">

<link href='<?php echo site_url('css/font-awesome.min.css'); ?>' rel='stylesheet'/>
<script type='text/javascript'>//<![CDATA[
!function(t,e,r){var n=function(t){var n={text:"",start:0,end:0};if(!t.value)return n;try{if(e.getSelection)n.start=t.selectionStart,n.end=t.selectionEnd,n.text=t.value.slice(n.start,n.end);else if(r.selection){t.focus();var s=r.selection.createRange(),a=r.body.createTextRange();n.text=s.text;try{a.moveToElementText(t),a.setEndPoint("StartToStart",s)}catch(c){a=t.createTextRange(),a.setEndPoint("StartToStart",s)}n.start=t.value.length-a.text.length,n.end=n.start+s.text.length}}catch(c){}return n},s={getPos:function(t){var e=n(t);return{start:e.start,end:e.end}},setPos:function(t,r,n){n=this._caretMode(n),"start"==n?r.end=r.start:"end"==n&&(r.start=r.end),t.focus();try{if(t.createTextRange){var s=t.createTextRange();e.navigator.userAgent.toLowerCase().indexOf("msie")>=0&&(r.start=t.value.substr(0,r.start).replace(/\r/g,"").length,r.end=t.value.substr(0,r.end).replace(/\r/g,"").length),s.collapse(!0),s.moveStart("character",r.start),s.moveEnd("character",r.end-r.start),s.select()}else t.setSelectionRange&&t.setSelectionRange(r.start,r.end)}catch(a){}},getText:function(t){return n(t).text},_caretMode:function(t){switch(t=t||"keep",0==t&&(t="end"),t){case"keep":case"start":case"end":break;default:t="keep"}return t},replace:function(e,r,s){var a=n(e),c=e.value,o=t(e).scrollTop(),i={start:a.start,end:a.start+r.length};e.value=c.substr(0,a.start)+r+c.substr(a.end),t(e).scrollTop(o),this.setPos(e,i,s)},insertBefore:function(e,r,s){var a=n(e),c=e.value,o=t(e).scrollTop(),i={start:a.start+r.length,end:a.end+r.length};e.value=c.substr(0,a.start)+r+c.substr(a.start),t(e).scrollTop(o),this.setPos(e,i,s)},insertAfter:function(e,r,s){var a=n(e),c=e.value,o=t(e).scrollTop(),i={start:a.start,end:a.end};e.value=c.substr(0,a.end)+r+c.substr(a.end),t(e).scrollTop(o),this.setPos(e,i,s)}};t.extend({selection:function(n){var s="text"==(n||"text").toLowerCase();try{if(e.getSelection){if(s)return e.getSelection().toString();var a,c=e.getSelection();return c.getRangeAt?a=c.getRangeAt(0):(a=r.createRange(),a.setStart(c.anchorNode,c.anchorOffset),a.setEnd(c.focusNode,c.focusOffset)),t("<div></div>").append(a.cloneContents()).html()}if(r.selection)return s?r.selection.createRange().text:r.selection.createRange().htmlText}catch(o){}return""}}),t.fn.extend({selection:function(t,e){switch(e=e||{},t){case"getPos":return s.getPos(this[0]);case"setPos":return this.each(function(){s.setPos(this,e)});case"replace":return this.each(function(){s.replace(this,e.text,e.caret)});case"insert":return this.each(function(){"before"==e.mode?s.insertBefore(this,e.text,e.caret):s.insertAfter(this,e.text,e.caret)});case"get":default:return s.getText(this[0])}return this}})}(jQuery,window,window.document);
//]]></script>
<script type='text/javascript'>//<![CDATA[
jQuery.fn.putCursorAtEnd=function(){return this.each(function(){var t=$(this),e=this;if(t.is(":focus")||t.focus(),e.setSelectionRange){var n=2*t.val().length;setTimeout(function(){e.setSelectionRange(n,n)},1)}else t.val(t.val());this.scrollTop=999999})};//]]></script>
