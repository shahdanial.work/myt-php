<!DOCTYPE html>
<html><head><?php if($this->session->has_userdata('draft_id') && user_info(current_user_id())['user_role'] == 'special admin'){
  echo '<!-- DRAFT id is: $this->session->userdata(\'post_id\') == '. $this->session->userdata('post_id') .'-->';
} ?><?php $this->load->view('v_tulis/v_head', $table); ?>
</head><body id='main-blog' class="tulis">
  <div class='hfeed section' id='blog-content'><div class='widget Blog' data-version='1' id='Blog1'>

    <div class="wysiwyg-editor">

      <?php $this->load->view('v_tulis/v_control', $table); ?>
      <?php /* OPEN FORM */
        $attributes = array('class' => 'main-form');
      echo form_open_multipart('submit', $attributes); ?>
      <table class='left-right'>
        <tbody>
          <tr>
            <td class='left'>
              <?php $this->load->view('v_tulis/v_left', $table); ?>
            </td>
            <td class='right'>
              <?php $this->load->view('v_tulis/v_right', $table); ?>
            </td>
          </tr>
        </tbody>
      </table>
      <?php /* CLOSE FORM */ echo form_close();  ?>
      <?php /* OPEN FORM */
        $attributes = array('class' => 'draft-form', 'id' => 'draft-form');
      echo form_open_multipart('save-draft', $attributes); ?>
        <div id="draft_hidden" class="content_wtf">
          <?php
          #category.
          if(is_numeric($table['category_id'])){
            $curr_cat = $this->db->conn_id->prepare("SELECT domain FROM category_tbl WHERE id = :id");
            $curr_cat->execute(array(':id'=>$table['category_id']));
            if($curr_cat){
              $cat_name = $curr_cat->fetch(PDO::FETCH_COLUMN);
            } else {
              $cat_name = '';
            }
            echo '<input id="category_draft" name="category" type="hidden" value="'. $cat_name .'" />';
          }
          #title.
          echo '<input id="title_draft" name="title" type="hidden" value="'.$table['title'].'" />';
          #content.
          echo '<textarea id="content_draft" name="content">'.$table['content'].'</textarea>';
          #meta_title.
          echo '<input id="meta_title_draft" name="meta_title" type="hidden" value="'.$table['meta_title'].'" />';
          #meta_description.
          echo '<input id="meta_description_draft" name="meta_description" type="hidden" value="'.$table['meta_description'].'" />';
          #first_img
          echo '<input id="first_img_draft" name="first_img" type="hidden" value="'.$table['first_img'].'" />';
          #button? lol.
          echo '<button type="sumbit">submit</button>';
           ?>
        </div>
      <?php /* CLOSE FORM */ echo form_close();  ?>
      <?php echo '</div>'; #end wysiwyg-editor ?>
      <?php $this->load->view('v_tulis/v_preview', $table); ?>
      <div class='modal'><div class='popup'>
        <div class='popup-header'><a class='popup-close'>&#215;</a><h4></h4></div>
        <?php $this->load->view('v_tulis/v_popup/v_image', $table); ?>
        <?php $this->load->view('v_tulis/v_popup/v_video', $table); ?>
        <?php $this->load->view('v_tulis/v_popup/v_link', $table); ?>
        <?php $this->load->view('v_tulis/v_popup/v_category', $table); ?>
        <?php $this->load->view('v_tulis/v_popup/v_related', $table); ?>
        <?php $this->load->view('v_tulis/v_popup/v_button', $table); ?>
        <?php $this->load->view('v_tulis/v_popup/v_table', $table); ?>
        <?php $this->load->view('v_tulis/v_popup/v_message', $table); ?>
        <?php $this->load->view('v_tulis/v_popup/v_qoute', $table); ?>
        <?php $this->load->view('v_tulis/v_popup/v_login', $table); ?>
        <!-- WTF -->
      </div>
    </div>

    <div id="wings"><script type="text/javascript" src="<?php echo base_url(). "js/tulis.js" ?>"></script></div>


  </div></div>



</body></html>
