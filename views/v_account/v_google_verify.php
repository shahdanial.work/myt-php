<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
  <title>Verify Google Email Registered</title>
   <link rel="stylesheet" type="text/css" href="<?php echo site_url('css/general.css') ?>"/>
</head>
<body class="box_form">
<div class="login_page_message">
  <div class="user_box">
    <!--START TABLE --><div class="table-wrap"><table class="content-table vertical">

  <thead>
   <tr>
     <th>Label</th>
     <th>Detail</th>
   </tr>
  </thead>

  <tbody>
   <tr>
     <td>Nama</td>
     <td><?php echo $row['nama'] ?></td>
   </tr>
   <tr>
     <td>Nama Pena</td>
     <td><?php echo $row['namapena'] ?></td>
   </tr>
   <tr>
     <td>Email</td>
     <td><?php echo $row['email'] ?></td>
   </tr>
   <tr>
     <td>Tarikh daftar</td>
     <td><?php echo $row['reg_time'] ?></td>
   </tr>
  </tbody>



  </table></div><!-- END TABLE -->
  </div>
  <br/>
  <?php echo form_open_multipart('login/google_verify'); ?>
  <blockquote>Email ini telah didaftar di Malayatimes. Diatas berikut adalah details yang telah didaftarkan. Sahkan melalui jawapan dibawah:</blockquote>
  <br/>
  <input type="radio" name="jawapan" value="yes"/><label>Ya, saya yang mendaftarkannya menggunakan email ini.</label><br/>
  <input type="radio" name="jawapan" value="no"/><label>Tidak, email saya di salah guna oleh seseorang.</label><br/>
  <br/>
  <input class="enter-button" type="submit" value="CONTINUE"/>
  <?php echo form_close(); ?>
</div>
</body>
</html>
