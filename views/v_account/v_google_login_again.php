<?php
defined('BASEPATH') OR exit('No direct script access allowed');
echo'<!DOCTYPE html>
<html><head><meta content="text/html; charset=UTF-8" http-equiv="Content-Type"><meta content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" name="viewport"><title>Become our Team</title>

<style>
html{
   min-height:100%;background:#fff;
}
body{
   margin:0;background:#fff;color:#e5e5e5;
   text-align: center;
   padding: 0;
}
body *{overflow:hidden;max-width:100%}
.login_page_width{
   width: 100%;
   display: inline-block;
   position: absolute;
   top: 35%;
   left: 0;
   text-align: center;
}
.login_page_width h1{margin:0 auto 20px}.login_page_width .enter-button,.login_page_message .enter-button{color:#fff;cursor:pointer;z-index:0;text-shadow:1px 1px 0 #487b2c;background-color:#6e593a;border:.188em solid #4e3f29;border-top-color:#8a704a;border-left-color:#887354;border-radius:3px;-moz-border-radius:3px;margin:1em 0 0}.content-table thead th{background:#333;color:#fff;padding:5px;font-weight:600}.warning{color:#bb5656}.suggest a{color:#69ae44;text-decoration:none;opacity:.9}.suggest a:hover{opacity:.8}.suggest{margin:.5em 0 0;font-size:13.3333px}.gold_color{color:#887354}.login_page_width input:hover{opacity:.8}.social_login{display:inline-block;font-family:monospace;text-transform:uppercase;font-weight:600;line-height:.7;font-size:16px;padding:0;border-radius:2px}.google-login{background-color:#db4437;color:#fff}.social_login>*{display:inline-block;line-height:1;padding:10px}.social_login .button_text{word-spacing:-3px}.social_login .button_logo{border-right:1px solid #fff}.social_login .button_logo i{width:21px;vertical-align:bottom}.inline_checkbox input{margin:0 5px 0 0;vertical-align:text-bottom}


.login-with-google {
    background-color: #fff;
    border: 1px solid #bababa;
    #bababa:
    -moz-flex;
    font-size: 15px;
    padding: 10px 10px 10px 40px;
    color: #757575;
    box-sizing: border-box;
    text-align: left;
    display: inline-block;
    width: 280px;
    line-height: 0;
}

.login-with-google:hover {
   background-color: #f3f3f3
}

.text {
    margin-left: 10px;
    display: inline-block;
    line-height: 1;
}
</style>
<link href="https://media.malayatimes.com/css/font-awesome.min.css" rel="stylesheet"/></head><body class="box_form">

<div class="login_page_width">

<h1><img src="https://media.malayatimes.com/assets/image/malayatimes.png" alt="Malayatimes Login"></h1>

<a rel="nofollow" href="login-again/google" class="login-with-google"><svg class="ico-google-login" viewBox="0 0 18 18" width="18" height="18" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><path d="M0 0h18v18H0z"></path><path fill="#EA4335" fill-rule="nonzero" d="M9 4.167c1.18 0 2.237.406 3.07 1.2l2.283-2.284C12.967 1.793 11.157 1 9 1a7.996 7.996 0 0 0-7.147 4.407l2.66 2.063c.63-1.897 2.4-3.303 4.487-3.303z"></path><path fill="#4285F4" fill-rule="nonzero" d="M16.66 9.183c0-.523-.05-1.03-.127-1.516H9v3.006h4.313a3.718 3.718 0 0 1-1.593 2.394l2.577 2c1.503-1.394 2.363-3.454 2.363-5.884z"></path><path fill="#FBBC05" fill-rule="nonzero" d="M4.51 10.53A4.86 4.86 0 0 1 4.257 9c0-.533.09-1.047.253-1.53L1.85 5.407A7.971 7.971 0 0 0 1 9a7.93 7.93 0 0 0 .853 3.593L4.51 10.53z"></path><path fill="#34A853" fill-rule="nonzero" d="M9 17c2.16 0 3.977-.71 5.297-1.937l-2.577-2c-.717.484-1.64.767-2.72.767-2.087 0-3.857-1.407-4.49-3.303L1.85 12.59A8.002 8.002 0 0 0 9 17z"></path><path d="M1 1h16v16H1z"></path></g></svg><span class="text">Sign in with Google</span></a></div></body></html>';
