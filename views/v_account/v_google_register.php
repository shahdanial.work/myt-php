<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
     <meta content="text/html; charset=UTF-8" http-equiv="Content-Type"/>
     <meta content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
     <title>Become our Team</title>
     <style>
     body {
          margin: 0;
          background: #000;
          color: #e5e5e5;
     }

     body * {
          overflow: hidden;
          max-width: 100%;
     }

     .box_form {
          text-align: center;
          padding: 110px 10px 20px;
     }

     .login_page_width {
          width: 300px;
          display: inline-block;
     }

     .login_page_width h1 {
          margin: 0;
     }


     .login_page_width .enter-button, .login_page_message .enter-button {
          color: #ffffff;
          cursor: pointer;
          z-index: 0;
          text-shadow: 1px 1px 0 #487b2c;
          background-color: #6e593a;
          border: .188em solid #4e3f29;
          border-top-color: #8a704a;
          border-left-color: #887354;
          border-radius: 3px;
          -moz-border-radius: 3px;
          margin: 1em 0 0;
     }

     .content-table thead th {
          background: #333;
          color: #fff;
          padding: 5px;
          font-weight: 600;
     }

     .warning {
          color: #bb5656;
     }

     .suggest a {
          color: #69ae44;
          text-decoration: none;
          opacity:.9;
     }

     .suggest a:hover{
          opacity:.8;
     }

     .suggest {
          margin: .5em 0 0;
          font-size:13.3333px;
     }

     .gold_color{
          color: #887354;
     }

     .login_page_width input:hover {
          opacity: .8;
     }

     .social_login {
          display: inline-block;
          font-family: monospace;
          text-transform: uppercase;
          font-weight: 600;
          line-height: .7;
          font-size: 16px;
          padding: 0;
          border-radius: 2px;
     }

     .google-login {
          background-color: #db4437;
          color: #fff;
     }

     .social_login > * {
          display: inline-block;
          line-height: 1;
          padding: 10px;
     }

     .social_login .button_text {
          word-spacing: -3px;
     }

     .social_login .button_logo {
          border-right: 1px solid #fff;
     }

     .social_login .button_logo i {
          width: 21px;
          vertical-align: bottom;
     }
     </style>
     <link href="http://media.malayatimes.com/css/font-awesome.min.css" rel="stylesheet"/>
</head>
<body class="box_form">

     <div class="login_page_width">

          <h1><img src="http://media.malayatimes.com/assets/image/malayatimes-form.png" alt="Malayatimes Login"></h1>
          <br/><br/>
          <a rel="nofollow" href="login/google" class="social_login google-login">
               <span class="button_logo">
                    <i aria-hidden="true" class="fa fa-google-plus"></i>
               </span>
               <span class="button_text">REGISTER USING GOOGLE</span>
          </a>
     </div>

</body>
</html>
