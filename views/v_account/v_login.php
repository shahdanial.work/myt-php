<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 ?><!DOCTYPE html>
 <head>
   <meta content='width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no' name='viewport'/>
   <title>
     Views > v_login.java
   </title>

   <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/general.css" ?>">
  <script src='<?php echo base_url() ?>js/jquery183.js' type='text/javascript'></script>
  <script src='<?php echo base_url() ?>js/ajax310.js' type='text/javascript'></script>
<!--  <script type="text/javascript">
  $(function(){
    var href = document.referrer;

if (href.indexOf("malayatimes.com") > 0){
   var refer = href;
 } else {
   var refer = "<?php echo base_url() ?>/dashboard";
 }
  document.getElementById('from').value = refer;
  });
  </script> -->
 </head>
 <body class="box_form">
     <div class="login_page_width">
     <h1><img src="<?php echo site_url('assets/image/malayatimes-form.png') ?>" alt="Malayatimes Login"/></h1>

      <?php
   // Cetak jika ada notifikasi
      if($this->session->flashdata('sukses')) {
           echo '<small class="warning">'.$this->session->flashdata('sukses').'</small>';
      } else {
           echo '<small class="warning">Login function ditutup sementara</small>';
      }
      ?>

<a href="<?php echo base_url();?>login/google">Login with Google</a>

      <?php echo form_open('login');
//echo form_open('shah-disable');
?>


      <p>
          <input class="email_input" type="text" name="email" value="<?php echo set_value('email'); ?>" placeholder="yourmail@gmail.com"/>
      </p>
      <?php echo form_error('email', '<small class="warning">', '</small>'); ?>


      <p>
          <input class="password_input" type="password" name="password" value="<?php echo set_value('password'); ?>" placeholder="Your password" autocomplete="off"/>
      </p>
      <?php echo form_error('password', '<small class="warning">', '</small>'); ?>

      <?php
      if (isset($_GET['redirect']) && !empty($_GET['redirect'])){
        $redirect = $_GET['redirect'];
      }else{
        $redirect = site_url('dashboard');
      }
      echo '<input id="from" type="hidden" name="redirect" value="'.$redirect.'"/>';
      ?>

      <p>
           <input class="enter-button" type="submit" name="btnSubmit" value="ENTER" />
      </p>

      <?php echo form_close();?>

      <p class="suggest">tiada akaun? <?php echo anchor(site_url().'register','Register sekarang'); ?></p>
     </div>
 </body>
 </html>
