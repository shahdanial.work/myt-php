 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 ?><!DOCTYPE html>
 <html>
 <head>
     <meta charset="UTF-8">
     <title>
         Views > Utama.java
     </title>
     <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/general.css" ?>">
 </head>
 <body>
     <div class="public_col">
         <header id="header"><h1 class="site-title">Malayatimes Writters</h1><?php echo anchor('login','Sign in', array('class' => 'signin gold_color')); ?></header>
         
         <h2 class="subtitle">Tawaran sertai team kami</h2>
         <h3>Benefit</h3>
             <ol>
                          <li>Menjana pendapatan lumayan melalui pengiklanan di posts/entri-entri anda</li>
                          <li>Potensi <b>bayaran yang sangat tinggi</b> diatas segala usaha anda sendiri menulis</li>
                          <li>Anda menulis di sebuah portal terbesar di Malaysia yang mempunyai trafik tinggi, lebih mudah boost income berbanding website sendiri</li>
                          <li>Anda tidak perlu pandai atau membuang masa untuk pengaturcaraan sebuah Website yang rumit</li>
                          <li>Setiap artikel-artikel anda mampu menjanakan pendapatan setiap bulan! selagi ada pembacanya</li>
                          <li>Kerja dari rumah sebenar tanpa risiko scammer online (penipu)</li>
                          <li>Sekiranya anda menulis bagus dan berkualiti serta rajin, anda akan menerima pelbagai tawaran dari syarikat (antaranya: kenaikan percent Share, featured dan sebagainya)</li>
                          <li>Sekiranya anda berminat dan berbakat menjadi wartawan atau penulis, inilah masanya untuk serlahkan bakat</li>
                          <li>Inilah kerjaya part time atau full time yang paling berpotensi untuk dilaksanakan di mana jua tanpa kekangan masa dan tekanan!</li>                          
             </ol>
                          <br/>
                          <h3>Kesimpulan</h3>
                          <ol>
                              <li>Anda bebas untuk berusaha lebih untuk income yang lebih</li>
                              <li>Atau busy dan melakukan potensi lain bila-bila masa tanpa dikekang</li>
                              <li>Artikel yang anda tulis <b>sekali</b>, akan memberikan bayaran berpanjangan selagi ada pembacanya. Bayangkan jika anda telah mengumpul banyak artikel</li>
                          </ol>
                          <h3>Tips jana income lebih</h3>
                          <ol>
                          <li>Sila menulis dengan banyak, copy paste dibenarkan, tapi sila olah setiap perenggan dan gunakan gambar lain</li>
                          <li>Tulis artikel yang menjadi minat natizen untuk viral</li>
                          <li>Tulis artikel yang berkaitan dengan kehangatan dan isu semasa</li>
                          <li>Tulis artikel yang berinformasi dan <b>sentiasa</b> di cari di Google, misalnya <i>Tempat menarik di Selangor</i></li>
                          <li>Share artikel anda dibanyak group dan pelbagai social media</li>
                          </ol>
                          <br/>
                          <h3>Terma dan syarat</h3>
                          <ol>
                          <li>Setiap artikel yang anda submit, akan disaring dahulu oleh department syarikat sebelum diterbitkan</li>
                          <li>Jangan membuat artikel yang mempunyai keyword dan usur 18sx, 18sg, fitnah dan sampah</li>
                          <li>Sila olah dan gunakan bahasa yang formal</li>
                          <li>Income dari artikel anda dibayar dalam frekunsi bulan demi bulan tanpa minimum cashout!</li>
                          </ol>
                          
         <br/>
         <p class="visitor_action"> 
         <span class="register-button the-button">
     <?php echo anchor('register','Register now'); ?>
         </span>
         </p>      
     </div>
 </body>
 </html>