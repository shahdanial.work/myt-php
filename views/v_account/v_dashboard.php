<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<head>
  <meta content='width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no' name='viewport'/>
  <title>Dashboard</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/tulis.css" ?>"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/element.css" ?>"/>
  <link href="<?php echo site_url('css/font-awesome.min.css') ?>" rel="stylesheet">

  <script src='<?php echo base_url() ?>js/jquery183.js' type='text/javascript'></script>

  <script src='<?php echo base_url() ?>js/ajax310.js' type='text/javascript'></script>

  <script type="text/javascript" src="<?php echo base_url(). "js/element.js" ?>"></script>
</head>
<body class="dashboard profile">
  <?php $this->load->view('v_element/v_nav'); ?>

  <div id="the-wrapper"><div id="wrapper-the">  <div class="kotak kiri">
    <div class="kekotak the-box">
      <h4>News and Tips</h4>

      <div class="isi">

  <?php
  // $url="http://blog.malayatimes.com/?m=1";
  // $html = file_get_contents($url);
  // $doc = new DOMDocument();
  // @$doc->loadHTML($html);
  //
  // $readmores = $doc->getElementsByTagName('article');
  // $i = 0;
  // $thelink = array();
  // foreach ($readmores as $readmore) {
  //   $i = $i+1;
  //   $link = $readmore->getAttribute('data-href');
  //   $title = $readmore->getAttribute('data-title');
  //
  //   $thelink[] = $link;
  //   $thetitle[] = $title;
  //
  //   if($i > 9){
  //     $nnn = $i;
  //   } else {
  //     $nnn = $i . ' ';
  //   }
  //
  //   $feedlist = '<div class="kotak-list">'. $nnn .'. <a class="title" href="'. str_replace('?m=1', '', $thelink[0]) .'">'. $thetitle[0] .'</a></div>';
  //
  // }
  //
  // echo $feedlist;
  // echo '   <a href="'. str_replace('?m=1', '', $url) .'" target="_blank">view all tips</a>';
  ?>

</div>
</div>
</div>

<div class="kotak kanan">
  <div class="kekotak the-box">
     <h4>Write and Earn Money</h4>
    <div class="isi">
    </div>
  </div>
</div>


<div class="kotak kiri">
  <div class="kekotak the-box">
    <h4>Action required</h4>
    <div class="isi">

    </div>
  </div>
</div>

<div class="kotak kanan">
  <div class="kekotak the-box">

    <h4>Reputation and Income</h4>

    <div class="badan">
      <div class="isi">
        <?php
        $this->load->helper('h_user_helper');
        $level_percent = author_income(current_user_id())['reputation_level'];
        $current_reputation = author_income(current_user_id())['current_reputation'];
        $bar_percent = author_income(current_user_id())['bar_percent'];
        $bonus_percent = author_income(current_user_id())['bonus_percent'];
        $bonus_amount = author_income(current_user_id())['bonus_amount'];
        $income_amount = author_income(current_user_id())['income_amount'];
        $taraf_user = author_income(current_user_id())['taraf_user'];
        $total_income = author_income(current_user_id())['total_income'];

        echo '
        <div class="rows">
        <b>Reputation bulan ini: </b> <span class="green"> '. $current_reputation .'</span>
        </div>

        <div class="rows">
        <b>Bonus tambahan: </b> <span class="green">'. $bonus_percent .'%</span>
        </div>

        <div class="rows">
        <b>Revenue: </b> <span class="green">RM'. $income_amount .'</span>
        </div>

        <div class="rows">
        <b>Bonus: </b> <span class="green">RM'. $bonus_amount .'</span>
        </div>

        <div class="rows">
        <b>Total income: </b> <span class="green">RM'. $total_income .'</span>
        </div>

        <div class="rows text-align-right">
        <a href="'.site_url('income').'">more details..</a>
        </div>
        ';

        ?>
      </div>
    </div>

  </div>
</div>

</div></div>
<?php $this->load->view('v_element/v_sidebar'); ?>
</body>
</html>
