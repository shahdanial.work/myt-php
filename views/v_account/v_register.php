<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 ?><!DOCTYPE html>
 <head>
   <meta charset="UTF-8">
   <title>
     Become our team
   </title>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/general.css" ?>">
 </head>
 <body class="box_form">
     <div class="login_page_width">

         <h1><img src="<?php echo site_url('assets/image/malayatimes-form.png') ?>" alt="Malayatimes Login"/></h1>

     <?php echo form_open('register');?>

     <p>
       <?php echo form_error('nama', '<small class="warning">', '</small>'); ?>
       <input type="text" name="nama" value="<?php if(isset($data['nama'])){echo $data['nama'];}else{echo set_value('nama');} ?>" placeholder="nama penuh (tidak dipaparkan)"/>
     </p>

     <p>
       <?php echo form_error('namapena', '<small class="warning">', '</small>'); if(isset($err['namapena'])){echo '<small class="warning">'.$err['namapena'].'</small>';} ?>
       <input type="text" name="namapena" value="<?php if(isset($data['namapena'])){echo $data['namapena'];}else{echo set_value('namapena');} ?>" placeholder="nama pena"/>
     </p>

     <p>
       <?php echo form_error('email', '<small class="warning">', '</small>'); if(isset($err['email'])){echo '<small class="warning">'.$err['email'].'</small>';}  ?>
       <input type="text" name="email" value="<?php if(isset($data['email'])){echo $data['email'];}else{echo set_value('email');} ?>" placeholder="yourmail@gmail.com"/>
     </p>

     <p>
       <?php echo form_error('password', '<small class="warning">', '</small>'); ?>
       <input type="password" name="password" value="<?php if(isset($data['password'])){echo $data['password'];}else{echo set_value('password');} ?>" placeholder="password"/>
     </p>

     <p>
       <?php echo form_error('password_conf', '<small class="warning">', '</small>'); ?>
       <input type="password" name="password_conf" value="<?php if(isset($data['password'])){echo $data['password'];}else{echo set_value('password_conf');} ?>" placeholder="ulang password"/>
     </p>

     <p><input class="enter-button" type="submit" name="btnSubmit" value="DAFTAR" /></p>


     <?php echo form_close();?>

     <p class="left_right">
     <span class="suggest left"><?php echo anchor(site_url(),'← BENEFIT'); ?></span>
     <span class="suggest right"><?php echo anchor(site_url().'login','SIGN IN →'); ?></span>
     </p>
     </div>
 </body>
 </html>
