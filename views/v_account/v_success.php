<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 ?><!DOCTYPE html>  
 <head>
   <meta charset="UTF-8">
   <title>
     Views > v_success.java
   </title>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/general.css" ?>">
 </head>
 <body>
     <div class='box'>
   <?php echo $message; ?>
     <div class='box_nav'>
    <span class='the-button'><?php echo anchor('login','Login Sekarang'); ?></span>
     </div>
     </div>
 </body>
 </html>