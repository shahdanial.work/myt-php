<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 ?><!DOCTYPE html>
 <head>
   <meta charset="UTF-8">
   <title>
     Views > v_register.java
   </title>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/general.css" ?>">
 </head>
 <body class="box_form">
     <div class="login_page_width">

         <h1><img src="https://3.bp.blogspot.com/-Gij5Kg-MGps/Vy4nlSr5JqI/AAAAAAAAAUo/r1-CqeV25iIm4rJXiAELiOrmOfwT-8G0wCLcB/s1600/Malaya-Times14.png" alt="Malayatimes Login"/></h1>

     <?php echo form_open('register');?>

     <p><input type="text" name="nama" value="<?php echo set_value('nama'); ?>" placeholder="nama penuh (tidak dipaparkan)"/></p>
     <?php echo form_error('nama', '<small class="warning">', '</small>'); ?>

     <p><input type="text" name="namapena" value="<?php echo set_value('namapena'); ?>" placeholder="nama pena"/></p>
     <?php echo form_error('namapena', '<small class="warning">', '</small>'); ?>

     <p><input type="text" name="email" value="<?php echo set_value('email'); ?>" placeholder="yourmail@gmail.com"/></p>
     <?php echo form_error('email', '<small class="warning">', '</small>'); ?>

     <p><input type="password" name="password" value="<?php echo set_value('password'); ?>" placeholder="password"/></p>
     <?php echo form_error('password', '<small class="warning">', '</small>'); ?>

     <p><input type="password" name="password_conf" value="<?php echo set_value('password_conf'); ?>" placeholder="ulang password"/></p>
     <?php echo form_error('password_conf', '<small class="warning">', '</small>'); ?>

     <p><input class="enter-button" type="submit" name="btnSubmit" value="DAFTAR" /></p>


     <?php echo form_close();?>

     <p class="left_right">
     <span class="suggest left"><?php echo anchor(site_url(),'← BENEFIT'); ?></span>
     <span class="suggest right"><?php echo anchor(site_url().'login','SIGN IN →'); ?></span>
     </p>
     </div>
 </body>
 </html>
