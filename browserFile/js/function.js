function beforeConvert(){

  // before we run htmlConvert(), lets remove the new lines

  $('.table-wrap').each(function(){
    $('.table-wrap br').remove();
    var tttt = $(this).html();
    $(this).html( tttt.replace(/>\s*\</g, '><') );
  });
  //$('.gallery br').remove(); // remove br
  $('.gallery br').addClass('galleryBr'); //br yg browser masukkan kena buat gini Wadepis
  $('.table-wrap br').remove();

  $('.gallery').each(function(){
    var WWWWWW = $(this).html();
    $(this).html( WWWWWW.replace(/\n/g, '') );
  });
  $('.gallery br').remove();
}

function htmlConvert(x){

  // start the function below

  var theCode = x[0].toString();
  var theCommand = x[1].toString();

  //  $('.gallery br').remove();
  //  $('.gallery br').addClass('.galleryBr');
  //  $('.galleryBr').remove();
  //  $('.table-wrap br').remove();

  beforeConvert();

  var withBr = theCode
  .replace(/<br class="galleryBr">|<br class="galleryBr"\/>/g, '')
  .replace(/\n|\r|\r\n/g, '<br/>') //buang jarak
  .replace(/<br>/g, '<br/>') // default to <br/>
  .replace(/(<br\/?>){3,}/gi, '<br/><br/>') // br lebih dari 3 boleh blah
  //.replace(/<br\/><br\/>/g, '\n\n<br/><br/>\nn') // add jarak pada double br
  .replace(/<br\/>/g, '\n\n<br/>\n\n') // add jarak single br
  .replace(/<br\/>\r\n\r\n\r\n\r\n<br\/>|<br\/>\n\n\n\n<br\/>|<br\/>\r\r\r\r<br\/>/g, '<br/>\n<br/>') // remove jarak between 2 br
  //.replace(/<!--/g, '\n<!--')
  //.replace(/-->/g, '-->\n')
  .replace(/(\r\n|\n|\r){3,}/gi, '\n\n') // line lebih dari 3 boleh blah

  //figure
  .replace(/<figure class="gallery">\s*\<!--start gambar/g, '<figure class="gallery">\n\n<!--start gambar')
  .replace(/of gallery-->\s*\<\/figure>/g, 'of gallery-->\n\n<\/figure>')
  .replace(/<!--start gambar of gallery-->\n\n/g, '<!--start gambar of gallery-->\n')
  .replace(/\n\n<!--end gambar of gallery-->/g, '\n<!--end gambar of gallery-->')
  .replace(/<!--end gambar of gallery-->\s*\<!--start gambar of gallery-->/g, '<!--end gambar of gallery-->\n\n<!--start gambar of gallery-->')
  .replace(/<!--start gambar of gallery-->/g, '<!--start gambar of gallery-->\n')
  .replace(/<!--end gambar of gallery-->/g, '\n<!--end gambar of gallery-->')

  //table
  .replace(/<thead>/g, '\n\n<thead>')
  .replace(/<\/thead>/g, '\n</thead>\n\n')
  .replace(/<tbody>/g, '<tbody>')
  .replace(/<\/tbody>/g, '\n</tbody>\n\n')

  .replace(/<tr>/g, '\n <tr>\n')
  .replace(/<\/tr>/g, ' </tr>')
  .replace(/<th>/g, '   <th>')
  .replace(/<td>/g, '   <td>')
  .replace(/<\/th>/g, '</th>\n')
  .replace(/<\/td>/g, '</td>\n')
  ;

  var withoutBr = withBr
  .replace(/<br>|<br\/>|<br \/>/g, '')
  .replace(/(\r\n|\n|\r){3,}/gi, '\n\n') // line lebih dari 3 boleh blah ke 3 shj

  //gallery
  .replace(/<!--start gambar of gallery-->\n\n/g, '<!--start gambar of gallery-->\n')
  .replace(/\n\n<!--end gambar of gallery-->/g, '\n<!--end gambar of gallery-->')

  //table
  .replace(/<div class="table-wrap">\s*\<table class="content-table vertical">/g, '<div class="table-wrap"><table class="content-table vertical">')
  .replace(/<thead>\s*\<tr>/g, '<thead>\n <tr>')
  .replace(/<\/th>\s*\<th>/g, '</th>\n         <th>')
  .replace(/<\/th>\s*\<\/tr>\s*\<\/thead>\s*\<tbody>\s*\<tr>\s*\<td>/g, '</th>\n </tr>\n</thead>\n\n<tbody>\n <tr>\n   <td>')
  .replace(/<\/td>\n\n/g, '</td>\n')
  .replace(/<\/tr>\s*\<tr>/g, '</tr>\n <tr>')
  .replace(/<tr>\n\n/g, '<tr>\n')
  .replace(/<\/td>\s*\<\/tr>/g, '</td>\n </tr>')
  .replace(/         <td>/g, '   <td>')
  .replace(/         <th>/g, '   <th>')
  ;

  var toHtml = withBr;

  if(theCommand == 'toBr'){
    return withBr;
  }
  if(theCommand == 'noBr'){
    return withoutBr;
  }
  if(theCommand == 'toHtml'){
    return toHtml;
  }

  //  $('.gallery br').remove();
  //  $('.gallery br').addClass('galleryBr');
  //  $('.table-wrap br').remove();

}
var title_kuar;
function time_out_title(){
  clearTimeout(title_kuar);
   title_kuar = setTimeout(function() {
       auto_draft();
   },5000);
}

var timeout;
var masakuar;
function toHtml(){
  clearTimeout(masakuar);
  masakuar = setTimeout(function(){
    $('.my-content, #content_draft').html( htmlConvert([$('#post-editor').val(), 'toHtml']) ),
    $('.gallery br').remove(),
    $('.content_wtf, #content_draft').val( htmlConvert([$('.my-content').html(), 'toHtml']) )
  }, 1000);

  clearTimeout(timeout);
   timeout = setTimeout(function() {
     $('.gallery br').remove(),
     $('.content_wtf, #content_draft').val( htmlConvert([$('.my-content').html(), 'toHtml']) ),
       auto_draft();
   },5000);


  $('textarea').each(function () {

    $('.gallery br').hide();
    $('.table-wrap br').remove();

    this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
    this.style.height = 'auto';
    this.style.height = (this.scrollHeight) + 'px';
  })

  //  $('.gallery br').remove();
  //  $('.gallery br').addClass('galleryBr');
  //  $('.table-wrap br').remove();
}

function filter_char(data){
  return data.replace(/[&\/\\#,+()$~%.!\@\^\_\=\-\[\]\"\;\`\'\|:*?<>{}]/g, '');
}

function sendLabel(valThis){
  //console.log(valThis);
  var tag_val = filter_char(valThis);
  var tag_id = 'tag_value_'+ tag_val+'_id';
  var tag_id = tag_id.replace(/ /g, '_');
  if(tag_val && !document.getElementById(tag_id)){
    $('.main-form').prepend('<input value="'+ tag_val +'" name="tag[]" id="'+tag_id+'" readonly="true" type="hidden"/>');
    $('#draft_hidden').prepend('<input value="'+ tag_val +'" name="tag[]" id="draft_'+tag_id+'" readonly="true" type="hidden"/>');

    var selected = '<span class="selected-label" title="click to remove" data-val="'+tag_val+'">'+tag_val+'<i class="fa fa-times" aria-hidden="true"></i></span>';
    $('#labelresult').prepend(selected);
    $('.label-list').hide();
    $('#label-search').val(null);
    auto_draft();
  }  else {
    alert('Dilarang memasukkan lebih dari satu tag yang sama.');
  }
}

function paras(){
   $('.img_wrpr').each(function(i, wrap){
      var that = $(this);
      $(wrap).children('.data_img').faceDetection({
         complete: function (faces) {
             console.log(faces);

            if(faces){
               faces.forEach(function(face) {
                  console.log(face);
                  var style = "left:"+face.offsetX+"px\;top:"+face.offsetY+"px\;width:"+face.width+"px;height:"+face.height+"px\;";
                  $(that).prepend('<div class="faces" style="'+style+'";></div>');
               });
            }

         }
      });
   });
}

function hideList(){
  $('.label-list').hide();
}

function field_draft(from, thing){
  if(from == 'category'){
    $('#category_draft').val(thing);
  } else {
    $('#title_draft').val($('#post-title').val());
  }
}

function auto_draft(dddd){
  if(dddd){
    var thing = dddd.getAttribute('data-domain');
    var from = 'category';
    field_draft(from, thing);
  } else {
    field_draft();
  }
  var form = document.getElementById('draft-form');
  $.ajax({
    type: 'POST',
    url: 'http://media.malayatimes.com/save-draft',
    //data:  {"title": titleVal},
    data: new FormData(form),
    mimeType:"multipart/form-data",
    contentType: false,
    cache: false,
    processData:false,
    success: function(data){
      //$('#draft_saved').show().html(data);
      console.log(data);

    },
    error: function(error){
      console.log(error);
    }
  })
}

function manual_draft(){
  field_draft();
  var form = document.getElementById('draft-form');
  form.submit();
}

//  $(".draft-form").on('submit',(function(e) {
//
// e.preventDefault();
//
//
// });
