$(function(){

  $(document.body).on('click', '.role-list li', function(e) {

    $('.role-list').hide();
    var value = $(this).text();
    $('#role-search').val(value);
    $('.role-form button').removeAttr('disabled');

  });

  $("#open-sidebar").click(function(){

  $(this).hide();
  $("#close-sidebar").show();

  $("body").addClass("menu-active");

    var lebarWindow = $( 'body' ).outerWidth();
    var lebarSidebar = $('.sidebar').outerWidth();
    $('#the-wrapper').width( lebarWindow - lebarSidebar);

  });




  $("#close-sidebar").click(function(){

  $(this).hide();
  $("#open-sidebar").show();

  $("body").removeClass("menu-active");

    var lebarWindow = $( 'body' ).outerWidth();
    $('#the-wrapper').removeAttr('style');
    $('#the-wrapper').width( lebarWindow );

  });




$(".basetooltip").hover(function(){var t=$(this).attr("title");$(this).data("tipText",t).removeAttr("title"),$('<p class="tooltip"></p>').text(t).appendTo("body").fadeIn("slow")},function(){$(this).attr("title",$(this).data("tipText")),$(".tooltip").remove()}).mousemove(function(t){var o=t.pageX+20,e=t.pageY+10;$(".tooltip").css({top:e,left:o})});


// moderateuser page

  $(document.body).on('focus', '#role-search', function(e) {
    $('.role-list').show();
  });

  $('.user-table .rows').click(function(){

    $(this).toggleClass('checked');

    var target = $(this).find('input');

    if(target.is(':checked')){
      $(target).removeAttr('checked');
      $(target).setAttribute("checked", ""); // For IE
      $(target).removeAttribute("checked"); // For other browsers
      $(target).checked = false;
    } else {
      $(target).attr( 'checked', 'checked' );
      $(target).setAttribute("checked", "checked");
      $(target).checked = true;
    }

  });

  $('.select-all').click(function(){
      $('.user-table .rows input').attr('checked', 'checked');
      $(this).hide();
      $('.unselect-all').show();
  });

  $('.unselect-all').click(function(){
    $('.user-table .rows input').removeAttr('checked');
    $(this).hide();
    $('.select-all').show();
  });



});
