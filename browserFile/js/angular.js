(function() {



   /* POPUP AREA */
   var
   dialogModal = document.getElementsByClassName('modal')[0],
   popupClose = document.getElementsByClassName('popup-close')
   ;
   //IMAGE POPUP.
   document.getElementById('image_control').addEventListener ("click", popup_image, false);
   //VIDEO POPUP
   document.getElementById('video_control').addEventListener ("click", popup_video, false);
   //LINK POPUP
   document.getElementById('link_control').addEventListener ("click", popup_link, false);
   //RELATED POPUP
   document.getElementById('related_control').addEventListener ("click", popup_related, false);
   //BUTTON POPUP
   document.getElementById('button_control').addEventListener ("click", popup_button, false);
   //BUTTON POPUP
   document.getElementById('table_control').addEventListener ("click", popup_table, false);
   //MESSAGE POPUP
   document.getElementById('message_control').addEventListener ("click", popup_message, false);
   //QUOTE POPUP
   document.getElementById('qoute_control').addEventListener ("click", popup_quote, false);

   /* CLOSE ALL POPUP */
   for (var i = 0; i < popupClose.length; i++) {
      popupClose[i].addEventListener('click', close_all_popup, false);
   }
   function close_all_popup(){
      dialogModal.style.display = 'none';
      var popup_contents = document.getElementsByClassName('popup-content');
      for (var i = 0; i < popup_contents.length; i++) {
         var popup_content = popup_contents[i];
         popup_content.style.display = 'none';
      }
   }
   /* SHOW POPUP */
   function popup_image(){
      dialogModal.style.display = 'block';
      document.getElementById('a_a_image').style.display = 'block';
   }
   function popup_video(){
      dialogModal.style.display = 'block';
      document.getElementById('a_a_video').style.display = 'block';
   }
   function popup_link(){
      dialogModal.style.display = 'block';
      document.getElementById('a_a_link').style.display = 'block';
   }
   function popup_related(){
      dialogModal.style.display = 'block';
      document.getElementById('a_a_related').style.display = 'block';
   }
   function popup_button(){
      dialogModal.style.display = 'block';
      document.getElementById('a_a_button').style.display = 'block';
   }
   function popup_table(){
      dialogModal.style.display = 'block';
      document.getElementById('a_a_table').style.display = 'block';
   }
   function popup_message(){
      dialogModal.style.display = 'block';
      document.getElementById('a_a_message').style.display = 'block';
   }
   function popup_quote(){
      dialogModal.style.display = 'block';
      document.getElementById('a_a_qoute').style.display = 'block';
   }



   /* GAMBAR POPUP TABBER AREA. */
   var menu_tab = document.getElementById("pilihan-option").querySelectorAll('span');
   if(menu_tab){

      for (var i = 0; i < menu_tab.length; i++) {
         if (typeof window.addEventListener === 'function'){
            (function (index) {
               menu_tab[i].addEventListener('click', function(){
                  proceed_tab(index.getAttribute('data-show'), index.getAttribute('id'));
               });
            })(menu_tab[i]);
         }
      }

   }
   function proceed_tab(data_show, id){
      //reset to display none all.
      var tab_content = document.querySelectorAll('#image-option > div');//document.getElementById("image-option").querySelectorAll('div');
      if(tab_content){
         for(var i = 0; i < tab_content.length; i++) {
            var tab_id = tab_content[i].getAttribute('id');
            if(tab_id == data_show){
               tab_content[i].style.display = 'block';
            } else {
               tab_content[i].style.display = 'none';
            }
         }
      }

      //add selected
      if(menu_tab){
         for(var i = 0; i < menu_tab.length; i++) {
            var theId = menu_tab[i].getAttribute('id');
            if(theId == id){
               // add selected
               var finalid = document.getElementById(theId);
               var semiKelas = finalid.className.replace("selected", "");
               var Kelas = semiKelas + ' ' + 'selected';
               finalid.setAttribute('class', Kelas);
               //var ddd = finalid.getAttribute('data-show');
               //var idContent = document.getElementById(ddd);
            } else {
               //remove selected
               var finalid = document.getElementById(theId);
               var Kelas = finalid.className;
               finalid.setAttribute('class', Kelas.replace("selected", "") );
            }
         }
      }
   }







   /* SELECT CATEGORY AREA*/
   document.getElementById('category-search').addEventListener('click', function(){
      document.getElementsByClassName('category-list')[0].style.display = 'block';
   });
   document.getElementsByClassName('category-list')[0].addEventListener('mouseleave',function(){
      document.getElementsByClassName('category-list')[0].style.display = 'none';
   });
   var li = document.getElementsByClassName('category-list')[0].querySelectorAll('li');
   for (var i = 0; i < li.length; i++) {
      if (typeof window.addEventListener === 'function'){
         (function (index) {
            li[i].addEventListener('click', function(){
               document.getElementById('category-search').value = index.getAttribute('data-domain');
            });
         })(li[i]);
      }
   }







   //BEFORE UPLOAD. ONCHANGE INPUT
   var myinput = document.getElementById("insert-file");
   myinput.addEventListener('change', function () {
      if(this.files.length > 0){
         fetchimage();
         document.getElementById('jumlah-file').innerHTML = this.files.length;
         document.getElementsByClassName('upload-button')[0].removeAttribute("disabled");
      } else {
         document.getElementById('jumlah-file').innerHTML = '0';
         document.getElementsByClassName('upload-button')[0].setAttribute('disabled', 'disabled');
      }
   });
   function fetchimage(){
      var filelist = myinput.files || [];
      for (var i = 0; i < filelist.length; i++) {
         getBase64(filelist[i], i);
      }
   }
   function getBase64(file, ai, adui) {
      var nama = file.name;
      nama = nama.replace(/\[|\]|{|}|\?|\/|\+|\-|_|\(|\)|&|\^|%|\#|\$|\@|\!|\"|\'+/g,' ');
      nama = nama.replace(/\s\s|.jpg|.png|.jpeg|.gif|.JPG|.PNG|.JPEG|.GIF|\/+/g,' ').trim();
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function () {
         document.getElementById("before-upload").innerHTML += '<div class="img_wrpr"><img src="'+reader.result+'"  id="img_'+ai+'" class="data_img"/><div class="data_box"><label for="usr_files[text]['+ai+']">TITLE</label><p class="box_right"><input class="nama-file" name="usr_files[text]['+ai+']" type="text" value="'+nama+'" multiple="multiple" required/></p></div><!--tag--><div class="data-tag"><div class="tag-message">Tag orang, tempat atau benda yang terlibat dalam gambar</div><span class="tag-message">Maximum 5 (yang popular atau general sahaja):</span> <div class="tag-input data_box"><label>TAG</label><p class="box_right"><input name="usr_files[img_tag][1]" class="tag-gambar" placeholder="contoh: Awie, Mahathir, Bali, Adidas, KFC" multiple="multiple"></p></div></div>';

      };
   }


   //AJAX UPLOAD AREA.
   document.getElementById('ajaxupload').addEventListener('submit', function(e) {
      e.preventDefault();
      run_ajax(this);
   });

   function run_ajax(getit){
      var waitUntil = function (fn, condition, interval) {
         interval = interval || 100;
         var shell = function () {
            var timer = setInterval(
               function () {
                  var check;

                  try { check = !!(condition()); } catch (e) { check = false; }

                  if (check) {
                     clearInterval(timer);
                     delete timer;
                     fn();
                  }
               },
               interval
            );
         };
         return shell;
      };
      waitUntil(
         function(){
            after_html_changed(); // only this way will made your js working for xhr element!
         },
         function(){
            // the code that tests here... (return true if test passes; false otherwise)
            return !!(document.getElementsByClassName('pics')[0].innerHTML !== '');
         },
         50 // amout to wait between checks
      )();

      document.getElementById('jumlah-file').innerHTML = '0';
      var formData = new FormData(getit);
      var xhr      = new XMLHttpRequest();
      xhr.open('POST', 'http://media.malayatimes.com/upload/', true);
      xhr.onreadystatechange = function() {
         if ( xhr.readyState === 4 && xhr.status === 200 ) {
            console.log(xhr.responseText);
            var ddaattaa = xhr.responseText;
            if(ddaattaa){
               document.getElementsByClassName('pics')[0].innerHTML = ddaattaa + document.getElementsByClassName('pics')[0].innerHTML;
               console.log('content loaded');
            }
         }
      }
      xhr.send( formData );
      document.getElementsByClassName('upload-button')[0].setAttribute('disabled', 'disabled');
      document.getElementById('before-upload').innerHTML = '';
   }

   function after_html_changed(){
      document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/jquery.js\"><\/script><script //type=\"text/javascript\" src=\"../js/angular.js\"><\/script>';
      var brdr = document.getElementsByClassName('pics')[0].querySelectorAll('div');
      for(var i = 0; i < brdr.length; i++) {
         if (typeof window.addEventListener === 'function'){
            (function (index) {
               brdr[i].addEventListener('click', function(){
                  var Klas = index.getAttribute('class');
                  if(Klas.trim() == 'border'){//true.
                     index.setAttribute('class', 'border selected');
                  } else {
                     index.setAttribute('class', 'border');
                  }
                  var jumlah = document.getElementsByClassName('pics')[0].querySelectorAll('.selected');
                  if(jumlah){
                  if(jumlah.length > 0){
                     document.getElementById('img-insert-btn').removeAttribute('disabled');
                  } else {
                     document.getElementById('img-insert-btn').setAttribute('disabled', 'disabled');
                  }
                  }
               });
            })(brdr[i]);
         }
      }
   }


   document.getElementById('img-insert-btn').addEventListener('click', function(){
      var jumlah = document.getElementsByClassName('pics')[0].querySelectorAll('.selected img');
      if(jumlah){
      if(jumlah.length == 1){
         build_single_image(jumlah);
      } else {
         build_gallery_image(jumlah);
      }
      }
   });
   function build_single_image(jumlah){
      var title = jumlah[0].getAttribute('alt');
      var path = jumlah[0].getAttribute('src');
      var path = path.substr(path.lastIndexOf('/') + 1);
      var myhtml = '<a href="'+'//media.malyatimes.com/gambar/' + path + '" title="'+ title +'" class="single_image"><img alt="'+ title +'" src="'+'//media.malyatimes.com/gambar/' + path + '"/>';
      document.getElementById('post-editor').focus();
    var selectPastedContent = myhtml;
    insertTextAtCursor(myhtml);
    return false;
   }

   function build_gallery_image(jumlah){
      for (var i = 0; i < jumlah.length; i++) {
         var title = jumlah[i].getAttribute('alt');
         var path = jumlah[i].getAttribute('src');
         var path = path.substr(path.lastIndexOf('/') + 1);
         console.log('title ialah: ' + title);
         console.log('path ialah: ' + path);
      }
   }




})();//end
