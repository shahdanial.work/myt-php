var check = {}
function run_js(){
   setup_search()
   listen_search()
   listen_menu()
}
run_js()

function setup_search(){
   var cock=rearray_cookies(get_cookie('searched'))
   var suggestBox=document.getElementById('search-suggest')
   var searchBox=document.getElementById('search-input')
   if(searchBox) searchBox.setAttribute('autocomplete', 'off')
   check = {}
   suggestBox.innerHTML = ''
   if(cock.length>0 && suggestBox && searchBox){
      searchBox.setAttribute('autocomplete','off')
      var sv = searchBox.value + ' ';
      if(sv.trim().length>0){
         var max = 3
         var i = 0
         while(i != max && cock[i]){
            if(!check[cock[i]] && cock[i] != searchBox.value){
               var item =document.createElement('div')
               item.className = 'item recent'
               item.setAttribute('data-val', cock[i])
               item.textContent = cock[i]
               suggestBox.appendChild(item)
               check[cock[i]] = true
            } else {
               max += 1
            }
            i++
         }
         listen_suggest(suggestBox,searchBox)
      } else {
         suggest_related(searchBox)
      }
   }
}

function listen_suggest(suggestBox,searchBox){
   var item = suggestBox.getElementsByClassName('item')
   if(item.length>0){
      for (var i = 0; i < item.length; i++) {
         if (typeof window.addEventListener === 'function'){
         (function (index) {
            item[i].addEventListener('click', function(){
               searchBox.value = index.getAttribute('data-val')
               document.getElementById('form').submit()
            })
         })(item[i])
         }
      }
   }
}

var src_time; var src_time_1;
function listen_search(){
   var body = document.body
   var searchBox = document.getElementById('search-input')

   searchBox.addEventListener('focus',function(){
      var sv = searchBox.value + ' '
      if(sv.trim().length>0){
         suggest_related(searchBox)
      } else {
         setup_search()
      }
         add_class(body, 'suggest_on')
   })

   searchBox.addEventListener('focusout',function(){
      src_time = setTimeout(function(){
         clearTimeout(src_time)
         remove_class(body, 'suggest_on')
         setup_search()
      },800)
   })

   searchBox.addEventListener('input',function(){
      src_time_1 = setTimeout(function(){
         clearTimeout(src_time_1)
         var sv = searchBox.value + ' '
         if(sv.trim().length>0) suggest_related(searchBox)
      },1000)
   })

}

function suggest_related(searchBox){
   var sv = searchBox.value + ' '
   var suggestBox=document.getElementById('search-suggest')
   suggestBox.innerHTML = ''
   var cock = rearray_cookies(get_cookie('searched'))
   var suggestion = autocomplete(searchBox.value, cock)
   check = {}
   if(suggestion.length>0){
      for (var i = 0; i < suggestion.length; i++) {
         if(!check[suggestion[i]] && suggestion[i] != sv.trim()){
            var item =document.createElement('div')
            item.className = 'item recent'
            item.setAttribute('data-val', suggestion[i])
            item.textContent = suggestion[i]
            suggestBox.appendChild(item)
            check[suggestion[i]] = true
         }
      }
      listen_suggest(suggestBox,searchBox)
   }

   if(suggestion.length < 11){
      var limit = parseFloat(10)-(suggestion.length)
      related_search_xhr(limit, searchBox, suggestBox, false)
   }

}

function related_search_xhr(limit, searchBox, suggestBox){
   var sv = searchBox.value + ' '
   if(sv.trim().length>0){
      var nak_filter = document.getElementById('nak_filter')
      var filter = 'nak_all'
      if(nak_filter) filter = nak_filter.value

      var x = new FormData()
      x.append('limit', limit)
      x.append('keyword', sv.trim())
      x.append('filtering', filter)
      var xhr = new XMLHttpRequest();
      xhr.open('POST', 'https://api.malayatimes.com/search/autocomplete?keyword='+sv.replace(/\s\s+/g, ' ').trim()+'&filtering='+filter+'&limit='+limit+'&type=json', true)
      xhr.onreadystatechange = function() {
      if (xhr.readyState === 4){
         var xx = xhr.responseText;
         if(xx.length>0){
            var data = JSON.parse(xx);
            if(data.title){
               for (var i = 0; i < data.title.length; i++) {
                  if(!check[data.title[i].toLowerCase()] && data.title[i] != sv.trim()){
                     var item =document.createElement('div')
                     item.className = 'item db'
                     item.setAttribute('data-val', data.title[i])
                     item.textContent = data.title[i]
                     suggestBox.appendChild(item)
                     check[data.title[i].toLowerCase()] = true
                  }
               }
               listen_suggest(suggestBox,searchBox)
            }
         }
      }
      }
      xhr.send(x);
   }
}

function listen_menu(){
   var men = document.getElementById('mobile-menu-toggle')
   var bod = document.getElementById('main-blog')
   men.addEventListener('click', function(){
      if(check_attribute(bod, 'class', 'menu-on')){
         remove_class(bod, 'menu-on')
      } else {
         add_class(bod, 'menu-on')
      }
   })
}

var timeout_main_event;
window.addEventListener('resize', function(){
     clearTimeout(timeout_main_event);
     timeout_main_event = setTimeout(function(){
          console.log(document.body.clientWidth);
     }, 2000);
});