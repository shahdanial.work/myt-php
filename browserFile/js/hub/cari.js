var first = true;
var check = {}

function run_js() {
   if (first) on_load();
   listen_search()
   listen_filter()
   setup_search()
   on_scroll()
}
run_js()

var end = false
var meloading = false

function on_scroll() {
   window.addEventListener('scroll', function() {
      if (!end && !meloading) load_post()
      var sticky = document.getElementById('sticky')
      var sticker = document.getElementsByClassName('sticker')
      if(sticky&&sticker) check_sticky(sticky)
   });
}

var stick = false
function check_sticky(sticky){
   var body = document.body
   if(is_visible(sticky)){
      remove_class(body, 'stick')
      stick = false
   } else if(!stick){
      stick = true
      add_class(body, 'stick')
   }
}

function get_offset() {
   var c = 0
   var box = document.getElementsByClassName('result')
   if (box.length > 0) {
      for (var i = 0; i < box.length; i++) {
         if (!check_attribute(box[i], 'id', 'image-result')) c++
      }
   } else {
      var img = document.getElementsByClassName('image')
      c = img.length
   }
   return c
}

function load_post() {
   var spin = document.getElementById('loading-spiner')
   var keyword = get_query_url('keyword').trim() || '' + ' '.trim()
   var filtering = get_query_url('filtering') || 'nak_all'
   if (spin && keyword.length > 0) {
      if (is_visible(spin)) {
         meloading = true
         var limit = 10
         if (filtering == 'nak_gambar') limit = 18
         var xhr = new XMLHttpRequest();
         xhr.open('POST', 'https://hub.malayatimes.com/cari?keyword=' + keyword + '&filtering=' + filtering + '&limit=' + limit + '&offset=' + get_offset() + '&type=json', true)
         xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
               var xx = xhr.responseText;
               var section = document.getElementById('results')
               if (xx.length > 0 && section) {
                  var data = JSON.parse(xx)
                  for (var i = 0; i < data.length; i++) {
                     if (filtering == 'nak_gambar') {
                        var zz = document.createElement('a')
                        zz.setAttribute('href', data[i]['link'])
                        zz.className = 'image'
                        zz.innerHTML = data[i]['inner']
                     } else {
                        var zz = document.createElement('div')
                        zz.className = 'result'
                        zz.innerHTML = data[i]['inner']
                     }
                     section.appendChild(zz)
                  }
                  if (data.length < limit) end = true, spin.innerHTML = '<div class="center">YOU REACH THE END <span class="red">❤</span></div>', spin.setAttribute('id', 'tora'), spin.className = 'load-more-message end', meloading = false;
               } else {
                  end = true
                  spin.innerHTML = '<div class="center">YOU REACH THE END <span class="red">❤</span></div>';
                  spin.setAttribute('id', 'tora')
                  spin.className = 'load-more-message end'
                  meloading = false
               }
            }
         }
         xhr.send()
      }
   }
}

function setup_search() {
   var cock = rearray_cookies(get_cookie('searched'))
   var suggestBox = document.getElementById('search-suggest')
   var searchBox = document.getElementById('search-input')
   check = {}
   suggestBox.innerHTML = ''
   if (cock.length > 0 && suggestBox && searchBox) {
      searchBox.setAttribute('autocomplete', 'off')
      var sv = searchBox.value + ' ';
      if (sv.trim().length > 0) {
         var max = 3
         var i = 0
         while (i != max && cock[i]) {
            if (!check[cock[i]] && cock[i] != searchBox.value) {
               var item = document.createElement('div')
               item.className = 'item recent'
               item.setAttribute('data-val', cock[i])
               item.textContent = cock[i]
               suggestBox.appendChild(item)
               check[cock[i]] = true
            } else {
               max += 1
            }
            i++
         }
         listen_suggest(suggestBox, searchBox)
      } else {
         suggest_related(searchBox)
      }
   }
}



function listen_suggest(suggestBox, searchBox) {
   var item = suggestBox.getElementsByClassName('item')
   if (item.length > 0) {
      for (var i = 0; i < item.length; i++) {
         if (typeof window.addEventListener === 'function') {
            (function(index) {
               item[i].addEventListener('click', function() {
                  searchBox.value = index.getAttribute('data-val')
                  document.getElementById('form').submit()
               })
            })(item[i])
         }
      }
   }
}

var src_time;
var src_time_1;

function listen_search() {
   var body = document.body
   var searchBox = document.getElementById('search-input')
   if (searchBox) searchBox.setAttribute('autocomplete', 'off')

   searchBox.addEventListener('focus', function() {
      var sv = searchBox.value + ' '
      if (sv.trim().length > 0) {
         suggest_related(searchBox)
      } else {
         setup_search()
      }
      add_class(body, 'suggest_on')
   })

   searchBox.addEventListener('focusout', function() {
      src_time = setTimeout(function() {
         clearTimeout(src_time)
         remove_class(body, 'suggest_on')
         setup_search()
      }, 800)
   })

   searchBox.addEventListener('input', function() {
      src_time_1 = setTimeout(function() {
         clearTimeout(src_time_1)
         var sv = searchBox.value + ' '
         if (sv.trim().length > 0) suggest_related(searchBox)
      }, 1000)
   })

}

function suggest_related(searchBox) {
   var sv = searchBox.value + ' '
   var suggestBox = document.getElementById('search-suggest')
   suggestBox.innerHTML = ''
   var cock = rearray_cookies(get_cookie('searched'))
   var suggestion = autocomplete(searchBox.value, cock)
   check = {}
   if (suggestion.length > 0) {
      for (var i = 0; i < suggestion.length; i++) {
         if (!check[suggestion[i]] && suggestion[i] != sv.trim()) {
            var item = document.createElement('div')
            item.className = 'item recent'
            item.setAttribute('data-val', suggestion[i])
            item.textContent = suggestion[i]
            suggestBox.appendChild(item)
            check[suggestion[i]] = true
         }
      }
      listen_suggest(suggestBox, searchBox)
   }

   if (suggestion.length < 11) {
      var limit = parseFloat(10) - (suggestion.length)
      related_search_xhr(limit, searchBox, suggestBox, false)
   }

}

function related_search_xhr(limit, searchBox, suggestBox) {
   var sv = searchBox.value + ' '
   if (sv.trim().length > 0) {
      var nak_filter = document.getElementById('nak_filter')
      var filter = 'nak_all'
      if (nak_filter) filter = nak_filter.value
      var xhr = new XMLHttpRequest();
      xhr.open('POST', 'https://api.malayatimes.com/search/autocomplete?keyword=' + sv.replace(/\s\s+/g, ' ').trim() + '&filtering=' + filter + '&limit=' + limit + '&type=json', true)
      xhr.onreadystatechange = function() {
         if (xhr.readyState === 4) {
            var xx = xhr.responseText
            if (xx.length > 0) {
               var data = JSON.parse(xx);
               if (data.title) {
                  for (var i = 0; i < data.title.length; i++) {
                     if (!check[data.title[i].toLowerCase()] && data.title[i] != sv.trim()) {
                        var item = document.createElement('div')
                        item.className = 'item db'
                        item.setAttribute('data-val', data.title[i])
                        item.textContent = data.title[i]
                        suggestBox.appendChild(item)
                        check[data.title[i].toLowerCase()] = true
                     }
                  }
                  listen_suggest(suggestBox, searchBox)
               }
            }
         }
      }
      xhr.send();
   }
}



function on_load() {
   var kwd = get_query_url('keyword').replace(/\+/g, ' ').trim()
   if (rearray_cookies('searched').length > 0) {
      var searched = rearray_cookies(get_cookie('searched'))
   } else {
      var searched = []
   }
   searched.push(kwd)
   set_cookie('searched', dearray_cookies(searched), 360)
   first = false;
}

function listen_filter() {
   var inp = document.getElementsByClassName('filter_search')
   if (inp) {
      for (var i = 0; i < inp.length; i++) {
         if (typeof window.addEventListener === 'function') {
            (function(index) {
               inp[i].addEventListener('change', function() {
                  nak_filter(inp, index)
               })
            })(inp[i])
         }
      }
   }
}

function nak_filter(inp, o) {
   for (var i = 0; i < inp.length; i++) {
      if (inp[i] != o) {
         inp[i].checked = false
         document.getElementById('form').submit()
      } else {
         inp[i].checked = true
         document.getElementById('nak_filter').value = inp[i].getAttribute('id')
         document.getElementById('form').submit()
      }
   }
}

var timeout_main_event;
window.addEventListener('resize', function() {
   clearTimeout(timeout_main_event);
   timeout_main_event = setTimeout(function() {
      console.log(document.body.clientWidth);
   }, 2000);
});