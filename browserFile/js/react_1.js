(function() {



     /*VARIABLE*/
     var
     editor = document.getElementById("post-editor"),
     preview = document.getElementById("post-preview"),
     previewBr = preview.getElementsByTagName('br'),
     previewPara = preview.getElementsByClassName('para'),
     editorPara = editor.getElementsByClassName('para'),
     elementPreview = preview.getElementsByTagName("*"),
     elementEditor = editor.getElementsByTagName("*"),
     paraPara = editor.getElementsByClassName('para'),
     cursorKau ='',
     table_backspace_support = 0,
     action_after_xhr_login = 0,
     set_post_thumb = false,
     window_is_open = false
     ;



     //onload
     after_html_changed();

     // EDITOR CARET.
     var selRange; ///selrangesaya
     function saveSelection() {
          //selRange = null;
          if (window.getSelection) {
               sel = window.getSelection();
               if (sel.getRangeAt && sel.rangeCount) {
                    var final = sel.getRangeAt(0);
               }
          } else if (document.selection && document.selection.createRange) {
               var final = document.selection.createRange();
          } else {
               var final = null;
          }
          console.log('%csave selection is below.', 'color:green;font-weight:600;')
          console.log(final);
          return final;
     }

     function save_cursor(){
          var fattiya = saveSelection();
          if(fattiya){
               if(get_wrapper(fattiya.startContainer, 'class', 'para') || check_attribute(fattiya.startContainer, 'class','para')){
                    selRange = fattiya;
                    console.log('range saved!!!!!!!');
               }
          }
     }


     function restoreSelection(range) {
          if (range) {
               if (window.getSelection) {
                    sel = window.getSelection();
                    sel.removeAllRanges();
                    sel.addRange(range);
               } else if (document.selection && range.select) {
                    range.select();
               }
          }
     }


     function pasteHtmlAtCaret(html, what) {
          //put back cursor before insert.
          restoreSelection(selRange);
          var sel, range;
          if (window.getSelection) {
               // IE9 and non-IE
               sel = window.getSelection();
               if (sel.getRangeAt && sel.rangeCount) {
                    range = sel.getRangeAt(0);
                    range.deleteContents();

                    // Range.createContextualFragment() would be useful here but is
                    // non-standard and not supported in all browsers (IE9, for one)
                    var el = document.createElement("div");
                    el.innerHTML = html;
                    var frag = document.createDocumentFragment(), node, lastNode;
                    while ( (node = el.firstChild) ) {
                         lastNode = frag.appendChild(node);
                    }
                    range.insertNode(frag);

                    // Preserve the selection
                    if (lastNode) {
                         range = range.cloneRange();
                         range.setStartAfter(lastNode);
                         range.collapse(true);
                         sel.removeAllRanges();
                         sel.addRange(range);
                    }
               }
          } else if (document.selection && document.selection.type != "Control") {
               // IE < 9
               document.selection.createRange().pasteHTML(html);
          }
          ////placeCaretAtEnd( document.getElementById("post-editor") );
          format_para();
          if(what == 'enter'){
               console.log('placeCaretAtEnd as enter');
               after_enter();
               no_paraPara();
               content_preview();
          } else {
               console.log('placeCaretAtEnd as insert');
               after_insert();
               remove_empty_para();
               content_preview();
          }

     }

     function placeCaretAtEnd(el, dimana) { // dimana: true = atStart, false = atEnd;
          meteor();
          if (typeof window.getSelection != "undefined"
          && typeof document.createRange != "undefined") {
               var range = document.createRange();
               range.selectNodeContents(el);
               range.collapse(dimana);
               var sel = window.getSelection();
               sel.removeAllRanges();
               sel.addRange(range);
          } else if (typeof document.body.createTextRange != "undefined") {
               var textRange = document.body.createTextRange();
               textRange.moveToElementText(el);
               textRange.collapse(dimana);
               textRange.select();
          }
          meteor();
          save_cursor();
     }

     function GetSelectedText() { //to call GetSelectedText(selRange);
          if (window.getSelection) {  // all browsers, except IE before version 9
               var range = window.getSelection ();
               return range.toString();
          } else if (document.selection.createRange) { // Internet Explorer
               var range = document.selection.createRange ();
               return range.text;
          } else {
               return 'false';
          }

     }



     /* TITLE AREA */
     var titleTimeout;
     document.getElementById('post-title').addEventListener('input', function(){
          if(this.value.length > 0){
               document.getElementsByClassName('entry-title')[0].innerHTML = this.value.trim();
               document.getElementById('title_draft').value = this.value.trim();
               if(action_after_xhr_login < 1){
                    clearTimeout(titleTimeout);
                    titleTimeout = setTimeout(function () {
                         //save_draft('title');
                         action_after_xhr_login = 1;
                         check_login();
                    }, 5000);
               }


          } else {
               document.getElementsByClassName('entry-title')[0].innerHTML = 'Tajuk yang baik, adalah tepat kepada keyword carian Popular';
               document.getElementById('title_draft').value = null;
          }
     });

     /* END TITLE AREA */


     listen_table();
     function listen_table(){
          //Once backspace table..
          var el_table = document.querySelectorAll('#post-editor .content-table *');
          if(el_table){
               for (var i = 0; i < el_table.length; i++) {
                    if (typeof window.addEventListener === 'function'){
                         (function (index) {
                              el_table[i].addEventListener('input', function(e){
                                   console.log('%cINDSIDE TABLE CHANGE!!', 'font-size:18px;color:pink');

                                   //let's try identify event. Is backspace? enter?
                                   //e.keyCode === not working.
                                   console.log(e);
                                   if(e.inputType == 'deleteContentBackward' || e.keyCode === 8){
                                        table_backspace_support = 1;
                                        backspace_table(index);
                                   }

                              });
                         })(el_table[i]);
                    }
               }
          }
     }


     /* EVEN LISTENER */
     //EDITOR.
     if(editor){
          //editor.addEventListener('input', content_preview, false);
          editor.addEventListener('input', function(){
               save_cursor();
               content_preview();
          });
          editor.addEventListener ('paste', paste_editor, false);
          editor.addEventListener('keydown', function(e){
               // apa jua perbuatan. save range dulu..
               save_cursor();
               if (e.keyCode === 13) { // once enter..
                    meteor();
                    e.preventDefault();
                    console.log('%cONCE ENTER', 'color:orange;font-size:big');
                    console.log(document.activeElement);
                    //check_attribute(document.activeElement, 'tagName', 'blockquote');

                    if(check_attribute(document.activeElement, 'class', 'message')){
                         console.log('anda enter dalam message');
                         pasteHtmlAtCaret('BRMESEJ', 'enter');
                    } else if(check_attribute(document.activeElement, 'tagName', 'blockquote')){
                         console.log('anda enter dalam blockquote');
                         pasteHtmlAtCaret('BRMESEJ', 'enter');
                    } else if(check_attribute(document.activeElement, 'class', 'para') && check_attribute(document.activeElement, 'contenteditable', 'true') && document.activeElement.innerHTML != '' && document.activeElement.innerHTML != null){
                         console.log('anda enter dalam para');
                         remove_kiamatlab();
                         remove_empty_para();
                         pasteHtmlAtCaret('<!--PEMISAH-->','enter');
                    }
                    //after_enter();
               }
               if (e.keyCode === 8) {// once backspace..
                    meteor();
                    console.log('%c BACKSPACE INSIDE EDITOR!', 'color:orange');
                    console.log(document.activeElement);
                    console.log('berapa para = '+editorPara.length);
                    var index = Array.prototype.slice.call(editorPara).indexOf(document.activeElement);
                    var index = parseFloat(index);
                    //var paraClass = document.activeElement.className.replace('kiamatlab', '').trim();

                    if(
                         check_attribute(document.activeElement, 'class', 'para') &&
                         check_attribute(document.activeElement, 'contenteditable', 'true') &&
                         editorPara.length > 1){
                              /* IN PARA*/
                              console.log('IN PARA');

                              /* all code move to backspace_para() */
                              backspace_para(document.activeElement);

                         } else if(check_attribute(document.activeElement, 'class', 'message') || check_attribute(document.activeElement, 'tagName', 'blockquote')){
                              /* IN MESSAGE/BLOCKQUOTE */
                              var para_dia = document.activeElement.parentNode;
                              console.log('ANDA BACKSPACE INSIDE BLOCKQUOTE OR MESSAGE');
                              console.log('jumlah content di dalam adalah = ' + document.activeElement.textContent.length );
                              //para_dia.previousSibling
                              if(document.activeElement.textContent.length < 2){ // jumlah content..
                                   // set up mouse..
                                   if(check_attribute(para_dia.previousSibling, 'class', 'para') && check_attribute(para_dia.previousSibling, 'contenteditable', 'true')){
                                        var target_gua = para_dia.previousSibling;
                                        var nak_mouse = 'true';
                                        var at_start = false;
                                   } else if(check_attribute(para_dia.nextSibling, 'class', 'para') && check_attribute(para_dia.nextSibling, 'contenteditable', 'true')){
                                        var target_gua = para_dia.nextSibling;
                                        var nak_mouse = 'true';
                                        var at_start = true;
                                   } else {
                                        var nak_mouse = 'false';
                                   }
                                   //remove element..
                                   para_dia.parentNode.removeChild(para_dia);
                                   if(nak_mouse == 'true'){
                                        placeCaretAtEnd(target_gua, at_start);
                                   }
                              } // jumlah content..

                         } else if( get_wrapper(document.activeElement, 'class', 'content-table') && table_backspace_support < 1 ){
                              //backspace table
                              console.log('YES IT WAS TABLE TD!');
                              backspace_table(document.activeElement);
                         }

                    } // end if bakcspace..
                    if(compare_domain(window.location.href,'*.malayatimes.com')){
                         //format_editor();
                         content_preview();
                    }
               });
          }

          function check_attribute(a, attr, valu){
               if(attr == 'tagName'){
                    if(a.tagName.toLowerCase() == valu.toLowerCase()){
                         return a.tagName;
                    } else {
                         return false;
                    }
               } else {
                    var vall = ' '+ valu +' ';
                    if(a.nodeType === 1 && a.getAttribute(attr)){
                         var get_attr = ' '+ a.getAttribute(attr) +' ';
                         if(get_attr.indexOf(vall) > -1){
                              return a.getAttribute(attr);
                         } else {
                              return false;
                         }
                    } else {
                         return false;
                    }
               }
          }

          function get_wrapper(a, attr, valu){
               var vall = ' '+ valu +' ';
               var c = false;
               var d = false;
               var w = a;

               while ((a && !c) || (!d)){
                    if(a != w){
                         if(a.nodeType === 1 && a.tagName && a.tagName != null && typeof a.tagName !== 'undefined'){
                              if(a.tagName.toLowerCase() == 'body'){
                                   // stop while when reach body...
                                   d = true;
                              }
                         }
                         if(a.nodeType === 1 && a.getAttribute(attr)){
                              var get_attr = ' '+ a.getAttribute(attr) +' ';
                              if(get_attr.indexOf(vall) > -1){
                                   return a;
                                   c = true; // stop while.
                              }
                         }
                    }
                    a = a.parentNode;
               }
               if(!c){// jika element takde.
                    return false;
               }

          }


          function get_domain(str){
               var str = str.replace(/http:|https:/g,'').replace('//','');
               if(str.indexOf('/') > -1){
                    var str = str.split('/');
                    var str = str[0];
               }
               if(str.indexOf('?') > -1){
                    var str = str.split('?');
                    var str = str[0];
               }
               if(str.indexOf('#') > -1){
                    var str = str.split('#');
                    var str = str[0];
               }
               return str.toLowerCase();
          }


          function check_allow_sub_domain(str, allowed){
               var str = str.toLowerCase().split('.');
               var allowed = allowed.toLowerCase().split('.');

               var myreturn = false;

               if(allowed.length == 3){ /* CONDITION 1*/ // *.domain.com
                    console.log('allowed 3');
                    //console.log(allowed);
                    //console.log(str);
                    if(str.length == 2){ // domain.com
                         if(allowed[1] == str[0] && allowed[2] == str[1]){
                              var myreturn = true;
                              console.log('A');
                         }
                    }
                    if(str.length == 3){ // sub.domain.com = lulus... domain.com.country = gagal..
                         if(allowed[1] == str[1] && allowed[2] == str[2]){
                              var myreturn = true;
                              console.log('B');
                         }
                    }
                    //4 takkan lepas. sub.domain.com.country vs *.domain.com(3aallowed).
               }

               if(allowed.length == 4){ /* CONDITION 1*/ // *.domain.com.country
                    console.log('allowed 4');
                    //2 xkan lepas .. domain.com vs *sub.domain.com.country
                    if(str.length == 3){ // (domain.com.country = lepas..) (sub.domain.com = x lepas..)
                         if(allowed[1] == str[0] && allowed[2] == str[1] && allowed[3] == str[2]){
                              var myreturn = true;
                              console.log('C');
                         }
                    }
                    if(str.length == 3){ // (sub.domain.com.country = lepas..)
                         if(allowed[1] == str[1] && allowed[2] == str[2] && allowed[3] == str[3]){
                              var myreturn = true;
                              console.log('D');
                         }
                    }
               }

               return myreturn;
          }

          function compare_domain(isi, arr){
               var myreturn = false;
               if(isi && arr){
                    if(typeof arr == 'string'){
                         if(arr.indexOf('*') > -1){// example: *.mydomain.com..

                              var myreturn = check_allow_sub_domain(get_domain(isi), arr);

                         } else  if(get_domain(isi) == arr.toLowerCase()){
                              var myreturn = true;
                         }
                    } else if(typeof arr == 'object') {
                         for (var i = 0; i < arr.length; i++) {
                              if(arr[i].indexOf('*') > -1 && check_allow_sub_domain(get_domain(isi), arr[i])){
                                   var myreturn = true;
                              }else if(get_domain(isi) == arr[i].toLowerCase()){
                                   var myreturn = true;
                              }
                         }
                    }
               } else {
                    console.log('isi or arr empty: compare_domain()');
                    var myreturn = false;
               }
               return myreturn;
          }

          // EDITOR PARA CLICKED..
          /*
          if(editorPara){
          for (var i = 0; i < editorPara.length; i++) {
          if (typeof window.addEventListener === 'function'){
          (function (index) {
          editorPara[i].addEventListener('click',function(){
          var theClass = ' '+ index.getAttribute('class') + ' ';
          if( index.getAttribute('contenteditable')  == 'true' &&  theClass.indexOf(" para ") > -1 ){
          save_cursor();
     } else {// element non editable para clicked...
     selRange = null;

     meteor();

     if( index.getElementsByTagName('blockquote')[0] != undefined || index.getElementsByClassName('message')[0] != undefined || index.getElementsByClassName('modal')[0] != undefined ){
     console.log('we will do nothing..');
} else {
//popup. ask user what to do. 1. delete this element. 2. edit this element. add paragraph before/after.
// SINGLE IMAGE CICKED
if(index.getElementsByClassName('single_image')){
single_image_clicked(index);
}
}

}
})
})(editorPara[i]);
}
}
}
*/

function backspace_para(index){
     //console.log(index);
     //CODE LAMA
     var check = selRange.startContainer.parentNode; // elem..
     if( check.className == 'wysiwyg-content'){
          console.log('wysiwyg-content');
          console.log(check.className);
          var elem = selRange.startContainer;
     } else {
          console.log('entah');
          console.log('check');console.log(check);
          var elem = selRange.startContainer.parentNode;
     }

     if(!check_attribute(elem, 'class', 'para') && check_attribute(index, 'class', 'para')){
          var elem = index;
     }

     if(elem.previousSibling){
          console.log('elem.previousSibling ada');
          var prevElem = elem.previousSibling;
          console.log('elem');console.log(elem);
          console.log('prevElem');console.log(prevElem);
          var cursor_index = selRange.startOffset;
          if( check_attribute(prevElem, 'class', 'para') && check_attribute(prevElem, 'contenteditable', 'true') && selRange.startOffset ==/*xde selection*/  selRange.endOffset && cursor_index < 1){
               //console.log(selRange);
               console.log('selRange.startOffset');
               console.log(selRange.startOffset);
               console.log('selRange.endOffset');
               console.log(selRange.endOffset);
               event.preventDefault();
               console.log(cursor_index);
               placeCaretAtEnd(prevElem, false);
               // prevElem.innerHTML = prevElem.innerHTML + ' ' + elem.innerHTML;
               var new_html = ' '+elem.innerHTML.replace('&nbsp;', ' ');
               var span = document.createElement('span');
               span.innerHTML = new_html;
               console.log('new_html');console.log(new_html);
               prevElem.append(span.innerHTML);
               elem.parentNode.removeChild(elem);
          }
     } else {
          console.log('elem.previousSibling takde');
     }
}

function backspace_table(index){
     if( check_attribute(index, 'tagName', 'td') ){
          td_backspace(index);
     } else if( check_attribute(index, 'tagName', 'th') ) {
          th_backspace(index);
     }
}


function th_backspace(index){
     /*the main goal of this thing:
     1. kalo di right th, backspace with zero textContent move caret to left.
     2. kalo di left, with zero textContent, check semua th dan td, kalo zero textContent delete table..
     3. dari parentNode (para). then para setAttribute('contenteditable).. then placeCaretAtEnd this para muah..
     */
     var kawan = 'false';
     var dimana = 0;
     if(index.previousElementSibling != null){
          if(index.previousElementSibling .tagName == index.tagName){
               var kawan = index.previousElementSibling;
          }
          var dimana = 2;
     } else if(index.nextElementSibling != null){
          if(index.nextElementSibling .tagName == index.tagName){
               var kawan = index.nextElementSibling;
          }
          var dimana = 1;
     }


     if(dimana > 0){

          if(dimana == 2 && index.textContent.length < 1){
               if(kawan != 'false'){
                    placeCaretAtEnd(kawan, false);
               }
          } else if(dimana == 1 && index.textContent.length < 1){

               var all_td = get_wrapper(index, 'class', 'content-table').getElementsByTagName('td');
               var jumlah = 0;
               if(all_td.length > 0){
                    for (var i = 0; i < all_td.length; i++) {
                         if(all_td[i].textContent.length > 1){
                              jumlah = parseFloat(all_td[i].textContent.length.trim()) + parseFloat(jumlah);
                         }
                    }
               }
               var all_tr = get_wrapper(index, 'class', 'content-table').getElementsByTagName('tr');
               if(all_tr.length > 0){
                    for (var i = 0; i < all_tr.length; i++) {
                         if(all_tr[i].textContent.length > 1){
                              jumlah_content = parseFloat(all_tr[i].textContent.trim().length) + parseFloat(jumlah);
                         }
                    }
               }

               if(jumlah < 1){
                    console.log('this table td and th all empty, so remove this table');
                    var this_table = get_wrapper(index, 'class', 'content-table');
                    var wrap_para = this_table.parentNode;
                    this_table.parentNode.removeChild(this_table);
                    wrap_para.setAttribute('contenteditable', 'true');
                    placeCaretAtEnd(wrap_para, true);
               }

          }

     }
}

function td_backspace(index){
     var kawan = 'false';
     if(index.previousElementSibling != null){
          if(index.previousElementSibling .tagName == index.tagName){
               var kawan = index.previousElementSibling;
          }
          var dimana = 2;
     } else if(index.nextElementSibling != null){
          if(index.nextElementSibling .tagName == index.tagName){
               var kawan = index.nextElementSibling;
          }
          var dimana = 1;
     }
     if(kawan != 'false'){
          var jumlah = parseFloat(kawan.textContent.length) + parseFloat(index.textContent.length);
          if(dimana == 2 && index.textContent.length < 1){
               placeCaretAtEnd(kawan, false);
          } else if(dimana == 1 && index.textContent.length < 1){

               var tr = index.parentNode;
               var tr_before_tr = tr.previousElementSibling;
               var td_tr_before_tr = 0;
               if(tr_before_tr != null){
                    if(check_attribute(tr_before_tr, 'tagName', 'tr')){
                         var check_td = tr_before_tr.getElementsByTagName('td')[1];
                         if(check_td && check_td != null){
                              var td_tr_before_tr = check_td;
                         }
                    }
               }
               var tbody = tr.parentNode;

               if(td_tr_before_tr != 0){
                    placeCaretAtEnd(td_tr_before_tr, false);
               } else {
                    console.log('element sebelum adalah th ke?');
                    var last_th = get_wrapper(index, 'class', 'content-table').getElementsByTagName('th')[1];
                    if(last_th && last_th != null){
                         placeCaretAtEnd(last_th, false);
                    }
               }

               if(jumlah < 1){//tr dah kosong..
                    tr.parentNode.removeChild(tr);
               }

          }

     }
}


if(compare_domain(window.location.href,'*.malayatimes.com')){
     listen_click_para();
}
function listen_click_para(){

     meteor();

     if(editorPara){
          for (var i = 0; i < editorPara.length; i++) {
               if (typeof window.addEventListener === 'function'){
                    (function (index) {

                         editorPara[i].addEventListener('click',function(e){

                              var theClass = ' '+ index.getAttribute('class') + ' ';

                              if( index.getAttribute('contenteditable')  == 'true' && theClass.indexOf(" para ") > -1 ){
                                   //klik para..
                                   save_cursor();
                                   console.log('%cKo click para editable, saveSelection()!!..', 'color:green;');

                              } else if( index.getAttribute('contenteditable')  != 'true' && index.getElementsByTagName('blockquote').length > 0 ||  index.getElementsByClassName('message').length > 0 ){

                                   console.log('%cKo click blockqoute atau message,  saveSelection()!!..', 'color:green;');
                                   save_cursor();

                              } else {// element non editable para clicked...

                                   console.log('%cKo klik bukan para dan bukan message or blockquote, posible gambar, video  : tak buat apa2 pada selrange..', 'color:green;');
                                   // selRange = null; jangan buh null biarkan selRange lama.

                              }

                         }); // addEventListener

                    })(editorPara[i]);
               }
          }

     }

}




function remove_empty_para(){
     console.log('remove_empty_para()');
     meteor();
     var editorParaGua = editor.getElementsByClassName('para');
     console.log('editorParaGua.length = ' + editorParaGua.length);
     if(editorParaGua .length > 0){
          for (var i = 0; i < editorParaGua.length; i++) {
               if (typeof window.addEventListener === 'function'){
                    (function (index) {
                         if( editorParaGua[i].innerHTML.trim().replace(' ','').length < 1 || editorParaGua[i].innerHTML.trim() == '' || editorParaGua[i].innerHTML.trim() == null){

                              console.log('kami remove ');
                              console.log(index);
                              var wrapper_dia = index.parentNode;
                              wrapper_dia.removeChild(index);

                         }
                    })(editorParaGua[i]);
               }
          }
     }
}

/* WHEN USER PASTE SOMETHING. FORMAT IT FIRST! */
function paste_editor(e){
     meteor();
     e.preventDefault();
     // var theKlas = document.activeElement.className;
     // if(theKlas){
     //    var theKlas = ' ' + theKlas + ' ';
     // }

     var text = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

     if(text){
          // if(theKlas.indexOf(" message ") > -1 || document.activeElement.tagName.toLowerCase() == 'blockquote'){
          if( check_attribute(document.activeElement, 'class', 'message') || check_attribute(document.activeElement, 'tagName', 'blockquote') ){
               //   case active element is para
               console.log('WANNA PASTE INSIDE MESSAGE???');
               var theHTML = text.trim();
               var html_array = theHTML.split('\n');
               var my_div = document.createElement('div');
               var jumlah = parseFloat(html_array.length) - parseFloat(1);
               for (var i = 0; i < html_array.length; i++) {
                    var parag =  html_array[i].trim();
                    if(parag.length > 0){
                         if(i != jumlah){
                              var paragh = parag + 'BRMESEJ';
                         } else {
                              var paragh = parag;
                         }
                         my_div.append(paragh);
                    }
               }

               pasteHtmlAtCaret(my_div.innerHTML.trim(), 'enter');
          } else {
               // case active element is mesej or blockquote
               var theHTML = text.trim(),
               theHTML = theHTML.replace(/\n{3,}/g, '\n');
               console.log('theHTML = '+ theHTML);
               var theHTML = theHTML.replace(/\n/g, '<!--PEMISAHAN-->');
               console.log('theHTML PEMISAHAN = '+ theHTML);
               pasteHtmlAtCaret(theHTML, 'enter');
          }
          meteor();

          no_paraPara();
          remove_empty_para();
          content_preview();
     }
}

/*
function paste_message(e){
console.log('below is paste_message console.log');
e.preventDefault();
var text = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

if(text){
var theHTML = text.trim(),
theHTML = theHTML.replace(/\n{3,}/g, '\n');
var my_array = theHTML.split('\n');
// var fake_array = new Array();
var my_elem  = document.createElement('div');
for (var i = 0; i < my_array.length; i++) {
var my_text = my_array[i].trim();
if(my_text.length > 0){ // keep only paragraph had a text..
//var my_parag = my_text + '<br class="newline"/><br class="newline"/>';
var my_parag = my_text + '\n\n';
// fake_array.push(my_parag);
my_elem.append(my_parag);
}
}
document.execCommand("insertHTML", false, my_elem.innerHTML);
}
}
*/




/* TOOL TO CLEAN ARRAY */
function cleanArray(actual) {
     var newArray = new Array();
     for (var i = 0; i < actual.length; i++) {
          if (newArray.indexOf(actual[i]) < 0) newArray.push(actual[i])
     }
     return newArray;
}


/* CALL IT AFTER YOU INSERT SOMETHING TO EDITOR. IT WILL REMOVE DOUBLE PARA PARA */
function no_paraPara(){ // THE PROBLERM IS HERE WTF LOL.
     meteor();

     var firstLevelPara = document.querySelectorAll('#post-editor > .para');

     if(firstLevelPara){
          for (var i = 0; i < firstLevelPara.length; i++) {
               var secondLevelPara = firstLevelPara[i].getElementsByClassName('para');
               if(secondLevelPara){
                    var mydiv = document.createElement('div');
                    for (var ii = 0; ii < secondLevelPara.length; ii++) {
                         console.log('ada secondLevelPara');
                         console.log(secondLevelPara[ii]);
                         mydiv.innerHTML = mydiv.innerHTML + secondLevelPara[ii].outerHTML;
                         secondLevelPara[ii].parentNode.removeChild(secondLevelPara[ii]);
                    }
                    firstLevelPara[i].outerHTML = firstLevelPara[i].outerHTML + mydiv.innerHTML;
                    meteor();
               }
          }
     }

     var kiamatlab = document.getElementsByClassName('kiamatlab');
     if(kiamatlab.length > 0){
          placeCaretAtEnd(kiamatlab[0], true);
          kiamatlab[0].className = 'para';
     }
     console.log('%cEND no_paraPara', 'color:red; font-size:18px;');
}



/* SEND EDITOR CONTENT TO PREVIEW */
function content_preview(){
     meteor();
     format_editor();
     save_cursor();
     listen_click_para();

     preview.innerHTML = editor.innerHTML.replace('<div class="para"><br></div>', '').replace('&nbsp\;', ' ');
     console.log(preview)
     format_preview();
     console.log(preview);
     document.getElementById('content_wtf').value = preview.innerHTML;
     console.log('we passed already preview innerHTML. then now below is content wtf value');
     var test = document.createElement('section');
     test.innerHTML = document.getElementById('content_wtf').value;
     console.log(test);

}


function first_load(){
   /* check adakah para ada, kalo xde kita create para then focus... */
   if(editorPara.length < 1){
        var new_para = document.createElement('div');
        new_para.className = 'para';
        new_para.setAttribute('contenteditable', 'true');
        new_para.setAttribute('autocomplete', 'off');
        editor.appendChild(new_para);
        meteor();
        placeCaretAtEnd(editorPara[0], false);
        console.log('theres no para, so we create new para...');
   }
}


/* FORMAT EDITOR */
format_editor(); // on fisrt time load
function format_editor(){
     meteor();

     /* kalo xde editable, add editble */
     first_load();

     var nak_remove = new Array();

     /* -- START FILTER element dan attribute -- */
     if(elementEditor){

          /////// allowed attribute ///////
          var allowedAttr = {
               class: true,
               'alt': true,
               'title': true,
               'href': true,
               'src': true,
               'contenteditable': true,
               'autocomplete': true,
               'data-tooltip': true // tooltip
          };

          /////// allowed class ///////
          var allowedClassVal = {
               'para': true, //<div class="para">text perenngan pertama</div>
               'gallery': true, //gallery in post wrapper.
               'single_image': true, // gambar single.
               'gallery_image': true, //gambar dalam gallery figure.
               'content-table': true, // table
               'message': true, // WIDGET inside para
               'video': true, // WIDGET inside para
               'related': true, // WIDGET inside para
               'button': true, // WIDGET inside para
               'kiamatlab': true, // when user enter between paragraph.. create new line, then focus to this class
               'newline': true, // blockqoute and message br use this class..
               'vertical': true,// table markup..
               'horizontal': true, // table markup..
               'sub-heading': true,


               'para_control add_para': true,
               'para_control remove_element': true,
               'para_control': true, // if current para is widget.
               'add_para': true,
               'remove_element': true // ? not-used anymore?


          };

          /////// disallow element ///////
          var disallowTag = {
               'script': true,
               'body': true,
               'noscript': true,
               'form': true,
               'style': true
          };

          var i;
          // foreach all element.
          for (i = 0; i < elementEditor.length; ++i) {
               var
               elem = elementEditor[i]
               ;

               /* FORMAT SEMANTIC AREA */
               // tukar p kepada div.para
               if(check_attribute(elem, 'tagName', 'p')){
                    var newPara = document.createElement('div');
                    newPara.className = 'para';
                    //masukkan para element dalam p
                    newPara.innerHTML = elem.innerHTML;
                    //masukkan p sebelum .para
                    elem.parentNode.insertBefore(newPara, elem);
                    //remove .para
                    nak_remove.push(elem);//elem.parentNode.removeChild(elem);
               }

               // remove br without  needed tag(class, newline)..
               if( check_attribute(elem, 'tagName', 'br') ){
                    if( check_attribute(elem, 'class', 'newline') && get_wrapper(elem,'class','para') ){
                        if(elem.className.trim() !== 'newline'){
                              elem.className = newline;
                        }
                    } else {
                        nak_remove.push(elem);//elem.parentNode.removeChild(elem);
                    }
               }

               /* REMOVE SEMANTIC AREA (sbb disallow) */
               if (disallowTag[elem.tagName.toLowerCase()]){
                    nak_remove(elem);//elem.parentNode.removeChild(elem);
               }

               /* REMOVE ATTRIBUTE AREA (sbb not listed as allowed attribute)*/
               if (elem.attributes) {
                    var ii;
                    for (ii = elem.attributes.length -1; ii >=0; --ii) {
                         var elemAttr = elem.attributes[ii];
                         if (!allowedAttr[elemAttr.name.toLowerCase()]){ //jika attribute ni x tersenarai
                              elem.attributes.removeNamedItem(elemAttr.name);
                         } else {
                              if(elemAttr.name == 'class'){
                                   //class area.
                                   if(!allowedClassVal[elemAttr.value.toLowerCase()]){
                                        var classArray = elemAttr.value.toLowerCase().split(' ');
                                        var fKlas = '';
                                        for (var ioo = 0; ioo < classArray.length; ioo++) {
                                             var Klas = classArray[ioo];
                                             if(allowedClassVal[Klas]){
                                                  fKlas += Klas +' ';
                                             }
                                        }
                                        elemAttr.value = fKlas;
                                   }
                              }
                         }
                    }
               }


          } // end foreach all element.
     }// if elementEditor

     listen_control();

}

format_para();
function format_para(){
     // FORMAT PARA /////////////////////////////////////////////////////////////////////////
     /*
     this function executed once page load only.
     */
     if(editor){

          var nak_delete = editor.getElementsByClassName('para_control')[0];
          while(nak_delete){
             var wrapper_nak_delete = nak_delete.parentNode;
             if(wrapper_nak_delete){
                wrapper_nak_delete.removeChild(nak_delete);
             }
             nak_delete = editor.getElementsByClassName('para_control')[0];
          }

          // ADD CONTROL BOX.
          var para_editor = editor.getElementsByClassName('para');
          if(para_editor.length > 0){

               for (var i = 0; i < para_editor.length; i++) {

                    if(para_editor[i]){

                         if(!check_attribute(para_editor[i], 'contenteditable', 'true')){

                              // Element ni mmg non editable. ok, proceed..

                              console.log('Element ni mmg non editable. ok, proceed..')
                              console.log(para_editor[i])

                              //control delete element.
                              var delete_element = document.createElement('span');
                              delete_element.className = 'para_control remove_element';
                              delete_element.setAttribute('data-tooltip', 'click to remove this Element')
                              delete_element.textContent = '×';


                              //control add para.
                              var add_para = document.createElement('span');
                              add_para.className = 'para_control add_para';
                              add_para.setAttribute('data-tooltip', 'click to add new Paragraph below')
                              add_para.textContent = '+';

                              // if(the_para.getElementsByClassName('remove_element').length < 1){
                              para_editor[i].appendChild(delete_element);
                              // }
                              // if(the_para.getElementsByClassName('add_para').length < 1){
                              para_editor[i].appendChild(add_para);
                              // }

                         }

                    }

               }

          }
     } // end if editor
     // FORMAT PARA /////////////////////////////////////////////////////////////////////////
     listen_control();
}

function listen_control(){
     if(editor){
          // LISTEN CLICK CONTROL///////////////////////////////////////////
          var a = editor.getElementsByClassName('para_control');
          if(a.length > 0){
               for (var i = 0; i < a.length; i++) {
                    if (typeof window.addEventListener === 'function'){
                         (function (index) {

                              a[i].addEventListener('click', function(){

                                   // add para
                                   if(check_attribute(index, 'class', 'add_para')){

                                        remove_empty_para();

                                        var target = index.parentNode;
                                        if(!check_attribute(target, 'class','para')){
                                             target = target.parentNode;
                                             if(!check_attribute(target, 'class','para')){
                                                  target = false;
                                             }
                                        }

                                        if(target){
                                             var new_para = document.createElement('div');
                                             new_para.className = 'para';
                                             new_para.setAttribute('contenteditable','true');
                                             new_para.setAttribute('autocomplete','off');

                                             if(target.nextSibling){
                                                  // element after ada.
                                                  target.parentNode.insertBefore(new_para, target.nextSibling);
                                             } else {
                                                  // ni element last pada editor
                                                  editor.appendChild(new_para);
                                             }
                                             placeCaretAtEnd(new_para, true);
                                        }

                                   }

                                   // delete para
                                   if(check_attribute(index, 'class', 'remove_element')){

                                        var target = index.parentNode;
                                        if(!check_attribute(target, 'class','para')){
                                             target = target.parentNode;
                                             if(!check_attribute(target, 'class','para')){
                                                  target = false;
                                             }
                                        }

                                        if(target){
                                             if(target.parentNode){
                                                  target.parentNode.removeChild(target);
                                                  content_preview();
                                             }
                                        }

                                        remove_empty_para();

                                   }
                              });

                         })(a[i]);
                    }
               }
          }
          // LISTEN CLICK CONTROL///////////////////////////////////////////
     }
}

function remove_emptry_para_preview(){
   /*
   WHY ? Make it easy, before format remove emptry para fist. if not do this, empty para will count in loop then remove loop, then count will change.
   */
   // meteor();

   if(preview){
      if(preview.getElementsByClassName('para').length > 0){

         var para_preview = preview.getElementsByClassName('para');
         var fake_preview = document.createElement('div');
         fake_preview.setAttribute('id', 'post-preview');
         fake_preview.className = 'my-content';

         for (var i = 0; i < para_preview.length; i++) {

            var index = para_preview[i];

                    if(index.innerHTML.replace(/\u00a0/g, ' ').trim().length > 0){

                       if(
                           index.getElementsByTagName('div').length > 0 ||
                           index.getElementsByTagName('table').length > 0 ||
                           index.getElementsByTagName('figure').length > 0 ||
                           index.getElementsByClassName('single_image').length > 0 ||
                           index.getElementsByTagName('blockquote').length > 0 ||
                           index.getElementsByTagName('iframe').length > 0 ||
                           index.getElementsByClassName('related').length > 0 ||
                           index.getElementsByClassName('sub-heading').length > 0
                       ){
                           //create div
                           var newP = document.createElement('div');
                           newP.className = 'para';
                           newP.innerHTML = index.innerHTML.replace(/\u00a0/g, ' ').trim();
                       } else {
                           //create p
                           var newP = document.createElement('p');
                           newP.className = 'para';
                           newP.innerHTML = index.innerHTML.replace(/\u00a0/g, ' ').trim();
                       }
                       fake_preview.appendChild(newP)

                    }

         }

         //if(preview.parentNode){
            //preview.parentNode.replaceChild(fake_preview, preview);
            preview.innerHTML = fake_preview.innerHTML;
         //}

      }
   }

}

function remove_control_button_preview(){

      var para_control = preview.getElementsByClassName('para_control');
      while(para_control[0]){
         console.log('para_control length = ' + para_control.length)
         if(para_control[0].parentNode){
            console.log('kami nak remove para_control dibawah')
            console.log(para_control[0])
            para_control[0].parentNode.removeChild(para_control[0])
         }
      }

}

function remove_br_preview(){

   var all_br = preview.getElementsByTagName('br');

   if(all_br){

      var br_xmau = new Array();

      for (var i = 0; i < all_br.length; i++) {

         if( !check_attribute(all_br[i], 'class', 'newline') ){
            br_xmau.push(all_br[i])
         }

      }

      if(br_xmau){

         for (var i = 0; i < br_xmau.length; i++) {
            if(br_xmau[i].parentNode){
               br_xmau[i].parentNode.removeChild(br_xmau[i])
            }
         }

      }

   }

}

/* TODO: perancangan saya adalah untuk save anything base on preview html format preview html agar terbaek! */

/*FORMAT HTML INSDE EDITOR */
format_preview(); //on first time load..
function format_preview(){
     console.log('%cFORMAT_PREVIEW()', 'color:brown;font-weight:600;');

     // console.log(editor)
     console.log(preview)

     /*
     1. Remove satu per satu control button
      */
      remove_control_button_preview();

      /*
      2. remove br yang class bukan newline
      */
      remove_br_preview();

     /*
     3. Remove empty para. also change semantic .para to div(if contain widget) or p(if only text)
     */
     remove_emptry_para_preview();


      filter_preview_attribute();

      content_draft();
}

function filter_preview_attribute(){

   console.log('%clast_format_preview()', 'color:blue;font-weight:600')
     var mypreview = document.getElementById('post-preview');
     var elementPreview = false;
     if(mypreview){
        elementPreview = mypreview.getElementsByTagName("*");
     }


     var nak_remove = new Array();

     /* -- START FILTER element dan attribute -- */
     if(elementPreview){

          /////// allowed attribute ///////
          var allowedAttr = {
               class: true,
               'alt': true,
               'title': true,
               'href': true,
               'src': true,
               'data-tooltip': true // tooltip
          };

          /////// allowed class ///////
          var allowedClassVal = {
               'para': true, //<div class="para">text perenngan pertama</div>
               'gallery': true, //gallery in post wrapper.
               'single_image': true, // gambar single.
               'gallery_image': true, //gambar dalam gallery figure.
               'content-table': true, // table
               'message': true, // WIDGET inside para
               'video': true, // WIDGET inside para
               'related': true, // WIDGET inside para
               'button': true, // WIDGET inside para
               'kiamatlab': true, // when user enter between paragraph.. create new line, then focus to this class
               'newline': true, // blockqoute and message br use this class..
               'vertical': true,// table markup..
               'horizontal': true, // table markup..
               'sub-heading': true,


          };

          /////// disallow element ///////
          var disallowTag = {
               'script': true,
               'body': true,
               'noscript': true,
               'form': true,
               'style': true
          };

          // foreach all element.
          for (i = 0; i < elementPreview.length; ++i) {
               var
               elem = elementPreview[i],
               attrs = listAttributes(elem)
               ;

               /* REMOVE ATTRIBUTE AREA (sbb not listed as allowed attribute)*/
               if(attrs){
                  for (var ii = 0; ii < attrs.length; ii++) {

                     if(attrs[ii].attribute_dia == 'class'){// ada class
                        //filter class
                        var classes = elem.className;
                        var Klas = classes.split(' ');
                        var ready_class = '';

                        for (var iii = 0; iii < Klas.length; iii++) {

                           if( allowedClassVal[Klas[iii]] ){
                              ready_class += Klas[iii]+' ';
                           }

                        }
                        if(ready_class.trim().length > 0){
                           elem.className = ready_class.trim();
                        }

                     } else {
                        //another attr (not class)
                        console.log(elem)
                        console.log(attrs[ii].attribute_dia)
                        console.log('status = '+allowedAttr[attrs[ii].attribute_dia])
                        if( !allowedAttr[attrs[ii].attribute_dia] ){
                           elem.removeAttribute(attrs[ii].attribute_dia);
                        }

                     }

                  }
               }


          } // end foreach all element.
     }// if elementPreview


}

var timeOutContent;
function content_draft(){
     meteor();
     document.getElementById('content_draft').value = document.getElementById('post-preview').innerHTML.trim(); // actally it's working but not change at Inspect Element Browser. But it will working when you try to console.log the the textarea value..
     if(action_after_xhr_login < 1){
          clearTimeout(timeOutContent);
          timeOutContent = setTimeout(function(){
               //save_draft('content');
               action_after_xhr_login = 1;
               check_login();
          }, 5000);
     }
}

function remove_kiamatlab(){
     /* remove kiamatlab yang mungkin ada terlepas */
     var theKiamatlab = editor.getElementsByClassName('kiamatlab');//document.querySelectorAll('#editor .kiamatlab');
     if(theKiamatlab.length){
          for (var i = 0; i < theKiamatlab.length; i++) {
               if (typeof window.addEventListener === 'function'){ (function (index) {
                    console.log('kita jumpa class kiamatlab, kita akan tukar class ke para shj');
                    console.log('class asal = '+ index.className);
                    index.className = 'para';
                    console.log('class selepas try ubah = '+ index.className);
               })(theKiamatlab[i]); }
          }
     }
}

function after_enter(){

     console.log('%cafter_enter()', 'color:red;font-size:18px; font-weigth:600;');
     //console.log('in after_enter() = ' + editor.innerHTML);
     if(editor.innerHTML.indexOf('<!--PEMISAHAN-->') > 0){
          //Pemisahan adalah tanda dari paste..
          console.log('after enter we find PEMISAHAN');
          editor.innerHTML = editor.innerHTML.replace(/<!--PEMISAHAN-->/g, '</div><div class="para" contenteditable="true" autocomplete="off">');
          meteor();
          no_paraPara();
     } else if(editor.innerHTML.indexOf('BRMESEJ') > 0){
          //case insert mesej. tukar komen tag ini kepada br muah ciket..
          console.log('after enter we find BRMESEJ');
          editor.innerHTML = editor.innerHTML.replace(/BRMESEJ/g, '<br class="newline"/><br class="newline"/>');
          meteor();
          console.log('ini insert mesej or blockquote lohh');
     } else if(editor.innerHTML.indexOf('<!--PEMISAH-->')){
          //Pemisah pula dari enter..
          console.log('after enter we find <!--PEMISAH-->');
          editor.innerHTML = editor.innerHTML.replace(/<!--PEMISAH-->/g, '</div><div class="para kiamatlab" contenteditable="true" autocomplete="off">');
          console.log(editor.innerHTML);
          meteor();

          if(editorPara){

               var array_para_kosong = new Array();
               for (var i = 0; i < editorPara.length; i++) {
                    if(editorPara[i].innerHTML.trim().length < 1){
                         array_para_kosong.push(i); // push number only.
                    }
               }

               console.log('%changguk start', 'font-size:16px;font-weight:600;');

               if(array_para_kosong.length > 0){ // hangguk
                    console.log( 'array_para_kosong = ' + array_para_kosong[0] );
                    placeCaretAtEnd(editorPara[array_para_kosong[0]], false);
               } else {

                    var myKiamatlab = editor.getElementsByClassName('kiamatlab');
                    console.log( 'myKiamatlab.length = ' + myKiamatlab.length );
                    if(myKiamatlab[0]){
                         console.log('ada kiamatlab tuk buh caret');
                         placeCaretAtEnd(myKiamatlab[0], true);
                         myKiamatlab.className = 'para';
                    } else {
                         console.log('takde kiamatlab untuk buh caret');
                    }

                    //recheck kiamatlab..
                    meteor();
                    var check_kiamatlab = editor.getElementsByClassName('kiamatlab');
                    if(check_kiamatlab.lengh > 0){
                         for (var i = 0; i < check_kiamatlab.length; i++) {
                              if( check_attribute(check_kiamatlab[i], 'class', 'para') && check_attribute(check_kiamatlab[i], 'class', 'kiamatlab') ){
                                   check_kiamatlab[i].className = 'para';

                              }
                         }
                    }

               } // hangguk

          }


     }
}


function after_insert(){

   if(paraPara){
        // foreach all element.
        var i;
        for (i = 0; i < paraPara.length; ++i) {
             var pePara = paraPara[i];
             var paraDalam = pePara.getElementsByClassName('para')[0];
             if(paraDalam){
                  var getParaDalam = paraDalam.outerHTML;
                  pePara.outerHTML = '<div class="para" contenteditable="true">' + pePara.innerHTML.replace(getParaDalam, '') + '</div>' + getParaDalam;
             }
        }
   }

     no_paraPara();

     close_all_popup();
     meteor();
     save_cursor();
}














/* POPUP AREA */
var
dialogModal = document.getElementsByClassName('modal')[0],
popupClose = document.getElementsByClassName('popup-close')
;
//CONTROL CLICKED, SHOW POPUP.
var controlButton = document.querySelectorAll('.wysiwyg-controls > a');
if(controlButton){
     for (var i = 0; i < controlButton.length; i++) {
          if (typeof window.addEventListener === 'function'){
               (function (index) {
                    controlButton[i].addEventListener('click', function(e){
                         if(check_attribute(index, 'class', 'sidebar_control')){
                              var sidebar = document.getElementsByClassName('sidebar')[0];
                              if(sidebar){
                                   var open = document.getElementById('open-sidebar');
                                   var close = document.getElementById('close-sidebar');
                                   index.style.display = 'none';
                                   if(check_attribute(index, 'id', 'close-sidebar')){
                                        sidebar.style.display = 'none';
                                        open.style.display = 'inline';
                                   }
                                   if(check_attribute(index, 'id', 'open-sidebar')){
                                        sidebar.style.display = 'block';
                                        close.style.display = 'inline';
                                   }
                              }
                         } else {
                              e.preventDefault();
                              meteor();

                              /* -------------- SEL RANGE START -------------- */
                              save_cursor();
                              console.log('%c*** dibawah selrange console once click control button ***' ,'color:green;font-weight:600');
                              console.log(selRange);

                              if(selRange == undefined || selRange== null || !selRange){
                                   console.log('yeah undefined, maybe load2 je x click apa2 terus click control..');
                                   selRange_takde();
                              } else {
                                   console.log('selRange ada, only one that we need selRange must have wrapper ');
                                   //console.log(check_attribute(selRange.startContainer, 'class','para'));//console.log(get_wrapper(selRange.startContainer, 'class', 'para'));
                                   if(get_wrapper(selRange.startContainer, 'class', 'para') || check_attribute(selRange.startContainer, 'class','para')){//kalo cursor tu dalam mana2 element ada para..
                                        /* it's okay, selrange kan dah save pada line first */
                                   } else {// false, bukan dalam para..
                                        console.log('Eh Selrange takde?');
                                        selRange_takde();
                                   }
                              }
                              /* --------------  SEL RANGE END   -------------- */

                              var id = document.getElementById(index.getAttribute('data-show'));
                              if(index.getAttribute('data-show') == 'a_a_link'){
                                   link_management();
                              }
                              if(index.getAttribute('data-show') == 'a_a_heading'){
                                   heading_management();
                              }
                              if(index.getAttribute('data-show') == 'a_a_button'){
                                   button_by_title();
                              }
                              if(index.getAttribute('data-show') == 'a_a_message'){
                                   message_box();
                              }
                              if(index.getAttribute('data-show') == 'a_a_qoute'){
                                   qoute_box();
                              }
                              if(id){
                                   dialogModal.style.display = 'block';
                                   //id.style.display = 'block';
                                   add_class(id, 'popup_content_show');
                              }
                         }
                    });
               })(controlButton[i]);
          }
     }
}

function thumbnail_switch(what){
     var img_box = document.getElementById('a_a_image');
     if(what){
          var box_title = 'Thumbnail Builder';
          add_class(img_box, 'for_thumb');
          set_post_thumb = true;
     } else {
          var box_title = 'Images Manager';
          remove_class(img_box, 'for_thumb');
          set_post_thumb = false;
     }
     img_box.getElementsByTagName('h4')[0].textContent = box_title;
}

// click set thumbnail button in box..
var thumb_picker = document.getElementsByClassName('pick-thumb-button');
if(thumb_picker){
     for (var i = 0; i < thumb_picker.length; i++) {
          thumb_picker[i].addEventListener('click', function(){
               dialogModal.style.display = 'block';
               add_class(document.getElementById('a_a_image'), 'popup_content_show');
               thumbnail_switch(true);
               console.log('set_post_thumb set to TRUE because youre click set thumb.');
          });
     }
}

function selRange_takde(){
     if(editorPara.length > 0){
          /* para ada, put cursor at end of last para.. */
          var jumlah = parseFloat(editorPara.length) - parseFloat(1)
          placeCaretAtEnd(editorPara[jumlah], false);
          save_cursor();
          //console.log('%c'+selrange,'background:black');
     } else {
          /* para xde, create new para. then put cursor at end */
          var new_para = document.createElement('div');
          new_para.className = 'para';
          new_para.setAttribute('contenteditable','true');
          new_para.setAttribute('autocomplete','off');
          editor.appendChild(new_para);
          var kiamatlab = editor.getElementsByClassName('para')[0];
          placeCaretAtEnd(kiamatlab, true);
          save_cursor();
     }
}

/* RESET SELECTED IMAGE IN IMAGE MODAL */
function reset_selected_image(){
     var img_box = document.getElementById('a_a_image');
     if(img_box){
          var pics = img_box.getElementsByClassName('pics');
          if(pics){
               var selected_img = pics[0].getElementsByClassName('selected');
               if(selected_img.length > 0){
                    for (var i = 0; i < selected_img.length; i++) {
                         // console.log(selected_img[i])
                         if(selected_img[i]){
                              selected_img[i].className  = 'border';
                         }
                    }
               }
          }
     }
}

/* CLOSE ALL POPUP */
for (var i = 0; i < popupClose.length; i++) {
     popupClose[i].addEventListener('click', close_all_popup, false);
}
function close_all_popup(){
     thumbnail_switch(false);
     dialogModal.style.display = 'none';
     var popup_contents = document.getElementsByClassName('popup-content');
     for (var i = 0; i < popup_contents.length; i++) {
          var popup_content = popup_contents[i];
          //popup_content.style.display = 'none';
          remove_class(popup_content, 'popup_content_show');
     }

     var all_input = dialogModal.getElementsByTagName('input');
     for (var i = 0; i < all_input.length; i++){
          if( !check_attribute(all_input[i], 'class', 'static_input') && !check_attribute(all_input[i], 'type', 'submit') && !get_wrapper(all_input[i], 'id', 'login-again-form') ){
               all_input[i].value = null;
          }
     }

     var all_textarea = dialogModal.getElementsByTagName('textarea');
     for (var i = 0; i < all_textarea.length; i++) {
          all_textarea[i].value = null;
     }

     var all_button = dialogModal.getElementsByClassName('insert');
     for (var i = 0; i < all_button.length; i++) {
          all_button[i].setAttribute('disabled','disabled');
     }

     //reset table modal...
     document.querySelectorAll('#a_a_table .table-content')[0].innerHTML = tableDefaultHtml;
     document.querySelectorAll('#a_a_table  .tablebg')[0].checked = false;
     document.querySelectorAll('#a_a_table  .tablebelang')[0].checked = false;

     //reset selected in image modal..
     reset_selected_image();

     document.getElementsByTagName('body')[0].setAttribute('id', 'main-blog');
     meteor();
}




/* GAMBAR POPUP TABBER AREA. */
var menu_tab = document.getElementById("pilihan-option").querySelectorAll('span');
if(menu_tab){

     for (var i = 0; i < menu_tab.length; i++) {
          if (typeof window.addEventListener === 'function'){
               (function (index) {
                    menu_tab[i].addEventListener('click', function(){
                         proceed_tab(index.getAttribute('data-show'), index.getAttribute('id'));
                    });
               })(menu_tab[i]);
          }
     }

}
function proceed_tab(data_show, id){
     //reset to display none all.
     var tab_content = document.querySelectorAll('#image-option > div');//document.getElementById("image-option").querySelectorAll('div');
     if(tab_content){
          for(var i = 0; i < tab_content.length; i++) {
               var tab_id = tab_content[i].getAttribute('id');
               if(tab_id == data_show){
                    tab_content[i].style.display = 'block';
               } else {
                    tab_content[i].style.display = 'none';
               }
          }
     }

     //add selected
     if(menu_tab){
          for(var i = 0; i < menu_tab.length; i++) {
               var theId = menu_tab[i].getAttribute('id');
               if(theId == id){
                    // add selected
                    var finalid = document.getElementById(theId);
                    var semiKelas = finalid.className.replace("selected", "");
                    var Kelas = semiKelas + ' ' + 'selected';
                    finalid.setAttribute('class', Kelas);
                    //var ddd = finalid.getAttribute('data-show');
                    //var idContent = document.getElementById(ddd);
               } else {
                    //remove selected
                    var finalid = document.getElementById(theId);
                    var Kelas = finalid.className;
                    finalid.setAttribute('class', Kelas.replace("selected", "") );
               }
          }
     }
}


//BEFORE UPLOAD. ONCHANGE INPUT
var myinput = document.getElementById("insert-file");
myinput.addEventListener('change', function () {
     if(this.files.length > 0){
          fetchimage();
          document.getElementById('jumlah-file').innerHTML = this.files.length;
          document.getElementsByClassName('upload-button')[0].removeAttribute("disabled");
     } else {
          document.getElementById('jumlah-file').innerHTML = '0';
          document.getElementsByClassName('upload-button')[0].setAttribute('disabled', 'disabled');
     }
});
function fetchimage(){
     var filelist = myinput.files || [];
     for (var i = 0; i < filelist.length; i++) {
          getBase64(filelist[i], i);
     }
}

var pre_timeout;
function getBase64(file, ai, adui) {
     var nama = file.name;
     nama = nama.replace(/\[|\]|{|}|\?|\/|\+|\-|_|\(|\)|&|\^|%|\#|\$|\@|\!|\"|\'+/g,' ');
     nama = nama.replace(/\s\s|.jpg|.png|.jpeg|.gif|.JPG|.PNG|.JPEG|.GIF|\/+/g,' ').trim();
     nama = nama.substring(0, 60);
     var reader = new FileReader();
     reader.readAsDataURL(file);
     reader.onload = function () {
          // document.getElementById("before-upload").innerHTML += '<div class="img_wrpr"><img src="'+reader.result+'"  id="img_'+ai+'" class="data_img"/><div class="data_box"><label for="usr_files[text]['+ai+']">TITLE</label><p class="box_right"><input class="nama-file" maxlength="60" name="usr_files[text]['+ai+']" type="text" value="'+nama+'" multiple="multiple" required/></p></div><!--tag--><div class="data-tag"><div class="tag-message">Tag orang, tempat atau benda yang terlibat dalam gambar</div><!--<span class="tag-message">Maximum 5 (yang popular atau general sahaja):</span>--> <div class="tag-input data_box"><label>TAG</label><p class="box_right"><input name="usr_files[img_tag]['+ai+']" class="tag-gambar" placeholder="contoh: Awie, Mahathir, Bali, Adidas, KFC" data-tooltip="Tag nama tokoh, organisasi, produk yang terlibat pada gambar ini.<br><br>Pisahkan menggunakan koma <span class=\'red\'>,<span> dengan format ejaan betul."></p></div></div>';
          //
          // alert('working lagi')



         // var kokok = '<img src="'+reader.result+'"  id="img_'+ai+'" class="data_img"/><div class="data_box"><label for="usr_files[text]['+ai+']">TITLE</label><p class="box_right"><input class="nama-file" maxlength="60" name="usr_files[text]['+ai+']" type="text" value="'+nama+'" multiple="multiple" required="required"/></p></div><div class="data-tag"> <div class="tag-input data_box"><label>TAG</label><p class="box_right"><input name="usr_files[img_tag]['+ai+']" class="tag-gambar" placeholder="contoh: Awie, Bali, Adidas, KFC" data-tooltip="hiiiii"/></p></div>';

         //wrapper
         var img_wrapper = document.createElement('div');
         img_wrapper.className = 'img_wrpr';

         //1.  <img src="'+reader.result+'"  id="img_'+ai+'" class="data_img"/>
         var imgs = document.createElement('img');
         imgs.setAttribute('src', reader.result);
         imgs.setAttribute('id', 'img_'+ai);
         imgs.className = 'data_img';

         //2. <div class="data_box"><label for="usr_files[text]['+ai+']">TITLE</label><p class="box_right"><input class="nama-file" maxlength="60" name="usr_files[text]['+ai+']" type="text" value="'+nama+'" multiple="multiple" required="required"/></p></div>
         var data_box = document.createElement('div');
         data_box.className = 'data_box';
           //2.1 <label for="usr_files[text]['+ai+']">TITLE</label>
           var lbl = document.createElement('label');
           lbl.setAttribute('for', 'usr_files[text]['+ai+']')
           lbl.textContent = 'TITLE';
           //2.2 <p class="box_right"><input class="nama-file" maxlength="60" name="usr_files[text]['+ai+']" type="text" value="'+nama+'" multiple="multiple" required="required"/></p>
           var p1 = document.createElement('p');
           p1.className = 'box_right';
             //2.2.1 <input class="nama-file" maxlength="60" name="usr_files[text]['+ai+']" type="text" value="'+nama+'" multiple="multiple" required="required"/>
               var box_right_input = document.createElement('input');
               box_right_input.className = 'nama-file';
               box_right_input.setAttribute('maxlength', '60');
               box_right_input.setAttribute('name', 'usr_files[text]['+ai+']');
               box_right_input.setAttribute('type', 'text');
               box_right_input.value = nama;
               box_right_input.setAttribute('multiple', 'multiple');
               box_right_input.setAttribute('required', 'required');
               box_right_input.setAttribute('data-tooltip', 'Masukkan keyword carian yang sesuai dengan gambar ini.<br><br>Maximum 60 aksara.');
               p1.appendChild(box_right_input);
         data_box.appendChild(lbl);
         data_box.appendChild(p1);

         //3. <div class="data-tag"> <div class="tag-input data_box"><label>TAG</label><p class="box_right"><input name="usr_files[img_tag]['+ai+']" class="tag-gambar" placeholder="contoh: Awie, Bali, Adidas, KFC" data-tooltip="hiiiii"/></p></div>
         var data_tag = document.createElement('div');
         data_tag.className = 'tag-input data_box';
               //3.1 <label>TAG</label>
               lbls = document.createElement('label');
               lbls.textContent = 'TAG';
               //3.2 <p class="box_right"><input name="usr_files[img_tag]['+ai+']" class="tag-gambar" placeholder="contoh: Awie, Bali, Adidas, KFC" data-tooltip="hiiiii"/></p>
               var p2 = document.createElement('p');
               p2.className = 'box_right';
                  //3.2.1 <input name="usr_files[img_tag]['+ai+']" class="tag-gambar" placeholder="contoh: Awie, Bali, Adidas, KFC" data-tooltip="hiiiii"/>
                  var box_right_1_input = document.createElement('input');
                  box_right_1_input.className = 'tag-gambar';
                  box_right_1_input.setAttribute('name', 'usr_files[img_tag]['+ai+']');
                  box_right_1_input.setAttribute('type', 'text');
                  box_right_1_input.setAttribute('placeholder', 'contoh: Awie, Bali, Adidas, KFC, Android');
                  box_right_1_input.setAttribute('data-tooltip', 'Tag Nama figura, organisasi, produk atau lain-lain yang terlibat dalam gambar ini.<br><br>Pisahkan tags menggunakan comma <b><span class="red">,</span><b> dengan format ejaan yang betul.<br><br><span class="red">cth:</span> ABPBH, BMW, Siti Nurhaliza.');
                  p2.appendChild(box_right_1_input);
         data_tag.appendChild(lbls);
         data_tag.appendChild(p2);

         img_wrapper.appendChild(imgs);
         img_wrapper.appendChild(data_box);
         img_wrapper.appendChild(data_tag);

         var bfr_upload = document.getElementById("before-upload");

         bfr_upload.appendChild(img_wrapper);

         listen_tooltip();

     };
}










/* HEADING AREA */
function heading_management(){
   var heading_inp =document.querySelectorAll('#a_a_heading .insert-text')[0];
   var button = document.querySelectorAll('#a_a_heading .insert')[0]
   heading_inp.value = GetSelectedText(selRange).trim();
   button.removeAttribute('disabled')
   listen_heading_link(button, heading_inp)
}

function listen_heading_link(button, text){
   var inp_link = document.getElementById('link_heading')
   if(inp_link.value.length >0) inp_link.value = inp_link.value.trim()

   button.addEventListener('click', function(){
      if(text.value.length > 0) text.value = text.value.trim()
      if(text.value.length > 0 && inp_link.value.length == 0){
         proceed_heading(text, false)
      } else if(text.value.length > 0 && inp_link.value.length > 0){//check link
         var xhr      = new XMLHttpRequest();
         xhr.open('POST', 'https://media.malayatimes.com/get-link-title?link='+ encodeURI(link_heading.value), true);
         xhr.onreadystatechange = function() {
             if (xhr.readyState === 4 && xhr.status === 200){
                 var data = xhr.responseText
                   if(data && data.length > 0){
                        proceed_heading(text, inp_link)
                   } else {
                       alert('link not valid')
                   }
             }
         }
         xhr.send();
      } else if(text.value.length == 0){
         alert('Failed to insert heading with no text, click X button if you want\'t to cancel')
      }
   })
}

function proceed_heading(tt, ll){
   var myhtml = '<div class="para"><b class="sub-heading">';
   if(ll.value) myhtml += '<a href="'+ll.value+'">';
   myhtml += tt.value;
   if(ll.value) myhtml += '</a>'
   myhtml += '</b></div>';
   pasteHtmlAtCaret(myhtml, 'insert');
}

/* LINK AREA */

//once link popup...
function link_management(){
     // fill text input with selection..
     document.querySelectorAll('#a_a_link .insert-text')[0].value = GetSelectedText(selRange).trim();
     link_by_title();
}

//once link text change...
var type_link_text;
document.querySelectorAll('#a_a_link .insert-text')[0].addEventListener('input', function(){
     clearTimeout(type_link_text);
     type_link_text = setTimeout(function(){
          link_by_title();
     }, 1000);
});

// search link to db by title...
function link_by_title(){
     var text = document.querySelectorAll('#a_a_link .insert-text')[0].value.trim();
     if(text.length > 0){
          //cari title ni.
          var xhr      = new XMLHttpRequest();
          xhr.open('POST', 'https://media.malayatimes.com/search_link/title?cari='+ encodeURI(text), true);
          xhr.onreadystatechange = function() {
               if (xhr.readyState === 4 && xhr.status === 200){
                    var data = xhr.responseText;
                    if(data && data.length > 0){
                         var link_input = document.querySelectorAll('#a_a_link .insert-link')[0];
                         if(link_input.value.length == 0){
                              link_input.value = data;
                         }
                         check_link_field();
                    } else {
                         check_link_field();
                    }
               }
          }
          xhr.send();
     } else {
          check_link_field();
     }
}

//once link insert button clicked..
document.querySelectorAll('#a_a_link .insert')[0].addEventListener('click', function(){
     var text = document.querySelectorAll('#a_a_link .insert-text')[0];
     var link = document.querySelectorAll('#a_a_link .insert-link')[0];
     if(text.value.length > 0 && link.value.length > 0){
          var str = GetSelectedText(selRange);
          if(str[str.length - 1] == ' '){
               var space = ' ';
               // console.log('last char is space..');
          } else {
               var space = '';
               // console.log('last char not space..');
          }
          var myhtml = '<a href="'+link.value+'">'+text.value.trim()+'</a>' + space;
          pasteHtmlAtCaret(myhtml, 'insert');
          text.value = '';
          link.value = '';
     } else {
          alert('Edit semula. Link dan text tidak boleh kosong...');
     }
});

function strHtmlToDom(MYSTR){
     /* EX: strHtmlToDom('<div class="wtf">HAHAHAHA</div>') */
     var div = MYSTR,
     parser = new DOMParser(),
     doc = parser.parseFromString(MYSTR, "text/xml");
     return doc.firstChild;
}

//once link input changed...
var tatuta;
document.querySelectorAll('#a_a_link .insert-link')[0].addEventListener('input', function(){
     clearTimeout(tatuta);
     tatuta = setTimeout(function(){
          var input_link = document.querySelectorAll('#a_a_link .insert-link')[0];
          if(input_link.value.length > 0){
               var xhr      = new XMLHttpRequest();
               xhr.open('POST', 'https://media.malayatimes.com/get-link-title?link='+ encodeURI(input_link.value.trim()), true);
               xhr.onreadystatechange = function() {
                    if (xhr.readyState === 4 && xhr.status === 200){
                         console.log('yeaaah xhr sudah finish');
                         console.log(xhr.responseText);
                         var data = xhr.responseText;
                         if(data && data.length > 0){
                              var text_input = document.querySelectorAll('#a_a_link .insert-text')[0];
                              text_input.value = data;
                              check_link_field();
                         } else {
                              check_link_field();
                         }
                    }
               }
               xhr.send();
          } else {
               check_link_field();
          }
     }, 1000);
});

function check_link_field(){
     var link = document.querySelectorAll('#a_a_link .insert-link')[0];
     var text =  document.querySelectorAll('#a_a_link .insert-text')[0];
     console.log('ABC');
     console.log(link.value + ' = ' + link.value.length);
     console.log(text.value + ' = ' + text.value.length);
     if(link.value.length > 6 && text.value.length > 0){
          console.log('DEF');
          document.querySelectorAll('#a_a_link .insert')[0].removeAttribute('disabled');
     } else {
          console.log('GHI');
          document.querySelectorAll('#a_a_link .insert')[0].setAttribute('disabled', 'disabled');
     }
}
/* END LINK AREA */


/* RELATED AREA */
var relatedLinkInput = document.querySelectorAll('#a_a_related .insert-related-link')[0];
var relatedTextInput = document.querySelectorAll('#a_a_related .insert-related-text')[0];
var relatedInsertButton = document.querySelectorAll('#a_a_related .insert')[0];

relatedLinkInput.addEventListener('input', function(){
     if(this.value.length > 0){
          var xhr      = new XMLHttpRequest();
          xhr.open('POST', 'https://media.malayatimes.com/get-link-title?link='+ encodeURI(this.value), true);
          xhr.onreadystatechange = function() {
               if (xhr.readyState === 4 && xhr.status === 200){
                    var data = xhr.responseText;
                    if(data){
                         document.querySelectorAll('#a_a_related .insert-related-text')[0].value = data;
                         if(relatedTextInput.value.length > 0){
                              relatedInsertButton.removeAttribute('disabled');
                         }
                    }
               }
          }
          xhr.send();
     } else {
          relatedInsertButton.setAttribute('disabled', 'disabled');
     }
});

//once change Text Related input..
var sakit_leher;
relatedTextInput.addEventListener('input', function(){
     if(this.value.length > 0){
          clearTimeout(sakit_leher);
          sakit_leher = setTimeout(function(){
               related_by_title();
          }, 2000);
     } else {
          relatedInsertButton.setAttribute('disabled', 'disabled');
     }
});

function related_by_title(){
     var theinput = document.querySelectorAll('#a_a_related .insert-related-text')[0];
     var xhr      = new XMLHttpRequest();
     xhr.open('POST', 'https://media.malayatimes.com/search_link/title?cari='+ encodeURI(theinput.value), true);
     xhr.onreadystatechange = function() {
          if (xhr.readyState === 4 && xhr.status === 200){
               var data = xhr.responseText;
               if(data){
                    document.querySelectorAll('#a_a_related .insert-related-link')[0].value = data;
                    if(relatedLinkInput.value.length > 0){
                         relatedInsertButton.removeAttribute('disabled');
                    }
               }
          }
     }
     xhr.send();
}

//insert related button clicked..
relatedInsertButton.addEventListener('click', function(){
     if(relatedLinkInput.value.length > 0 && relatedTextInput.value.length > 0){
          var str = GetSelectedText(selRange);
          if(str[str.length - 1] == ' '){
               var space = ' ';
          } else {
               var space = '';
          }
          var myhtml = '<div class="para" autocomplete="off"><span class="related"><a href="'+relatedLinkInput.value+'" class="link">'+relatedTextInput.value+'</a></span>' + space+'</div>';
          pasteHtmlAtCaret(myhtml, 'insert');
          this.setAttribute('disabled','disabled');
          relatedLinkInput.value = null;
          relatedTextInput.value = null;
     }
})
/* END RELATED AREA */

/* BUTTON AREA */
var buttonTextInput = document.querySelectorAll('#a_a_button .insert-text-button')[0];
var buttonLinkInput = document.querySelectorAll('#a_a_button .insert-link-button')[0];
var buttonInsertInput = document.querySelectorAll('#a_a_button .insert')[0];
var buttonTimer;
buttonTextInput.addEventListener('input', function(){
     if(this.value.length > 0){
          clearTimeout(buttonTimer);
          buttonTimer = setTimeout(function(){
               button_by_title();
          }, 1000);
     } else {
          buttonInsertInput.setAttribute('disabled', 'disabled');
     }
});

function button_by_title(){
     if(buttonTextInput.value.length < 1){
          buttonTextInput.value = GetSelectedText(selRange);
     }

     if(buttonTextInput.value.length > 0){
          var xhr      = new XMLHttpRequest();
          xhr.open('POST', 'https://media.malayatimes.com/search_link/title?cari='+ encodeURI(buttonTextInput.value), true);
          xhr.onreadystatechange = function() {
               if (xhr.readyState === 4 && xhr.status === 200){
                    var data = xhr.responseText;
                    if(data.length > 0){
                         buttonLinkInput.value = data;
                         buttonInsertInput.removeAttribute('disabled');
                    }
               }
          }
          xhr.send();
     }
}

var buttonTiming;
buttonLinkInput.addEventListener('input', function(){
     if(this.value.length > 0){
          clearTimeout(buttonTiming);
          buttonTiming = setTimeout(function(){
               button_by_link();
          }, 1000);
     } else {
          buttonInsertInput.setAttribute('disabled', 'disabled');
     }
});

function button_by_link(){
     var xhr      = new XMLHttpRequest();
     xhr.open('POST', 'https://media.malayatimes.com/get-link-title?link='+ encodeURI(buttonLinkInput.value), true);
     xhr.onreadystatechange = function() {
          if (xhr.readyState === 4 && xhr.status === 200){
               var data = xhr.responseText;
               if(data.length > 0){
                    buttonTextInput.value = data;
                    buttonInsertInput.removeAttribute('disabled');
               }
          }
     }
     xhr.send();
}

// once click insert button widget
buttonInsertInput.addEventListener('click', function(){
     var myhtml = '<a class="button" href="'+buttonLinkInput.value.trim()+'">'+buttonTextInput.value.trim()+'</a>';
     pasteHtmlAtCaret(myhtml,'insert');
     buttonLinkInput.value = null;
     buttonTextInput.value = null;
     buttonInsertInput.setAttribute('disabled','disabled');
});


/* END BUTTON AREA */


/* TABLE AREA */
var tableDefaultHtml = '<table class="content-table"> <thead> <tr> <th contenteditable="true" autocomplete="off">EDIT ME</th> <th contenteditable="true" autocomplete="off">EDIT ME</th> </tr> </thead> <tbody> <tr> <td contenteditable="true" autocomplete="off">-</td> <td contenteditable="true" autocomplete="off">-</td> </tr> <tr> <td contenteditable="true" autocomplete="off">-</td> <td contenteditable="true" autocomplete="off">-</td> </tr> </tbody></table>';

// listen horizontal table...
document.querySelectorAll('#a_a_table .tablebg')[0].addEventListener('change', function(){
     if(this.checked === true){
          document.querySelectorAll('#a_a_table  .content-table')[0].className = 'content-table horizontal';
          if(document.querySelectorAll('#a_a_table  .tablebelang')[0].checked == true){
               document.querySelectorAll('#a_a_table  .tablebelang')[0].checked = false;
          }
     } else if(this.checked === false){
          document.querySelectorAll('#a_a_table  .content-table')[0].className = 'content-table';
     }
});

// listen vertical table..
document.querySelectorAll('#a_a_table  .tablebelang')[0].addEventListener('change', function(){
     if(this.checked === true){
          document.querySelectorAll('#a_a_table  .content-table')[0].className = 'content-table vertical';
          if(document.querySelectorAll('#a_a_table  .tablebg')[0].checked == true){
               document.querySelectorAll('#a_a_table  .tablebg')[0].checked = false;
          }
     } else if(this.checked === false){
          document.querySelectorAll('#a_a_table  .content-table')[0].className = 'content-table';
     }
});

document.querySelectorAll('#a_a_table .add-rows')[0].addEventListener('click', function(){
     var tbody = document.querySelectorAll('#a_a_table tbody')[0];
     tbody.innerHTML = tbody.innerHTML + '<tr><td contenteditable="true" autocomplete="off">-</td><td contenteditable="true" autocomplete="off">-</td></tr>';
});

document.querySelectorAll('#a_a_table .remove-rows')[0].addEventListener('click', function(){
     var tbody = document.querySelectorAll('#a_a_table tbody')[0];
     var rws=tbody.getElementsByTagName('tr');
     tbody.removeChild(rws[rws.length-1]);
});

document.querySelectorAll('#a_a_table .insert')[0].addEventListener('click', function(){
     var table = document.querySelectorAll('#a_a_table .table-content')[0].innerHTML;
     pasteHtmlAtCaret('<div class="para" autocomplete="off">'+table+'</div>', 'insert');
     document.querySelectorAll('#a_a_table .table-content')[0].innerHTML = tableDefaultHtml;
     document.querySelectorAll('#a_a_table  .tablebg')[0].checked = false;
     document.querySelectorAll('#a_a_table  .tablebelang')[0].checked = false;
     listen_table();
});
/* END TABLE AREA */

/* MESSAGE AREA */
function message_box(){
     document.querySelectorAll('#a_a_message .insert-message')[0].value = GetSelectedText(selRange);
     document.querySelectorAll('#a_a_message .insert-message')[0].value = document.querySelectorAll('#a_a_message .insert-message')[0].value.trim();
     if(document.querySelectorAll('#a_a_message .insert-message')[0].value.length > 0){
          document.querySelectorAll('#a_a_message .insert')[0].removeAttribute('disabled');
     }
}

document.querySelectorAll('#a_a_message .insert-message')[0].addEventListener('input', function(){
     if(this.value.length > 0){
          document.querySelectorAll('#a_a_message .insert')[0].removeAttribute('disabled');
     } else {
          document.querySelectorAll('#a_a_message .insert')[0].setAttribute('disabled','disabled');
     }
});

// once click insert mesage widget
document.querySelectorAll('#a_a_message .insert')[0].addEventListener('click', function(){
     var box = document.querySelectorAll('#a_a_message .insert-message')[0];
     if(box.value.length > 0){
          pasteHtmlAtCaret('<div class="para"><div class="message" contenteditable="true" autocomplete="off">'+format_mesage(box.value)+'</div></div>', 'enter');
          close_all_popup();
     } else {
          alert('Anda tidak boleh memasukkan message box yang tiada isi. Edit atau close untuk cancel..');
     }
});
/* END MESSAGE AREA */

function format_mesage(balue){
     var balue = balue.split("\n");
     var my_array = new Array();
     for (var i = 0; i < balue.length; i++) {
          vall = balue[i].trim();
          if(vall.length > 0){
               my_array.push(vall);
               //my_div.append(vall);
          }
     }
     var my_div = document.createElement('div');
     var last = parseFloat(my_array.length) - parseFloat(1);
     for (var i = 0; i < my_array.length; i++) {
          if( i != last){
               my_div.append(my_array[i] + 'BRMESEJ');
          } else { // last
               my_div.append(my_array[i]);
          }
     }
     return my_div.innerHTML;
}

/* BLOCKQUOTE AREA */
function qoute_box(){
     document.querySelectorAll('#a_a_quote .insert-quote')[0].value = GetSelectedText(selRange);
     document.querySelectorAll('#a_a_quote .insert-quote')[0].value = document.querySelectorAll('#a_a_quote .insert-quote')[0].value.trim();
     if(document.querySelectorAll('#a_a_quote .insert-quote')[0].value.length > 0){
          document.querySelectorAll('#a_a_quote .insert')[0].removeAttribute('disabled');
     }
}

//perubahan blockqoute.
document.querySelectorAll('#a_a_quote .insert-quote')[0].addEventListener('input', function(){
     if(this.value.length > 0){
          document.querySelectorAll('#a_a_quote .insert')[0].removeAttribute('disabled');
     } else {
          document.querySelectorAll('#a_a_quote .insert')[0].setAttribute('disabled','disabled');
     }
});

document.querySelectorAll('#a_a_quote .insert')[0].addEventListener('click', function(){
     var thevalue = document.querySelectorAll('#a_a_quote .insert-quote')[0].value;
     if(thevalue.length > 0){
          pasteHtmlAtCaret('<div class="para"><blockquote contenteditable="true" autocomplete="off">'+format_mesage(thevalue)+'</blockquote></div>','enter');
          document.querySelectorAll('#a_a_quote .insert')[0].setAttribute('disabled','disabled');
          document.querySelectorAll('#a_a_quote .insert-quote')[0].value = null;
          close_all_popup();
     } else {
          alert('Untuk memasukkan element ini anda hendaklah mengisi field yang disediakan. Isi atau close untuk cancel..');
     }
});
/* END BLOCKQUOTE AREA */

/* TAG AREA */
function build_tag_class(txt){
   var tag_class = filter_char(txt);
   if(tag_class.length < 1){ return false; }
   tag_class = tag_class.trim();
   if(tag_class.length < 1){ return false; }
   tag_class = tag_class.replace(/ /g, '_');
   return tag_class.replace(/ /g, '_');
}
//var search_tag_kuar;
function cari_tag(){
     //clearTimeout(search_tag_kuar);
     document.getElementsByClassName('label-list')[0].innerHTML = '<li><i>relavan tags appear when you stop typing..</i></li>';
     //search_tag_kuar = setTimeout(function(){
     var carian = document.getElementById('label-search');
     document.getElementsByClassName('label-list')[0].style.display = 'block';
     if(carian.value.length > 2){
          var xhr      = new XMLHttpRequest();
          xhr.open('GET', 'https://media.malayatimes.com/search-tag/ajax?cari='+ encodeURI(carian.value), true);
          xhr.onreadystatechange = function() {
               if (xhr.readyState === 4 && xhr.status === 200){
                    var data = xhr.responseText;
                    if(data){
                       document.getElementsByClassName('label-list')[0].style.display = 'none';
                         var check_berapa = document.getElementsByClassName('label-list')[0].innerHTML;
                         var the_el = document.getElementById('label-list');//document.getElementsByClassName('label-list')[0];
                         // the_el.innerHTML = data;
                         the_el.innerHTML = data;
                         label_list(check_berapa);
                    }
               }
          }
          xhr.send();
     } else {
          document.getElementsByClassName('label-list')[0].innerHTML = '<li><i>atleast 3 huruf..</i></li>';
     }
     //}, 2000);
     return false;
}
function label_list(check_berapa){
     var waitUntil = function (fn, condition, interval) {
          interval = interval || 100;
          var shell = function () {
               var timer = setInterval(
                    function () {
                         var check;

                         try { check = !!(condition()); } catch (e) { check = false; }

                         if (check) {
                              clearInterval(timer);
                              delete timer;
                              fn();
                         }
                    },
                    interval
               );
          };
          return shell;
     };
     waitUntil(
          function(){
               //-----
               meteor();
               var label_list = document.getElementsByClassName('label-list')[0].querySelectorAll('li');
               for (var i = 0; i < label_list.length; i++) {
                    if (typeof window.addEventListener === 'function'){
                         (function (index) {
                              var label_timeout;
                              document.getElementsByClassName('label-list')[0].style.display = 'block';
                              label_list[i].addEventListener('click', function(){

                                 document.getElementsByClassName('label-list')[0].style.display = 'none';

                                   var tag_class = build_tag_class(index.textContent);

                                   console.log('inilah dia: ', tag_class, document.getElementsByClassName(tag_class).length < 1);

                                   if(tag_class && document.getElementsByClassName(tag_class).length < 1){

                                        var main_form = document.createElement('input');
                                        main_form.setAttribute('value', index.textContent);
                                        main_form.setAttribute('name', 'tag[]');
                                        main_form.className = tag_class;
                                        main_form.setAttribute('readonly', 'true');
                                        main_form.setAttribute('type', 'hidden');
                                        var form_elem = document.getElementsByClassName('main-form')[0];
                                        form_elem.insertBefore(main_form, form_elem.childNodes[0]);

                                        var draft_hidden = document.createElement('input');
                                        draft_hidden.setAttribute('value', index.textContent);
                                        draft_hidden.setAttribute('name', 'tag[]');
                                        draft_hidden.className = tag_class;
                                        draft_hidden.setAttribute('readonly', 'true');
                                        draft_hidden.setAttribute('type', 'hidden');
                                        var draft_form = document.getElementById('draft-form');
                                        draft_form.insertBefore(draft_hidden, draft_form.childNodes[0]);

                                        labelresult = document.createElement('span');
                                        labelresult.setAttribute('class', 'selected-label '+ tag_class);
                                        labelresult.setAttribute('title', 'click to remove');
                                        labelresult.setAttribute('data-val', index.textContent.trim());
                                        labelresult.innerHTML = index.textContent.trim()+'<i class="fa fa-times" aria-hidden="true"></i>';
                                        var labelresult_elem = document.getElementById('labelresult');
                                        labelresult_elem.insertBefore(labelresult, labelresult_elem.childNodes[0]);

                                        document.getElementById('label-search').value = null;
                                        listen_remove_label();
                                        clearTimeout(label_timeout);
                                        label_timeout = setTimeout(function () {
                                             action_after_xhr_login = 1;
                                             check_login();
                                        }, 5000);
                                    }
                                   else {
                                        console.log('Dilarang mengulangi Tag yang sama..');
                                   }
                              });
                         })(label_list[i]);
                    }
               }
               //-----
          },
          function(){
               // the code that tests here... (return true if test passes; false otherwise)
               var check_border = document.getElementsByClassName('label-list')[0].innerHTML;
               return !!(check_border !== check_berapa);
               //return !!(document.getElementsByClassName('pics')[0].innerHTML !== '');
          },
          50 // amout to wait between checks
     )();
}

document.getElementById('label-search').addEventListener('input',function(){
     cari_tag();
});

var hide_list;
document.getElementsByClassName('label-list')[0].addEventListener('mouseleave', function(){
     clearTimeout(hide_list);
     hide_list = setTimeout(function(){
          document.getElementsByClassName('label-list')[0].style.display = 'none';
     }, 500);
});

listen_remove_label();

function listen_remove_label(){

     var selectedLabel_box = document.getElementById('labelresult');
     var selected_label = selectedLabel_box.getElementsByClassName('selected-label');
     if(selected_label){
          for (var i = 0; i < selected_label.length; i++) {
               if (typeof window.addEventListener === 'function'){
                    (function (index) {
                         var remove_label_timeout;
                         selected_label[i].addEventListener('click', function(e){

                              // remove selected inside labelresult...
                              var el = index;
                              if(el){
                                   if( el.parentNode ) el.parentNode.removeChild( el );
                              }

                              //remove all hidden input with class.
                              var tag_class = build_tag_class(index.getAttribute('data-val'));
                              if(tag_class){
                                 console.log('nak remove class', tag_class)
                                 var ell;
                                 while(document.getElementsByClassName(tag_class)[0]){
                                    var ell = document.getElementsByClassName(tag_class)[0];
                                    if(ell.parentNode) ell.parentNode.removeChild(ell);
                                 }
                              }

                              clearTimeout(remove_label_timeout);
                              remove_label_timeout = setTimeout(function() {
                                   action_after_xhr_login = 1;
                                   check_login();
                              }, 5000);
                         });
                    })(selected_label[i]);
               }
          }
     }
}

listen_remove_interest();

function listen_remove_interest(){

   var selectedLabel_box = document.getElementById('adsresult');
   if(selectedLabel_box){

      var selected_label = selectedLabel_box.getElementsByClassName('selected-interest');
      if(selected_label){
           for (var i = 0; i < selected_label.length; i++) {
                if (typeof window.addEventListener === 'function'){
                     (function (index) {
                          selected_label[i].addEventListener('click', function(e){
                               var tag_val = index.getAttribute('data-val');
                               var tag_val = filter_char(tag_val);
                               var the_tag_id = tag_val.replace(/ /g, '_');
                               var tag_id = 'interest_value_'+ tag_val+'_id';
                               var tag_id = tag_id.replace(/ /g, '_');
                               // remove selected inside labelresult...
                               var el = index;
                               if(el){
                                    if( el.parentNode ) el.parentNode.removeChild( el );
                               }
                               //remove .main-form tag input copy...
                               var ele = document.getElementById(tag_id);
                               if(ele){
                                    if(ele.parentNode) ele.parentNode.removeChild(ele);
                               }
                               //remove #draft-form tag input copy...
                               var elem = document.getElementById('draft_'+tag_id);
                               if(elem){
                                    elem.parentNode.removeChild(elem);
                               }
                               remove_label_timeout = setTimeout(function() {
                                    action_after_xhr_login = 1;
                                    check_login();
                               }, 5000);
                          });
                     })(selected_label[i]);
                }
           }
      }

   }

}


function filter_char(data){
     return data.replace(/[&\/\\#,+()$~%.!\@\^\_\=\-\[\]\"\;\`\'\|:*?<>{}]/g, '');
}
/* END TAG AREA */


// AJAX UPLOAD AREA.
document.getElementById('ajaxupload').addEventListener('submit', function(e) {
     rainbow_msg(document.getElementById('wait-full'), true, 'MEMERIKSA FAIL', 'SILA TUNGGU SEBENTAR..');
     e.preventDefault();
     //run_ajax(this)
     action_after_xhr_login = 2;
     check_login();
});

// function after_xhr_1() = #save_draft();

function after_xhr_2(){
   run_ajax(document.getElementById('ajaxupload')),document.getElementsByClassName('modal')[0].style.display='block';
}

function run_ajax(getit){
     var plus = document.querySelectorAll('.img_wrpr').length;
     var old = document.querySelectorAll('.pics .border').length;
     var total = parseFloat(plus) + parseFloat(old);
     var waitUntil = function (fn, condition, interval) {
          interval = interval || 100;
          var shell = function () {
               var timer = setInterval(
                    function () {
                         var check;

                         try { check = !!(condition()); } catch (e) { check = false; }

                         if (check) {
                              clearInterval(timer);
                              delete timer;
                              fn();
                         }
                    },
                    interval
               );
          };
          return shell;
     };
     waitUntil(
          function(){
               after_html_changed(); // only this way will made your js working for xhr element!
          },
          function(){
               // the code that tests here... (return true if test passes; false otherwise)
               var check_border = document.querySelectorAll('.pics .border');
               return !!(check_border.length === total);
               //return !!(document.getElementsByClassName('pics')[0].innerHTML !== '');
          },
          50 // amout to wait between checks
     )();
     rainbow_msg(document.getElementById('wait-full'), true, 'MEMUATNAIK GAMBAR', 'SILA TUNGGU SEBENTAR..');
     document.getElementById('jumlah-file').innerHTML = '0';
     var formData = new FormData(getit);
     var xhr      = new XMLHttpRequest();
     xhr.open('POST', 'https://media.malayatimes.com/upload-wiki/', true);
     xhr.onreadystatechange = function() {
          if ( xhr.readyState === 4 ) {
               var data = xhr.responseText;
               console.log('upload ajax response is here', data);
               if(data){
                    var fake_div = document.createElement('div');
                    fake_div.innerHTML = data;
                    console.log('fake_div');
                    console.log(fake_div);
                    var border_wrapper = fake_div.getElementsByClassName('border');
                    if(border_wrapper){
                         console.log(border_wrapper.length);
                         for (var i = 0; i < border_wrapper.length; i++) {
                              document.getElementById('pass-thumbnail').value = border_wrapper[i].outerHTML;
                              console.log('value pass-thumbnail = ' + document.getElementById('pass-thumbnail').value);
                              if(!check_pics()){
                                   var pics = document.getElementsByClassName('pics')[0];
                                   pics.innerHTML = border_wrapper[i].outerHTML + pics.innerHTML;
                                   seo_pics();
                                   //after_html_changed();
                              }
                         }
                    }
               }

               rainbow_msg(document.getElementById('wait-full'), false, 'MEMERIKSA DATA', 'SILA TUNGGU SEBENTAR..');
          }
     }
     xhr.send( formData );
     document.getElementsByClassName('upload-button')[0].setAttribute('disabled', 'disabled');
     document.getElementById('before-upload').innerHTML = '';
}

function after_html_changed(){
     meteor();
     var brdr = document.getElementsByClassName('pics')[0].querySelectorAll('div');
     for(var i = 0; i < brdr.length; i++) {
          if (typeof window.addEventListener === 'function'){
               (function (index) {
                    brdr[i].addEventListener('click', function(){
                         console.log(brdr.length);
                         var Klas = index.className;//index.getAttribute('class');
                         console.log(Klas.trim());
                         if(Klas.trim() == 'border'){//true.
                              //index.setAttribute('class', 'border selected');
                              add_class(index,'selected')
                         } else {
                              //index.setAttribute('class', 'border');
                              remove_class(index, 'selected');
                         }
                         var jumlah = document.getElementsByClassName('pics')[0].querySelectorAll('.selected');
                         if(jumlah){
                              if(jumlah.length > 0){
                                   document.getElementById('img-insert-btn').removeAttribute('disabled');
                              } else {
                                   document.getElementById('img-insert-btn').setAttribute('disabled', 'disabled');
                              }
                         }
                         meteor();
                    });
               })(brdr[i]);
          }
     }
     // insert_gambar();
}

//CARI GAMBAR AJAX AREA.
var search_gambar;
document.getElementsByClassName('cari-gambar')[0].addEventListener('keydown', function(){
     var carian = this.value;
     if(carian && carian.length > 2){
          clearTimeout(search_gambar);
          var check_berapa = false;
          search_gambar = setTimeout(function() {
               var xhr      = new XMLHttpRequest();
               xhr.open('POST', 'https://media.malayatimes.com/search_image/ajax?cari='+ encodeURI(carian), true);
               xhr.onreadystatechange = function() {
                    if ( xhr.readyState === 4 && xhr.status === 200 ) {
                         var ddaattaa = xhr.responseText;
                         console.log(ddaattaa)
                         if(ddaattaa == 'logout'){
                            check_login()
                         } else {
                              var check_berapa = (ddaattaa.match(/class=\"pale\"/g)||[]).length;
                              document.getElementById('cari-gambar-result').innerHTML = ddaattaa;
                              document.getElementById('cari-gambar-result').style.display = 'block';
                              after_cari_gambar(check_berapa);
                         }
                    }
               }
               xhr.send();
          }, 1000);

     }// end if carian..
});

function after_cari_gambar(check_berapa){
     if(check_berapa){
          if(check_berapa != 0){
               var waitUntil = function (fn, condition, interval) {
                    interval = interval || 100;
                    var shell = function () {
                         var timer = setInterval(
                              function () {
                                   var check;

                                   try { check = !!(condition()); } catch (e) { check = false; }

                                   if (check) {
                                        clearInterval(timer);
                                        delete timer;
                                        fn();
                                   }
                              },
                              interval
                         );
                    };
                    return shell;
               };
               waitUntil(
                    function(){
                         var pale = document.querySelectorAll('.picca .pale');
                         if(pale){
                              for (var i = 0; i < pale.length; i++) {
                                   if (typeof window.addEventListener === 'function'){
                                        (function (index) {

                                             pale[i].addEventListener('click', function(e){
                                                  e.preventDefault();
                                                  index.setAttribute('class', 'border');
                                                  // set thumbnail seo and viral..
                                                  document.getElementById('pass-thumbnail').value = index.getElementsByTagName('img')[0].outerHTML;
                                                  console.log(check_pics());
                                                  if(!check_pics()){
                                                       var pics = document.getElementsByClassName('pics')[0];
                                                       pics.innerHTML = index.outerHTML + pics.innerHTML;
                                                       index.outerHTML = '';
                                                       seo_pics();
                                                       after_html_changed();
                                                  } else {
                                                       index.setAttribute('class', 'pale');
                                                       index.outerHTML = '';
                                                  }

                                                  if(set_post_thumb){
                                                       close_all_popup();
                                                  }

                                             });
                                        })(pale[i]);
                                   }
                              }
                         }
                    },
                    function(){
                         // the code that tests here... (return true if test passes; false otherwise)
                         var check_border = document.querySelectorAll('.picca .pale');
                         return !!(check_border.length === check_berapa);
                         //return !!(document.getElementsByClassName('pics')[0].innerHTML !== '');
                    },
                    50 // amout to wait between checks
               )();
          }
     }
}

function check_pics(){ // check_pics() will true jika ada. false jika xde
     var gambar = document.getElementById('pass-thumbnail').value;
     var pics = document.getElementsByClassName('pics')[0];
     if(pics){
          var img = pics.getElementsByTagName('img');
          if(img){
               var img_array = new Array();
               for (var i = 0; i < img.length; i++) {
                    img_array.push(img[i].getAttribute('src').trim());
               }
          }
          var wrap_gambar = document.createElement('div');
          wrap_gambar.innerHTML = gambar;
          var gambir = wrap_gambar.getElementsByTagName('img')[0].getAttribute('src').trim();
          var check_array = img_array.indexOf(gambir) > -1;
          return check_array; // true jika ada. false jika xde
     }
}




function seo_pics(){
     var gambar = document.getElementById('pass-thumbnail').value;

     //masukkan data ke SEO  and VIRAL meta..
     var seoThumbnails = document.getElementById('seo-thumbnails');
     var viralThumbnails = document.getElementById('viral-thumbnails');
     var postThumbnails = document.getElementById('post-thumnails');

     var id_array = [postThumbnails,seoThumbnails,viralThumbnails];
     for (var i = 0; i < id_array.length; i++) {
          var div = document.createElement('div');
          div.innerHTML = gambar;
          var img_path = div.getElementsByTagName('img')[0].getAttribute('src').trim();
          var post_path = document.getElementById('post-thumbnails-path');
          var seo_path = document.getElementById('seo-thumbnails-path');
          var viral_path = document.getElementById('viral-thumbnails-path');

          var draft_post_path = document.getElementById('first_img_draft');
          var draft_seo_path = document.getElementById('seo-thumbnails-path_draft');
          var draft_viral_path = document.getElementById('viral-thumbnails-path_draft');

          if(i == 0){
               if(post_path.value == '' || post_path.value == null){//overide make it selected then set input value..
                    div.className = 'post_pics selected';
                    post_path.value = img_path;
                    draft_post_path.value = img_path;
               } else {
                    div.className = 'post_pics';
               }
          }
          if(i == 1){
               if(seo_path.value == '' || seo_path.value == null){//overide make it selected then set input value..
                    div.className = 'seo_pics selected';
                    seo_path.value = img_path;
                    draft_seo_path.value = img_path;
               } else {
                    div.className = 'seo_pics';
               }
          }
          if(i == 2) {
               if(viral_path.value == '' || viral_path.value == null){
                    div.className = 'viral_pics selected';
                    viral_path.value = img_path;
                    draft_viral_path.value = img_path;
               } else {
                    div.className = 'viral_pics';
               }
          }
          console.log(div);
          id_array[i].insertBefore(div,  id_array[i].childNodes[0]);
     }
     console.log(id_array);

     //show thumbnail meta..
     var tethumbnail = document.getElementsByClassName('tethumbnail');
     for (var i = 0; i < tethumbnail.length; i++) {
          tethumbnail[i].style.display = 'block';
     }

     var seo_thumbs = document.getElementsByClassName('seo_pics');
     var viral_thumbs = document.getElementsByClassName('viral_pics');
     var post_thumbs = document.getElementsByClassName('post_pics');
     var waitUntil = function (fn, condition, interval) {
          interval = interval || 100;
          var shell = function () {
               var timer = setInterval(
                    function () {
                         var check;

                         try { check = !!(condition()); } catch (e) { check = false; }

                         if (check) {
                              clearInterval(timer);
                              delete timer;
                              fn();
                         }
                    },
                    interval
               );
          };
          return shell;
     };
     waitUntil(
          function(){
               //-----
               // reload this script to addEventListener...
               meteor();
               console.log('ready');
               onclick_thumbnail_meta();

               if(set_post_thumb){
                    close_all_popup();
               }

               //-----
          },
          function(){
               // the code that tests here... (return true if test passes; false otherwise)
               console.log('not ready');
               var check_berapa = document.getElementsByClassName('seo_pics').length;
               var check_lagi_berapa = document.getElementsByClassName('viral_pics').length;
               var check_lagi_lagi_berapa = document.getElementsByClassName('post_pics').length;
               return !!(seo_thumbs!== check_berapa && viral_thumbs !== check_lagi_berapa && post_thumbs !== check_lagi_lagi_berapa);
          },
          50 // amout to wait between checks
     )(); // end waitUntil.

}

function onclick_thumbnail_meta(){
     // once click select POST thumbnail..
     var thumb_post = document.getElementsByClassName('post_pics');
     if(thumb_post){
          for (var ii = 0; ii < thumb_post.length; ii++) {
               if (typeof window.addEventListener === 'function'){
                    (function (index) {
                         thumb_post[ii].addEventListener('click', function(){
                              //remove selected..
                              remove_post_selected();
                              //add selected class, and set hidden input value: get path only
                              index.className = 'post_pics selected';
                              document.getElementById('post-thumbnails-path').value = index.getElementsByTagName('img')[0].getAttribute('src');
                              document.getElementById('first_img_draft').value = index.getElementsByTagName('img')[0].getAttribute('src');
                         });
                    })(thumb_post[ii]);
               }
          }
     }
     // once click select SEO thumbnail..
     var thumb_seo = document.getElementsByClassName('seo_pics');
     if(thumb_seo){
          for (var ii = 0; ii < thumb_seo.length; ii++) {
               if (typeof window.addEventListener === 'function'){
                    (function (index) {
                         thumb_seo[ii].addEventListener('click', function(){
                              //remove selected..
                              remove_seo_selected();
                              //add selected class, and set hidden input value: get path only
                              index.className = 'seo_pics selected';
                              document.getElementById('seo-thumbnails-path').value = index.getElementsByTagName('img')[0].getAttribute('src');
                              document.getElementById('seo-thumbnails-path_draft').value = index.getElementsByTagName('img')[0].getAttribute('src');
                         });
                    })(thumb_seo[ii]);
               }
          }
     }

     // once click select Viral thumbnail..
     var thumb_viral = document.getElementsByClassName('viral_pics');
     if(thumb_viral){
          for (var ii = 0; ii < thumb_viral.length; ii++) {
               if (typeof window.addEventListener === 'function'){
                    (function (index) {
                         thumb_viral[ii].addEventListener('click', function(){
                              //remove selected..
                              remove_viral_selected();
                              //add selected class, and set hidden input value: get path only
                              index.className = 'viral_pics selected';
                              document.getElementById('viral-thumbnails-path').value = index.getElementsByTagName('img')[0].getAttribute('src');
                              document.getElementById('viral-thumbnails-path_draft').value = index.getElementsByTagName('img')[0].getAttribute('src');
                         });
                    })(thumb_viral[ii]);
               }
          }
     }
}

function remove_post_selected(){
     var postThumbnails = document.getElementById('post-thumnails');
     var post_selected = postThumbnails.querySelectorAll('.selected');
     if(post_selected[0]){
          post_selected[0].className = 'post_pics';
     }
}

function remove_seo_selected(){
     var seoThumbnails = document.getElementById('seo-thumbnails');
     var seo_selected = seoThumbnails.querySelectorAll('.selected');
     if(seo_selected[0]){
          seo_selected[0].className = 'seo_pics';
     }
}

function remove_viral_selected(){
     var viralThumbnails = document.getElementById('viral-thumbnails');
     var viral_selected = viralThumbnails.querySelectorAll('.selected');
     if(viral_selected[0]){
          viral_selected[0].className = 'viral_pics';
     }
}


document.getElementById('img-insert-btn').addEventListener('click', function(){
     var jumlah = document.getElementsByClassName('pics')[0].querySelectorAll('.selected img');
     if(jumlah){
          if(jumlah.length == 1){
               build_single_image(jumlah);
          } else {
               build_gallery_image(jumlah);
          }
     }
     reset_selected_image();
});

function build_single_image(jumlah){
     var title = jumlah[0].getAttribute('alt');
     var path = jumlah[0].getAttribute('src');
     var path = path.substr(path.lastIndexOf('/') + 1);
     var myhtml = '<a href="'+'https://media.malayatimes.com/gambar/' + path + '" title="'+ title +'" class="single_image"><img alt="'+ title +'" src="'+'https://media.malayatimes.com/gambar/' + path + '"/></a>';
     var selectPastedContent = myhtml;
     console.log('%cBUILD SINGLE IMAGE: dibawah (1) selRange.startContainer (2) myhtml to build single image.', 'color:brown;font-weight:600;');
     console.log(selRange.startContainer);
     console.log(myhtml);
     // pasteHtmlAtCaret(myhtml,'insert');
     pasteHtmlAtCaret('<div class="para" autocomplete="off">'+myhtml+'</div>','insert');
     return false;
}

function build_gallery_image(jumlah){
     var figure = document.createElement('figure');
     figure.className = 'gallery';
     for (var i = 0; i < jumlah.length; i++) {
          var title = jumlah[i].getAttribute('alt');
          var path = jumlah[i].getAttribute('src');
          var path = path.substr(path.lastIndexOf('/') + 1);
          var a = document.createElement('a');
          a.className = 'gallery_image';
          a.setAttribute('href', 'https://media.malayatimes.com/gambar/' + path);
          a.setAttribute('title', title);
          a.innerHTML = '<img alt="'+ title +'" src="'+'https://media.malayatimes.com/image/' + path + '"/>';
          figure.appendChild(a);
     }
     pasteHtmlAtCaret('<div class="para" autocomplete="off">'+figure.outerHTML+'</div>','insert');
     return false;
}

//example path_gambar('https://media.malayatimes.com/picker/try-gambar-with-caption.jpg')
// function path_gambar(link_gambar){
// var filename = link_gambar.substring(link_gambar.lastIndexOf('/')+1);
// return filename;
// }
/* END GAMBAR AREA */






/* VIDEO AREA */
function videoid(x){
     var url = x.toString();
     var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
     if(videoid != null) {
          return videoid[1];
     } else {
          return false;
     }
}

var check_bedio;
document.getElementById('youtube-reader').addEventListener('input', function(e){
     //e.preventDefault();
     clearTimeout(check_bedio);
     check_bedio = setTimeout(function() {
          var ee = document.getElementById('youtube-reader');
          if(ee.value.length > 3 && videoid(ee.value)){
               document.getElementsByClassName('embed-video')[0].innerHTML = '<iframe src="https://www.youtube.com/embed/'+videoid(ee.value)+'?autohide=1&amp;rel=0&amp;showinfo=0" allowfullscreen="true" class="video"></iframe>';
               document.getElementById('a_a_video').querySelector('.insert').removeAttribute('disabled');
          } else {
               document.getElementById('a_a_video').querySelector('.insert').setAttribute('disabled','disabled');
               document.getElementsByClassName('embed-video')[0].innerHTML = '';
          }
     }, 1000);
});

//once click insert video widget.
document.querySelectorAll('#a_a_video .insert')[0].addEventListener('click', function(e){
     e.preventDefault();
     pasteHtmlAtCaret('<div class="para" autocomplete="off">'+document.getElementsByClassName('embed-video')[0].innerHTML+'</div>','insert');
     document.getElementById('a_a_video').querySelector('.insert').setAttribute('disabled','disabled');
     document.getElementsByClassName('embed-video')[0].innerHTML = '';
     document.getElementById('youtube-reader').value = '';
});
/* END VIDEO AREA */




/* SELECT CATEGORY AREA*/
var categoryTimeOut;
document.getElementById('category-search').addEventListener('click', function(){
     document.getElementsByClassName('category-list')[0].style.display = 'block';
});
document.getElementsByClassName('category-list')[0].addEventListener('mouseleave',function(){
     document.getElementsByClassName('category-list')[0].style.display = 'none';
});
var li = document.getElementsByClassName('category-list')[0].querySelectorAll('li');
for (var i = 0; i < li.length; i++) {
     if (typeof window.addEventListener === 'function'){
          (function (index) {
               li[i].addEventListener('click', function(){
                    var cat_input = document.getElementById('category-search');
                    cat_input.removeAttribute('readonly');
                    cat_input.value = index.getAttribute('data-domain');
                    cat_input.setAttribute('value', index.getAttribute('data-domain'));
                    document.getElementById('category_draft').value = index.getAttribute('data-domain');
                    document.getElementById('category_draft').setAttribute('value', index.getAttribute('data-domain'));
                    cat_input.setAttribute('readonly','true');
                    clearTimeout(categoryTimeOut);
                    categoryTimeOut = setTimeout(function () {
                         action_after_xhr_login = 1;
                         check_login();
                    }, 5000);
               });
          })(li[i]);
     }
}


/* GO AREA */
var go = document.querySelectorAll('.go');
if(go){
   for (var i = 0; i < go.length; i++) {
      go[i].addEventListener('click', function(){

         click_go(this)

      })
   }
}


function click_go(go){
   if(go.getAttribute('data-input') && go.getAttribute('data-target')){
      var inputnya = document.getElementById(go.getAttribute('data-input'));
      var targetnya = document.getElementById(go.getAttribute('data-target'));
      if(inputnya.value && targetnya){

         //var val = inputnya.value.trim();

         var tara = inputnya.value.split(',');
         if(tara){

            for (var i = 0; i < tara.length; i++) {
               var val = tara[i];
               if(val.trim()){

                  if(get_wrapper(inputnya, 'id', 'targetting')){
                     var interest_val = filter_char(val.trim());
                     var the_interest_id = interest_val.replace(/ /g, '_');
                     var interest_id = 'interest_value_'+ interest_val+'_id';
                     var interest_id = interest_id.replace(/ /g, '_');

                     var b_html = '<span class="selected-interest '+interest_id+'" title="click to remove" data-val="'+interest_id+'">'+val+'<i class="fa fa-times" aria-hidden="true"></i></span>';
                     targetnya.innerHTML = targetnya.innerHTML + b_html;

                     var main_form = document.createElement('input');
                     main_form.setAttribute('value', val.trim());
                     main_form.setAttribute('name', 'interest[]');
                     main_form.setAttribute('id', interest_id);
                     main_form.setAttribute('readonly', 'true');
                     main_form.setAttribute('type', 'hidden');
                     var form_elem = document.getElementsByClassName('main-form')[0];
                     form_elem.insertBefore(main_form, form_elem.childNodes[0]);

                     var draft_hidden = document.createElement('input');
                     draft_hidden.setAttribute('value', val.trim());
                     draft_hidden.setAttribute('name', 'interest[]');
                     draft_hidden.setAttribute('id', 'draft_'+interest_id);
                     draft_hidden.setAttribute('readonly', 'true');
                     draft_hidden.setAttribute('type', 'hidden');
                     var draft_form = document.getElementById('draft-form');
                     draft_form.insertBefore(draft_hidden, draft_form.childNodes[0]);

                     inputnya.value = null;
                     listen_remove_interest();
                     save_draft('targetting')
                  }

                  //label area
                  if(get_wrapper(inputnya, 'id', 'label-box')){

                     var tag_class = build_tag_class(val);

                     if(tag_class && document.getElementsByClassName(tag_class).length < 1){

                        var b_html = '<span class="selected-label '+tag_class+'" title="click to remove" data-val="'+val.trim()+'">'+val.trim()+'<i class="fa fa-times" aria-hidden="true"></i></span>';
                        targetnya.innerHTML = targetnya.innerHTML + b_html;

                        var main_form = document.createElement('input');
                        main_form.setAttribute('value', val.trim());
                        main_form.setAttribute('name', 'tag[]');
                        main_form.className = tag_class;
                        main_form.setAttribute('readonly', 'true');
                        main_form.setAttribute('type', 'hidden');
                        var form_elem = document.getElementsByClassName('main-form')[0];
                        form_elem.insertBefore(main_form, form_elem.childNodes[0]);

                        var draft_hidden = document.createElement('input');
                        draft_hidden.setAttribute('value', val.trim());
                        draft_hidden.setAttribute('name', 'tag[]');
                        draft_hidden.className = tag_class;
                        draft_hidden.setAttribute('readonly', 'true');
                        draft_hidden.setAttribute('type', 'hidden');
                        var draft_form = document.getElementById('draft-form');
                        draft_form.insertBefore(draft_hidden, draft_form.childNodes[0]);

                        document.getElementsByClassName('label-list')[0].style.display = 'none';
                        inputnya.value = null;
                        listen_remove_label();
                        save_draft('label')

                     }

                  }

               }
            }

         }

      }
   }
}
/* END GO AREA */


/* GENDER AREA */

var g_input = document.getElementsByClassName('gender_input');
if(g_input){
   for (var i = 0; i < g_input.length; i++) {
      if (typeof window.addEventListener === 'function'){
         (function (index) {
            g_input[i].addEventListener('change', function(){
               change_gender();
            })
         })(g_input[i]);
      }
   }
}

function change_gender(){
   var h_gender = document.getElementsByClassName('hidden_gender');
   //remove gender.
   if(h_gender){
      for (var i = 0; i < h_gender.length; i++) {
         if(h_gender[i] && h_gender[i].parentNode) h_gender[i].parentNode.removeChild(h_gender[i]);
      }
   }
   //re-add gender.
   if(g_input){
      for (var i = 0; i < g_input.length; i++) {
         if(g_input[i].checked){
            var my_inp = '<input class="hidden_gender" type="hidden" name="gender[]" value="'+g_input[i].value+'">';
            var my_inpu = strHtmlToDom(my_inp)

            var draft_form = document.getElementById('draft-form')

            if(draft_form) draft_form.appendChild(my_inpu)
         }
      }
   }
   save_draft('gender')
}

/* END GENDER AREA */


/* META BOX AREA */

// SEO meta title.
var meta_title_timeout;
var seoTitle = document.getElementById('seo-title');
if(seoTitle){
     seoTitle.addEventListener('input', function(){
          document.getElementById('title-count').textContent = this.value.length;
          document.getElementById('meta_title_draft').value = this.value;
          clearTimeout(meta_title_timeout);
          meta_title_timeout = setTimeout(function() {
               action_after_xhr_login = 1;
               check_login();
          }, 5000);
     });
}

// SEO meta description.
var metaDescription = document.getElementById('meta-description');
if(metaDescription){
     var meta_desc_timeout;
     metaDescription.addEventListener('input', function(){
          document.getElementById('description-count').textContent = this.value.length;
          document.getElementById('meta_description_draft').value = this.value;
          clearTimeout(meta_desc_timeout);
          meta_desc_timeout = setTimeout(function() {
               action_after_xhr_login = 1;
               check_login();
          }, 5000);
     });
}

// Viral meta title.
var viralTitle = document.getElementById('viral-title');
if(viralTitle){
     var viral_title_timeout;
     viralTitle.addEventListener('input', function(){
          document.getElementById('viral-title-count').textContent = this.value.length;
          document.getElementById('viral_title_draft').value = this.value;
          clearTimeout(viral_title_timeout);
          viral_title_timeout = setTimeout(function() {
               action_after_xhr_login = 1;
               check_login();
          }, 5000);
     });
}

// Viral meta description.
var viral_metaDescription = document.getElementById('viral-meta-description');
if(viral_metaDescription){
     var viral_desc_timeout;
     viral_metaDescription.addEventListener('input', function(){
          document.getElementById('viral-description-count').textContent = this.value.length;
          document.getElementById('viral_description_draft').value = this.value;
          action_after_xhr_login = 1
          clearTimeout(viral_desc_timeout);
          viral_desc_timeout = setTimeout(function(){
               action_after_xhr_login = 1;
               check_login();
          }, 5000);
     });
}

// V_S_Thumb select..
var vsThumb = document.getElementsByClassName('v_s_thumb');
if(vsThumb){
     for (var ii = 0; ii < vsThumb.length; ii++) {
          var puc = vsThumb[ii];
          if(puc){
               var pic = puc.getElementsByTagName('div');
               if(pic){
                    for (var i = 0; i < pic.length; i++) {
                         // document.getElementsByClassName(pic[i].className)[i]
                         var thumbnail_timeout;
                         pic[i].addEventListener('click', function(){
                              var klas = this.className.replace('selected', '').trim();
                              if(klas == 'seo_pics'){
                                   remove_seo_selected();
                                   document.getElementById('seo-thumbnails-path').value = this.getElementsByTagName('img')[0].getAttribute('src');
                                   document.getElementById('seo-thumbnails-path_draft').value = this.getElementsByTagName('img')[0].getAttribute('src');
                              } else if(klas == 'viral_pics'){
                                   remove_viral_selected();
                                   document.getElementById('viral-thumbnails-path').value = this.getElementsByTagName('img')[0].getAttribute('src');
                                   document.getElementById('viral-thumbnails-path_draft').value = this.getElementsByTagName('img')[0].getAttribute('src');
                              } else if(klas == 'post_pics'){
                                   remove_post_selected();
                                   document.getElementById('post-thumbnails-path').value = this.getElementsByTagName('img')[0].getAttribute('src');
                                   document.getElementById('first_img_draft').value = this.getElementsByTagName('img')[0].getAttribute('src');
                              } else {
                                   alert('there\'s error to detect className of thumbnail.. Please report to Malayatimes Developer. Then you should use another browser (Google Chrome browser is higly recommended).');
                              }
                              this.className = klas + ' selected';
                              clearTimeout(thumbnail_timeout);
                              thumbnail_timeout = setTimeout(function(){
                                   action_after_xhr_login = 1;
                                   check_login();
                              }, 5000);
                         });
                    }
               }
          }
     }
}

/* END META BOX AREA */

/* MANUAL SAVE DRAFT */
var derap_button = document.getElementById('draft');
if(derap_button){
     derap_button.addEventListener('click',function(){
          //document.getElementById('draft-form').submit();
          var main_form = document.getElementsByClassName('main-form')[0];
          main_form.setAttribute('action', '/save-draft');
          main_form.submit();
     });
}


/*  AUTO SAVE DRAFT AREA / AUTODRAFT */
function save_draft(){
     content_draft();
     document.getElementById('content_draft').value = preview.innerHTML;
     console.log(document.getElementById('content_draft').value);
     var myForm = document.getElementById('draft-form');
     var formData = new FormData(myForm);
     var xhr      = new XMLHttpRequest();

     xhr.open('POST', 'https://media.malayatimes.com/wiki_autosave/', true);
     xhr.onreadystatechange = function() {
          if ( xhr.readyState === 4 ) {
                var data = xhr.responseText;
                // if(data){
                //    console.log('data dibawah!!!!!!!!')
                //    console.log(data);
                // }
               var m = 'autosave done';
               var d = new Date();
               var masa = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
               var msg = masa + ' - ' + m;
          } else {
               var masa = false;
               var msg = 'failed to autosave post data. Please report to shahdanial@gmail.com';
          }
          if(!masa){
               add_class(document.getElementById('draft_saved'), 'warning');
          } else {
               remove_class(document.getElementById('draft_saved'), 'warning');
          }
          document.getElementById('draft_saved').style.display = 'block';
          document.getElementById('draft_saved').innerHTML = msg;

     }
     xhr.send( formData );
}

/* END AUTODRAFT */

/* CLICK PUBLISH BUTTON : once publish */

document.getElementById('save').addEventListener('click', function(e){
     rainbow_msg(document.getElementById('wait-full'), true, 'MEMERIKSA DATA', 'SILA TUNGGU SEBENTAR..');
     content_preview();
     e.preventDefault();
     action_after_xhr_login = 3;
     check_login();
});

// document.getElementById('login-again-form').addEventListener('submit', function(e){
//      console.log('YOURE SUBMIT LOGIN');
//      e.preventDefault();
//      rainbow_msg(document.getElementById('wait-full'), true, 'VALIDASI AKAUN', 'SILA TUNGGU SEBENTAR..');
//      var formData = new FormData(this);
//      var xhr      = new XMLHttpRequest();
//      xhr.open('POST', 'https://media.malayatimes.com/login', true);
//      xhr.onreadystatechange = function() {
//           if ( xhr.readyState === 4 && xhr.status === 200 ) {
//                var data = xhr.responseText;
//                if(data){
//                     console.log('SUBMIT LOGIN data is below');
//                     console.log(data);
//                     after_check_login(data);
//                } else {
//                     console.log('no data >>??');
//                }
//           }
//      }
//      xhr.send( formData );
// });
var interval_is_run = false,
checking;

var google_login = document.getElementById('google_login');
if(google_login){
     google_login.addEventListener('click', function(){
          console.log('google-login clicked');

          var follow_box = document.getElementById('login-again');
          if(follow_box){
               if(follow_box.clientWidth > 0 && follow_box.clientHeight > 0){
                    var docWidth = document.documentElement.clientWidth || document.body.clientWidth;
                    var my_offset_left = (parseFloat(docWidth) - parseFloat(follow_box.clientWidth)) / parseFloat(2);
                    var my_window_style = 'top=0,left='+my_offset_left+',width='+follow_box.clientWidth+',height='+document.body.clientHeight+',scrollbars=no,resizable=no,toolbar=no,titlebar=no,menubar=no';
               } else {
                    var my_window_style = 'fullscreen=yes,scrollbars=no,resizable=no,toolbar=no,titlebar=no,menubar=no,top=0,left=0';
               }
          }
          var my_window = window.open('https://media.malayatimes.com/login-again?redirect=https%3A%2F%2Fmedia.malayatimes.com%2Fclose', 'MsgWindow', my_window_style);
          if (window.focus) {
               my_window.focus();
          }
          my_window.onload = function(){
               console.log('%cCHECK LOGIN POPUP WINDOW OPEN', 'font-weight:600;color:red;');
               console.log(my_window.location.href)
               window_is_open = true;
               interval_is_run = true;
               checking = setInterval(function(){
                    if(window_is_open){
                         console.log('interval is checking..');
                         check_login();
                    } else {
                         clearInterval(checking);
                    }
               }, 5000);
          }
     });
}

function nak_publish(){
     if(!error_msg()){// lulus..
          document.getElementsByClassName('main-form')[0].submit();
     } else {// gagal.. error_msg return some message muah..
        console.log('gagal', error_msg())
          document.getElementById('error-msg').innerHTML = error_msg().outerHTML;
          add_class(document.getElementById('a_a_publish'), 'popup_content_show');
          document.getElementsByClassName('modal')[0].style.display = 'block';
     }
     rainbow_msg(document.getElementById('wait-full'), false);
}

function check_login(){
     var fromData      = new FormData();
     fromData.append('status', get_query_url('status'));
     fromData.append('id', get_query_url('id'));
     console.log('your status');
     console.log(get_query_url('status'));
     console.log('you id');
     console.log(get_query_url('id'));
     var xhr = new XMLHttpRequest();
     xhr.open('POST', 'https://media.malayatimes.com/check-login-wiki/', true);
     xhr.onreadystatechange = function() {
          if (xhr.readyState === 4){
               if(xhr.status === 200){
                    var data = xhr.responseText;
                    console.log('check login api done, datanya dibawah');
                    console.log(data);
                    after_check_login(data);
               }
          }
     }
     xhr.send(fromData);
}

function hide_login_action(what){
     if(what){ // true = hide it
          var myBox = document.getElementsByClassName('popup-content');
          for (var i = 0; i < myBox.length; i++) {
               add_class(myBox[i], 'hide_login_action');
          }
     } else { // false = show it.
          var elem = document.getElementsByClassName('hide_login_action');
          for (var i = 0; i < elem.length; i++) {
               remove_class(elem[i], 'hide_login_action');
                document.getElementById('login-again').style.display = 'none';
          }
     }
}

function after_check_login(data){

     console.log('%cafter_check_login()', 'color:red;');
     console.log('data is below');
     console.log(data);
     if(data.length > 0){
          if(isNumber(data)){
               console.log('you\'re logged in. matching..., so we stoped interval checking');
               clearInterval(checking);
               window_is_open = false;
               if(action_after_xhr_login){
                    var popup_active = document.getElementsByClassName('popup_content_show');
                    console.log('jumlah popup active dibawah', popup_active.length)
                    if(popup_active.length < 1){
                         console.log('kami close popup (hide modal) sbb tiada popup box active')
                         document.getElementsByClassName('modal')[0].style.display = 'none';
                    } else {
                         console.log('kami x close popup (x hide modal) sbb ada popupbox active');
                    }
                    document.getElementById('login-again').style.display = 'none';

                    // evalkan(action_after_xhr_login);
                    if(action_after_xhr_login == 1){
                        save_draft();
                    } else if(action_after_xhr_login == 2){
                       after_xhr_2();
                    } else if(action_after_xhr_login == 3){
                       nak_publish();
                    } else {
                       console.log('something wrong, action_after_xhr_login not in your number list shah')
                    }
                    action_after_xhr_login = 0;
               }
               hide_login_action(false);
          } else {
               console.log('login data not match');
               hide_login_action(true);
               popup_login(data);// data is flashdata..
          }
          rainbow_msg(document.getElementById('wait-full'), false);
     }




}

function popup_login(flashdata){
     // close_all_popup()
     dialogModal.style.display = 'block';
     document.getElementById('login-again').style.display = 'block';
     if(flashdata){
          console.log('TEXT login-again-message should be: '+flashdata);
          document.getElementById('login-again-message').textContent = flashdata;
     }
     rainbow_msg(document.getElementById('wait-full'), false);
}
var my_error;
function error_msg(){
   my_error = 0;
     console.log('%cerror_msg()', 'color:red;font-weigth:600');
     var fake_editor = document.createElement('div');
     fake_editor.innerHTML = preview.innerHTML;//format_content();
     console.log(fake_editor);
     var error_msg = document.createElement('ol');

     /* LINK */
     var links = fake_editor.getElementsByTagName('a');

     if(links.length > 2){
          var external_link = new Array();

          for (var i = 0; i < links.length; i++) {

               var a_tag = links[i];

               if(a_tag.getAttribute('href')){//ada href..
                    if(!compare_domain(a_tag.getAttribute('href').trim(), '*.malayatimes.com')){
                         console.log(compare_domain(a_tag.getAttribute('href').trim(), '*.malayatimes.com'));
                         console.log(a_tag.getAttribute('href').trim()+ ' adalah external link');
                         external_link.push(a_tag.getAttribute('href').trim());
                    }
               }

          }

          if(external_link.length > 2){
             my_error++;
               error_msg.appendChild(strHtmlToDom('<li>Link ke website luar selain dari <i class="red">malayatimes.com</i>, tidak boleh <b>melebihi 2</b>.</li>'));
          }

     } else {
          //error_msg.appendChild(strHtmlToDom('<li>Sekurang-kurangnya ada <b>3</b> link di dalam post (gambar juga dikira sebagai link).</li>'));
     }
     /* LINK */

     /* IMAGE */
     var imgs = fake_editor.getElementsByTagName('img');
     if(imgs.length > 0){//imgs ada..
          for (var i = 0; i < imgs.length; i++) {

               var o = parseFloat(i) + parseFloat(1);
               var img = imgs[i];

               if(img.getAttribute('src')){
                    var domain_gambar = get_domain(img.getAttribute('src'));
                    if( !compare_domain(domain_gambar, 'media.malayatimes.com') ){
                       my_error++;
                         error_msg.appendChild(strHtmlToDom('<li>Gambar ke <b>'+o+'</b> tidak dibenarkan kerana menggunakan gambar yang bukan diupload dari media.malayatimes.com.</li>'));
                    }
               } else {// src
                  my_error++;
                    error_msg.appendChild(strHtmlToDom('<li>Element gambar ke <b>'+o+'</b> tidak mempunyai attribute <b class="red">src</b>.</li>'));
               }

          }
     } else {
          //DISABLED
          // error_msg.appendChild(strHtmlToDom('<li>Wajib sekurang-kurangnya ada satu gambar yang berkaitan di dalam post.</li>'));
     }
     /* end IMAGE */

     /* PARAGRAPH */
     if(fake_editor.getElementsByClassName('para').length < 3){
        my_error++;
          error_msg.appendChild(strHtmlToDom('<li>Sekurang-kurangnya ada <b>3</b> perenggan.</li>'));
     }
     /* end PARAGRAPH */


     /*return */
     console.log( 'fake_editor');
     console.log(fake_editor);
     if(my_error > 0){
          return error_msg;
     } else {
          return false;
     }
}

function rainbow_msg(outer, what, heading, mesej){ // outer = target element.. what = true start, what = false to stop.. mesej = small mesej..
     var myRainbow = false;

     var headingEl = document.getElementsByClassName('hug-message');
     var mesejEl = document.getElementsByClassName('small-message');

     var myHeading = 'TUNGGU SEBENTAR';
     var myMesej = 'MEMERIKSA DATA..';

     if(heading){
          var myHeading = heading;
     }

     if(mesej){
          var myMesej = mesej;
     }

     for (var i = 0; i < headingEl.length; i++) {
          headingEl[i].textContent = myHeading;
     }

     for (var i = 0; i < mesejEl.length; i++) {
          mesejEl[i].textContent = myMesej;
     }

     if(what !== false){//start
          console.log('what true');

          // var colors = [
          //    "#c0392b", //red
          //    "#d35400", //orange
          //     "#f1c40f", //yellow
          //     "#27ae60", //green
          //     "#2980b9", //blue
          //     "#8e44ad", //purple
          //     "#2c3e50" // midnight blue.
          //  ];
          outer.style.display = 'block';
          if(outer.getAttribute('class')){
               outer.className  = outer.getAttribute('class') + ' rainbow ';
          } else {
               outer.className = 'rainbow';
          }

          // myRainbow = setInterval(function(){
          //    outer.style.backgroundColor = colors[i];
          // 	i++;
          // 	if (i === colors.length){
          // 		i=0;
          // 	}
          // }, 500);

     } else { //stop..
          console.log('what false');
          console.log(outer);

          //clearInterval(myRainbow);
          //outer.removeAttribute('style');
          if(outer.className == 'rainbow'){
               outer.removeAttribute('class');
               outer.removeAttribute('style');
          } else {
               outer.className = outer.className.replace(' rainbow ', '');
          }

     }
}

function isNumber(str){
     if(str.length > 0){
          return !isNaN(str)
     } else {
          return false;
     }
}

function in_array(variable, target_array){
     return target_array.indexOf(variable) > -1;
}

function add_class(elem, new_class){
     if(elem.className.length > 0){
          if( !in_array(new_class, elem.className.split(' ')) ){
               elem.className = elem.className.trim() + ' ' + new_class;
          }
     } else {
          elem.className = new_class;
     }
}

function remove_class(elem, target_class){
     if(elem){
          if(elem.className.length > 0){
               var classes = elem.className.trim().split(' ');
               elem.removeAttribute('class');
               new_class = '';
               for(var i = 0; i < classes.length; i++) {
                    if(classes[i] != target_class.trim()){
                         new_class = new_class + classes[i] + ' ';
                    }
               }
               if(new_class.trim().length > 0){
                    elem.className = new_class.trim();
               }
          } else {
               elem.removeAttribute('class');
          }
     }
}

/* CLOSE ERROR MSG PUBLISH*/
document.getElementsByClassName('close_all_popup')[0].addEventListener('click', function(){
     close_all_popup();
});

/* END CLICK PUBLISH BUTTON */



/* START TOOLTIP */

function getOffsetLeft(elem){
     var offsetLeft = 0;
     do {
          if ( !isNaN( elem.offsetLeft ) )
          {
               offsetLeft += elem.offsetLeft;
          }
     } while( elem = elem.offsetParent );
     return offsetLeft;
}


var tooltip_active = false;
var slow_event;
var slow_lagi_event;

listen_tooltip();
function listen_tooltip(){
   meteor();
   var tooltip = document.querySelectorAll('[data-tooltip]');
   var body = document.getElementsByTagName('body')[0];
   if(tooltip){
        for (var i = 0; i < tooltip.length; i++) {
             var elem = tooltip[i];
             var tops = document.getElementsByClassName('tooltip');
             console.log(tooltip_active);
             if(!tooltip_active){

                  elem.addEventListener('mouseenter',function(e){

                       tooltipactive = true;


                       console.log('hover');
                       var tip = document.createElement('div');
                       tip.className = 'tooltip';
                       tip.innerHTML = this.getAttribute('data-tooltip');
                       var X = getOffsetLeft(this);//this.left;//parseFloat(e.pageX) + parseFloat(2);
                       var Y = parseFloat(e.pageY) + parseFloat(10);
                       console.log(X);
                       var style = 'left:'+X+'px;top:'+Y+'px;';
                       tip.setAttribute('style', style);
                       body.appendChild(tip);

                       this.addEventListener('mouseleave', function(){
                            close_tooltip();
                       });

                       document.body.addEventListener('click', function(){
                            close_tooltip();
                       });

                  });

             }

        }
   }
}

function close_tooltip(){
     var tips = document.getElementsByClassName('tooltip');
     for (var ii = 0; ii < tips.length; ii++) {
          var benda = tips[ii];
          if(benda.parentNode){
               benda.parentNode.removeChild(benda);
               clearTimeout(slow_event);
               slow_event = setTimeout(function(){
                    tooltip_active = false;
               },2500);
          }
     }
}
/* END TOOLTIP */

var control_wrap = document.getElementsByClassName('wysiwyg-controls')[0];
if(control_wrap){
     console.log(control_wrap.clientWidth)
}

function get_query_url (key) {
     return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}



function listAttributes(elem) {
     var paragraph = elem;

     // First, let's verify that the paragraph has some attributes
     if (paragraph.hasAttributes()) {

       var attrs = paragraph.attributes;
       var nak_return = new Array();
       for(var i = attrs.length - 1; i >= 0; i--) {
         nak_return.push({'attribute_dia': attrs[i].name, 'value_dia': attrs[i].value})
       }
       return nak_return;
     } else {
       return false;
     }
   }

/*
usage:
var element = new Array; or 'string';
remove_child(element);
*/
function remove_element(arg){

   if(typeof arg == 'string'){
      // string je.
      if(arg.parentNode){
         arg.parentNode.removeChild(arg)
      }

   } else {
      //posible object
      for (var i = 0; i < arg.length; i++) {
         if(arg[i].parentNode){
            arg[i].parentNode.removeChild(arg[i])
         }
      }

   }

   return false;

}

function ie(){
     if (navigator.appName == 'Microsoft Internet Explorer' || navigator.appName.toLowerCase().indexOf('microsoft') > -1 ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1)){
          return true;
     } else {
          return false;
     }
}

function meteor(){
     document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/react_1.js\"><\/script>';
}


})();//end