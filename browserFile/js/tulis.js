
 $(function() {

   $('#post-editor').bind('input propertychange', function() {
     toHtml();
     $('.content_wtf, #content_draft').val( htmlConvert([$('.my-content').html(), 'toHtml']) );
     $('.gallery br').remove();
     $('.gallery br').addClass('galleryBr');
   });

  //  $('.wysiwyg-editor').click(function() {
  //    toHtml();
  //     $('.content_wtf, #content_draft').val( htmlConvert([$('.my-content').html(), 'toHtml']) );
  //    $('.gallery br').remove();
  //    $('.gallery br').addClass('galleryBr');
  //  });



   // # OTHER -----------------

  $(".basetooltip").hover(function(){var t=$(this).attr("title");$(this).data("tipText",t).removeAttr("title"),$('<p class="tooltip"></p>').text(t).appendTo("body").fadeIn("slow")},function(){$(this).attr("title",$(this).data("tipText")),$(".tooltip").remove()}).mousemove(function(t){var o=t.pageX+20,e=t.pageY+10;$(".tooltip").css({top:e,left:o})});

$('textarea').each(function () {

     $('.gallery br').hide();
     $('.table-wrap br').remove();

  this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
  this.style.height = 'auto';
  this.style.height = (this.scrollHeight) + 'px';
}).on('input', function () {
  this.style.height = 'auto';
  this.style.height = (this.scrollHeight) + 'px';
});

   function videoid(x){
     var url = x.toString();
     var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
     if(videoid != null) {
       return "video id = ",videoid[1];
     } else {
       return "The youtube url is not valid.";
     }
   }

   // # KLIK CONTROL BUTTON ----------------------------

   $('.select-upload').click(function(){
     $('.image-select span').removeClass('selected');
     $(this).attr('class', 'selected')
     $('.image-option > div').hide();
     $('.upload-select').show();
     $('.img-insert-btn').removeAttr('disabled');
   });

   $('.select-search').click(function(){
     $('.image-select span').removeClass('selected');
     $(this).attr('class', 'selected')
     $('.image-option > div').hide();
     $('.search-select').show();
   });

   $(".a_image").click(function() {

     $('.modal').show().attr('id', 'a_image');
     $('#a_a_image').show();
     $('.image-header h4').html('Images Manager');
     //$('#a_a_image .button').removeAttr('disabled');

   }); // #end .a_image

   $(".image-close").click(function() {

     $('.modal').hide().removeAttr('id');
     $('#a_a_image').hide();

   });

   $('#insert-file').on('change', function(){
     //document.getElementById("before-upload").html = '';

     $('.warning-gambar, .img_wrpr').remove();
    var number = $(this)[0].files.length;
    console.log(number);
    if(number){
      $('.upload-button').removeAttr('disabled');
    } else {
      $('.upload-button').attr('disabled', 'disabled');
    }

    $('.label-insert-file span').text(number);
   });






var border = document.getElementsByClassName("border");

for(var i=0; i<border.length; i++){
 border[i].onclick = function(){
   if(this.className == 'border'){
   this.className = "border selected";
   } else {
   this.className = "border";
   }
 }
}


   $(".a_link").click(function() {

     $('.modal').show().attr('id', 'a_link');
     $('#a_a_link').show();
     $('.popup h4').html('Insert Text Link');

     var highlight = $('#post-editor').selection();

     if (highlight === '') {} else {

       $('#a_link .insert-text').val(highlight);

     }
   }); // #end .a_link

   $(".a_video").click(function() {

     $('.modal').show().attr('id', 'a_video');
     $('#a_a_video').show();
     $('.popup h4').html('Embed Youtube Video');

   }); // #end .a_video

   $(".a_related").click(function() {

     $('.modal').show().attr('id', 'a_related');
     $('#a_a_related').show();
     $('.popup h4').html('Insert Related Article');

   }); // #end .a_video

   $(".a_table").click(function() {
     var line = '\n',
         space = '  ';
     $('.modal').show().attr('id', 'a_table');
     $('#a_a_table').show();
     $('.popup h4').html('Insert Simple Table');
     $('#a_a_table .content-table td').attr('contenteditable', true).addClass('field');
     $('#a_a_table .table-content').html('<div class="table-wrap"><table class="content-table"><thead><tr><th contenteditable="true" class="field">EDIT ME</th><th contenteditable="true" class="field">EDIT ME</th></tr>\n</thead><tbody><tr><td contenteditable="true" class="field">-</td><td contenteditable="true" class="field">-</td></tr><tr><td contenteditable="true" class="field">-</td><td contenteditable="true" class="field">-</td></tr></tbody></table></div>');

   }); // #end .a_table

   // # TURN ON 'INSERT' BUTTON, IF FIELD NOT EMPTY ---------------

   $("#post-title, #a_a_quote .field, #a_a_message .field, #a_a_table .field, #a_a_button .field,#a_a_related .field, #a_a_link .field, #a_a_video .field").on('input', function(e) {

     var entryTitle = $('#post-title').val();

     if(entryTitle){
     $('#my-content .entry-title').text(entryTitle);
     $('.tajuk_wtf, #title_draft').val(entryTitle);
     time_out_title();
     }else{
     $('#my-content .entry-title').text('Tajuk yang tepat dengan apa yang orang kerap search di Google adalah yang terbaik');
     }


     var text = $('.insert-text').val();
     var link = $('.insert-link').val();

     if (link && text && link.indexOf("//") != -1 && link.indexOf(".") != -1) {
       $('#a_link .insert').removeAttr('disabled');
     } else {
       $('#a_link .insert').attr('disabled', 'disabled');
     }

     var video = $('.insert-youtube').val();
     var idyoutube = videoid(video);

     if (video) {
       $('#a_video .insert').removeAttr('disabled');
       $('.embed-video').html('<iframe width="644" height="388.13" src="https://www.youtube.com/embed/'+ idyoutube +'?autoplay=0&autohide=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>');
     } else {
       $('#a_video .insert').attr('disabled', 'disabled');
     }

     var relatedLink = $('.insert-related-link').val();
     var relatedText = $('.insert-related-text').val();

     if (relatedLink && relatedText && relatedLink.indexOf("malayatimes.com") != -1) {
       $('#a_related .insert').removeAttr('disabled');
     } else {
       $('#a_related .insert').attr('disabled', 'disabled');
     }

     var textButton = $('.insert-text-button').val();
     var linkButton = $('.insert-link-button').val();

     if (linkButton && textButton && linkButton.indexOf("//") != -1 && linkButton.indexOf(".") != -1) {
       $('#a_button .insert').removeAttr('disabled');
     } else {
       $('#a_button .insert').attr('disabled', 'disabled');
     }

     var textVal = $('.insert-message').val();

     if (textVal) {
       $('#a_message .insert').removeAttr('disabled');
     } else {
       $('#a_message .insert').attr('disabled', 'disabled');
     }

     var quoteVal = $('.insert-quote').val();

     if (quoteVal) {
       $('#a_quote .insert').removeAttr('disabled');
     } else {
       $('#a_quote .insert').attr('disabled', 'disabled');
     }

   }); //#end on button

   $(".a_button").click(function() {

     $('.modal').show().attr('id', 'a_button');
     $('#a_a_button').show();
     $('.popup h4').html('Insert Link Button');

     var highlight = $('#post-editor').selection();

     if (highlight === '') {} else {

       $('#a_button .insert-text-button').val(highlight);

     }
   }); // #end .a_button

   $(".a_message").click(function() {

     $('.modal').show().attr('id', 'a_message');
     $('#a_a_message').show();
     $('.popup h4').html('Insert Message Box');

   }); // #end .a_message

   $(".a_quote").click(function() {

     $('.modal').show().attr('id', 'a_quote');
     $('#a_a_quote').show();
     $('.popup h4').html('Insert Qoute Box');

   }); // #end .a_quote


   // # TOGGLE 'INFO' DISPLAY BY CLASS  -----------------

   $('#a_a_quote .popup-tips, #a_a_message .popup-tips, #a_a_table .popup-tips, #a_a_button .popup-tips, #a_a_related .popup-tips, #a_a_video .popup-tips, #a_a_link .popup-tips, .shah_tips button').click(function() {
     $('.popup').toggleClass('show_info');
   });

   // # CHANGE INFO FIELD ON DISPLAY THE 'INFO' --------

   $('#a_a_link .popup-tips').click(function() {
     var key = $('#a_link .insert-text').val();
     var keyword = key.replace(' ', '+');

     if (key === '') {} else {

       //$('.keywordada').each(function() {
         var keywordSpace = keyword.replace(' ', '+');

         var selectA = $('.keywordada').first();
         var targetA = selectA.attr('href');
         var resA = targetA.replace('GANTI+DENGAN+KEYWORD+ANDA', keyword);

         var selectB = $('.keywordada').last();
         var targetB = selectB.attr('href');
         var resB = targetB.replace('GANTI+DENGAN+KEYWORD+ANDA', keyword);

         $(selectA).attr('href', resA);
         $(selectB).attr('href', resB);
         $('#a_link .adakeyword').text("'" + key + "' ").css('color', '#d00');
       //});

     }

   });

   $(".shah_tips .search-related").on('input', function(e) {

     var keyword = $(".shah_tips .search-related").val().replace(' ', '+');
     $(".shah_tips .search-button").attr('href', 'https://www.google.com/search?q='+keyword+'+site:malayatimes.com');

   });




   // # BRING BACK  'INFO' FIELD ON CLOSE INFO --------
   function closeInfoLink() {
     $('.shah_tips .field').html('');

     $('.keywordada').first().attr('href', 'https://www.google.com/search?q=GANTI+DENGAN+KEYWORD+ANDA+site:malayatimes.com');

     $('.keywordada').last().attr('href', 'https://www.google.com/search?q=GANTI+DENGAN+KEYWORD+ANDA');

     $('.shah_tips .field').text('');

   }



   // # CLICKED 'CLOSE TIPS' ---------------------------

   $('.shah_tips button').click(function() {
     closeInfoLink();
   });

   // # ADD ROWS TABLE ---------------------------------

   $('.boxs input[type="checkbox"]').click(function() {
     $(this).siblings('input:checkbox').prop('checked', false);
   });

   $('.add-rows').click(function(){
   var line = '\n';
   var newRows = '<tr>'+line+'<td class="field" contenteditable="true">-</td>'+line+'<td class="field" contenteditable="true">-</td>'+line+'</tr>'+line;
   var table = $('.content-table tbody');
   $(table).append(newRows);
   });

   $('.remove-rows').click(function(){
   $('.content-table tbody tr').last().remove();
   });


   $('.tablebg').change(function() {

    var $check = $(this);

    if ($check.prop('checked')) {

        $('.content-table').addClass('horizontal').removeClass('vertical');

    } else {

        $('.content-table').removeClass('horizontal').addClass('vertical');

    }

   });

   $('.tablebelang').change(function() {

    var $check = $(this);

    if ($check.prop('checked')) {

        $('.content-table').addClass('vertical').removeClass('horizontal');

    } else {

        $('.content-table').removeClass('vertical').addClass('horizontal');

    }

   });

   // # INSERT BUTTON CLICKED --------------------------

   $('#a_a_image .insert').click(function() {
     //image
      var totalSelected = $('.pics .selected').length;

       var single_gambar = $('.pics .selected').html();
       var single_gambar_link = $('.pics .selected img').attr('src');
       var single_gambar_alt = $('.pics .selected img').attr('alt');

     var line = '\n',
         space = '  ';

     if(totalSelected == '1'){

     var html = space + space + '<a href="' + single_gambar_link.replace('/picker/', '/gambar/') + '" title="'+single_gambar_alt+'">' + single_gambar.replace('/picker/', '/gambar/') +"</a>"+ space + space ;

       $('#post-editor').selection('replace', {
            text: '<!--START IMAGE -->' + html + '<!-- END IMAGE -->'
        });

     }

     if ( totalSelected > '1') {
     var imageHtml = '';

       $('.pics .selected img').each(function(i){
     var AAA = $(this).attr('src');
      var titleGambar = $(this).attr('alt');
     var BBB = AAA.replace('/picker/', '/gambar/');
     var CCC = AAA.replace('/picker/', '/image/');
     var i = i+1;
     var HtmlImage = line + '<!--start gambar of gallery-->' + line + space + space + '<a title="'+titleGambar+'" href="'+BBB+'" class="image"><img src="'+CCC+'" alt="'+titleGambar+'"/></a>' + line + '<!--end gambar of gallery-->' + line;
     imageHtml += HtmlImage;
       });

$('#post-editor').selection('insert', {text: '<!-- START GALLERY --><figure class="gallery">' + line, mode: 'before'}).selection('replace', {text: imageHtml}).selection('insert', {text: line + '</figure><!-- END GALLERY -->', mode: 'after'});

     }

     $('.pics .border').removeClass('selected');
     //$('#a_image .step_2, #a_image .insert').hide();
   });

   $('#a_a_link .insert').click(function() {
     //link
     var selText = $('#post-editor').selection();
     var text = $('.insert-text').val();
     var link = $('.insert-link').val().replace('?m=1', '');

     if (text && link) {
       $('#post-editor')
         // insert before string '<a href="'
         .selection('insert', {
           text: '<a href="',
           mode: 'before'
         })
         // replace selected text by string 'http://'
         .selection('replace', {
           text: link
         })
         // insert after string '">SELECTED TEXT</a>'
         .selection('insert', {
           text: '" target="_blank">' + text + '</a>',
           mode: 'after'
         });
     }
   });

   $('#a_a_video .insert').click(function() {
     // video
     var video = $('.insert-youtube').val();
     var idyoutube = videoid(video);
     if (video && idyoutube) {
       $('#post-editor')
         // insert before string '<a href="'
         .selection('insert', {
           text: '<!-- START VIDEO --><iframe src="https://www.youtube.com/embed/',
           mode: 'before'
         })
         // replace selected text by string 'http://'
         .selection('replace', {
           text: idyoutube
         })
         // insert after string '">SELECTED TEXT</a>'
         .selection('insert', {
           text: '?autoplay=0&amp;autohide=1&amp;rel=0&amp;showinfo=0" allowfullscreen="true" class="video"></iframe><!-- END VIDEO -->',
           mode: 'after'
         });
     }
   });

   $('#a_a_related .insert').click(function() {
     //related
     var selText = $('#post-editor').selection();
     var textRelated = $('.insert-related-text').val();
     var linkRelated = $('.insert-related-link').val().replace('?m=1', '');
     var html = '<span class="related"><a href="'+ linkRelated +'" class="link">'+ textRelated +'</a></span>';
     var cursor = '';

     if (textRelated && linkRelated) {
       $('#post-editor').selection('replace', {
            text: '<!--START RELATED -->' + html + '<!-- END RELATED -->'
        });
     }
   });



   $('#a_a_button .insert').click(function() {
     //button
     var selText = $('#post-editor').selection();
     var textButton = $('.insert-text-button').val();
     var linkButton = $('.insert-link-button').val().replace('?m=1', '');
     var html = '<span class="button"><a href="'+ linkButton +'" class="link">'+ textButton +'</a></span>';


     if (textButton && linkButton) {
       $('#post-editor').selection('insert', {text: '<!-- START BUTTON -->', mode: 'before'}).selection('replace', {text: html}).selection('insert', {text: '<!-- END BUTTON -->', mode: 'after'});
     }
   });

   $('#a_a_table .insert').click(function() {
     //table
     var selText = $('#post-editor').selection();
     //$('#a_a_table .table.content br').remove();
     $('#a_a_table .content-table td, .content-table thead th').removeAttr('contenteditable').removeAttr('class');
     $('#a_a_table .table-content br').remove();
     var tableHtml = $('#a_a_table .table-content').html()
                  .replace(/>\s*\</g, '><') // buang jarak
                  .replace(/<thead>/g, '\n\n<thead>')
                  .replace(/<\/thead>/g, '\n</thead>\n\n')
                  .replace(/<tbody>/g, '<tbody>')
                  .replace(/<\/tbody>/g, '\n</tbody>\n\n')

                  .replace(/<tr>/g, '\n <tr>\n')
                  .replace(/<\/tr>/g, ' </tr>')
                  .replace(/<th>/g, '   <th>')
                  .replace(/<td>/g, '   <td>')
                  .replace(/<\/th>/g, '</th>\n')
                  .replace(/<\/td>/g, '</td>\n')
                  ;

       $('#post-editor').selection('replace', {
            text: '<!--START TABLE -->' + tableHtml + '<!-- END TABLE -->'
        })

   });

   $('#a_a_message .insert').click(function() {
     //message

     var value = $('.insert-message').val().replace('?m=1', '');

     if (value) {
       $('#post-editor').selection('replace', {
           text: '<!-- START MESSAGE --><div class="message">'+ value + '</div><!-- END MESSAGE -->'
         })
     }
   });

   $('#a_a_quote .insert').click(function() {
     //quote

     var value = $('.insert-quote').val().replace('?m=1', '');

     if (value) {
       $('#post-editor').selection('replace', {
           text: '<!-- START QUOTE BOX --><blockquote>'+ value + '</blockquote><!-- END QUOTE BOX -->'
         })
     }
   });

   $('.popup-content .insert').click(function() {
     // other
     $('.modal, .popup-content').hide();
     $('.popup h4').html('');
     $('.popup-content').hide();
     $('.field').val('').html('');
     $('#a_link .adakeyword').text('').removeClass('selected');
     $('.popup .insert').attr('disabled', 'disabled');

   });

   $('.insert, .button').click(function(){
      toHtml();
   });

   // # 'CLOSE' POPUP BUTTON CLICKED -------------------

   $(".popup-close").click(function() {
     $('.pics .border').removeClass('selected');
     $('.modal, .popup-content').hide();
     $('.popup h4').html('');
     $('.popup-content').hide();
     $('.popup-content > .field').val('').html('');
     $('#a_link .adakeyword').text('');
     closeInfoLink();
     $("#post-editor").putCursorAtEnd();
     $('.popup .insert').attr('disabled', 'disabled');
     $('#a_a_table .content-table td').html('-');
     $('.popup h4').html('Upload Images');
     $('#a_image .step_1, #a_a_image .next').show();
     $('#a_image .step_2, #a_a_image .insert').hide();
   });


// CATEGORY SECTION



$(document.body).on('focus', '#category-search', function(e) {
$('.category-list').show();
});

$(document.body).on('click', '.category-list li', function(e) {
var
    currentVal = $('#category-search').val(),
    domain = $(this).attr('data-domain');

  if(domain != currentVal){
$('#category-search').val(domain);
$('.kategori_wtf').val(domain);
  }

});


$('#label-search').keyup(function() {
   var valThis = $(this).val().toLowerCase();
   if (valThis == "") {
      $('.label-list > li').show();
   } else {
      $('.label-list > li').each(function() {
         var text = $(this).text().toLowerCase();
         if (text.indexOf(valThis) >= 0) {
            $(this).prependTo(".label-list");
         }
      });
   };
});

$(document.body).on("keyup", '#label-search', function (e) {
    if (e.keyCode == 188) {
   var valThis = filter_char($('#label-search').val());
   sendLabel(valThis);
    }
});


//$('#label-search').focus(function() {
var search_tag_kuar;
$(document.body).on('keyup', '#label-search', function(e) {
  var cari = $(this).val();
  //$('.label-list').html('<li><img style="width:100%; height:18px;" src="http://media.malayatimes.com/assets/image/ajax-loader.gif"/></li>').show();
  if(cari && cari.length < 2){
  $('.label-list').html('<li><i>Taip lagi..</i><li>').show();
  } else {
   $('.label-list').html('<li><i>relavan tags appear when you stop typing..</i><li>').show();
  }
  clearTimeout(search_tag_kuar);
  search_tag_kuar = setTimeout(function() {
  if(cari && cari.length > 1){

  $.ajax({
              url: "http://media.malayatimes.com/search-tag/ajax?cari=" + cari,
              type: "POST",
              //data:  new FormData(this),
              //mimeType:"multipart/form-data",
              dataType : "html",
              contentType: 'application/x-www-form-urlencoded; charset=UTF-8',//false,
              cache: true,//false,
              async:true,
              processData:true,
              success: function(data){
                $('.label-list').html(data);
                $('#wings').html('<script type=\"text/javascript\" src=\"http://media.malayatimes.com/js/label_ajax.js\"><\/script>');
                $('.label-list > li').each(function() {
         var text = $(this).text().toLowerCase();
         if (text.indexOf(cari) >= 0) {
            $(this).prependTo(".label-list");
         }

      });
              },
              error: function(wtf){
                console.log(wtf);
              }
         });
}
},1000);
});

$('.wrap-box .go').click(function() {
   var val = filter_char($('#label-search').val());
   if (val) {
      sendLabel(val);
   }
});

$(document.body).on('click', '#labelresult span', function() {
  var tag_id = '#tag_value_'+ $(this).attr('data-val') +'_id';
  var tag_id = tag_id.replace(/ /g, '_');
  var draft_tag_id = '#draft_tag_value_'+ $(this).attr('data-val') +'_id';
  var draft_tag_id = draft_tag_id.replace(/ /g, '_');
   $(tag_id).remove();
   $(draft_tag_id).remove();
   $(this).remove();
   auto_draft();
});



$(document.body).on('focusout', '#label-search', function() {
   setTimeout(hideList, 500);
});

   function hideCategory(){
$('.category-list').hide();
   }

$(document.body).on('focusout', '#category-search', function() {
   setTimeout(hideCategory, 500);
});











 }); // end $(function(){});
