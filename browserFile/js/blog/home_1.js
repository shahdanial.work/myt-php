/*
all page..
by: Shah Danial
*/


/* PUBLIC VAR */

var offset_post = 16;
var limit_post = 8;
var loaded = false;
var social_popup = false;

/* ON LOAD */
run_js();

function run_js(){

     //slider_popular();//slider on
     listen_slider();//listen click slider

     mobile_menu();//listen mobile menu nav

     even_run_js();

     on_scroll();

     listen_popup_button();

}

function listen_popup_button(){
   var button_login = document.getElementsByClassName('social-media')
   var button_search = document.getElementsByClassName('search')
   var search_box = document.getElementById('search_box')
   if(button_login)
   button_login[0].addEventListener('click', function(){
      clicked_popup_button()
      if(!social_popup) load_social_popup();
   })
   ;
   if(button_search)
   button_search[0].addEventListener('click', function(){
      clicked_popup_button()
      if(!social_popup) load_social_popup();
   })
   ;
   var close_popup_button = document.getElementById('close-login-search')
   if(close_popup_button)
   close_popup_button.addEventListener('click', clicked_close_popup, false)
   ;
}

function load_social_popup(){

   var dalam = "<div class='social_account'><a class='fb-like' data-action='like' data-href='https://www.facebook.com/malayatimes' data-layout='button' data-share='false' data-show-faces='true'></a></div> <div class='social_account'><a class='twitter-follow-button' data-show-count='false' data-show-screen-name='false' data-size='medium' href='https://twitter.com/MalayaTimes'>Follow @malayatimes</a></div> <div class='social_account'><div class='subscribe_youtube'><div class='g-ytsubscribe' data-channelid='UCYPj3cyJIeEf4zhnKqXHJzA' data-layout='default' data-count='hidden'></div></div></div>";

   var table = document.createElement('div')
   table.setAttribute('id', 'socials')
   table.innerHTML = dalam;
   var fans = document.getElementById('fans')
   if(fans) fans.appendChild(table), pisbuk_script(), google_script(), twitter_script(), social_popup = true;
}

function pisbuk_script(){
   !function(e,n,t){var o,c=e.getElementsByTagName(n)[0];e.getElementById(t)||(o=e.createElement(n),o.id=t,o.src="//connect.facebook.net/en_GB/sdk.j"+"s#xfbml=1&version=v2.6&appId=215494712150096",c.parentNode.insertBefore(o,c))}(document,"script","facebook-jssdk");
}

function twitter_script(){
   window.twttr=function(t,e,r){
      var n,i=t.getElementsByTagName(e)[0],w=window.twttr||{};
      return t.getElementById(r)?w:(n=t.createElement(e),
      n.id=r,n.src="https://platform.twitter.com/widgets.j"+"s",
      i.parentNode.insertBefore(n,i),
      w._e=[],
      w.ready=function(t){
         w._e.push(t)
      },
      w)
   }(document,"script","twitter-wjs");
}

function google_script(){
   var head= document.getElementsByTagName('head')[0];
   var script= document.createElement('script');
      script.type= 'text/javascript';
      script.src= 'https://apis.google.com/js/platform.js';
      script.id = 'mygooglescript';
      head.appendChild(script);
}

function clicked_close_popup(){
   if(check_attribute(document.body, 'class', 'menu-2-on'))
    remove_class(document.body, 'menu-2-on')
   ;
}

function clicked_popup_button(){
   if(!check_attribute(document.body, 'class', 'menu-2-on'))
   add_class(document.body, 'menu-2-on')
   ;
}

var timeout_main_event;
function even_run_js(){
     if(window){
          window.addEventListener('resize', function(){
               clearTimeout(timeout_main_event);
               timeout_main_event = setTimeout(function(){
                    console.log(document.body.clientWidth);
                    run_js();
               }, 2000);
          });
     }
}



function reset_popular(popular_box){
     for (var i = 0; i < popular_box.length; i++) {
          popular_box[i].style.left = parseFloat(popular_box['0'].clientWidth) * parseFloat(i) +'px';
     }
     set_titik(popular_box);
}

function set_titik(popular_box){
     var wrap_titik = document.getElementsByClassName('slidecount')[0];
     if(wrap_titik && popular_box){

          var index_active = 'haha';
          for (var i = 0; i < popular_box.length; i++) {
               if(popular_box[i].style.left == '0px'){
                    index_active = i;
               }
          }

          var titik = wrap_titik.getElementsByTagName('span');
          if(titik){
               for (var i = 0; i < titik.length; i++) {
                    titik[i].className = '';
               }
               if(titik[index_active]){
                    titik[index_active].className = 'active';
               }
          }

     }

}

function click_titik(popular_box){
     var wrap_titik = document.getElementsByClassName('slidecount')[0];
     if(wrap_titik && popular_box){
          var titik = wrap_titik.getElementsByTagName('span');
          if(titik){


               for (var i = 0; i < titik.length; i++) {

                    if (typeof window.addEventListener === 'function'){
                         (function (clickedElement) {

                              titik[i].addEventListener('click',function(){
                                   var index = clickedElement.getAttribute('id').replace('count','');
                                   console.log(index);
                                   if(popular_box[index]){
                                        var sebelum = popular_box[index].style.left.replace('px','');
                                        //popular_box[index].style.left = 0;
                                        if(sebelum > 0){
                                             //semua tolak.
                                             var angka = '-'+sebelum;
                                        } else {
                                             //semua tambah
                                             var angka = sebelum;
                                        }
                                        moving_popular(popular_box, angka,index);
                                        set_titik(popular_box);
                                   }
                              });

                         })(titik[i]);
                    }


               }


          }
     }
}

function moving_popular(popular_box, tambah, index){
     for (var ii = 0; ii< popular_box.length; ii++) {
          if(ii == index){
               popular_box[ii].style.left = '0px';
          } else {
               popular_box[ii].style.left = parseFloat(popular_box[ii].style.left.replace('px','')) + parseFloat(tambah) + 'px';
          }
     }
     jom_slide(popular_box);
}

function listen_slider(){

     var left_button = document.getElementsByClassName('slide_left')[0];
     var right_button = document.getElementsByClassName('slide_right')[0];
     var wrap = document.getElementsByClassName('popular-articles')[0];
     if(wrap){
          var popular_box = wrap.getElementsByClassName('popular-article');

          if(popular_box){

               jom_slide(popular_box);

               reset_popular(popular_box);

               click_titik(popular_box);

               if(left_button){
                    left_button.addEventListener('click',function(){
                         //+left
                         popular_to_right(popular_box);
                         jom_slide(popular_box)
                    });
               }
               if(right_button){
                    right_button.addEventListener('click',function(){
                         // - left
                         popular_to_left(popular_box);
                         jom_slide(popular_box)
                    });
               }

          }
     }

}

function popular_to_left(popular_box){
     if(popular_box){

        if(popular_box[last_array(popular_box)]){

           if(popular_box[last_array(popular_box)].style.left.replace('px','') == 0){
               reset_popular(popular_box);
          } else {
               for (var i = 0; i < popular_box.length; i++) {
                    var current_left = popular_box[i].style.left.replace('px','');
                    popular_box[i].style.left = parseFloat(current_left) - parseFloat(popular_box[0].clientWidth) + 'px';
               }
               set_titik(popular_box);
          }

        }

     }

}

function popular_to_right(popular_box){
     if(popular_box){
          for (var i = 0; i < popular_box.length; i++) {
               var current_left = popular_box[i].style.left.replace('px','');
               popular_box[i].style.left = parseFloat(current_left) + parseFloat(popular_box[0].clientWidth) + 'px';
          }
          set_titik(popular_box);
     }
}

function slider_popular(){
     var wrap = document.getElementsByClassName('popular-articles')[0];
     if(wrap){
          var popular_box = wrap.getElementsByClassName('popular-article');
          if(popular_box){
               jom_slide(popular_box);
          }
     }
}

var slide_timer;
function jom_slide(popular_box){
     clearInterval(slide_timer);
     slide_timer = setInterval(function(){
          popular_to_left(popular_box);
     }, 5000);

}

function on_scroll(){
     window.addEventListener('scroll',function(){
          load_post();
     });
}

function load_post(){
     var load_more = document.getElementById('load-more');
     if(load_more){
          if(is_visible(load_more) && !loaded) loaded = true, xhr_load_post(load_more);
     }
}

var dah_jumpa = 0;
function xhr_load_post(load_more){
     var fromData      = new FormData();
     fromData.append('offset', offset_post);
     fromData.append('category', window.location.hostname);
     fromData.append('limit_post', limit_post);
     var xhr = new XMLHttpRequest();
     xhr.open('POST', 'https://api.malayatimes.com/category/new-post', true);
      xhr.onreadystatechange = function() {
         if (xhr.readyState === 4){
              // if(xhr.status === 200){
                   var data = xhr.responseText;
                   // console.log(data)
                   if(data && data.length > 0){
                      var section = document.createElement('section');
                      section.className = 'wrapper rows';
                      section.innerHTML = data;
                      if(limit_post == 7){
                        dah_jumpa = 0;
                        var slotName = generateNextSlotName();

                        var aside = document.createElement('aside')
                        aside.className = 'iklan odd big';

                        var content_group = document.createElement('div')
                        content_group.className = 'content_group_inner_wrapper';
                        content_group.innerHTML = '<div class="content-item"><div class="premium" id="'+slotName+'"></div></div>';

                        aside.appendChild(content_group)

                        section.innerHTML += aside.outerHTML;

                      }
                      load_more.parentNode.insertBefore(section, load_more);
                      if(limit_post == 7){
                        while(dah_jumpa < 1){
                          if(document.getElementById(slotName)) create_ad(slotName), dah_jumpa = 1;
                        }
                      }
                      loaded = false;
                      offset_post += limit_post;
                      if(limit_post == 7){ var tmbh = 8; } else { var tmbh = 7; }
                      limit_post = tmbh;
                   } else {
                        var p = document.createElement('p');
                        p.className = 'load-more-message end';
                        p.innerHTML = 'YOU REACH THE END <span class="red">❤</span>';
                        load_more.parentNode.insertBefore(p, load_more);
                        var myload_more = document.getElementById('load-more')
                        if(myload_more) load_more.parentNode.removeChild(myload_more)
                   }
         }
      }
      xhr.send(fromData);
}


var nextSlotId = 1;
 function generateNextSlotName() {
   var id = nextSlotId++;
   var my_slot_id = 'adslot-' + id;
   while(document.getElementById(my_slot_id)){
      id = id++;
      my_slot_id = 'adslot-' + id;
   }
   return my_slot_id;
 }

 function create_ad(slotName){
    googletag.cmd.push(function() {

      var infi_ad = googletag.sizeMapping().
      addSize([300, 0], [ [300, 250], [250, 250], [200, 200]]).
      addSize([0, 0], [[200, 200], [250, 250]]).
      build();

     var slot = googletag.defineSlot('/21705193160/sdbr', [
        [300, 250],
        [250, 250],
        [200, 200]
     ], slotName)
     .defineSizeMapping(infi_ad)
     .addService(googletag.pubads());

     //googletag.pubads().disableInitialLoad();
     // googletag.pubads().enableAsyncRendering();
     // googletag.enableServices();

     // Display has to be called before
     // refresh and after the slot div is in the page.
     googletag.display(slotName);
     googletag.pubads().refresh([slot]);
   });
}






////////////////// NAV BAR MENU ////////////////////////

function mobile_menu() {
     var button = document.getElementById('mobile-menu-toggle');
     if(button){
          var menu_active = false;
          button.addEventListener('click',function(){
               if( menu_active ){
                    remove_class(document.body, 'menu-active');
                    menu_active = false;
               } else {
                    add_class(document.body, 'menu-active');
                    menu_active = true;
               }
          });
     }
}

////////////////// END NAV BAR MENU ////////////////////////
