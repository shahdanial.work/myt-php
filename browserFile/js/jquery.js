(function() { // start

   /*VARIABLE*/
   var
   editor = document.getElementById("post-editor"),
   preview = document.getElementById("post-preview"),
   previewBr = preview.getElementsByTagName('br'),
   previewPara = preview.getElementsByClassName('para'),
   editorPara = editor.getElementsByClassName('para'),
   elementPreview = preview.getElementsByTagName("*"),
   elementEditor = editor.getElementsByTagName("*")
   paraPara = editor.getElementsByClassName('para')
   ;












   // EDITOR CARET.
   var selRange;
   function saveSelection() {
      if (window.getSelection) {
         sel = window.getSelection();
         if (sel.getRangeAt && sel.rangeCount) {
            return sel.getRangeAt(0);
         }
      } else if (document.selection && document.selection.createRange) {
         return document.selection.createRange();
      }
      return null;
   }

   function restoreSelection(range) {
      if (range) {
         if (window.getSelection) {
            sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
         } else if (document.selection && range.select) {
            range.select();
         }
      }
   }

   function insertTextAtCursor(text) {
      var sel, range, html;
      if (window.getSelection) {
         sel = window.getSelection();
         if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();
            var textNode = document.createTextNode(text);
            range.insertNode(textNode);
            sel.removeAllRanges();
            range = range.cloneRange();
            range.selectNode(textNode);
            range.collapse(false);
            sel.addRange(range);
         }
      } else if (document.selection && document.selection.createRange) {
         range = document.selection.createRange();
         range.pasteHTML(text);
         range.select();
      }
   }

   function pasteHtmlAtCaret(html) {
      //put back cursor before insert.
      restoreSelection(selRange);
      var sel, range;
      if (window.getSelection) {
         // IE9 and non-IE
         sel = window.getSelection();
         if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();

            // Range.createContextualFragment() would be useful here but is
            // non-standard and not supported in all browsers (IE9, for one)
            var el = document.createElement("div");
            el.innerHTML = html;
            var frag = document.createDocumentFragment(), node, lastNode;
            while ( (node = el.firstChild) ) {
               lastNode = frag.appendChild(node);
            }
            range.insertNode(frag);

            // Preserve the selection
            if (lastNode) {
               range = range.cloneRange();
               range.setStartAfter(lastNode);
               range.collapse(true);
               sel.removeAllRanges();
               sel.addRange(range);
            }
         }
      } else if (document.selection && document.selection.type != "Control") {
         // IE < 9
         document.selection.createRange().pasteHTML(html);
      }
      //placeCaretAtEnd( document.getElementById("post-editor") );
      after_insert();
   }

   function placeCaretAtEnd(el) {
      if (typeof window.getSelection != "undefined"
      && typeof document.createRange != "undefined") {
         var range = document.createRange();
         range.selectNodeContents(el);
         range.collapse(false);
         var sel = window.getSelection();
         sel.removeAllRanges();
         sel.addRange(range);
      } else if (typeof document.body.createTextRange != "undefined") {
         var textRange = document.body.createTextRange();
         textRange.moveToElementText(el);
         textRange.collapse(false);
         textRange.select();
      }
   }

   function GetSelectedText() { //to call GetSelectedText(selRange);
      if (window.getSelection) {  // all browsers, except IE before version 9
         var range = window.getSelection ();
         return range.toString().trim();
      } else if (document.selection.createRange) { // Internet Explorer
         var range = document.selection.createRange ();
         return range.text.trim();
      } else {
         return 'false';
      }

   }












   /* EVEN LISTENER */
   //EDITOR.
   if(editor){
      editor.addEventListener('input', content_preview, false);
      editor.addEventListener ("paste", paste_editor, false);
      editor.addEventListener('keydown', function(e){
         if (e.keyCode === 13) {
            e.preventDefault();
            //new_line();
            selRange = saveSelection();//contenteditable
            //pasteHtmlAtCaret('<div class"para" id="kiamatlab">Edit Perenggan baru ini . . .</div>');
            pasteHtmlAtCaret('</div><div class="para" contenteditable="true">');
            /*
            1. Onclick, ask want to edit or Add new paragraph Before this paragraph or After this paragraph..
            2. once click edit, make sure there's nothing will  hapend, they're can edit what ever in paragraph but prevenDefault Enter.
            3. once click add paragraph, add them selected location..
            */
         }
      });
   }



   /* WHEN USER PASTE SOMETHING. FORMAT IT FIRST! */
   function paste_editor(e){
      e.preventDefault();
      var text = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

      if(text){
         var wrap = document.createElement('div');
         var arrayText = text.replace(/\n{3,}/g, '\n').split('\n');
         for (i = 0; i < arrayText.length; ++i) {
            var perenggan = arrayText[i];
            var sementara = document.createElement("div");
            sementara.className = 'para';
            sementara.innerHTML = perenggan;
            wrap.appendChild(sementara);
         }
         //console.log(wrap.innerHTML);
         document.execCommand("insertHTML", false, wrap.innerHTML);

      }
      no_paraPara();
      selRange = saveSelection();
      document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/jquery.js\"><\/script>';
   }


   /* TOOL TO CLEAN ARRAY */
   function cleanArray(actual) {
      var newArray = new Array();
      for (var i = 0; i < actual.length; i++) {
         if (actual[i]) {
            newArray.push(actual[i]);
         }
      }
      return newArray;
   }


   /* CALL IT AFTER YOU INSERT SOMETHING TO EDITOR. IT WILL REMOVE DOUBLE PARA PARA */
   function no_paraPara(){

      if(paraPara){
         // foreach all element.
         var i;
         for (i = 0; i < paraPara.length; ++i) {
            var pePara = paraPara[i];
            var paraDalam = pePara.getElementsByClassName('para');
            if(paraDalam){
               pePara.innerHTML = pePara.innerText || pePara.textContent;
               var arr = pePara.innerHTML.replace(/\n{3,}/g, '\n').split('\n');
               var arr = cleanArray(arr);
               var ii; var wrap = document.createElement('div');
               for (ii = 0; ii < arr.length; ++ii) {
                  var perenggan = arr[ii];
                  var sementara = document.createElement("div");
                  sementara.className = 'para';
                  sementara.innerHTML = perenggan;
                  wrap.appendChild(sementara);
               }
               pePara.outerHTML = wrap.innerHTML;
            }
            //console.log(pePara.innerHTML);
         }
      }
      return false;
   }

   function after_insert(){
      // var kiamatlab = document.getElementById('kiamatlab');
      // if(kiamatlab){
      //    var wrapper_dia = kiamatlab.parentNode;
      //    if(wrapper_dia.className == 'para'){
      //       wrapper_dia.removeChild(kiamatlab);
      //       var lagi_wrapper = wrapper_dia.parentNode;
      //       var perengganBaru = document.createElement('div');
      //       perengganBaru.className = 'para';
      //       perengganBaru.innerHTML = 'Edit perenggan baru ini...';
      //       var fakeWrap = document.createElement('div');
      //       fakeWrap.id = 'fakerWrap';
      //       fakeWrap.innerHTML = wrapper_dia.outerHTML + perengganBaru.outerHTML;
      //       lagi_wrapper.replaceChild(fakeWrap, wrapper_dia);
      //
      //       var wtf = document.getElementById('fakerWrap');
      //       if(wtf){
      //          wtf.outerHTML = wtf.innerHTML;
      //       }
      //    }
      // }

      if(paraPara){
         // foreach all element.
         var i;
         for (i = 0; i < paraPara.length; ++i) {
            var pePara = paraPara[i];
            var paraDalam = pePara.getElementsByClassName('para')[0];
            if(paraDalam){
               var getParaDalam = paraDalam.outerHTML;
               pePara.outerHTML = '<div class="para">' + pePara.innerHTML.replace(getParaDalam, '') + '</div>' + getParaDalam;
            }
         }
      }
      close_all_popup();
      new_line();
      content_preview();
   }

   function new_line(){
      newline = document.createElement('div');
      newline.className = 'para';
      newline.innerHTML = '<br>';
      editor.appendChild(newline);
      placeCaretAtEnd( document.getElementById("post-editor") );
   }

   /* FORMAT EDITOR */
   function format_editor(){
      //remove certain thing pada semua element.
      if(elementEditor){
         /* VARIALBLE */
         //set up allowed attr and allowed value.
         var allowedAttr = {
            class: true,
            'alt': true,
            'title': true,
            'href': true,
            'src': true,
            'contenteditable': true
         };
         var allowedClassVal = {
            'para': true, //<div class="para">text perenngan pertama</div>
            'gallery': true, //gallery in post wrapper.
            'single_image': true, // gambar single.
            'gallery_image': true, //gambar dalam gallery figure.
            'content-table': true, // table
            'message': true,
            'video': true,
            'kiamatlab': true, // when user enter between paragraph.. create new line, then focus to this class.
            'true': true

         };
         var disallowTag = {
            'script': true,
            'body': true,
            'noscript': true,
            'style': true,
            'br': true
         };

         var i;
         // foreach all element.
         for (i = 0; i < elementEditor.length; ++i) {
            var
            elem = elementEditor[i]
            ;

            // <element> area.
            if(elem){
               // tukar p kepada div.para
               if(elem.tagName.toLowerCase() == 'p'){
                  var newPara = document.createElement('div');
                  newPara.className = 'para';
                  //masukkan para element dalam p
                  newPara.innerHTML = elem.innerHTML;
                  //masukkan p sebelum .para
                  elem.parentNode.insertBefore(newPara, elem);
                  //remove .para
                  elem.parentNode.removeChild(elem);
               }

               //remove disallowTag.
               if (disallowTag[elem.tagName.toLowerCase()]){
                  //console.log(elem.tagName + ' ni tag yg x allowed');
                  elem.parentNode.removeChild(elem);
               }

            }
            //arrtibute="value" area
            if (elem.attributes) {
               var ii;
               for (ii = elem.attributes.length -1; ii >=0; --ii) {
                  var elemAttr = elem.attributes[ii];
                  //console.log(elemAttr);
                  if (!allowedAttr[elemAttr.name.toLowerCase()]){ //jika attribute ni x tersenarai
                     elem.attributes.removeNamedItem(elemAttr.name);
                  } else {
                     if(elemAttr.name == 'class'){
                        //class area.
                        if(!allowedClassVal[elemAttr.value.toLowerCase()]){
                           var classArray = elemAttr.value.toLowerCase().split(' ');
                           var fKlas = '';
                           for (var ioo = 0; ioo < classArray.length; ioo++) {
                              var Klas = classArray[ioo];
                              if(allowedClassVal[Klas]){
                                 fKlas += Klas +' ';
                              }
                           }
                           elemAttr.value = fKlas;
                           //console.log(fKlas);
                           //elem.attributes.removeNamedItem(elemAttr.name);
                           //if( !elemAttr.value.toLowerCase.trim().indexOf('para') > -1 ){ // xde class para.
                        }
                     }
                  }
               }
            }
         } // end foreach all element.
      }// if elementEditor
   }


   /*FORMAT HTML INSDE EDITOR */
   function format_preview(){

      //tukar .para to p
      if(previewPara){
         for (var i = 0; i < previewPara.length; i++) {
            para = previewPara[i];
            if (typeof window.addEventListener === 'function'){
               (function (index) {

                  if(index.innerHTML.indexOf('</div>') > -1 || index.innerHTML.indexOf('</table>') > -1){
                     //create p
                     var newP = document.createElement('div');
                  } else {
                     //create p
                     var newP = document.createElement('p');
                  }

                  newP.className = 'para';
                  //masukkan para element dalam p
                  newP.innerHTML = para.innerHTML;
                  //masukkan p sebelum .para
                  para.parentNode.insertBefore(newP, para);
                  //remove .para
                  para.parentNode.removeChild(para);
               })(para);
            }
         }
      }

      if(previewBr){
         for (var i = 0; i < previewBr.length; i++) {
            if (typeof window.addEventListener === 'function'){
               (function (index) {
                  index.parentNode.removeChild(index);
               })(previewBr[i]);
            }
         }
      }

      if(elementPreview){
         for (var i = 0; i < elementPreview.length; i++) {
            if (typeof window.addEventListener === 'function'){
               (function (index) {
                  var dalam = index.textContent.replace(/\s+/g, '');
                  if(dalam == null || dalam == undefined || dalam.length < 1){
                     index.parentNode.removeChild(index);
                  }
               })(elementPreview[i]);
            }
         }
      }


   }


      /* SEND EDITOR CONTENT TO PREVIEW */
      function content_preview(){
         format_editor();
         selRange = saveSelection();
         preview.innerHTML = editor.innerHTML.replace('<div class="para"><br></div>', '');
         format_preview();
      }























      /* POPUP AREA */
      var
      dialogModal = document.getElementsByClassName('modal')[0],
      popupClose = document.getElementsByClassName('popup-close')
      ;
      //CONTROL CLICKED, SHOW POPUP.
      var controlButton = document.querySelectorAll('.wysiwyg-controls > a');
      if(controlButton){
         for (var i = 0; i < controlButton.length; i++) {
            if (typeof window.addEventListener === 'function'){
               (function (index) {
                  controlButton[i].addEventListener('click', function(e){
                     e.preventDefault();
                     //kalo takde selRange.

                     if(!selRange){
                        placeCaretAtEnd( document.getElementById("post-editor") );
                        selRange = saveSelection();
                     }

                     var id = document.getElementById(index.getAttribute('data-show'));
                     if(index.getAttribute('data-show') == 'a_a_link'){
                        link_management();
                     }
                     if(index.getAttribute('data-show') == 'a_a_button'){
                        button_by_title();
                     }
                     if(index.getAttribute('data-show') == 'a_a_message'){
                        message_box();
                     }
                     if(index.getAttribute('data-show') == 'a_a_qoute'){
                        qoute_box();
                     }
                     if(id){
                        dialogModal.style.display = 'block';
                        id.style.display = 'block';
                     }

                  });
               })(controlButton[i]);
            }
         }
      }

      /* CLOSE ALL POPUP */
      for (var i = 0; i < popupClose.length; i++) {
         popupClose[i].addEventListener('click', close_all_popup, false);
      }
      function close_all_popup(){
         dialogModal.style.display = 'none';
         var popup_contents = document.getElementsByClassName('popup-content');
         for (var i = 0; i < popup_contents.length; i++) {
            var popup_content = popup_contents[i];
            popup_content.style.display = 'none';
         }
      }




      /* GAMBAR POPUP TABBER AREA. */
      var menu_tab = document.getElementById("pilihan-option").querySelectorAll('span');
      if(menu_tab){

         for (var i = 0; i < menu_tab.length; i++) {
            if (typeof window.addEventListener === 'function'){
               (function (index) {
                  menu_tab[i].addEventListener('click', function(){
                     proceed_tab(index.getAttribute('data-show'), index.getAttribute('id'));
                  });
               })(menu_tab[i]);
            }
         }

      }
      function proceed_tab(data_show, id){
         //reset to display none all.
         var tab_content = document.querySelectorAll('#image-option > div');//document.getElementById("image-option").querySelectorAll('div');
         if(tab_content){
            for(var i = 0; i < tab_content.length; i++) {
               var tab_id = tab_content[i].getAttribute('id');
               if(tab_id == data_show){
                  tab_content[i].style.display = 'block';
               } else {
                  tab_content[i].style.display = 'none';
               }
            }
         }

         //add selected
         if(menu_tab){
            for(var i = 0; i < menu_tab.length; i++) {
               var theId = menu_tab[i].getAttribute('id');
               if(theId == id){
                  // add selected
                  var finalid = document.getElementById(theId);
                  var semiKelas = finalid.className.replace("selected", "");
                  var Kelas = semiKelas + ' ' + 'selected';
                  finalid.setAttribute('class', Kelas);
                  //var ddd = finalid.getAttribute('data-show');
                  //var idContent = document.getElementById(ddd);
               } else {
                  //remove selected
                  var finalid = document.getElementById(theId);
                  var Kelas = finalid.className;
                  finalid.setAttribute('class', Kelas.replace("selected", "") );
               }
            }
         }
      }


      //BEFORE UPLOAD. ONCHANGE INPUT
      var myinput = document.getElementById("insert-file");
      myinput.addEventListener('change', function () {
         if(this.files.length > 0){
            fetchimage();
            document.getElementById('jumlah-file').innerHTML = this.files.length;
            document.getElementsByClassName('upload-button')[0].removeAttribute("disabled");
         } else {
            document.getElementById('jumlah-file').innerHTML = '0';
            document.getElementsByClassName('upload-button')[0].setAttribute('disabled', 'disabled');
         }
      });
      function fetchimage(){
         var filelist = myinput.files || [];
         for (var i = 0; i < filelist.length; i++) {
            getBase64(filelist[i], i);
         }
      }
      function getBase64(file, ai, adui) {
         var nama = file.name;
         nama = nama.replace(/\[|\]|{|}|\?|\/|\+|\-|_|\(|\)|&|\^|%|\#|\$|\@|\!|\"|\'+/g,' ');
         nama = nama.replace(/\s\s|.jpg|.png|.jpeg|.gif|.JPG|.PNG|.JPEG|.GIF|\/+/g,' ').trim();
         var reader = new FileReader();
         reader.readAsDataURL(file);
         reader.onload = function () {
            document.getElementById("before-upload").innerHTML += '<div class="img_wrpr"><img src="'+reader.result+'"  id="img_'+ai+'" class="data_img"/><div class="data_box"><label for="usr_files[text]['+ai+']">TITLE</label><p class="box_right"><input class="nama-file" name="usr_files[text]['+ai+']" type="text" value="'+nama+'" multiple="multiple" required/></p></div><!--tag--><div class="data-tag"><div class="tag-message">Tag orang, tempat atau benda yang terlibat dalam gambar</div><span class="tag-message">Maximum 5 (yang popular atau general sahaja):</span> <div class="tag-input data_box"><label>TAG</label><p class="box_right"><input name="usr_files[img_tag]['+ai+']" class="tag-gambar" placeholder="contoh: Awie, Mahathir, Bali, Adidas, KFC" multiple="multiple"></p></div></div>';

         };
      }









      /* LINK AREA */
      function link_management(){
         document.querySelectorAll('#a_a_link .insert-text')[0].value = GetSelectedText(selRange);
         link_by_title();
      }

      document.querySelectorAll('#a_a_link .insert-text')[0].addEventListener('input', function(){
         link_by_title();
      });

      function link_by_title(){
         var text = document.querySelectorAll('#a_a_link .insert-text')[0].value;
         if(text){
            //cari title ni.
            var cari_link;
            clearTimeout(cari_link);
            cari_link = setTimeout(function() {
               var xhr      = new XMLHttpRequest();
               xhr.open('POST', 'http://media.malayatimes.com/search_link/title?cari='+ encodeURI(text), true);
               xhr.onreadystatechange = function() {
                  if (xhr.readyState === 4 && xhr.status === 200){
                     var data = xhr.responseText;
                     if(data){
                        var link_input = document.querySelectorAll('#a_a_link .insert-link')[0];
                        if(link_input.value.length == 0){
                           link_input.value = data;
                           document.querySelectorAll('#a_a_link .insert')[0].removeAttribute('disabled');
                        }
                     }
                  }
               }
               xhr.send();
            }, 1000);
         }
      }

      document.querySelectorAll('#a_a_link .insert')[0].addEventListener('click', function(){
         var text = document.querySelectorAll('#a_a_link .insert-text')[0];
         var link = document.querySelectorAll('#a_a_link .insert-link')[0];
         if(text.value.length > 0 && link.value.length > 0){
            var str = GetSelectedText(selRange);
            if(str[str.length - 1] == ' '){
               var space = ' ';
            } else {
               var space = '';
            }

            pasteHtmlAtCaret('<a href="'+link.value+'">'+text.value+'</a>' + space);
            text.value = '';
            link.value = '';
         } else {
            alert('Edit semula. Link dan text tidak boleh kosong...');
         }
      });

      document.querySelectorAll('#a_a_link .insert-link')[0].addEventListener('paste', function(){
         var tatuta;
         clearTimeout(tatuta);
         tatuta = setTimeout(function(){
            var carian = document.querySelectorAll('#a_a_link .insert-link')[0].value;
            var xhr      = new XMLHttpRequest();
            xhr.open('POST', 'http://media.malayatimes.com/search_link/link?cari='+ encodeURI(carian), true);
            xhr.onreadystatechange = function() {
               if (xhr.readyState === 4 && xhr.status === 200){
                  var data = xhr.responseText;
                  if(data){
                     var text_input = document.querySelectorAll('#a_a_link .insert-text')[0];
                     text_input.value = data;
                     document.querySelectorAll('#a_a_link .insert')[0].removeAttribute('disabled');
                  }
               }
            }
            xhr.send();
         }, 500);
      });
      /* END LINK AREA */


      /* RELATED AREA */
      document.querySelectorAll('#a_a_related .insert-related-link')[0].addEventListener('input', function(){
         if(this.value.length > 0){
            var xhr      = new XMLHttpRequest();
            xhr.open('POST', 'http://media.malayatimes.com/search_link/link?cari='+ encodeURI(this.value), true);
            xhr.onreadystatechange = function() {
               if (xhr.readyState === 4 && xhr.status === 200){
                  var data = xhr.responseText;
                  if(data){
                     document.querySelectorAll('#a_a_related .insert-related-text')[0].value = data;
                     document.querySelectorAll('#a_a_related .insert')[0].removeAttribute('disabled');
                  }
               }
            }
            xhr.send();
         } else {
            document.querySelectorAll('#a_a_related .insert')[0].setAttribute('disabled', 'disabled');
         }
      });

      document.querySelectorAll('#a_a_related .insert-related-text')[0].addEventListener('input', function(){
         if(this.value.length > 0){
            related_by_title();
         } else {
            document.querySelectorAll('#a_a_related .insert')[0].setAttribute('disabled', 'disabled');
         }
      });

      function related_by_title(){
         var sakit_leher;
         sakit_leher = setTimeout(function(){
            var theinput = document.querySelectorAll('#a_a_related .insert-related-text')[0];
            var xhr      = new XMLHttpRequest();
            xhr.open('POST', 'http://media.malayatimes.com/search_link/title?cari='+ encodeURI(theinput.value), true);
            xhr.onreadystatechange = function() {
               if (xhr.readyState === 4 && xhr.status === 200){
                  clearTimeout(sakit_leher);
                  var data = xhr.responseText;
                  if(data){
                     document.querySelectorAll('#a_a_related .insert-related-link')[0].value = data;
                     document.querySelectorAll('#a_a_related .insert')[0].removeAttribute('disabled');
                  }
               }
            }
            xhr.send();
         }, 2000);
      }
      /* END RELATED AREA */

      /* BUTTON AREA */
      document.querySelectorAll('#a_a_button .insert-text-button')[0].addEventListener('input', function(){
         if(this.value.length > 0){
            button_by_title();
         } else {
            document.querySelectorAll('#a_a_related .insert')[0].setAttribute('disabled', 'disabled');
         }
      });

      function button_by_title(){
         document.querySelectorAll('#a_a_button .insert-text-button')[0].value = GetSelectedText(selRange);
         var sakit_pale;
         sakit_pale = setTimeout(function(){
            var theinput = document.querySelectorAll('#a_a_button .insert-text-button')[0];
            var xhr      = new XMLHttpRequest();
            xhr.open('POST', 'http://media.malayatimes.com/search_link/title?cari='+ encodeURI(theinput.value), true);
            xhr.onreadystatechange = function() {
               if (xhr.readyState === 4 && xhr.status === 200){
                  clearTimeout(sakit_pale);
                  var data = xhr.responseText;
                  if(data){
                     document.querySelectorAll('#a_a_button .insert-link-button')[0].value = data;
                     document.querySelectorAll('#a_a_button .insert')[0].removeAttribute('disabled');
                  }
               }
            }
            xhr.send();
         }, 2000);
      }

      /* END BUTTON AREA */


      /* TABLE AREA */
      document.querySelectorAll('#a_a_table .add-rows')[0].addEventListener('click', function(){
         var tbody = document.querySelectorAll('#a_a_table tbody')[0];
         tbody.innerHTML = tbody.innerHTML + '<tr><td contenteditable="true">-</td><td contenteditable="true">-</td></tr>';
      });

      document.querySelectorAll('#a_a_table .remove-rows')[0].addEventListener('click', function(){
         var tbody = document.querySelectorAll('#a_a_table tbody')[0];
         var rws=tbody.getElementsByTagName('tr');
         tbody.removeChild(rws[rws.length-1]);
      });

      document.querySelectorAll('#a_a_table .insert')[0].addEventListener('click', function(){
         var table = document.querySelectorAll('#a_a_table .table-content')[0].innerHTML;
         pasteHtmlAtCaret('<div class="para">'+table+'</div>');
      });
      /* END TABLE AREA */

      /* MESSAGE AREA */
      function message_box(){
         document.querySelectorAll('#a_a_message .insert-message')[0].value = GetSelectedText(selRange);
      }
      document.querySelectorAll('#a_a_message .insert')[0].addEventListener('click', function(){
         var box = document.querySelectorAll('#a_a_message .insert-message')[0];
         if(box.value.length > 0){
            pasteHtmlAtCaret('<div class="para"><div class="message">'+box.value+'</div></div>');
         } else {
            alert('Anda tidak boleh memasukkan message box yang tiada isi. Edit atau close untuk cancel..');
         }
      });
      /* END MESSAGE AREA */

      /* BLOCKQUOTE AREA */
      function qoute_box(){
         document.querySelectorAll('#a_a_quote .insert-quote')[0].value = GetSelectedText(selRange);
      }

      document.querySelectorAll('#a_a_quote .insert')[0].addEventListener('click', function(){
         var thevalue = document.querySelectorAll('#a_a_quote .insert-quote')[0].value;
         if(thevalue.length > 0){
            pasteHtmlAtCaret('<div class="para"><blockquote>'+thevalue+'</blockquote></div>');
         } else {
            alert('Untuk memasukkan element ini anda hendaklah mengisi field yang disediakan. Isi atau close untuk cancel..');
         }
      });
      /* END BLOCKQUOTE AREA */

      /* TAG AREA */
      document.querySelectorAll('#label-box .go')[0].addEventListener('click', function(){
         cari_tag();
      });
      //var search_tag_kuar;
      function cari_tag(){
         //clearTimeout(search_tag_kuar);
         document.getElementsByClassName('label-list')[0].innerHTML = '<li><i>relavan tags appear when you stop typing..</i></li>';
         //search_tag_kuar = setTimeout(function(){
         var carian = document.getElementById('label-search');
         document.getElementsByClassName('label-list')[0].style.display = 'block';
         if(carian.value.length > 2){
            var xhr      = new XMLHttpRequest();
            xhr.open('GET', 'http://media.malayatimes.com/search-tag/ajax?cari='+ encodeURI(carian.value), true);
            xhr.onreadystatechange = function() {
               if (xhr.readyState === 4 && xhr.status === 200){
                  var data = xhr.responseText;
                  if(data){
                     var check_berapa = document.getElementsByClassName('label-list')[0].innerHTML;
                     document.getElementsByClassName('label-list')[0].innerHTML = data;
                     label_list(check_berapa);
                  }
               }
            }
            xhr.send();
         } else {
            document.getElementsByClassName('label-list')[0].innerHTML = '<li><i>atleast 3 huruf..</i></li>';
         }
         //}, 2000);
         return false;
      }
      function label_list(check_berapa){
         var waitUntil = function (fn, condition, interval) {
            interval = interval || 100;
            var shell = function () {
               var timer = setInterval(
                  function () {
                     var check;

                     try { check = !!(condition()); } catch (e) { check = false; }

                     if (check) {
                        clearInterval(timer);
                        delete timer;
                        fn();
                     }
                  },
                  interval
               );
            };
            return shell;
         };
         waitUntil(
            function(){
               //-----
               document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/jquery.js\"><\/script>';
               var label_list = document.getElementsByClassName('label-list')[0].querySelectorAll('li');
               for (var i = 0; i < label_list.length; i++) {
                  if (typeof window.addEventListener === 'function'){
                     (function (index) {
                        label_list[i].addEventListener('click', function(){
                           var tag_val = filter_char(index.textContent);
                           var the_tag_id = tag_val.replace(/ /g, '_');
                           var tag_id = 'tag_value_'+ tag_val+'_id';
                           var tag_id = tag_id.replace(/ /g, '_');
                           if(tag_val.length > 0 && !document.getElementById(tag_id)){

                              var main_form = document.createElement('input');
                              main_form.setAttribute('value', tag_val);
                              main_form.setAttribute('name', 'tag[]');
                              main_form.setAttribute('id', tag_id);
                              main_form.setAttribute('readonly', 'true');
                              main_form.setAttribute('type', 'hidden');
                              document.getElementsByClassName('main-form')[0].prepend(main_form);

                              var draft_hidden = document.createElement('input');
                              draft_hidden.setAttribute('value', tag_val);
                              draft_hidden.setAttribute('name', 'tag[]');
                              draft_hidden.setAttribute('id', 'draft_'+tag_id);
                              draft_hidden.setAttribute('readonly', 'true');
                              draft_hidden.setAttribute('type', 'hidden');
                              document.getElementById('draft-form').prepend(draft_hidden);

                              labelresult = document.createElement('span');
                              labelresult.setAttribute('class', 'selected-label '+ the_tag_id);
                              labelresult.setAttribute('title', 'click to remove');
                              labelresult.setAttribute('data-val', tag_val);
                              labelresult.innerHTML = tag_val+'<i class="fa fa-times" aria-hidden="true"></i>';
                              document.getElementById('labelresult').prepend(labelresult);

                              document.getElementsByClassName('label-list')[0].style.display = 'none';
                              document.getElementById('label-search').value = null;
                              document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/jquery.js\"><\/script>';
                              document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/jquery.js\"><\/script>';
                              listen_remove_label();
                           } else {
                              alert('Dilarang mengulangi Tag yang sama..');
                           }
                        });
                     })(label_list[i]);
                  }
               }
               //-----
            },
            function(){
               // the code that tests here... (return true if test passes; false otherwise)
               var check_border = document.getElementsByClassName('label-list')[0].innerHTML;
               return !!(check_border !== check_berapa);
               //return !!(document.getElementsByClassName('pics')[0].innerHTML !== '');
            },
            50 // amout to wait between checks
         )();
      }

      document.getElementById('label-search').addEventListener('input',function(){
         cari_tag();
      });

      var hide_list;
      document.getElementsByClassName('label-list')[0].addEventListener('mouseleave', function(){
         clearTimeout(hide_list);
         hide_list = setTimeout(function(){
            document.getElementsByClassName('label-list')[0].style.display = 'none';
         }, 500);
      });

      function listen_remove_label(){
         var selected_label = document.querySelectorAll('#labelresult .selected-label');
         if(selected_label){
            for (var i = 0; i < selected_label.length; i++) {
               if (typeof window.addEventListener === 'function'){
                  (function (index) {
                     selected_label[i].addEventListener('click', function(e){
                        var tag_val = index.getAttribute('data-val');
                        var tag_val = filter_char(tag_val);
                        var the_tag_id = tag_val.replace(/ /g, '_');
                        var tag_id = 'tag_value_'+ tag_val+'_id';
                        var tag_id = tag_id.replace(/ /g, '_');
                        // remove selected inside labelresult...
                        var el = document.querySelectorAll('#labelresult .'+the_tag_id)[0];
                        el.parentNode.removeChild( el );
                        //remove .main-form tag input copy...
                        var ele = document.getElementById(tag_id);
                        ele.parentNode.removeChild(ele);
                        //remove #draft-form tag input copy...
                        var elem = document.getElementById('draft_'+tag_id);
                        elem.parentNode.removeChild(elem);
                     });
                  })(selected_label[i]);
               }
            }
         }
      }


      function filter_char(data){
         return data.replace(/[&\/\\#,+()$~%.!\@\^\_\=\-\[\]\"\;\`\'\|:*?<>{}]/g, '');
      }
      /* END TAG AREA */


      //AJAX UPLOAD AREA.
      document.getElementById('ajaxupload').addEventListener('submit', function(e) {
         e.preventDefault();
         run_ajax(this);
      });

      function run_ajax(getit){
         var plus = document.querySelectorAll('.img_wrpr').length;
         var old = document.querySelectorAll('.pics .border').length;
         var total = parseFloat(plus) + parseFloat(old);
         var waitUntil = function (fn, condition, interval) {
            interval = interval || 100;
            var shell = function () {
               var timer = setInterval(
                  function () {
                     var check;

                     try { check = !!(condition()); } catch (e) { check = false; }

                     if (check) {
                        clearInterval(timer);
                        delete timer;
                        fn();
                     }
                  },
                  interval
               );
            };
            return shell;
         };
         waitUntil(
            function(){
               after_html_changed(); // only this way will made your js working for xhr element!
            },
            function(){
               // the code that tests here... (return true if test passes; false otherwise)
               var check_border = document.querySelectorAll('.pics .border');
               return !!(check_border.length === total);
               //return !!(document.getElementsByClassName('pics')[0].innerHTML !== '');
            },
            50 // amout to wait between checks
         )();

         document.getElementById('jumlah-file').innerHTML = '0';
         var formData = new FormData(getit);
         var xhr      = new XMLHttpRequest();
         xhr.open('POST', 'http://media.malayatimes.com/upload/', true);
         xhr.onreadystatechange = function() {
            if ( xhr.readyState === 4 && xhr.status === 200 ) {
               //console.log(xhr.responseText);
               var ddaattaa = xhr.responseText;
               if(ddaattaa){
                  document.getElementsByClassName('pics')[0].innerHTML = ddaattaa + document.getElementsByClassName('pics')[0].innerHTML;
                  //console.log('content loaded');
               }
            }
         }
         xhr.send( formData );
         document.getElementsByClassName('upload-button')[0].setAttribute('disabled', 'disabled');
         document.getElementById('before-upload').innerHTML = '';
      }

      function after_html_changed(){
         document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/jquery.js\"><\/script>';
         var brdr = document.getElementsByClassName('pics')[0].querySelectorAll('div');
         for(var i = 0; i < brdr.length; i++) {
            if (typeof window.addEventListener === 'function'){
               (function (index) {
                  brdr[i].addEventListener('click', function(){
                     var Klas = index.getAttribute('class');
                     if(Klas.trim() == 'border'){//true.
                        index.setAttribute('class', 'border selected');
                     } else {
                        index.setAttribute('class', 'border');
                     }
                     var jumlah = document.getElementsByClassName('pics')[0].querySelectorAll('.selected');
                     if(jumlah){
                        if(jumlah.length > 0){
                           document.getElementById('img-insert-btn').removeAttribute('disabled');
                        } else {
                           document.getElementById('img-insert-btn').setAttribute('disabled', 'disabled');
                        }
                     }
                  });
               })(brdr[i]);
            }
         }
         insert_gambar();
      }

      //CARI GAMBAR AJAX AREA.
      var search_gambar;
      document.getElementsByClassName('cari-gambar')[0].addEventListener('keydown', function(){
         var carian = this.value;
         if(carian && carian.length > 2){
            clearTimeout(search_gambar);
            var check_berapa = false;
            search_gambar = setTimeout(function() {
               var xhr      = new XMLHttpRequest();
               xhr.open('POST', 'http://media.malayatimes.com/search_image/ajax?cari='+ encodeURI(carian), true);
               xhr.onreadystatechange = function() {
                  if ( xhr.readyState === 4 && xhr.status === 200 ) {
                     //console.log(xhr.responseText);
                     var ddaattaa = xhr.responseText;
                     if(ddaattaa){
                        var check_berapa = (ddaattaa.match(/class=\"pale\"/g)||[]).length;
                        document.getElementById('cari-gambar-result').innerHTML = ddaattaa;
                        document.getElementById('cari-gambar-result').style.display = 'block';
                        //console.log('content loaded');
                        after_cari_gambar(check_berapa);
                     }
                  }
               }
               xhr.send();
            }, 1000);

         }// end if carian..
      });

      function after_cari_gambar(check_berapa){
         if(check_berapa){
            if(check_berapa != 0){
               var waitUntil = function (fn, condition, interval) {
                  interval = interval || 100;
                  var shell = function () {
                     var timer = setInterval(
                        function () {
                           var check;

                           try { check = !!(condition()); } catch (e) { check = false; }

                           if (check) {
                              clearInterval(timer);
                              delete timer;
                              fn();
                           }
                        },
                        interval
                     );
                  };
                  return shell;
               };
               waitUntil(
                  function(){
                     var pale = document.querySelectorAll('.picca .pale');
                     if(pale){
                        for (var i = 0; i < pale.length; i++) {
                           if (typeof window.addEventListener === 'function'){
                              (function (index) {
                                 pale[i].addEventListener('click', function(e){
                                    e.preventDefault();
                                    if(!selRange){
                                       placeCaretAtEnd( document.getElementById("post-editor") );
                                    }

                                    index.setAttribute('class', 'border');
                                    var gambar = index;
                                    var pics = document.getElementsByClassName('pics')[0];
                                    pics.innerHTML = index.outerHTML + pics.innerHTML;
                                    index.outerHTML = '';
                                    after_html_changed();
                                 });
                              })(pale[i]);
                           }
                        }
                     }
                  },
                  function(){
                     // the code that tests here... (return true if test passes; false otherwise)
                     var check_border = document.querySelectorAll('.picca .pale');
                     return !!(check_border.length === check_berapa);
                     //return !!(document.getElementsByClassName('pics')[0].innerHTML !== '');
                  },
                  50 // amout to wait between checks
               )();
            }
         }
      }

      function insert_gambar(){
         document.getElementById('img-insert-btn').addEventListener('click', function(){
            var jumlah = document.getElementsByClassName('pics')[0].querySelectorAll('.selected img');
            if(jumlah){
               if(jumlah.length == 1){
                  build_single_image(jumlah);
               } else {
                  build_gallery_image(jumlah);
               }
            }
         });
      }

      function build_single_image(jumlah){
         var title = jumlah[0].getAttribute('alt');
         var path = jumlah[0].getAttribute('src');
         var path = path.substr(path.lastIndexOf('/') + 1);
         var myhtml = '<a href="'+'//media.malayatimes.com/gambar/' + path + '" title="'+ title +'" class="single_image"><img alt="'+ title +'" src="'+'//media.malayatimes.com/gambar/' + path + '"/></a>';
         var selectPastedContent = myhtml;
         pasteHtmlAtCaret('<div class="para">'+myhtml+'</div>');
         return false;
      }

      function build_gallery_image(jumlah){
         var figure = document.createElement('figure');
         figure.className = 'gallery';
         for (var i = 0; i < jumlah.length; i++) {
            var title = jumlah[i].getAttribute('alt');
            var path = jumlah[i].getAttribute('src');
            var path = path.substr(path.lastIndexOf('/') + 1);
            // console.log('title ialah: ' + title);
            // console.log('path ialah: ' + path);
            // var item = '<a href="'+'//media.malayatimes.com/gambar/' + path + '" title="'+ title +'" class="gallery_image"><img alt="'+ title +'" src="'+'//media.malayatimes.com/image/' + path + '"/></a>';
            var a = document.createElement('a');
            a.className = 'gallery_image';
            a.setAttribute('href', '//media.malayatimes.com/gambar/' + path);
            a.setAttribute('title', title);
            a.innerHTML = '<img alt="'+ title +'" src="'+'//media.malayatimes.com/image/' + path + '"/>';
            figure.appendChild(a);
         }
         pasteHtmlAtCaret('<div class="para">'+figure.outerHTML+'</div>');
         return false;
      }
      /* END GAMBAR AREA */






      /* VIDEO AREA */
      function videoid(x){
         var url = x.toString();
         var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
         if(videoid != null) {
            return videoid[1];
         } else {
            return false;
         }
      }

      var check_bedio;
      document.getElementById('youtube-reader').addEventListener('change', function(e){
         e.preventDefault();
         clearTimeout(check_bedio);
         check_bedio = setTimeout(function() {
            var ee = document.getElementById('youtube-reader');
            if(ee.value.length > 3 && videoid(ee.value)){
               document.getElementsByClassName('embed-video')[0].innerHTML = '<iframe src="https://www.youtube.com/embed/'+videoid(ee.value)+'?autohide=1&amp;rel=0&amp;showinfo=0" allowfullscreen="true" class="video"></iframe>';
               document.getElementById('a_a_video').querySelector('.insert').removeAttribute('disabled');
            } else {
               document.getElementById('a_a_video').querySelector('.insert').setAttribute('disabled','disabled');
               document.getElementsByClassName('embed-video')[0].innerHTML = '';
            }
         }, 1000);
      });

      document.querySelectorAll('#a_a_video .insert')[0].addEventListener('click', function(e){
         e.preventDefault();
         console.log(document.getElementsByClassName('embed-video')[0].innerHTML);
         pasteHtmlAtCaret('<div class="para">'+document.getElementsByClassName('embed-video')[0].innerHTML+'</div>');
      });
      /* END VIDEO AREA */




      /* SELECT CATEGORY AREA*/
      document.getElementById('category-search').addEventListener('click', function(){
         document.getElementsByClassName('category-list')[0].style.display = 'block';
      });
      document.getElementsByClassName('category-list')[0].addEventListener('mouseleave',function(){
         document.getElementsByClassName('category-list')[0].style.display = 'none';
      });
      var li = document.getElementsByClassName('category-list')[0].querySelectorAll('li');
      for (var i = 0; i < li.length; i++) {
         if (typeof window.addEventListener === 'function'){
            (function (index) {
               li[i].addEventListener('click', function(){
                  document.getElementById('category-search').value = index.getAttribute('data-domain');
                  new_line();
               });
            })(li[i]);
         }
      }









   })();//end
