/*
all page..
by: Shah Danial
*/


/* PUBLIC VAR */

var loaded = false;
var social_popup = false;
var limit_post = 7;

/* ON LOAD */
run_js();

function run_js(){
     listen_slider()//listen click slider

     mobile_menu()//listen mobile menu nav

     even_run_js()

     on_scroll()

     listen_popup_button()

}

function listen_popup_button(){
   var button_login = document.getElementsByClassName('social-media')
   var button_search = document.getElementsByClassName('search')
   var search_box = document.getElementById('search_box')
   if(button_login)
   button_login[0].addEventListener('click', function(){
      if(!social_popup) load_social_popup();
   })
   ;
   if(button_search)
   button_search[0].addEventListener('click', function(){
      clicked_popup_button()
      if(!social_popup) load_social_popup();
   })
   ;
   var close_popup_button = document.getElementById('close-login-search')
   if(close_popup_button)
   close_popup_button.addEventListener('click', clicked_close_popup, false)
   ;
}

function load_social_popup(){

   var dalam = "<div class='social_account'><a class='fb-like' data-action='like' data-href='https://www.facebook.com/malayatimes' data-layout='button' data-share='false' data-show-faces='true'></a></div> <div class='social_account'><a class='twitter-follow-button' data-show-count='false' data-show-screen-name='false' data-size='medium' href='https://twitter.com/MalayaTimes'>Follow @malayatimes</a></div> <div class='social_account'><div class='subscribe_youtube'><div class='g-ytsubscribe' data-channelid='UCYPj3cyJIeEf4zhnKqXHJzA' data-layout='default' data-count='hidden'></div></div></div>";

   var table = document.createElement('div')
   table.setAttribute('id', 'socials')
   table.innerHTML = dalam;
   var fans = document.getElementById('fans')
   if(fans) fans.appendChild(table), pisbuk_script(), google_script(), twitter_script(), social_popup = true;
}

function pisbuk_script(){
   !function(e,n,t){var o,c=e.getElementsByTagName(n)[0];e.getElementById(t)||(o=e.createElement(n),o.id=t,o.src="//connect.facebook.net/en_GB/sdk.j"+"s#xfbml=1&version=v2.6&appId=215494712150096",c.parentNode.insertBefore(o,c))}(document,"script","facebook-jssdk");
}

function twitter_script(){
   window.twttr=function(t,e,r){
      var n,i=t.getElementsByTagName(e)[0],w=window.twttr||{};
      return t.getElementById(r)?w:(n=t.createElement(e),
      n.id=r,n.src="https://platform.twitter.com/widgets.j"+"s",
      i.parentNode.insertBefore(n,i),
      w._e=[],
      w.ready=function(t){
         w._e.push(t)
      },
      w)
   }(document,"script","twitter-wjs");
}

function google_script(){
   var head= document.getElementsByTagName('head')[0];
   var script= document.createElement('script');
      script.type= 'text/javascript';
      script.src= 'https://apis.google.com/js/platform.js';
      script.id = 'mygooglescript';
      head.appendChild(script);
}

function clicked_close_popup(){
   if(check_attribute(document.body, 'class', 'menu-2-on'))
    remove_class(document.body, 'menu-2-on')
   ;
}

function clicked_popup_button(){
   if(!check_attribute(document.body, 'class', 'menu-2-on'))
   add_class(document.body, 'menu-2-on')
   ;
}

var timeout_main_event;
function even_run_js(){
     if(window){
          window.addEventListener('resize', function(){
               clearTimeout(timeout_main_event);
               timeout_main_event = setTimeout(function(){
                    console.log(document.body.clientWidth);
                    run_js();
               }, 2000);
          });
     }
}



function reset_popular(popular_box){
     for (var i = 0; i < popular_box.length; i++) {
          popular_box[i].style.left = parseFloat(popular_box['0'].clientWidth) * parseFloat(i) +'px';
     }
     set_titik(popular_box);
}

function set_titik(popular_box){
     var wrap_titik = document.getElementsByClassName('slidecount')[0];
     if(wrap_titik && popular_box){

          var index_active = 'haha';
          for (var i = 0; i < popular_box.length; i++) {
               if(popular_box[i].style.left == '0px'){
                    index_active = i;
               }
          }

          var titik = wrap_titik.getElementsByTagName('span');
          if(titik){
               for (var i = 0; i < titik.length; i++) {
                    titik[i].className = '';
               }
               if(titik[index_active]){
                    titik[index_active].className = 'active';
               }
          }

     }

}

function click_titik(popular_box){
     var wrap_titik = document.getElementsByClassName('slidecount')[0];
     if(wrap_titik && popular_box){
          var titik = wrap_titik.getElementsByTagName('span');
          if(titik){


               for (var i = 0; i < titik.length; i++) {

                    if (typeof window.addEventListener === 'function'){
                         (function (clickedElement) {

                              titik[i].addEventListener('click',function(){
                                   var index = clickedElement.getAttribute('id').replace('count','');
                                   console.log(index);
                                   if(popular_box[index]){
                                        var sebelum = popular_box[index].style.left.replace('px','');
                                        //popular_box[index].style.left = 0;
                                        if(sebelum > 0){
                                             //semua tolak.
                                             var angka = '-'+sebelum;
                                        } else {
                                             //semua tambah
                                             var angka = sebelum;
                                        }
                                        moving_popular(popular_box, angka,index);
                                        set_titik(popular_box);
                                   }
                              });

                         })(titik[i]);
                    }


               }


          }
     }
}

function moving_popular(popular_box, tambah, index){
     for (var ii = 0; ii< popular_box.length; ii++) {
          if(ii == index){
               popular_box[ii].style.left = '0px';
          } else {
               popular_box[ii].style.left = parseFloat(popular_box[ii].style.left.replace('px','')) + parseFloat(tambah) + 'px';
          }
     }
     jom_slide(popular_box);
}

function listen_slider(){

     var left_button = document.getElementsByClassName('slide_left')[0];
     var right_button = document.getElementsByClassName('slide_right')[0];
     var wrap = document.getElementsByClassName('popular-articles')[0];
     if(wrap){
          var popular_box = wrap.getElementsByClassName('popular-article');

          if(popular_box){

               jom_slide(popular_box);

               reset_popular(popular_box);

               click_titik(popular_box);

               if(left_button){
                    left_button.addEventListener('click',function(){
                         //+left
                         popular_to_right(popular_box);
                         jom_slide(popular_box)
                    });
               }
               if(right_button){
                    right_button.addEventListener('click',function(){
                         // - left
                         popular_to_left(popular_box);
                         jom_slide(popular_box)
                    });
               }

          }
     }

}

function popular_to_left(popular_box){
     if(popular_box){

        if(popular_box[last_array(popular_box)]){

           if(popular_box[last_array(popular_box)].style.left.replace('px','') == 0){
               reset_popular(popular_box);
          } else {
               for (var i = 0; i < popular_box.length; i++) {
                    var current_left = popular_box[i].style.left.replace('px','');
                    popular_box[i].style.left = parseFloat(current_left) - parseFloat(popular_box[0].clientWidth) + 'px';
               }
               set_titik(popular_box);
          }

        }

     }

}

function popular_to_right(popular_box){
     if(popular_box){
          for (var i = 0; i < popular_box.length; i++) {
               var current_left = popular_box[i].style.left.replace('px','');
               popular_box[i].style.left = parseFloat(current_left) + parseFloat(popular_box[0].clientWidth) + 'px';
          }
          set_titik(popular_box)
     }
}

function slider_popular(){
     var wrap = document.getElementsByClassName('popular-articles')[0];
     if(wrap){
          var popular_box = wrap.getElementsByClassName('popular-article');
          if(popular_box){
               jom_slide(popular_box)
          }
     }
}

var slide_timer;
function jom_slide(popular_box){
     clearInterval(slide_timer);
     slide_timer = setInterval(function(){
          popular_to_left(popular_box)
     }, 5000)

}

// var catTimeout;
function on_scroll(){
     window.addEventListener('scroll',function(){
          // clearTimeout(catTimeout);
          // catTimeout = setTimeout(function () {
               is_catbox_visible()
          // }, 2000);
     });
}



////////////////// NAV BAR MENU ////////////////////////

function mobile_menu() {
     var button = document.getElementById('mobile-menu-toggle');
     if(button){
          var menu_active = false;
          button.addEventListener('click',function(){
               if( menu_active ){
                    remove_class(document.body, 'menu-active');
                    menu_active = false;
               } else {
                    add_class(document.body, 'menu-active');
                    menu_active = true;
               }
          });
     }
}

////////////////// END NAV BAR MENU ////////////////////////


////////////////////////////// category posts box //////////////////////
function is_catbox_visible(){
   var catPostBox = document.getElementsByClassName('scroll_xhr_content')
   for (var i = 0; i < catPostBox.length; i++) {

      var remove_spinner = false;
      if(i == last_array(catPostBox)) remove_spinner = true
      var before_count = parseFloat(i) - parseFloat(1)
      var before_el_loaded  = false
      if(catPostBox[before_count]){
         if(catPostBox[before_count].getAttribute('data-loaded') == 'true') before_el_loaded = true
      } else {
         before_el_loaded = true
      }

      if(is_visible(catPostBox[i]) && check_attribute(catPostBox[i], 'data-loaded', 'false') && before_el_loaded) catPostBox[i].setAttribute('data-loaded', 'loading'), loadCatPOst(catPostBox[i], remove_spinner, limit_post)

      if(document.body.clientWidth > 1180){ limit_post == 8 ? limit_post = 7 : limit_post = 8; } else { limit_post = 7 }
      if(performance_ad().speed == 'slowest') limit_post = 8;

   }
}

var dah_jumpa = 0;
function loadCatPOst(elem, remove_spinner, limit_post){
   var ids = document.getElementById('posts-id')
   var dataCat = elem.getAttribute('data-category')
   var b_width = document.body.clientWidth || 'false'
   var fromData      = new FormData()
   fromData.append('limit_post', limit_post)
   fromData.append('ids', ids.value)
   fromData.append('category', dataCat)
   fromData.append('speed', performance_ad().speed)
   fromData.append('b_width', b_width)
   var xhr = new XMLHttpRequest();
   xhr.open('POST', 'https://api.malayatimes.com/www/category-box', true)
   xhr.onreadystatechange = function() {
   if (xhr.readyState === 4){

       var data = xhr.responseText;
       if(data && data.length > 0){
          //<h2><a href="https://politik.malayatimes.com" title="Gosip Artis Malaysia">Politik Malaysia</a></h2>
          var heading = document.createElement('h2')
          var heading_a = document.createElement('a')
          heading_a.setAttribute('href', 'https://'+dataCat+'.malayatimes.com')
          heading_a.textContent = elem.getAttribute('data-heading')
          heading.appendChild(heading_a)
          elem.appendChild(heading)

          var content_latest = document.createElement('div')
          content_latest.className = 'content latest';
          content_latest.innerHTML = data;

          elem.appendChild(content_latest)
          if(limit_post == 7){
            dah_jumpa = 0;
            var slotName = generateNextSlotName();

            var aside = document.createElement('aside')
            aside.className = 'iklan odd big';

            var content_group = document.createElement('div')
            content_group.className = 'content_group_inner_wrapper';
            content_group.innerHTML = '<div class="content-item"><div class="premium" id="'+slotName+'"></div></div>';

            aside.appendChild(content_group)

            content_latest.appendChild(aside)
          }

          elem.appendChild(content_latest)

          if(limit_post == 7){
            while(dah_jumpa < 1){
             if(document.getElementById(slotName)) create_ad(slotName), dah_jumpa = 1;
            }
          }
       }

       elem.setAttribute('data-loaded', 'true')

       if(remove_spinner){
          var temp_spinner = document.getElementById('loading-spiner')
          if(temp_spinner) temp_spinner.parentNode.removeChild(temp_spinner)
       }

   }
   is_catbox_visible()
   }
   xhr.send(fromData);
}

var nextSlotId = 1;
 function generateNextSlotName() {
   var id = nextSlotId++;
   var my_slot_id = 'adslot-' + id;
   while(document.getElementById(my_slot_id)){
      id = id++;
      my_slot_id = 'adslot-' + id;
   }
   return my_slot_id;
 }

 function create_ad(slotName){
    googletag.cmd.push(function() {

      var infi_ad = googletag.sizeMapping().
      addSize([300, 0], [ [300, 250], [250, 250], [200, 200]]).
      addSize([0, 0], [[200, 200], [250, 250]]).
      build();

     var slot = googletag.defineSlot('/21705193160/sdbr', [
        [300, 250],
        [250, 250],
        [200, 200]
     ], slotName)
     .defineSizeMapping(infi_ad)
     .addService(googletag.pubads());

     //googletag.pubads().disableInitialLoad();
     // googletag.pubads().enableAsyncRendering();
     // googletag.enableServices();

     // Display has to be called before
     // refresh and after the slot div is in the page.
     googletag.display(slotName);
     googletag.pubads().refresh([slot]);
   });
}

function speedy_up(){
   var speedo = performance_ad().speed;
   if(typeof googletag == 'function'){if(typeof googletag.destroySlots() != 'undefined')googletag.destroySlots()}
   var nak_kecik = performance_ad().must_low_box;
   if(typeof document.body.clientWidth != 'undefined'){
      var bdy = document.getElementById('main-blog')
      var pp = document.getElementsByClassName('popular-articles');
      if(document.body.clientWidth < 1181){
         if(bdy){
            if(pp.length>0){
               if(pp[0].parentNode){
                  pp[0].parentNode.removeChild(pp[0])
                  document.getElementById('Blog1').setAttribute('style','padding-top:15px')
               }
            }
         }
         if(document.body. clientWidth < 612) var nak_kecik = true;
      } else {
         if(bdy){

            var plr = document.getElementsByClassName('popular-article')

               for (var i = 0; i < plr.length; i++) {

                     var a_img = plr[i].getElementsByClassName('img')[0]
                     if(a_img){

                        var img = a_img.getElementsByTagName('img')[0]

                        var teruskan = true;
                        if(img){
                           if(img.complete) var teruskan = false;
                        }

                        if(!check_attribute(a_img, 'class', speedo) && teruskan){

                           if(a_img.getAttribute('style')){

                              if(speedo == 'slow'){
                                 var mmm = a_img.getAttribute('style').replace(/\/gambar\/|\/foto\//g,'/image/')
                                 a_img.setAttribute('style', mmm)
                              }
                              if(speedo == 'slowest'){
                                 var  mmm = a_img.getAttribute('style').replace(/\/gambar\/|\/image\/|\/foto\//g,'/picker/')
                                 a_img.setAttribute('style', mmm)
                              }

                              if(img){if(img.parentNode) img.parentNode.removeChild(img)}

                           }

                           add_class(a_img, speedo)

                        } // if speedo not exist

                     }// a_img

               } //for

         }//bdy
      }//else width

   }//undefined
   var w_img = document.getElementsByClassName('img');
   for (var i = 0; i < w_img.length; i++) {
      if(!check_attribute(w_img[i], 'class', speedo)){
         var img = w_img[i].getElementsByTagName('img')[0]
         if(img){if(!img.complete){
            var img_src = img.getAttribute('src')
            if(img_src){
               var my_wrapper = get_wrapper(w_img[i], 'class', 'wrapper');
               var my_big = get_wrapper(w_img[i], 'class', 'big');
               var my_link = w_img[i].getAttribute('href')
               var text_content = img.getAttribute('alt')
               if(my_wrapper && my_big && my_link && text_content){

                  var src_img = img_src.replace('/picture/','/picker/').replace('/photo/','/picker/');

                  if(nak_kecik){
                     var satu = document.createElement('div')
                     satu.className = 'recent-articles even';

                        var dua = document.createElement('article')
                        dua.className = 'hentry recent-article'
                        dua.innerHTML = '<a style="background: url('+src_img+') no-repeat" class="img '+speedo+'" href="'+my_link+'">'+text_content.trim()+'</a><h3 class="entry-title"><a class="title" href='+my_link+'">'+text_content.trim()+'</a></h3>';
                        satu.appendChild(dua)

                     if(my_wrapper.parentNode) my_wrapper.parentNode.insertBefore(satu, my_wrapper)
                     if(my_big.parentNode) my_big.parentNode.removeChild(my_big)

                  } else {
                     var src_img = img_src.replace('/photo/','/picker/')
                     w_img[i].setAttribute('style','background:url('+src_img+') no-repeat')
                     if(img.parentNode) img.parentNode.removeChild(img)
                  }

               } else {
                  var src_img = img_src.replace('/picture/','/picker/')
                  w_img[i].setAttribute('style','background:url('+src_img+') no-repeat')
                  if(img.parentNode) img.parentNode.removeChild(img)
               }
            }
         }}
         add_class(w_img[i], speedo)
      }
   }
   if(typeof document.body.clientWidth != 'undefined'){
      if(document.body.clientWidth<612){
         var ikke = document.getElementsByClassName('iklan')
         for (var i = 0; i < ikke.length; i++) {
             if(ikke[i]){ if(ikke[i].parentNode) ikke[i].parentNode.removeChild(ikke[i]) }
         }
      }
   }
}
