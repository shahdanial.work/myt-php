/*
all page..
by: Shah Danial
*/


/* PUBLIC VAR */

var offset_post = 10;
var loaded = false;
var social_popup = false;

/* ON LOAD */
run_js();

function run_js(){

     //slider_popular();//slider on
     listen_slider();//listen click slider

     mobile_menu();//listen mobile menu nav

     even_run_js();

     var related_box = document.getElementById('related')
     if(related_box)
     related_post() // single post related post..
     ;

     share_button();

     on_scroll();

     listen_popup_button();

     listen_search();

}

function listen_popup_button(){
   var button_login = document.getElementsByClassName('social-media')
   var button_search = document.getElementsByClassName('search')
   var search_box = document.getElementById('search_box')
   if(button_login)
   button_login[0].addEventListener('click', function(){
      clicked_popup_button()
      if(search_box) placeCaretAtEnd(search_box, false);
      if(!social_popup) load_social_popup();
   })
   ;
   if(button_search)
   button_search[0].addEventListener('click', function(){
      clicked_popup_button()
      if(search_box) placeCaretAtEnd(search_box, false);
      if(!social_popup) load_social_popup();
   })
   ;
   var close_popup_button = document.getElementById('close-login-search')
   if(close_popup_button)
   close_popup_button.addEventListener('click', clicked_close_popup, false)
   ;
}

function load_social_popup(){

   var dalam = "<div class='social_account'><a class='fb-like' data-action='like' data-href='https://www.facebook.com/malayatimes' data-layout='button' data-share='false' data-show-faces='true'></a></div> <div class='social_account'><a class='twitter-follow-button' data-show-count='false' data-show-screen-name='false' data-size='medium' href='https://twitter.com/MalayaTimes'>Follow @malayatimes</a></div> <div class='social_account'><div class='subscribe_youtube'><div class='g-ytsubscribe' data-channelid='UCYPj3cyJIeEf4zhnKqXHJzA' data-layout='default' data-count='hidden'></div></div></div>";

   var table = document.createElement('div')
   table.setAttribute('id', 'socials')
   table.innerHTML = dalam;
   var fans = document.getElementById('fans')
   if(fans) fans.appendChild(table), pisbuk_script(), google_script(), twitter_script(), social_popup = true;
}

function pisbuk_script(){
   !function(e,n,t){var o,c=e.getElementsByTagName(n)[0];e.getElementById(t)||(o=e.createElement(n),o.id=t,o.src="//connect.facebook.net/en_GB/sdk.j"+"s#xfbml=1&version=v2.6&appId=215494712150096",c.parentNode.insertBefore(o,c))}(document,"script","facebook-jssdk");
}

function twitter_script(){
   window.twttr=function(t,e,r){
      var n,i=t.getElementsByTagName(e)[0],w=window.twttr||{};
      return t.getElementById(r)?w:(n=t.createElement(e),
      n.id=r,n.src="https://platform.twitter.com/widgets.j"+"s",
      i.parentNode.insertBefore(n,i),
      w._e=[],
      w.ready=function(t){
         w._e.push(t)
      },
      w)
   }(document,"script","twitter-wjs");
}

function google_script(){
   var head= document.getElementsByTagName('head')[0];
   var script= document.createElement('script');
      script.type= 'text/javascript';
      script.src= 'https://apis.google.com/js/platform.js';
      script.id = 'mygooglescript';
      head.appendChild(script);
}

function clicked_close_popup(){
   if(check_attribute(document.body, 'class', 'menu-2-on'))
    remove_class(document.body, 'menu-2-on')
   ;
}

function clicked_popup_button(){
   if(!check_attribute(document.body, 'class', 'menu-2-on'))
   add_class(document.body, 'menu-2-on')
   ;
}

var timeout_main_event;
function even_run_js(){
     if(window){
          window.addEventListener('resize', function(){
               clearTimeout(timeout_main_event);
               timeout_main_event = setTimeout(function(){
                    console.log(document.body.clientWidth);
                    run_js();
               }, 2000);
          });
     }
}



function reset_popular(popular_box){
     for (var i = 0; i < popular_box.length; i++) {
          popular_box[i].style.left = parseFloat(popular_box['0'].clientWidth) * parseFloat(i) +'px';
     }
     set_titik(popular_box);
}

function set_titik(popular_box){
     var wrap_titik = document.getElementsByClassName('slidecount')[0];
     if(wrap_titik && popular_box){

          var index_active = 'haha';
          for (var i = 0; i < popular_box.length; i++) {
               if(popular_box[i].style.left == '0px'){
                    index_active = i;
               }
          }

          var titik = wrap_titik.getElementsByTagName('span');
          if(titik){
               for (var i = 0; i < titik.length; i++) {
                    titik[i].className = '';
               }
               if(titik[index_active]){
                    titik[index_active].className = 'active';
               }
          }

     }

}

function click_titik(popular_box){
     var wrap_titik = document.getElementsByClassName('slidecount')[0];
     if(wrap_titik && popular_box){
          var titik = wrap_titik.getElementsByTagName('span');
          if(titik){


               for (var i = 0; i < titik.length; i++) {

                    if (typeof window.addEventListener === 'function'){
                         (function (clickedElement) {

                              titik[i].addEventListener('click',function(){
                                   var index = clickedElement.getAttribute('id').replace('count','');
                                   console.log(index);
                                   if(popular_box[index]){
                                        var sebelum = popular_box[index].style.left.replace('px','');
                                        //popular_box[index].style.left = 0;
                                        if(sebelum > 0){
                                             //semua tolak.
                                             var angka = '-'+sebelum;
                                        } else {
                                             //semua tambah
                                             var angka = sebelum;
                                        }
                                        moving_popular(popular_box, angka,index);
                                        set_titik(popular_box);
                                   }
                              });

                         })(titik[i]);
                    }


               }


          }
     }
}

function moving_popular(popular_box, tambah, index){
     for (var ii = 0; ii< popular_box.length; ii++) {
          if(ii == index){
               popular_box[ii].style.left = '0px';
          } else {
               popular_box[ii].style.left = parseFloat(popular_box[ii].style.left.replace('px','')) + parseFloat(tambah) + 'px';
          }
     }
     jom_slide(popular_box);
}

function listen_slider(){

     var left_button = document.getElementsByClassName('slide_left')[0];
     var right_button = document.getElementsByClassName('slide_right')[0];
     var wrap = document.getElementsByClassName('popular-articles')[0];
     if(wrap){
          var popular_box = wrap.getElementsByClassName('popular-article');

          if(popular_box){

               jom_slide(popular_box);

               reset_popular(popular_box);

               click_titik(popular_box);

               if(left_button){
                    left_button.addEventListener('click',function(){
                         //+left
                         popular_to_right(popular_box);
                         jom_slide(popular_box)
                    });
               }
               if(right_button){
                    right_button.addEventListener('click',function(){
                         // - left
                         popular_to_left(popular_box);
                         jom_slide(popular_box)
                    });
               }

          }
     }

}

function popular_to_left(popular_box){
     if(popular_box){

        if(popular_box[last_array(popular_box)]){

           if(popular_box[last_array(popular_box)].style.left.replace('px','') == 0){
               reset_popular(popular_box);
          } else {
               for (var i = 0; i < popular_box.length; i++) {
                    var current_left = popular_box[i].style.left.replace('px','');
                    popular_box[i].style.left = parseFloat(current_left) - parseFloat(popular_box[0].clientWidth) + 'px';
               }
               set_titik(popular_box);
          }

        }

     }

}

function popular_to_right(popular_box){
     if(popular_box){
          for (var i = 0; i < popular_box.length; i++) {
               var current_left = popular_box[i].style.left.replace('px','');
               popular_box[i].style.left = parseFloat(current_left) + parseFloat(popular_box[0].clientWidth) + 'px';
          }
          set_titik(popular_box);
     }
}

function slider_popular(){
     var wrap = document.getElementsByClassName('popular-articles')[0];
     if(wrap){
          var popular_box = wrap.getElementsByClassName('popular-article');
          if(popular_box){
               jom_slide(popular_box);
          }
     }
}

var slide_timer;
function jom_slide(popular_box){
     clearInterval(slide_timer);
     slide_timer = setInterval(function(){
          popular_to_left(popular_box);
     }, 5000);

}

function on_scroll(){
     window.addEventListener('scroll',function(){
          load_post();
     });
}

function load_post(){
     var load_more = document.getElementById('load-more');
     if(load_more){
          if(is_visible(load_more) && !loaded){
               offset_post += 8;
               loaded = true;
               xhr_load_post(load_more);
          }
     }
}

function xhr_load_post(load_more){
     var fromData      = new FormData();
     fromData.append('offset', offset_post);
     fromData.append('category', window.location.hostname);
     var xhr = new XMLHttpRequest();
     xhr_open('POST', 'https://media.malayatimes.com/get-new-post');
      xhr.onreadystatechange = function() {
         if (xhr.readyState === 4){
              // if(xhr.status === 200){
                   var data = xhr.responseText;
                   if(data && data.length > 0){
                      var section = document.createElement('section');
                      section.className = 'wrapper rows';
                      section.innerHTML = data;
                      load_more.parentNode.insertBefore(section, load_more);
                      loaded = false;
                   } else {
                        var data = xhr.responseText;
                        if(data){console.log(data)}
                        var p = document.createElement('p');
                        p.className = 'load-more-message end';
                        p.innerHTML = 'YOU REACH THE END <span class="red">❤</span>';
                        load_more.parentNode.insertBefore(p, load_more);
                        var myload_more = document.getElementById('load-more')
                        if(myload_more) load_more.parentNode.removeChild(myload_more)
                   }
         }
      }
      xhr.send(fromData);
}








////////////////// NAV BAR MENU ////////////////////////

function mobile_menu() {
     var button = document.getElementById('mobile-menu-toggle');
     if(button){
          var menu_active = false;
          button.addEventListener('click',function(){
               if( menu_active ){
                    remove_class(document.body, 'menu-active');
                    menu_active = false;
               } else {
                    add_class(document.body, 'menu-active');
                    menu_active = true;
               }
          });
     }
}

////////////////// END NAV BAR MENU ////////////////////////


///////////////////////// SHARE BUTTON AREA //////////////////
function share_button(){
     var fb_share = document.getElementById('share-facebook');
     if(fb_share){
          fb_share.addEventListener('click', function(){
               popup_fb_share();
               //reputation_share();
               // console.log(this.getAttribute('data-href'));
          });
     }
}

// fb popup_fb_share
function popup_fb_share(){
     // mari popup fb share..
}
///////////////////////// END SHARE BUTTON AREA //////////////////





///////////////////////////////////////  RELATED SINGLE POST AREA /////////////////////////////////
function related_post(){
   var related_box = document.getElementById('related')
   if(related_box)
      var right_related = related_box.getElementsByClassName('slide_right')
      // after_slide_related() // reset button
      if(right_related)
           right_related[0].addEventListener('click', function(){
                related_to_left()
                // after_slide_related()
           })
      ;

      var left_related = related_box.getElementsByClassName('slide_left')
      if(left_related)
           left_related[0].addEventListener('click', function(){
                related_to_right()
                // after_slide_related()
           })
      ;
   ;
}

// function after_slide_related(){
    //  var right_related = document.getElementsByClassName('slide_right')[0];
    //  var left_related = document.getElementsByClassName('slide_left')[0];
    //  var  related_post = document.getElementsByClassName('related_post');
    // if(related_post){ if(related_post[0]){
    //      left_related.style.display = 'block';
    //      right_related.style.display = 'block';
    //      if(related_post[0].style.left.trim().toLowerCase() == '0px'){
    //          left_related.style.display = 'none';
    //      }
    //      if(related_post[last_array(related_post)]){
    //          if(related_post[last_array(related_post)].style.left.trim().toLowerCase() == '0px'){
    //             /* RELATED LAST ITEM is appeared */
    //
    //               //#1 display none right button..
    //               right_related.style.display = 'none';
    //
    //          }
    //      }
    // } }
// }



function related_to_right(){
     var  related_post = document.getElementsByClassName('related_post');
     var left_related = document.getElementsByClassName('slide_left')[0];
     if(related_post){

          if(related_post[0].style.left.toLowerCase().trim() != '0px'){

              for (var i = 0; i < related_post.length; i++) {
                   var current_left = related_post[i].style.left.replace('px','');
                   var new_left = parseFloat(current_left) + parseFloat(305);
                   related_post[i].style.left = new_left + 'px';
              }

         }  else {

              related_to_left();

         }

     }
}

function related_to_left(){
     var  related_post = document.getElementsByClassName('related_post');
     var right_related = document.getElementsByClassName('slide_right')[0];
     if(related_post){
          if(related_post[last_array(related_post)]){

               if(related_post[last_array(related_post)].style.left.trim().toLowerCase() != '0px'){

                    for (var i = 0; i < related_post.length; i++) {
                         var current_left = related_post[i].style.left.replace('px','');
                         var new_left = parseFloat(current_left) - parseFloat(305);
                         related_post[i].style.left = new_left + 'px';
                    }

               } else {

                    related_to_right();

               }

          }
     }
}
/////////////////////////////////////// END RELATED SINGLE POST AREA ////////////////////////////////



/////////////////////////////// CARIAN OR SEARCH ///////////////////////
function listen_search(){
   var button = document.getElementById('search-button')
   var clear = document.getElementById('close-search-result')
   if(button) button.addEventListener('click', mula_cari, false);
   if(clear) clear.addEventListener('click', clear_search, false);

}

function mula_cari(){

   var search_box = document.getElementById('search_box'),
       the_value = false;

   //1. clear search-result.
   clear_search();

   //2. proceed carian
   if(search_box.textContent){
      the_value = search_box.textContent.trim();
      if(the_value.length > 0) get_search(the_value);
   }

}

function clear_search(){
   var search_result = document.getElementById('search-result');
   var the_list = false;
   if(search_result) var the_list = search_result.getElementsByTagName('a');
   console.log(the_list.length)
   while (the_list[0]) {
      the_list[0].parentNode.removeChild(the_list[0])
      the_list = search_result.getElementsByTagName('a')
   }
   search_result.style.display = 'none';
   console.log('nak clear?')
}

function get_search(text){
         var box = document.getElementById('search-result')
         var search_box = document.getElementById('search_box')
         if(box) box.style.display = 'block';
         if(box && search_box) box.style.width = search_box.clientWidth+'px';
         var clear = document.getElementById('close-search-result')
         if(clear) clear.style.display = 'none';
         var std = document.getElementById('stanby')
         if(std) std.style.display = 'block';


          var xhr      = new XMLHttpRequest();
          xhr_open('POST', 'https://media.malayatimes.com/search?q='+ encodeURI(text));
          xhr.onreadystatechange = function() {
               if (xhr.readyState === 4 && xhr.status === 200){
                    var data = xhr.responseText;
                    if(data && data.length > 0 && data != '0'){

                         console.log('berjaya')
                         search_result(data);
                         if(clear) clear.style.display = 'block';
                         if(std) std.style.display = 'none';

                    } else {

                       if(box){
                          var p = document.createElement('a')
                          p.textContent = 'no result..';
                          p.setAttribute('href', '#')
                          box.appendChild(p)
                       }
                       if(clear) clear.style.display = 'block';
                       if(std) std.style.display = 'none';

                    }
               }
          }

          xhr.send();
}

function search_result(data){

   var data = JSON.parse(data);
   var search_result = document.getElementById('search-result');

   for (var i = 0; i < data.length; i++) {
      var myData = data[i];
      var box = document.createElement('a')
      if(myData.from_tbl == 'posts'){

         box.setAttribute('href', 'https://'+myData.type+'/'+myData.link)

      }else if(myData.from_tbl == 'tags'){

         box.setAttribute('href', 'https://hub.malayatimes.com/'+myData.type+'/'+myData.link)

      }else if(myData.from_tbl == 'categories'){

         box.setAttribute('href', 'https://'+myData.link)

      }

      box.className = 'the_'+myData.from_tbl;

      if(myData.title) {
         var myText = myData.title;
      } else if(myData.alt_title){
         var myText = myData.alt_title;
      }
      box.textContent = myText;

      if(search_result) search_result.appendChild(box), search_result.style.display = 'block', search_result.style.width = search_box.clientWidth+'px';
   }
}
/////////////////////////////// CARIAN OR SEARCH ///////////////////////
