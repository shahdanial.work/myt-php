(function() {


   /*VARIABLE*/
   var
   editor = document.getElementById("post-editor"),
   preview = document.getElementById("post-preview"),
   previewBr = preview.getElementsByTagName('br'),
   previewPara = preview.getElementsByClassName('para'),
   editorPara = editor.getElementsByClassName('para'),
   elementPreview = preview.getElementsByTagName("*"),
   elementEditor = editor.getElementsByTagName("*"),
   paraPara = editor.getElementsByClassName('para'),
   cursorKau =''
   ;

   //onload
   // check_last_para();
   // check_first_para();
   after_html_changed();

   // EDITOR CARET.
   var selRange;
   function saveSelection() {
      //selRange = null;
      if (window.getSelection) {
         sel = window.getSelection();
         if (sel.getRangeAt && sel.rangeCount) {
            var final = sel.getRangeAt(0);
         }
      } else if (document.selection && document.selection.createRange) {
         var final = document.selection.createRange();
      } else {
         var final = null;
      }
      console.log(final);
      return final;
   }

   function restoreSelection(range) {
      if (range) {
         if (window.getSelection) {
            sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
         } else if (document.selection && range.select) {
            range.select();
         }
      }
   }


   function pasteHtmlAtCaret(html, what) {
      //put back cursor before insert.
      restoreSelection(selRange);
      var sel, range;
      if (window.getSelection) {
         // IE9 and non-IE
         sel = window.getSelection();
         if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();

            // Range.createContextualFragment() would be useful here but is
            // non-standard and not supported in all browsers (IE9, for one)
            var el = document.createElement("div");
            el.innerHTML = html;
            var frag = document.createDocumentFragment(), node, lastNode;
            while ( (node = el.firstChild) ) {
               lastNode = frag.appendChild(node);
            }
            range.insertNode(frag);

            // Preserve the selection
            if (lastNode) {
               range = range.cloneRange();
               range.setStartAfter(lastNode);
               range.collapse(true);
               sel.removeAllRanges();
               sel.addRange(range);
            }
         }
      } else if (document.selection && document.selection.type != "Control") {
         // IE < 9
         document.selection.createRange().pasteHTML(html);
      }
      ////placeCaretAtEnd( document.getElementById("post-editor") );
      if(what == 'enter'){
         console.log('placeCaretAtEnd as enter');
         after_enter();
         no_paraPara();
      } else {
         console.log('placeCaretAtEnd as insert');
         after_insert();
         remove_empty_para();
      }
      content_preview();
   }

   function placeCaretAtEnd(el, dimana) { // dimana: true = atStart, false = atEnd;
      document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
      if (typeof window.getSelection != "undefined"
      && typeof document.createRange != "undefined") {
         var range = document.createRange();
         range.selectNodeContents(el);
         range.collapse(dimana);
         var sel = window.getSelection();
         sel.removeAllRanges();
         sel.addRange(range);
      } else if (typeof document.body.createTextRange != "undefined") {
         var textRange = document.body.createTextRange();
         textRange.moveToElementText(el);
         textRange.collapse(dimana);
         textRange.select();
      }
      document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
      selrange = saveSelection();
   }

   function GetSelectedText() { //to call GetSelectedText(selRange);
      if (window.getSelection) {  // all browsers, except IE before version 9
         var range = window.getSelection ();
         return range.toString();
      } else if (document.selection.createRange) { // Internet Explorer
         var range = document.selection.createRange ();
         return range.text;
      } else {
         return 'false';
      }

   }



   /* TITLE AREA */
   var titleTimeout;
   document.getElementById('post-title').addEventListener('input', function(){
      if(this.value.length > 0){
         document.getElementsByClassName('entry-title')[0].innerHTML = this.value.trim();
         document.getElementById('title_draft').value = this.value.trim();

         clearTimeout(titleTimeout);
         titleTimeout = setTimeout(function () {
            save_draft('title');
         }, 10000);


      } else {
         document.getElementsByClassName('entry-title')[0].innerHTML = 'Tajuk yang baik, adalah tepat kepada keyword carian Popular';
         document.getElementById('title_draft').value = null;
      }
   });

   /* END TITLE AREA */




   /* EVEN LISTENER */
   //EDITOR.
   if(editor){
      editor.addEventListener('input', content_preview, false);
      editor.addEventListener ('paste', paste_editor, false);
      editor.addEventListener('keydown', function(e){
         // apa jua perbuatan. save range dulu..
         selrange = saveSelection();
         if (e.keyCode === 13) { // once enter..
            console.log('class is below:');
            console.log(document.activeElement.className);
            document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
            selrange = saveSelection();//must save it back
            console.log('ONCE ENTER');
            e.preventDefault();
            var theKlas = document.activeElement.className;
            if(theKlas){
               var theKlas = ' ' + theKlas + ' ';
            }
            /*
            if(theKlas.indexOf(" para ") > -1 ){ // active element is para contenteditable..
            var div = document.createElement('div');
            div.innerHTML = document.activeElement.outerHTML;
            if(div.firstChild.innerHTML.length > 0){
            remove_empty_para();
            // build new empty para.
            pasteHtmlAtCaret('<!--PEMISAH-->','enter');
         } //else wil not do noting, not create new para..
      } else {// posible active element is <div class="message"> or  <blockquote> or inside table widget...'
      console.log('posible active element is <div class="message"> or  <blockquote> or inside table widget...');
      if(theKlas.indexOf(" message ") > -1 || document.activeElement.tagName.toLowerCase() == 'blockquote'){
      pasteHtmlAtCaret('BRMESEJ', 'enter');
   }
} */
if(theKlas.indexOf(" message ") > -1 || document.activeElement.tagName.toLowerCase() == 'blockquote'){
   console.log('enter ni nak letak <br class="newline" sbb dia enter waktu dalam blockquote or div.message..');
   pasteHtmlAtCaret('BRMESEJ', 'enter');
} else if(theKlas.indexOf(" para ") > -1){
   console.log('enter ni di dalam para contenteditable so kami akan tambah contenteditable');
   // enter ni di dalam para contenteditable..
   var div = document.createElement('div');
   div.innerHTML = document.activeElement.outerHTML;
   if(div.firstChild.innerHTML.length > 0){
      remove_empty_para();
      // build new empty para.
      pasteHtmlAtCaret('<!--PEMISAH-->','enter');
   } //else wil not do noting, not create new para..
}
}
if (e.keyCode === 8) {// once backspace..
   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
   console.log('%c BACKSPACE INSIDE EDITOR!', 'color:orange');
   console.log(document.activeElement);
   console.log('berapa para = '+editorPara.length);
   var index = Array.prototype.slice.call(editorPara).indexOf(document.activeElement);
   var index = parseFloat(index);
   var paraClass = document.activeElement.className.replace('kiamatlab', '').trim();
   if(document.activeElement.textContent.length < 2 && // = 1 kebawah..
      paraClass == 'para' &&
      document.activeElement.getAttribute('contenteditable') == 'true' &&
      editorPara.length > 1){
         var elem = document.activeElement;
         var wrapper_dia = elem.parentNode;
         wrapper_dia.removeChild(elem);
         document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
         console.log('NI INDEX');
         console.log(index);
         if(index > 0){
            //remove para 2, got to end para 1
            placeCaretAtEnd(editorPara[index - parseFloat(1)], false);
         /*} else if(check_attribute(document.activeElement, 'class', 'message')){
            console.log('%cbackspace on message', 'color:orange');
            console.log(document.activeElement.textContent.length);*/
         } else {
            // remove para 1, go to start para 1.
            placeCaretAtEnd(editorPara[index], false);
         }
      }

   } // end if bakcspace..
   format_editor();
   content_preview();
});
}

function check_attribute(a, attr, valu){
   var vall = ' '+ valu +' ';
   if(a.nodeType === 1 && a.getAttribute(attr)){
      var get_attr = ' '+ a.getAttribute(attr) +' ';
      if(get_attr.indexOf(vall) > -1){
         return a.getAttribute(attr);
      } else {
         return false;
      }
   } else {
      return false;
   }
}

function get_wrapper(a, attr, valu){
   /*get_wrapper(a, 'class', 'wrapper22')
   if(get_wrapper(a, 'class', 'wrapper22').getAttribute('id') ){//if this attribute is available.
   #ex to use it: get_wrapper(a, 'class', 'wrapper22') .getAttribute('id');
} else {
#do something
}
*/
var vall = ' '+ valu +' ';
var c = false;
var w = a;

while (a && !c){
   if(a != w){
      if(a.nodeType === 1 && a.getAttribute(attr)){
         var get_attr = ' '+ a.getAttribute(attr) +' ';
         if(get_attr.indexOf(vall) > -1){
            return a;
            c = true; // stop while.
         }
      }
   }
   a = a.parentNode;
}
if(!c){// jika element takde.
   return false;
}

}

// EDITOR PARA CLICKED..
/*
if(editorPara){
for (var i = 0; i < editorPara.length; i++) {
if (typeof window.addEventListener === 'function'){
(function (index) {
editorPara[i].addEventListener('click',function(){
var theClass = ' '+ index.getAttribute('class') + ' ';
if( index.getAttribute('contenteditable')  == 'true' &&  theClass.indexOf(" para ") > -1 ){
selRange = saveSelection();
} else {// element non editable para clicked...
selRange = null;

document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';

if( index.getElementsByTagName('blockquote')[0] != undefined || index.getElementsByClassName('message')[0] != undefined || index.getElementsByClassName('modal')[0] != undefined ){
console.log('we will do nothing..');
} else {
//popup. ask user what to do. 1. delete this element. 2. edit this element. add paragraph before/after.
// SINGLE IMAGE CICKED
if(index.getElementsByClassName('single_image')){
single_image_clicked(index);
}
}

}
})
})(editorPara[i]);
}
}
}
*/
listen_click_para();
function listen_click_para(){

   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';

   if(editorPara){
      for (var i = 0; i < editorPara.length; i++) {
         if (typeof window.addEventListener === 'function'){
            (function (index) {

               editorPara[i].addEventListener('click',function(e){

                  var theClass = ' '+ index.getAttribute('class') + ' ';

                  if( index.getAttribute('contenteditable')  == 'true' && theClass.indexOf(" para ") > -1 ){
                     //klik para..
                     selRange = saveSelection();
                     console.log('%cKo click para editable, saveSelection()!!..', 'color:green;');

                  } else if( index.getAttribute('contenteditable')  != 'true' && index.getElementsByTagName('blockquote').length > 0 ||  index.getElementsByClassName('message').length > 0 ){

                     console.log('%cKo click blockqoute atau message,  saveSelection()!!..', 'color:green;');
                     selRange = saveSelection();

                  } else {// element non editable para clicked...

                     console.log('%cKo klik bukan para dan bukan message or blockquote, posible gambar, video  : tak buat apa2 pada selrange..', 'color:green;');
                     // selRange = null; jangan buh null biarkan selRange lama.

                     document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';

                     var myPopup = '<div class="modal" unselectable="on" style="display:block;"><div class="popup"><section class="popup-content" style="display:block"><div class="popup-header"><h4>Choose an action</h4><a class="popup-close">×</a></div><!--end header--><div class="popup-body"><div class="inner-wrapper" style="text-align:center;"><span class="button" id="para-before"><i aria-hidden="true" class="fa fa-arrow-up"></i> Add paragraph</span></div><div class="inner-wrapper" style="text-align:center;"><span class="button" id="remove-this"><i aria-hidden="true" class="fa fa-remove"></i> Remove element</span></div><div class="inner-wrapper" style="text-align:center;"><span class="button" id="para-after"><i aria-hidden="true" class="fa fa-arrow-down"></i> Add paragraph</span></div></div></section></div></div>';

                     if(index.getElementsByClassName('single_image').length > 0){

                        e.preventDefault();
                        console.log('%cClick gambar', 'color:orange');
                        single_image_clicked(index, myPopup);
                        console.log(index.getElementsByClassName('modal').length);

                     } else if(index.getElementsByClassName('gallery').length > 0){

                        e.preventDefault();
                        console.log('%cClick gallery', 'color:orange');
                        if( (document.activeElement) && check_attribute(document.activeElement, 'class', 'gallery_image') || get_wrapper(document.activeElement, 'class', 'gallery_image') ){
                           console.log('gallery image clicked hehe');
                           console.log(document.activeElement);
                           var myPopup = '<div class="modal" unselectable="on" style="display:block;"><div class="popup"><section class="popup-content" style="display:block"><div class="popup-header"><h4>Choose an action</h4><a class="popup-close">×</a></div><!--end header--><div class="popup-body"><div class="inner-wrapper" style="text-align:center;"><span class="button" id="para-before"><i aria-hidden="true" class="fa fa-arrow-up"></i> Add paragraph</span></div><div class="inner-wrapper" style="text-align:center;"><span class="button" id="this-remove"><i aria-hidden="true" class="fa fa-remove"></i> Remove image</span></div><div class="inner-wrapper" style="text-align:center;"><span class="button" id="remove-this"><i aria-hidden="true" class="fa fa-remove"></i> Remove gallery</span></div><div class="inner-wrapper" style="text-align:center;"><span class="button" id="para-after"><i aria-hidden="true" class="fa fa-arrow-down"></i> Add paragraph</span></div></div></section></div></div>';
                           gallery_image_clicked(document.activeElement, index, myPopup);

                        } else {
                           console.log('nak remove my popup ni?');
                           console.log(index);
                           //gallery_clicked(index, myPopup);
                        }
                     } else if(index.getElementsByClassName('video').length > 0){

                        console.log('%cClick video', 'color:orange');
                        e.preventDefault();
                        video_clicked(index,myPopup);

                     } else if(index.getElementsByClassName('related').length > 0){

                        console.log('%cClick Related', 'color:orange');
                        e.preventDefault();
                        related_clicked(index,myPopup);

                     } else if(index.getElementsByClassName('content-table').length > 0){

                        console.log('%cClick Table', 'color:orange');
                        if(e.target.getAttribute('contenteditable') != 'true'){
                           object_clicked(index,myPopup, index.getElementsByClassName('content-table')[0]);
                        }

                     }

                     document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';

                  }

               }); // addEventListener

            })(editorPara[i]);
         }
      }

   }

}

function object_clicked(index, myPopup, target){
   var sebelum = index.innerHTML;
   var kembali = index.outerHTML + '';
   //var target = index.getElementsByClassName('related')[0];
   index.innerHTML = myPopup + target.outerHTML;
   selepas = myPopup + target.outerHTML;

   /* WAIT UNTIL START */
   var waitUntil = function (fn, condition, interval) {
      interval = interval || 100;
      var shell = function () {
         var timer = setInterval(
            function () {
               var check;

               try { check = !!(condition()); } catch (e) { check = false; }

               if (check) {
                  clearInterval(timer);
                  delete timer;
                  fn();
               }
            },
            interval
         );
      };
      return shell;
   };
   waitUntil(
      function(){
         //-----
         document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
         if(editor){

            var popupClose = editor.getElementsByClassName('popup-close')[0];
            if(popupClose){
               popupClose.addEventListener('click', function(){
                  click_close_popup_para(index, kembali);
               });
            }
            var para_before_button = document.getElementById('para-before');
            if(para_before_button){
               para_before_button.addEventListener('click', function(){
                  remove_empty_para();
                  add_para_before(index, kembali);
               });
            }
            var para_after_button = document.getElementById('para-after');
            if(para_after_button){
               para_after_button.addEventListener('click', function(){
                  remove_empty_para();
                  add_para_after(index, kembali);
               });
            }
            var remove_this = document.getElementById('remove-this');
            if(remove_this){
               remove_this.addEventListener('click', function(){
                  remove_this_element(index,kembali);
               });
            }

            var modal = index.getElementsByClassName('modal')[0];
            if(modal){
               modal.addEventListener('click', function(){
                  click_close_popup_para(index, kembali);
               });
            }
         }

         document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
         //-----
      },
      function(){
         // the code that tests here... (return true if test passes; false otherwise)
         return !!(sebelum !== selepas);
         //return !!(document.getElementsByClassName('pics')[0].innerHTML !== '');
      },
      50 // amout to wait between checks
   )();
   /* WAIT UNTIL STOP */
}

function related_clicked(index, myPopup){
   var sebelum = index.innerHTML;
   var kembali = index.outerHTML + '';
   var target = index.getElementsByClassName('related')[0];
   index.innerHTML = myPopup + target.outerHTML;
   selepas = myPopup + target.outerHTML;

   /* WAIT UNTIL START */
   var waitUntil = function (fn, condition, interval) {
      interval = interval || 100;
      var shell = function () {
         var timer = setInterval(
            function () {
               var check;

               try { check = !!(condition()); } catch (e) { check = false; }

               if (check) {
                  clearInterval(timer);
                  delete timer;
                  fn();
               }
            },
            interval
         );
      };
      return shell;
   };
   waitUntil(
      function(){
         //-----
         document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
         if(editor){

            var popupClose = editor.getElementsByClassName('popup-close')[0];
            if(popupClose){
               popupClose.addEventListener('click', function(){
                  click_close_popup_para(index, kembali);
               });
            }
            var para_before_button = document.getElementById('para-before');
            if(para_before_button){
               para_before_button.addEventListener('click', function(){
                  remove_empty_para();
                  add_para_before(index, kembali);
               });
            }
            var para_after_button = document.getElementById('para-after');
            if(para_after_button){
               para_after_button.addEventListener('click', function(){
                  remove_empty_para();
                  add_para_after(index, kembali);
               });
            }
            var remove_this = document.getElementById('remove-this');
            if(remove_this){
               remove_this.addEventListener('click', function(){
                  remove_this_element(index,kembali);
               });
            }

            var modal = index.getElementsByClassName('modal')[0];
            if(modal){
               modal.addEventListener('click', function(){
                  click_close_popup_para(index, kembali);
               });
            }
         }

         document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
         //-----
      },
      function(){
         // the code that tests here... (return true if test passes; false otherwise)
         return !!(sebelum !== selepas);
         //return !!(document.getElementsByClassName('pics')[0].innerHTML !== '');
      },
      50 // amout to wait between checks
   )();
   /* WAIT UNTIL STOP */
}


function gallery_image_clicked(a_img, index, myPopup){
   var sebelum = index.innerHTML;
   var kembali = index.outerHTML + '';
   var target = index.getElementsByClassName('gallery')[0];
   index.innerHTML = myPopup + target.outerHTML;
   selepas = myPopup + target.outerHTML;

   /* WAIT UNTIL START */
   var waitUntil = function (fn, condition, interval) {
      interval = interval || 100;
      var shell = function () {
         var timer = setInterval(
            function () {
               var check;

               try { check = !!(condition()); } catch (e) { check = false; }

               if (check) {
                  clearInterval(timer);
                  delete timer;
                  fn();
               }
            },
            interval
         );
      };
      return shell;
   };
   waitUntil(
      function(){
         //-----
         document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
         if(editor){

            var popupClose = editor.getElementsByClassName('popup-close')[0];
            if(popupClose){
               popupClose.addEventListener('click', function(){
                  click_close_popup_para(index, kembali);
               });
            }
            var para_before_button = document.getElementById('para-before');
            if(para_before_button){
               para_before_button.addEventListener('click', function(){
                  remove_empty_para();
                  add_para_before(index, kembali);
               });
            }
            var para_after_button = document.getElementById('para-after');
            if(para_after_button){
               para_after_button.addEventListener('click', function(){
                  remove_empty_para();
                  add_para_after(index, kembali);
               });
            }
            var remove_this = document.getElementById('remove-this');
            if(remove_this){
               remove_this.addEventListener('click', function(){
                  remove_this_element(index,kembali);
               });
            }

            var this_remove = document.getElementById('this-remove');
            if(this_remove){
               this_remove.addEventListener('click', function(){
                  console.log('%cDIBAWAH', 'color:purple;');
                  console.log(index.getElementsByClassName('gallery_image')[0]);
               });
            }

            var modal = index.getElementsByClassName('modal')[0];
            if(modal){
               modal.addEventListener('click', function(){
                  click_close_popup_para(index, kembali);
               });
            }
         }

         document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
         //-----
      },
      function(){
         // the code that tests here... (return true if test passes; false otherwise)
         return !!(sebelum !== selepas);
         //return !!(document.getElementsByClassName('pics')[0].innerHTML !== '');
      },
      50 // amout to wait between checks
   )();
   /* WAIT UNTIL STOP */
}

function single_image_clicked(index, myPopup){
   var sebelum = index.innerHTML;
   var kembali = index.outerHTML + '';
   var target = index.getElementsByClassName('single_image')[0];
   index.innerHTML = myPopup + target.outerHTML;
   selepas = myPopup + target.outerHTML;

   /* WAIT UNTIL START */
   var waitUntil = function (fn, condition, interval) {
      interval = interval || 100;
      var shell = function () {
         var timer = setInterval(
            function () {
               var check;

               try { check = !!(condition()); } catch (e) { check = false; }

               if (check) {
                  clearInterval(timer);
                  delete timer;
                  fn();
               }
            },
            interval
         );
      };
      return shell;
   };
   waitUntil(
      function(){
         //-----
         document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
         if(editor){

            var popupClose = editor.getElementsByClassName('popup-close')[0];
            if(popupClose){
               popupClose.addEventListener('click', function(){
                  click_close_popup_para(index, kembali);
               });
            }
            var para_before_button = document.getElementById('para-before');
            if(para_before_button){
               para_before_button.addEventListener('click', function(){
                  remove_empty_para();
                  add_para_before(index, kembali);
               });
            }
            var para_after_button = document.getElementById('para-after');
            if(para_after_button){
               para_after_button.addEventListener('click', function(){
                  remove_empty_para();
                  add_para_after(index, kembali);
               });
            }
            var remove_this = document.getElementById('remove-this');
            if(remove_this){
               remove_this.addEventListener('click', function(){
                  remove_this_element(index,kembali);
               });
            }

            var modal = index.getElementsByClassName('modal')[0];
            if(modal){
               modal.addEventListener('click', function(){
                  click_close_popup_para(index, kembali);
               });
            }
         }

         document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
         //-----
      },
      function(){
         // the code that tests here... (return true if test passes; false otherwise)
         return !!(sebelum !== selepas);
         //return !!(document.getElementsByClassName('pics')[0].innerHTML !== '');
      },
      50 // amout to wait between checks
   )();
   /* WAIT UNTIL STOP */
}

function video_clicked(index, myPopup){
   var sebelum = index.innerHTML;
   var kembali = index.outerHTML + '';
   var target = index.getElementsByClassName('video')[0];
   index.innerHTML = myPopup + target.outerHTML;
   selepas = myPopup + target.outerHTML;

   /* WAIT UNTIL START */
   var waitUntil = function (fn, condition, interval) {
      interval = interval || 100;
      var shell = function () {
         var timer = setInterval(
            function () {
               var check;

               try { check = !!(condition()); } catch (e) { check = false; }

               if (check) {
                  clearInterval(timer);
                  delete timer;
                  fn();
               }
            },
            interval
         );
      };
      return shell;
   };
   waitUntil(
      function(){
         //-----
         document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
         if(editor){
            var popupClose = editor.getElementsByClassName('popup-close')[0];
            if(popupClose){
               popupClose.addEventListener('click', function(){
                  click_close_popup_para(index, kembali);
               });
            }
            var para_before_button = document.getElementById('para-before');
            if(para_before_button){
               para_before_button.addEventListener('click', function(){
                  remove_empty_para();
                  add_para_before(index, kembali);
               });
            }
            var para_after_button = document.getElementById('para-after');
            if(para_after_button){
               para_after_button.addEventListener('click', function(){
                  remove_empty_para();
                  add_para_after(index, kembali);
               });
            }
            var remove_this = document.getElementById('remove-this');
            if(remove_this){
               remove_this.addEventListener('click', function(){
                  remove_this_element(index,kembali);
               })
            }
         }
         //-----
      },
      function(){
         // the code that tests here... (return true if test passes; false otherwise)
         return !!(sebelum !== selepas);
         //return !!(document.getElementsByClassName('pics')[0].innerHTML !== '');
      },
      50 // amout to wait between checks
   )();
   /* WAIT UNTIL STOP */
}

function click_close_popup_para(index, kembali){
   var modal = editor.getElementsByClassName('modal')[0];
   if(modal){
      index.outerHTML = kembali;
      document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
      listen_click_para();
      content_preview();
   }
}

function add_para_before(index, kembali){
   var new_para = document.createElement('div');
   new_para.className = 'para kiamatlab';
   new_para.setAttribute('contenteditable','true');
   new_para.setAttribute('autocomplete','off');
   index.parentNode.insertBefore(new_para, index);
   var kiamatlab = editor.getElementsByClassName('kiamatlab')[0];
   click_close_popup_para(index, kembali);
   placeCaretAtEnd(kiamatlab, true);
   if(kiamatlab){
      kiamatlab.className = 'para';
   }
   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
   selRange = saveSelection();
   console.log('%cafter add para. below is the sel range', 'color:pink');
   console.log(selRange);
}

function remove_this_element(index,kembali){
   var wrapper_dia = index.parentNode;
   wrapper_dia.removeChild(index);
   content_preview();
}

function add_para_after(index, kembali){
   var new_para = document.createElement('div');
   new_para.className = 'para kiamatlab';
   new_para.setAttribute('contenteditable','true');
   new_para.setAttribute('autocomplete','off');
   index.parentNode.insertBefore(new_para, index.nextSibling);
   var kiamatlab = editor.getElementsByClassName('kiamatlab')[0];
   click_close_popup_para(index, kembali);
   placeCaretAtEnd(kiamatlab, true);
   if(kiamatlab){
      kiamatlab.className = 'para';
   }
   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
   selRange = saveSelection();
}


function remove_empty_para(){
   console.log('remove_empty_para()');
   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
   if(editorPara){
      for (var i = 0; i < editorPara.length; i++) {
         if (typeof window.addEventListener === 'function'){
            (function (index) {
               if(index.innerHTML.trim() == '' || index.innerHTML.trim() == null /*&& index.getAttribute('contenteditable') == 'true'*/){

                  console.log('kami remove ' + index.outerHTML);
                  var wrapper_dia = index.parentNode;
                  wrapper_dia.removeChild(index);

               }
            })(editorPara[i]);
         }
      }
   }
}

/* WHEN USER PASTE SOMETHING. FORMAT IT FIRST! */
function paste_editor(e){
   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
   e.preventDefault();
   var theKlas = document.activeElement.className;
   if(theKlas){
      var theKlas = ' ' + theKlas + ' ';
   }

   var text = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

   if(text){
      if(theKlas.indexOf(" message ") > -1 || document.activeElement.tagName.toLowerCase() == 'blockquote'){
         //   case active element is para
         console.log('WANNA PASTE INSIDE MESSAGE???');
         var theHTML = text.trim();
         var html_array = theHTML.split('\n');
         var my_div = document.createElement('div');
         var jumlah = parseFloat(html_array.length) - parseFloat(1);
         for (var i = 0; i < html_array.length; i++) {
            var parag =  html_array[i].trim();
            if(parag.length > 0){
               if(i != jumlah){
                  var paragh = parag + 'BRMESEJ';
               } else {
                  var paragh = parag;
               }
               my_div.append(paragh);
            }
         }

         pasteHtmlAtCaret(my_div.innerHTML.trim(), 'enter');
      } else {
         // case active element is mesej or blockquote
         var theHTML = text.trim(),
         theHTML = theHTML.replace(/\n{3,}/g, '\n');
         console.log('theHTML = '+ theHTML);
         var theHTML = theHTML.replace(/\n/g, '<!--PEMISAHAN-->');
         console.log('theHTML PEMISAHAN = '+ theHTML);
         pasteHtmlAtCaret(theHTML, 'enter');
      }
      document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
      remove_empty_para();
      no_paraPara();
      content_preview();
   }
}

/*
function paste_message(e){
console.log('below is paste_message console.log');
e.preventDefault();
var text = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

if(text){
var theHTML = text.trim(),
theHTML = theHTML.replace(/\n{3,}/g, '\n');
var my_array = theHTML.split('\n');
// var fake_array = new Array();
var my_elem  = document.createElement('div');
for (var i = 0; i < my_array.length; i++) {
var my_text = my_array[i].trim();
if(my_text.length > 0){ // keep only paragraph had a text..
//var my_parag = my_text + '<br class="newline"/><br class="newline"/>';
var my_parag = my_text + '\n\n';
// fake_array.push(my_parag);
my_elem.append(my_parag);
}
}
document.execCommand("insertHTML", false, my_elem.innerHTML);
}
}
*/




/* TOOL TO CLEAN ARRAY */
function cleanArray(actual) {
   var newArray = new Array();
   for (var i = 0; i < actual.length; i++) {
      if (actual[i]) {
         newArray.push(actual[i]);
      }
   }
   return newArray;
}


/* CALL IT AFTER YOU INSERT SOMETHING TO EDITOR. IT WILL REMOVE DOUBLE PARA PARA */
function no_paraPara(){ // THE PROBLERM IS HERE WTF LOL.
   console.log('no_paraPara run');
   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
   if(editorPara){
      // foreach all element.
      var i;
      for (i = 0; i < editorPara.length; ++i) {
         if (typeof window.addEventListener === 'function'){
            (function (index) {

               var paraDalam = index.getElementsByClassName('para');
               if(paraDalam){
                  document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
                  var wrap = document.createElement('div');
                  for (var ii = 0; ii < paraDalam.length; ii++) {
                     if (typeof window.addEventListener === 'function'){
                        (function (index_x) {
                           var sementara = document.createElement("div");
                           sementara.className = 'para';
                           if(
                              !paraDalam[ii].getElementsByClassName('message') || !paraDalam[ii].getElementsByTagName('blockquote') ||
                              !paraDalam[ii].getElementsByClassName('video') ||
                              !paraDalam[ii].getElementsByClassName('single_image') ||
                              !paraDalam[ii].getElementsByClassName('gallery') ||
                              !paraDalam[ii].getElementsByClassName('related') ||
                              !paraDalam[ii].getElementsByClassName('button') ||
                              !paraDalam[ii].getElementsByClassName('content-table')
                           ) { // this class no need editable..
                              sementara.setAttribute('contentEditable', 'true');
                              sementara.setAttribute('autocomplete', 'off');
                           }
                           sementara.innerHTML = index_x.innerHTML;
                           wrap.appendChild(sementara);
                        })(paraDalam[ii]);
                     }
                  }
                  if(wrap.innerHTML.trim() != null && wrap.innerHTML.trim() != ''){
                     index.outerHTML = wrap.innerHTML;
                     document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
                     remove_empty_para();
                  }

               }
            })(editorPara[i]);
         }
      }
   }
   return false;
}


/* Check last  and first paragraph, if not editable (possible it's a widget) so create new paragraph with attribue contenteditable*/
function check_last_para(){
   console.log('we check last para');
   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
   if(paraPara){
      var last_para = paraPara[paraPara.length -1];
      if(last_para){
         if(!last_para.getAttribute('contenteditable') || last_para.getAttribute('contenteditable') == 'false'){
            var getAttr = false || last_para;
            var new_para = document.createElement('div');
            new_para.className = 'para';
            new_para.setAttribute('contenteditable','true');
            new_para.setAttribute('autocomplete','off');
            editor.appendChild(new_para);
         }
      }
   }
   return false;
}
function check_first_para(){
   console.log('we check first para');
   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
   if(paraPara){
      var first_para = paraPara[0];
      if(first_para){
         if(!first_para.getAttribute('contenteditable') || first_para.getAttribute('contenteditable') == 'false'){
            var new_para = document.createElement('div');
            new_para.className = 'para';
            new_para.setAttribute('contenteditable','true');
            new_para.setAttribute('autocomplete','off');
            first_para.parentNode.insertBefore(new_para, first_para);
         }
      }
   }
   return false;
}

/* SEND EDITOR CONTENT TO PREVIEW */
function content_preview(){
   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
   format_editor();
   selrange = saveSelection();
   listen_click_para();
   // check_last_para();
   // check_first_para();
   preview.innerHTML = editor.innerHTML.replace('<div class="para"><br></div>', '').replace('&nbsp\;', ' ');
   format_preview();
}

/* FORMAT EDITOR */
function format_editor(){
   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
   //check adakah para ada, kalo xde kita create para then focus...
   if(editorPara.length < 1){
      var new_para = document.createElement('div');
      new_para.className = 'para';
      new_para.setAttribute('contenteditable', 'true');
      new_para.setAttribute('autocomplete', 'off');
      editor.innerHTML(new_para);
      document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
      placeCaretAtEnd(editorPara[0], false);
      console.log('theres no para, so we create new para...');
   }
   //remove certain thing pada semua element.
   if(elementEditor){
      /* VARIALBLE */
      //set up allowed attr and allowed value.
      var allowedAttr = {
         class: true,
         'alt': true,
         'title': true,
         'href': true,
         'src': true,
         'contenteditable': true,
         'autocomplete': true
      };
      var allowedClassVal = {
         'para': true, //<div class="para">text perenngan pertama</div>
         'gallery': true, //gallery in post wrapper.
         'single_image': true, // gambar single.
         'gallery_image': true, //gambar dalam gallery figure.
         'content-table': true, // table
         'message': true,
         'video': true,
         'related': true,
         'button': true,
         'kiamatlab': true, // when user enter between paragraph.. create new line, then focus to this class.
         'true': true,
         'newline': true

      };
      var disallowTag = {
         'script': true,
         'body': true,
         'noscript': true,
         'style': true,
         //'br': true
      };

      var i;
      // foreach all element.
      for (i = 0; i < elementEditor.length; ++i) {
         var
         elem = elementEditor[i]
         ;

         // <element> area.
         if(elem){
            // tukar p kepada div.para
            if(elem.tagName.toLowerCase() == 'p'){
               var newPara = document.createElement('div');
               newPara.className = 'para';
               //masukkan para element dalam p
               newPara.innerHTML = elem.innerHTML;
               //masukkan p sebelum .para
               elem.parentNode.insertBefore(newPara, elem);
               //remove .para
               elem.parentNode.removeChild(elem);
            }

            // remove br without  needed tag..
            if(elem.tagName.toLowerCase() == 'br'){
               if(elem.className){
                  if(elem.className.trim() != 'newline'){
                     elem.parentNode.removeChild(elem);
                  }
               } else {
                  elem.parentNode.removeChild(elem);
               }
            }

            //remove disallowTag.
            if (disallowTag[elem.tagName.toLowerCase()]){
               elem.parentNode.removeChild(elem);
            }

         }
         //arrtibute="value" area
         if (elem.attributes) {
            var ii;
            for (ii = elem.attributes.length -1; ii >=0; --ii) {
               var elemAttr = elem.attributes[ii];
               if (!allowedAttr[elemAttr.name.toLowerCase()]){ //jika attribute ni x tersenarai
                  elem.attributes.removeNamedItem(elemAttr.name);
               } else {
                  if(elemAttr.name == 'class'){
                     //class area.
                     if(!allowedClassVal[elemAttr.value.toLowerCase()]){
                        var classArray = elemAttr.value.toLowerCase().split(' ');
                        var fKlas = '';
                        for (var ioo = 0; ioo < classArray.length; ioo++) {
                           var Klas = classArray[ioo];
                           if(allowedClassVal[Klas]){
                              fKlas += Klas +' ';
                           }
                        }
                        elemAttr.value = fKlas;
                        //elem.attributes.removeNamedItem(elemAttr.name);
                        //if( !elemAttr.value.toLowerCase.trim().indexOf('para') > -1 ){ // xde class para.
                     }
                  }
               }
            }
         }
      } // end foreach all element.
   }// if elementEditor
}


/*FORMAT HTML INSDE EDITOR */
function format_preview(){
   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';

   //tukar .para to p
   if(previewPara){
      for (var i = 0; i < previewPara.length; i++) {
         para = previewPara[i];
         if (typeof window.addEventListener === 'function'){
            (function (index) {

               if(
                  index.innerHTML.indexOf('</div>') > -1 ||
                  index.innerHTML.indexOf('</table>') > -1 ||
                  index.innerHTML.indexOf('</figure>') > -1 ||
                  index.innerHTML.indexOf('single_image') > -1 ||
                  index.innerHTML.indexOf('</blockquote>') > -1 ||
                  index.innerHTML.indexOf('<iframe ') > -1 ||
                  index.innerHTML.indexOf('class="related"')
               ){
                  //create p
                  var newP = document.createElement('div');
               } else {
                  //create p
                  var newP = document.createElement('p');
               }

               newP.className = 'para';
               //masukkan para element dalam p
               newP.innerHTML = para.innerHTML;
               //masukkan p sebelum .para
               para.parentNode.insertBefore(newP, para);
               //remove .para
               para.parentNode.removeChild(para);
            })(para);
         }
      }
   }

   // remove all br..
   if(previewBr){
      for (var i = 0; i < previewBr.length; i++) {
         if (typeof window.addEventListener === 'function'){
            (function (index) {
               if(index.className != 'newline'){
                  console.log('br dibawah akan di delete mew muah..');
                  console.log(index);
                  index.parentNode.removeChild(index);
               }
            })(previewBr[i]);
         }
      }
   }


   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';

   console.log('CARIAN');

   var preview_first = preview.children;//document.querySelectorAll('#post-preview > *')

   for (var i = 0; i < preview_first.length; i++) {
      var elem = preview_first[i];
      if (typeof window.addEventListener === 'function'){
         (function (index) {

            //nak remove element dalam para yang kosong..
            var elem_dalam = elem.querySelectorAll('*');
            if(elem_dalam){
               for (var i = 0; i < elem_dalam.length; i++) {
                  if (typeof window.addEventListener === 'function'){
                     (function (undux) {
                        //console.log(elem_dalam[i].getAttribute('src'));
                        if(elem_dalam[i].getAttribute('src') == null || elem_dalam[i].getAttribute('src') == undefined || elem_dalam[i].getAttribute('src') == '' && elem_dalam[i].className != 'newline'){
                           //dibawah ni adalah element yg tiada src. yg ada src selalalunya element kosong. ex: img, iframe, dll.
                           if(elem_dalam[i].textContent){
                              var textContent = elem_dalam[i].textContent.trim();
                           } else {
                              var textContent = '';
                           }

                           if(textContent.length < 1){
                              if(elem_dalam[i].innerHTML){
                                 var innerHTML = elem_dalam[i].innerHTML.trim();
                              } else {
                                 var innerHTML = '';
                              }
                              if(innerHTML < 1 && elem_dalam[i].className != 'newline'){
                                 // ni element dalam para yg betul2 empty accidently..
                                 console.log('we will remove ' + undux + ' inside ' + index);
                                 undux.parentNode.removeChild(undux);
                                 document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
                              }
                           }
                        }
                     })(elem_dalam[i]);
                  }
               }
            }

            //nak remove para kosong..
            if(elem.textContent.replace(/\s+/g, '').trim() == null || elem.textContent.replace(/\s+/g, '').trim() == undefined || elem.textContent.replace(/\s+/g, '').trim().length < 1){
               if(elem.innerHTML.trim() == ''){ // element ni truly kosong..
                  index.parentNode.removeChild(index);
               }
            }

         })(elem);
      }
   }


   content_draft();
}

var timeOutContent;
function content_draft(){
   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
   document.getElementById('content_draft').value = document.getElementById('post-preview').innerHTML.trim(); // actally it's working but not change at Inspect Element Browser. But it will working when you try to console.log the the textarea value..
   clearTimeout(timeOutContent);
   timeOutContent = setTimeout(function(){
      save_draft('content');
   }, 5000);
}

function after_enter(){
   //console.log('in after_enter() = ' + editor.innerHTML);
   if(editor.innerHTML.indexOf('<!--PEMISAHAN-->') > 0){
      //Pemisahan adalah tanda dari paste..
      editor.innerHTML = editor.innerHTML.replace(/<!--PEMISAHAN-->/g, '</div><div class="para" contenteditable="true" autocomplete="off">');
      document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
      console.log('ini pemisahan lohh');
   } else if(editor.innerHTML.indexOf('BRMESEJ') > 0){
      //case insert mesej. tukar komen tag ini kepada br muah ciket..
      editor.innerHTML = editor.innerHTML.replace(/BRMESEJ/g, '<br class="newline"/><br class="newline"/>');
      document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
      console.log('ini insert mesej or blockquote lohh');
   } else {
      //Pemisah pula dari enter..
      editor.innerHTML = editor.innerHTML.replace(/<!--PEMISAH-->/g, '</div><div class="para kiamatlab" contenteditable="true" autocomplete="off">');
      document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';

      if(editorPara){

         var array_para_kosong = new Array();
         for (var i = 0; i < editorPara.length; i++) {
            if(editorPara[i].innerHTML.trim().length < 1){
               array_para_kosong.push(i);
            }
         }

         if(array_para_kosong.length > 0){ // hangguk
            placeCaretAtEnd(editorPara[array_para_kosong[0]], false);
         } else {

            // place caret at kiamatlab...
            for (var i = 0; i < editorPara.length; i++) {
               if (typeof window.addEventListener === 'function'){
                  (function (index) {
                     var theClass = ' ' + editorPara[i].className + ' ';
                     if(theClass.indexOf(" kiamatlab ") > -1){
                        //alert(i);
                        index.setAttribute('class', 'para'); index.className = 'para';
                        index.className = 'para';
                        document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
                        placeCaretAtEnd( index, true );
                        document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
                        //actually in some inspect element of some browser, you can't see kiamatlab removed. but actually is it working, the class was change to 'para', not 'para kiamatlab' anymore..
                     }
                  })(editorPara[i]);
               }
            }

         } // hangguk
      }

   }
}


function after_insert(){

   // if(paraPara){
   //    // foreach all element.
   //    var i;
   //    for (i = 0; i < paraPara.length; ++i) {
   //       var pePara = paraPara[i];
   //       var paraDalam = pePara.getElementsByClassName('para')[0];
   //       if(paraDalam){
   //          var getParaDalam = paraDalam.outerHTML;
   //          pePara.outerHTML = '<div class="para" contenteditable="true">' + pePara.innerHTML.replace(getParaDalam, '') + '</div>' + getParaDalam;
   //       }
   //    }
   // }

   no_paraPara();

   close_all_popup();
   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
   content_preview();
   selRange = saveSelection();
}














/* POPUP AREA */
var
dialogModal = document.getElementsByClassName('modal')[0],
popupClose = document.getElementsByClassName('popup-close')
;
//CONTROL CLICKED, SHOW POPUP.
var controlButton = document.querySelectorAll('.wysiwyg-controls > a');
if(controlButton){
   for (var i = 0; i < controlButton.length; i++) {
      if (typeof window.addEventListener === 'function'){
         (function (index) {
            controlButton[i].addEventListener('click', function(e){
               e.preventDefault();
               document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';

               /* -------------- SEL RANGE START -------------- */
               selRange = saveSelection();
               console.log('%c*** dibawah selrange console once click control button ***' ,'color:green;font-weight:600');
               console.log(selRange);

               if(selRange == undefined || selRange== null || !selRange){
                  console.log('yeah undefined, maybe load2 je x click apa2 terus click control..');
                  selRange_takde();
               } else {
                  console.log('selRange ada, only one that we need selRange must have wrapper ');
                  //console.log(check_attribute(selRange.startContainer, 'class','para'));//console.log(get_wrapper(selRange.startContainer, 'class', 'para'));
                  if(get_wrapper(selRange.startContainer, 'class', 'para') || check_attribute(selRange.startContainer, 'class','para')){//kalo cursor tu dalam mana2 element ada para..
                     /* it's okay, selrange kan dah save pada line first */
                  } else {// false, bukan dalam para..
                     console.log('Eh Selrange takde?');
                     selRange_takde();
                  }
               }
               /* --------------  SEL RANGE END   -------------- */

               var id = document.getElementById(index.getAttribute('data-show'));
               if(index.getAttribute('data-show') == 'a_a_link'){
                  link_management();
               }
               if(index.getAttribute('data-show') == 'a_a_button'){
                  button_by_title();
               }
               if(index.getAttribute('data-show') == 'a_a_message'){
                  message_box();
               }
               if(index.getAttribute('data-show') == 'a_a_qoute'){
                  qoute_box();
               }
               if(id){
                  dialogModal.style.display = 'block';
                  id.style.display = 'block';
               }
            });
         })(controlButton[i]);
      }
   }
}

function selRange_takde(){
   if(editorPara.length > 0){
      /* para ada, put cursor at end of last para.. */
      var jumlah = parseFloat(editorPara.length) - parseFloat(1)
      placeCaretAtEnd(editorPara[jumlah], false);
      selRange = saveSelection();
      console.log('%c'+selrange,'background:black');
   } else {
      /* para xde, create new para. then put cursor at end */
      var new_para = document.createElement('div');
      new_para.className = 'para';
      new_para.setAttribute('contenteditable','true');
      new_para.setAttribute('autocomplete','off');
      editor.appendChild(new_para);
      var kiamatlab = editor.getElementsByClassName('para')[0];
      placeCaretAtEnd(kiamatlab, true);
      selRange = saveSelection();
   }
}

/* CLOSE ALL POPUP */
for (var i = 0; i < popupClose.length; i++) {
   popupClose[i].addEventListener('click', close_all_popup, false);
}
function close_all_popup(){
   dialogModal.style.display = 'none';
   var popup_contents = document.getElementsByClassName('popup-content');
   for (var i = 0; i < popup_contents.length; i++) {
      var popup_content = popup_contents[i];
      popup_content.style.display = 'none';
   }

   var all_input = dialogModal.getElementsByTagName('input');
   for (var i = 0; i < all_input.length; i++) {
      all_input[i].value = null;
   }

   var all_textarea = dialogModal.getElementsByTagName('textarea');
   for (var i = 0; i < all_textarea.length; i++) {
      all_textarea[i].value = null;
   }

   var all_button = dialogModal.getElementsByClassName('insert');
   for (var i = 0; i < all_button.length; i++) {
      all_button[i].setAttribute('disabled','disabled');
   }

   document.getElementsByTagName('body')[0].setAttribute('id', 'main-blog');
   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
}




/* GAMBAR POPUP TABBER AREA. */
var menu_tab = document.getElementById("pilihan-option").querySelectorAll('span');
if(menu_tab){

   for (var i = 0; i < menu_tab.length; i++) {
      if (typeof window.addEventListener === 'function'){
         (function (index) {
            menu_tab[i].addEventListener('click', function(){
               proceed_tab(index.getAttribute('data-show'), index.getAttribute('id'));
            });
         })(menu_tab[i]);
      }
   }

}
function proceed_tab(data_show, id){
   //reset to display none all.
   var tab_content = document.querySelectorAll('#image-option > div');//document.getElementById("image-option").querySelectorAll('div');
   if(tab_content){
      for(var i = 0; i < tab_content.length; i++) {
         var tab_id = tab_content[i].getAttribute('id');
         if(tab_id == data_show){
            tab_content[i].style.display = 'block';
         } else {
            tab_content[i].style.display = 'none';
         }
      }
   }

   //add selected
   if(menu_tab){
      for(var i = 0; i < menu_tab.length; i++) {
         var theId = menu_tab[i].getAttribute('id');
         if(theId == id){
            // add selected
            var finalid = document.getElementById(theId);
            var semiKelas = finalid.className.replace("selected", "");
            var Kelas = semiKelas + ' ' + 'selected';
            finalid.setAttribute('class', Kelas);
            //var ddd = finalid.getAttribute('data-show');
            //var idContent = document.getElementById(ddd);
         } else {
            //remove selected
            var finalid = document.getElementById(theId);
            var Kelas = finalid.className;
            finalid.setAttribute('class', Kelas.replace("selected", "") );
         }
      }
   }
}


//BEFORE UPLOAD. ONCHANGE INPUT
var myinput = document.getElementById("insert-file");
myinput.addEventListener('change', function () {
   if(this.files.length > 0){
      fetchimage();
      document.getElementById('jumlah-file').innerHTML = this.files.length;
      document.getElementsByClassName('upload-button')[0].removeAttribute("disabled");
   } else {
      document.getElementById('jumlah-file').innerHTML = '0';
      document.getElementsByClassName('upload-button')[0].setAttribute('disabled', 'disabled');
   }
});
function fetchimage(){
   var filelist = myinput.files || [];
   for (var i = 0; i < filelist.length; i++) {
      getBase64(filelist[i], i);
   }
}
function getBase64(file, ai, adui) {
   var nama = file.name;
   nama = nama.replace(/\[|\]|{|}|\?|\/|\+|\-|_|\(|\)|&|\^|%|\#|\$|\@|\!|\"|\'+/g,' ');
   nama = nama.replace(/\s\s|.jpg|.png|.jpeg|.gif|.JPG|.PNG|.JPEG|.GIF|\/+/g,' ').trim();
   var reader = new FileReader();
   reader.readAsDataURL(file);
   reader.onload = function () {
      document.getElementById("before-upload").innerHTML += '<div class="img_wrpr"><img src="'+reader.result+'"  id="img_'+ai+'" class="data_img"/><div class="data_box"><label for="usr_files[text]['+ai+']">TITLE</label><p class="box_right"><input class="nama-file" name="usr_files[text]['+ai+']" type="text" value="'+nama+'" multiple="multiple" required/></p></div><!--tag--><div class="data-tag"><div class="tag-message">Tag orang, tempat atau benda yang terlibat dalam gambar</div><span class="tag-message">Maximum 5 (yang popular atau general sahaja):</span> <div class="tag-input data_box"><label>TAG</label><p class="box_right"><input name="usr_files[img_tag]['+ai+']" class="tag-gambar" placeholder="contoh: Awie, Mahathir, Bali, Adidas, KFC" multiple="multiple"></p></div></div>';

   };
}












/* LINK AREA */

//once link popup...
function link_management(){
   // fill text input with selection..
   document.querySelectorAll('#a_a_link .insert-text')[0].value = GetSelectedText(selRange).trim();
   link_by_title();
}

//once link text change...
var type_link_text;
document.querySelectorAll('#a_a_link .insert-text')[0].addEventListener('input', function(){
   clearTimeout(type_link_text);
   type_link_text = setTimeout(function(){
      link_by_title();
   }, 1000);
});

// search link to db by title...
function link_by_title(){
   var text = document.querySelectorAll('#a_a_link .insert-text')[0].value.trim();
   if(text.length > 0){
      //cari title ni.
      var xhr      = new XMLHttpRequest();
      xhr.open('POST', 'http://media.malayatimes.com/search_link/title?cari='+ encodeURI(text), true);
      xhr.onreadystatechange = function() {
         if (xhr.readyState === 4 && xhr.status === 200){
            var data = xhr.responseText;
            if(data && data.length > 0){
               var link_input = document.querySelectorAll('#a_a_link .insert-link')[0];
               if(link_input.value.length == 0){
                  link_input.value = data;
               }
               check_link_field();
            } else {
               check_link_field();
            }
         }
      }
      xhr.send();
   } else {
      check_link_field();
   }
}

//once link insert button clicked..
document.querySelectorAll('#a_a_link .insert')[0].addEventListener('click', function(){
   var text = document.querySelectorAll('#a_a_link .insert-text')[0];
   var link = document.querySelectorAll('#a_a_link .insert-link')[0];
   if(text.value.length > 0 && link.value.length > 0){
      var str = GetSelectedText(selRange);
      if(str[str.length - 1] == ' '){
         var space = ' ';
         // console.log('last char is space..');
      } else {
         var space = '';
         // console.log('last char not space..');
      }
      var myhtml = '<a href="'+link.value+'">'+text.value.trim()+'</a>' + space;
      pasteHtmlAtCaret(myhtml, 'insert');
      text.value = '';
      link.value = '';
   } else {
      alert('Edit semula. Link dan text tidak boleh kosong...');
   }
});

function strHtmlToDom(MYSTR){
   /* EX: strHtmlToDom('<div class="wtf">HAHAHAHA</div>') */
   var div = MYSTR,
   parser = new DOMParser(),
   doc = parser.parseFromString(MYSTR, "text/xml");
   return doc.firstChild;
}

//once link input changed...
var tatuta;
document.querySelectorAll('#a_a_link .insert-link')[0].addEventListener('input', function(){
   if(this.value > 0){
      clearTimeout(tatuta);
      tatuta = setTimeout(function(){
         var xhr      = new XMLHttpRequest();
         xhr.open('POST', 'http://media.malayatimes.com/get-link-title?link='+ encodeURI(this.value.trim()), true);
         xhr.onreadystatechange = function() {
            if (xhr.readyState === 4 && xhr.status === 200){
               var data = xhr.responseText;
               if(data && data.length > 0){
                  var text_input = document.querySelectorAll('#a_a_link .insert-text')[0];
                  text_input.value = data;
                  check_link_field();
               } else {
                  check_link_field();
               }
            }
         }
         xhr.send();
      }, 1000);
   } else {
      check_link_field();
   }
});

function check_link_field(){
   var link = document.querySelectorAll('#a_a_link .insert-link')[0];
   var text =  document.querySelectorAll('#a_a_link .insert-text')[0];
   console.log('ABC');
   console.log(link.value + ' = ' + link.value.length);
   console.log(text.value + ' = ' + text.value.length);
   if(link.value.length > 6 && text.value.length > 0){
      console.log('DEF');
      document.querySelectorAll('#a_a_link .insert')[0].removeAttribute('disabled');
   } else {
      console.log('GHI');
      document.querySelectorAll('#a_a_link .insert')[0].setAttribute('disabled', 'disabled');
   }
}
/* END LINK AREA */


/* RELATED AREA */
var relatedLinkInput = document.querySelectorAll('#a_a_related .insert-related-link')[0];
var relatedTextInput = document.querySelectorAll('#a_a_related .insert-related-text')[0];
var relatedInsertButton = document.querySelectorAll('#a_a_related .insert')[0];

relatedLinkInput.addEventListener('input', function(){
   if(this.value.length > 0){
      var xhr      = new XMLHttpRequest();
      xhr.open('POST', 'http://media.malayatimes.com/get-link-title?link='+ encodeURI(this.value), true);
      xhr.onreadystatechange = function() {
         if (xhr.readyState === 4 && xhr.status === 200){
            var data = xhr.responseText;
            if(data){
               document.querySelectorAll('#a_a_related .insert-related-text')[0].value = data;
               if(relatedTextInput.value.length > 0){
                  relatedInsertButton.removeAttribute('disabled');
               }
            }
         }
      }
      xhr.send();
   } else {
      relatedInsertButton.setAttribute('disabled', 'disabled');
   }
});

//once change Text Related input..
var sakit_leher;
relatedTextInput.addEventListener('input', function(){
   if(this.value.length > 0){
      clearTimeout(sakit_leher);
      sakit_leher = setTimeout(function(){
         related_by_title();
      }, 2000);
   } else {
      relatedInsertButton.setAttribute('disabled', 'disabled');
   }
});

function related_by_title(){
   var theinput = document.querySelectorAll('#a_a_related .insert-related-text')[0];
   var xhr      = new XMLHttpRequest();
   xhr.open('POST', 'http://media.malayatimes.com/search_link/title?cari='+ encodeURI(theinput.value), true);
   xhr.onreadystatechange = function() {
      if (xhr.readyState === 4 && xhr.status === 200){
         var data = xhr.responseText;
         if(data){
            document.querySelectorAll('#a_a_related .insert-related-link')[0].value = data;
            if(relatedLinkInput.value.length > 0){
               relatedInsertButton.removeAttribute('disabled');
            }
         }
      }
   }
   xhr.send();
}

//insert related button clicked..
relatedInsertButton.addEventListener('click', function(){
   if(relatedLinkInput.value.length > 0 && relatedTextInput.value.length > 0){
      var str = GetSelectedText(selRange);
      if(str[str.length - 1] == ' '){
         var space = ' ';
      } else {
         var space = '';
      }
      var myhtml = '<div class="para" autocomplete="off"><span class="related"><a href="'+relatedLinkInput.value+'" class="link">'+relatedTextInput.value+'</a></span>' + space+'</div>';
      pasteHtmlAtCaret(myhtml, 'insert');
      this.setAttribute('disabled','disabled');
      relatedLinkInput.value = null;
      relatedTextInput.value = null;
   }
})
/* END RELATED AREA */

/* BUTTON AREA */
var buttonTextInput = document.querySelectorAll('#a_a_button .insert-text-button')[0];
var buttonLinkInput = document.querySelectorAll('#a_a_button .insert-link-button')[0];
var buttonInsertInput = document.querySelectorAll('#a_a_button .insert')[0];
var buttonTimer;
buttonTextInput.addEventListener('input', function(){
   if(this.value.length > 0){
      clearTimeout(buttonTimer);
      buttonTimer = setTimeout(function(){
         button_by_title();
      }, 1000);
   } else {
      buttonInsertInput.setAttribute('disabled', 'disabled');
   }
});

function button_by_title(){
   if(buttonTextInput.value.length < 1){
      buttonTextInput.value = GetSelectedText(selRange);
   }

   if(buttonTextInput.value.length > 0){
      var xhr      = new XMLHttpRequest();
      xhr.open('POST', 'http://media.malayatimes.com/search_link/title?cari='+ encodeURI(buttonTextInput.value), true);
      xhr.onreadystatechange = function() {
         if (xhr.readyState === 4 && xhr.status === 200){
            var data = xhr.responseText;
            if(data.length > 0){
               buttonLinkInput.value = data;
               buttonInsertInput.removeAttribute('disabled');
            }
         }
      }
      xhr.send();
   }
}

var buttonTiming;
buttonLinkInput.addEventListener('input', function(){
   if(this.value.length > 0){
      clearTimeout(buttonTiming);
      buttonTiming = setTimeout(function(){
         button_by_link();
      }, 1000);
   } else {
      buttonInsertInput.setAttribute('disabled', 'disabled');
   }
});

function button_by_link(){
   var xhr      = new XMLHttpRequest();
   xhr.open('POST', 'http://media.malayatimes.com/get-link-title?link='+ encodeURI(buttonLinkInput.value), true);
   xhr.onreadystatechange = function() {
      if (xhr.readyState === 4 && xhr.status === 200){
         var data = xhr.responseText;
         if(data.length > 0){
            buttonTextInput.value = data;
            buttonInsertInput.removeAttribute('disabled');
         }
      }
   }
   xhr.send();
}

buttonInsertInput.addEventListener('click', function(){
   var myhtml = '<a class="button" href="'+buttonLinkInput.value.trim()+'">'+buttonTextInput.value.trim()+'</a>';
   pasteHtmlAtCaret(myhtml,'insert');
   buttonLinkInput.value = null;
   buttonTextInput.value = null;
   buttonInsertInput.setAttribute('disabled','disabled');
});


/* END BUTTON AREA */


/* TABLE AREA */
var tableDefaultHtml = '<table class="content-table"> <thead> <tr> <th contenteditable="true">EDIT ME</th> <th contenteditable="true">EDIT ME</th> </tr> </thead> <tbody> <tr> <td contenteditable="true">-</td> <td contenteditable="true">-</td> </tr> <tr> <td contenteditable="true">-</td> <td contenteditable="true">-</td> </tr> </tbody></table>';

document.querySelectorAll('#a_a_table .add-rows')[0].addEventListener('click', function(){
   var tbody = document.querySelectorAll('#a_a_table tbody')[0];
   tbody.innerHTML = tbody.innerHTML + '<tr><td contenteditable="true">-</td><td contenteditable="true">-</td></tr>';
});

document.querySelectorAll('#a_a_table .remove-rows')[0].addEventListener('click', function(){
   var tbody = document.querySelectorAll('#a_a_table tbody')[0];
   var rws=tbody.getElementsByTagName('tr');
   tbody.removeChild(rws[rws.length-1]);
});

document.querySelectorAll('#a_a_table .insert')[0].addEventListener('click', function(){
   var table = document.querySelectorAll('#a_a_table .table-content')[0].innerHTML;
   pasteHtmlAtCaret('<div class="para">'+table+'</div>', 'insert');
   document.querySelectorAll('#a_a_table tbody')[0].innerHTML = tableDefaultHtml;
});
/* END TABLE AREA */

/* MESSAGE AREA */
function message_box(){
   document.querySelectorAll('#a_a_message .insert-message')[0].value = GetSelectedText(selRange);
   document.querySelectorAll('#a_a_message .insert-message')[0].value = document.querySelectorAll('#a_a_message .insert-message')[0].value.trim();
   if(document.querySelectorAll('#a_a_message .insert-message')[0].value.length > 0){
      document.querySelectorAll('#a_a_message .insert')[0].removeAttribute('disabled');
   }
}

document.querySelectorAll('#a_a_message .insert-message')[0].addEventListener('input', function(){
   if(this.value.length > 0){
      document.querySelectorAll('#a_a_message .insert')[0].removeAttribute('disabled');
   } else {
      document.querySelectorAll('#a_a_message .insert')[0].setAttribute('disabled','disabled');
   }
});

document.querySelectorAll('#a_a_message .insert')[0].addEventListener('click', function(){
   var box = document.querySelectorAll('#a_a_message .insert-message')[0];
   if(box.value.length > 0){
      pasteHtmlAtCaret('<div class="para"><div class="message" contenteditable="true" autocomplete="off">'+format_mesage(box.value)+'</div></div>', 'enter');
      close_all_popup();
   } else {
      alert('Anda tidak boleh memasukkan message box yang tiada isi. Edit atau close untuk cancel..');
   }
});
/* END MESSAGE AREA */

function format_mesage(balue){
   var balue = balue.split("\n");
   var my_array = new Array();
   for (var i = 0; i < balue.length; i++) {
      vall = balue[i].trim();
      if(vall.length > 0){
         my_array.push(vall);
         //my_div.append(vall);
      }
   }
   var my_div = document.createElement('div');
   var last = parseFloat(my_array.length) - parseFloat(1);
   for (var i = 0; i < my_array.length; i++) {
      if( i != last){
         my_div.append(my_array[i] + 'BRMESEJ');
      } else { // last
         my_div.append(my_array[i]);
      }
   }
   return my_div.innerHTML;
}

/* BLOCKQUOTE AREA */
function qoute_box(){
   document.querySelectorAll('#a_a_quote .insert-quote')[0].value = GetSelectedText(selRange);
   document.querySelectorAll('#a_a_quote .insert-quote')[0].value = document.querySelectorAll('#a_a_quote .insert-quote')[0].value.trim();
   if(document.querySelectorAll('#a_a_quote .insert-quote')[0].value.length > 0){
      document.querySelectorAll('#a_a_quote .insert')[0].removeAttribute('disabled');
   }
}

//perubahan blockqoute.
document.querySelectorAll('#a_a_quote .insert-quote')[0].addEventListener('input', function(){
   if(this.value.length > 0){
      document.querySelectorAll('#a_a_quote .insert')[0].removeAttribute('disabled');
   } else {
      document.querySelectorAll('#a_a_quote .insert')[0].setAttribute('disabled','disabled');
   }
});

document.querySelectorAll('#a_a_quote .insert')[0].addEventListener('click', function(){
   var thevalue = document.querySelectorAll('#a_a_quote .insert-quote')[0].value;
   if(thevalue.length > 0){
      pasteHtmlAtCaret('<div class="para"><blockquote contenteditable="true" autocomplete="off">'+format_mesage(thevalue)+'</blockquote></div>','enter');
      document.querySelectorAll('#a_a_quote .insert')[0].setAttribute('disabled','disabled');
      document.querySelectorAll('#a_a_quote .insert-quote')[0].value = null;
      close_all_popup();
   } else {
      alert('Untuk memasukkan element ini anda hendaklah mengisi field yang disediakan. Isi atau close untuk cancel..');
   }
});
/* END BLOCKQUOTE AREA */

/* TAG AREA */
document.querySelectorAll('#label-box .go')[0].addEventListener('click', function(){
   cari_tag();
});
//var search_tag_kuar;
function cari_tag(){
   //clearTimeout(search_tag_kuar);
   document.getElementsByClassName('label-list')[0].innerHTML = '<li><i>relavan tags appear when you stop typing..</i></li>';
   //search_tag_kuar = setTimeout(function(){
   var carian = document.getElementById('label-search');
   document.getElementsByClassName('label-list')[0].style.display = 'block';
   if(carian.value.length > 2){
      var xhr      = new XMLHttpRequest();
      xhr.open('GET', 'http://media.malayatimes.com/search-tag/ajax?cari='+ encodeURI(carian.value), true);
      xhr.onreadystatechange = function() {
         if (xhr.readyState === 4 && xhr.status === 200){
            var data = xhr.responseText;
            if(data){
               var check_berapa = document.getElementsByClassName('label-list')[0].innerHTML;
               document.getElementsByClassName('label-list')[0].innerHTML = data;
               label_list(check_berapa);
            }
         }
      }
      xhr.send();
   } else {
      document.getElementsByClassName('label-list')[0].innerHTML = '<li><i>atleast 3 huruf..</i></li>';
   }
   //}, 2000);
   return false;
}
function label_list(check_berapa){
   var waitUntil = function (fn, condition, interval) {
      interval = interval || 100;
      var shell = function () {
         var timer = setInterval(
            function () {
               var check;

               try { check = !!(condition()); } catch (e) { check = false; }

               if (check) {
                  clearInterval(timer);
                  delete timer;
                  fn();
               }
            },
            interval
         );
      };
      return shell;
   };
   waitUntil(
      function(){
         //-----
         document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/jquery.js\"><\/script>';
         var label_list = document.getElementsByClassName('label-list')[0].querySelectorAll('li');
         for (var i = 0; i < label_list.length; i++) {
            if (typeof window.addEventListener === 'function'){
               (function (index) {
                  label_list[i].addEventListener('click', function(){
                     var tag_val = filter_char(index.textContent);
                     var the_tag_id = tag_val.replace(/ /g, '_');
                     var tag_id = 'tag_value_'+ tag_val+'_id';
                     var tag_id = tag_id.replace(/ /g, '_');
                     if(tag_val.length > 0 && !document.getElementById(tag_id)){

                        var main_form = document.createElement('input');
                        main_form.setAttribute('value', tag_val);
                        main_form.setAttribute('name', 'tag[]');
                        main_form.setAttribute('id', tag_id);
                        main_form.setAttribute('readonly', 'true');
                        main_form.setAttribute('type', 'hidden');
                        var form_elem = document.getElementsByClassName('main-form')[0];
                        form_elem.insertBefore(main_form, form_elem.childNodes[0]);

                        var draft_hidden = document.createElement('input');
                        draft_hidden.setAttribute('value', tag_val);
                        draft_hidden.setAttribute('name', 'tag[]');
                        draft_hidden.setAttribute('id', 'draft_'+tag_id);
                        draft_hidden.setAttribute('readonly', 'true');
                        draft_hidden.setAttribute('type', 'hidden');
                        var draft_form = document.getElementById('draft-form');
                        draft_form.insertBefore(draft_hidden, draft_form.childNodes[0]);

                        labelresult = document.createElement('span');
                        labelresult.setAttribute('class', 'selected-label '+ the_tag_id);
                        labelresult.setAttribute('title', 'click to remove');
                        labelresult.setAttribute('data-val', tag_val);
                        labelresult.innerHTML = tag_val+'<i class="fa fa-times" aria-hidden="true"></i>';
                        var labelresult_elem = document.getElementById('labelresult');
                        labelresult_elem.insertBefore(labelresult, labelresult_elem.childNodes[0]);

                        document.getElementsByClassName('label-list')[0].style.display = 'none';
                        document.getElementById('label-search').value = null;
                        listen_remove_label();
                     } else {
                        alert('Dilarang mengulangi Tag yang sama..');
                     }
                  });
               })(label_list[i]);
            }
         }
         //-----
      },
      function(){
         // the code that tests here... (return true if test passes; false otherwise)
         var check_border = document.getElementsByClassName('label-list')[0].innerHTML;
         return !!(check_border !== check_berapa);
         //return !!(document.getElementsByClassName('pics')[0].innerHTML !== '');
      },
      50 // amout to wait between checks
   )();
}

document.getElementById('label-search').addEventListener('input',function(){
   cari_tag();
});

var hide_list;
document.getElementsByClassName('label-list')[0].addEventListener('mouseleave', function(){
   clearTimeout(hide_list);
   hide_list = setTimeout(function(){
      document.getElementsByClassName('label-list')[0].style.display = 'none';
   }, 500);
});

function listen_remove_label(){
   var selected_label = document.querySelectorAll('#labelresult .selected-label');
   if(selected_label){
      for (var i = 0; i < selected_label.length; i++) {
         if (typeof window.addEventListener === 'function'){
            (function (index) {
               selected_label[i].addEventListener('click', function(e){
                  var tag_val = index.getAttribute('data-val');
                  var tag_val = filter_char(tag_val);
                  var the_tag_id = tag_val.replace(/ /g, '_');
                  var tag_id = 'tag_value_'+ tag_val+'_id';
                  var tag_id = tag_id.replace(/ /g, '_');
                  // remove selected inside labelresult...
                  var el = document.querySelectorAll('#labelresult .'+the_tag_id)[0];
                  el.parentNode.removeChild( el );
                  //remove .main-form tag input copy...
                  var ele = document.getElementById(tag_id);
                  ele.parentNode.removeChild(ele);
                  //remove #draft-form tag input copy...
                  var elem = document.getElementById('draft_'+tag_id);
                  elem.parentNode.removeChild(elem);
               });
            })(selected_label[i]);
         }
      }
   }
}


function filter_char(data){
   return data.replace(/[&\/\\#,+()$~%.!\@\^\_\=\-\[\]\"\;\`\'\|:*?<>{}]/g, '');
}
/* END TAG AREA */


//AJAX UPLOAD AREA.
document.getElementById('ajaxupload').addEventListener('submit', function(e) {
   e.preventDefault();
   run_ajax(this);
});

function run_ajax(getit){
   var plus = document.querySelectorAll('.img_wrpr').length;
   var old = document.querySelectorAll('.pics .border').length;
   var total = parseFloat(plus) + parseFloat(old);
   var waitUntil = function (fn, condition, interval) {
      interval = interval || 100;
      var shell = function () {
         var timer = setInterval(
            function () {
               var check;

               try { check = !!(condition()); } catch (e) { check = false; }

               if (check) {
                  clearInterval(timer);
                  delete timer;
                  fn();
               }
            },
            interval
         );
      };
      return shell;
   };
   waitUntil(
      function(){
         after_html_changed(); // only this way will made your js working for xhr element!
      },
      function(){
         // the code that tests here... (return true if test passes; false otherwise)
         var check_border = document.querySelectorAll('.pics .border');
         return !!(check_border.length === total);
         //return !!(document.getElementsByClassName('pics')[0].innerHTML !== '');
      },
      50 // amout to wait between checks
   )();
   document.getElementById('jumlah-file').innerHTML = '0';
   var formData = new FormData(getit);
   var xhr      = new XMLHttpRequest();
   xhr.open('POST', 'http://media.malayatimes.com/upload/', true);
   xhr.onreadystatechange = function() {
      if ( xhr.readyState === 4 && xhr.status === 200 ) {
         var data = xhr.responseText;
         if(data){
            var fake_div = document.createElement('div');
            fake_div.innerHTML = data;
            console.log('fake_div');
            console.log(fake_div);
            var border_wrapper = fake_div.getElementsByClassName('border');
            if(border_wrapper){
               console.log(border_wrapper.length);
               for (var i = 0; i < border_wrapper.length; i++) {
                  document.getElementById('pass-thumbnail').value = border_wrapper[i].outerHTML;
                  console.log('value pass-thumbnail = ' + document.getElementById('pass-thumbnail').value);
                  if(!check_pics()){
                     var pics = document.getElementsByClassName('pics')[0];
                     pics.innerHTML = border_wrapper[i].outerHTML + pics.innerHTML;
                     seo_pics();
                     //after_html_changed();
                  }
               }
            }
         }
      }
   }
   xhr.send( formData );
   document.getElementsByClassName('upload-button')[0].setAttribute('disabled', 'disabled');
   document.getElementById('before-upload').innerHTML = '';
}

function after_html_changed(){
   document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/jquery.js\"><\/script>';
   var brdr = document.getElementsByClassName('pics')[0].querySelectorAll('div');
   for(var i = 0; i < brdr.length; i++) {
      if (typeof window.addEventListener === 'function'){
         (function (index) {
            brdr[i].addEventListener('click', function(){
               console.log(brdr.length);
               var Klas = index.className;//index.getAttribute('class');
               console.log(Klas.trim());
               if(Klas.trim() == 'border'){//true.
                  index.setAttribute('class', 'border selected');
               } else {
                  index.setAttribute('class', 'border');
               }
               var jumlah = document.getElementsByClassName('pics')[0].querySelectorAll('.selected');
               if(jumlah){
                  if(jumlah.length > 0){
                     document.getElementById('img-insert-btn').removeAttribute('disabled');
                  } else {
                     document.getElementById('img-insert-btn').setAttribute('disabled', 'disabled');
                  }
               }
               document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
            });
         })(brdr[i]);
      }
   }
   // insert_gambar();
}

//CARI GAMBAR AJAX AREA.
var search_gambar;
document.getElementsByClassName('cari-gambar')[0].addEventListener('keydown', function(){
   var carian = this.value;
   if(carian && carian.length > 2){
      clearTimeout(search_gambar);
      var check_berapa = false;
      search_gambar = setTimeout(function() {
         var xhr      = new XMLHttpRequest();
         xhr.open('POST', 'http://media.malayatimes.com/search_image/ajax?cari='+ encodeURI(carian), true);
         xhr.onreadystatechange = function() {
            if ( xhr.readyState === 4 && xhr.status === 200 ) {
               var ddaattaa = xhr.responseText;
               if(ddaattaa){
                  var check_berapa = (ddaattaa.match(/class=\"pale\"/g)||[]).length;
                  document.getElementById('cari-gambar-result').innerHTML = ddaattaa;
                  document.getElementById('cari-gambar-result').style.display = 'block';
                  after_cari_gambar(check_berapa);
               }
            }
         }
         xhr.send();
      }, 1000);

   }// end if carian..
});

function after_cari_gambar(check_berapa){
   if(check_berapa){
      if(check_berapa != 0){
         var waitUntil = function (fn, condition, interval) {
            interval = interval || 100;
            var shell = function () {
               var timer = setInterval(
                  function () {
                     var check;

                     try { check = !!(condition()); } catch (e) { check = false; }

                     if (check) {
                        clearInterval(timer);
                        delete timer;
                        fn();
                     }
                  },
                  interval
               );
            };
            return shell;
         };
         waitUntil(
            function(){
               var pale = document.querySelectorAll('.picca .pale');
               if(pale){
                  for (var i = 0; i < pale.length; i++) {
                     if (typeof window.addEventListener === 'function'){
                        (function (index) {

                           pale[i].addEventListener('click', function(e){
                              e.preventDefault();
                              index.setAttribute('class', 'border');
                              // set thumbnail seo and viral..
                              document.getElementById('pass-thumbnail').value = index.getElementsByTagName('img')[0].outerHTML;
                              console.log(check_pics());
                              if(!check_pics()){
                                 var pics = document.getElementsByClassName('pics')[0];
                                 pics.innerHTML = index.outerHTML + pics.innerHTML;
                                 index.outerHTML = '';
                                 seo_pics();
                                 after_html_changed();
                              } else {
                                 index.setAttribute('class', 'pale');
                                 index.outerHTML = '';
                              }

                           });
                        })(pale[i]);
                     }
                  }
               }
            },
            function(){
               // the code that tests here... (return true if test passes; false otherwise)
               var check_border = document.querySelectorAll('.picca .pale');
               return !!(check_border.length === check_berapa);
               //return !!(document.getElementsByClassName('pics')[0].innerHTML !== '');
            },
            50 // amout to wait between checks
         )();
      }
   }
}

function check_pics(){ // check_pics() will true jika ada. false jika xde
   var gambar = document.getElementById('pass-thumbnail').value;
   var pics = document.getElementsByClassName('pics')[0];
   if(pics){
      var img = pics.getElementsByTagName('img');
      if(img){
         var img_array = new Array();
         for (var i = 0; i < img.length; i++) {
            img_array.push(img[i].getAttribute('src').trim());
         }
      }
      var wrap_gambar = document.createElement('div');
      wrap_gambar.innerHTML = gambar;
      var gambir = wrap_gambar.getElementsByTagName('img')[0].getAttribute('src').trim();
      var check_array = img_array.indexOf(gambir) > -1;
      return check_array; // true jika ada. false jika xde
   }
}




function seo_pics(){

   var gambar = document.getElementById('pass-thumbnail').value;

   //masukkan data ke SEO  and VIRAL meta..
   var seoThumbnails = document.getElementById('seo-thumbnails');
   var viralThumbnails = document.getElementById('viral-thumbnails');
   var postThumbnails = document.getElementById('post-thumnails');

   var id_array = [postThumbnails,seoThumbnails,viralThumbnails];
   for (var i = 0; i < id_array.length; i++) {
      var div = document.createElement('div');
      div.innerHTML = gambar;
      var img_path = div.getElementsByTagName('img')[0].getAttribute('src').trim();
      var post_path = document.getElementById('post-thumbnails-path');
      var seo_path = document.getElementById('seo-thumbnails-path');
      var viral_path = document.getElementById('viral-thumbnails-path');

      if(i == 0){
         if(post_path.value == '' || post_path.value == null){//overide make it selected then set input value..
            div.className = 'post_pics selected';
            post_path.value = img_path;
         } else {
            div.className = 'post_pics';
         }
      }
      if(i == 1){
         if(seo_path.value == '' || seo_path.value == null){//overide make it selected then set input value..
            div.className = 'seo_pics selected';
            seo_path.value = img_path;
         } else {
            div.className = 'seo_pics';
         }
      }
      if(i == 2) {
         if(viral_path.value == '' || viral_path.value == null){
            div.className = 'viral_pics selected';
            viral_path.value = img_path;
         } else {
            div.className = 'viral_pics';
         }
      }
      console.log(div);
      id_array[i].insertBefore(div,  id_array[i].childNodes[0]);
   }
   console.log(id_array);

   //show thumbnail meta..
   var tethumbnail = document.getElementsByClassName('tethumbnail');
   for (var i = 0; i < tethumbnail.length; i++) {
      tethumbnail[i].style.display = 'block';
   }

   var seo_thumbs = document.getElementsByClassName('seo_pics');
   var viral_thumbs = document.getElementsByClassName('viral_pics');
   var post_thumbs = document.getElementsByClassName('post_pics');
   var waitUntil = function (fn, condition, interval) {
      interval = interval || 100;
      var shell = function () {
         var timer = setInterval(
            function () {
               var check;

               try { check = !!(condition()); } catch (e) { check = false; }

               if (check) {
                  clearInterval(timer);
                  delete timer;
                  fn();
               }
            },
            interval
         );
      };
      return shell;
   };
   waitUntil(
      function(){
         //-----
         // reload this script to addEventListener...
         document.getElementById('meteor').innerHTML = '<script type=\"text/javascript\" src=\"../js/meteor.js\"><\/script>';
         console.log('ready');

         onclick_thumbnail_meta();

         //-----
      },
      function(){
         // the code that tests here... (return true if test passes; false otherwise)
         console.log('not ready');
         var check_berapa = document.getElementsByClassName('seo_pics').length;
         var check_lagi_berapa = document.getElementsByClassName('viral_pics').length;
         var check_lagi_lagi_berapa = document.getElementsByClassName('post_pics').length;
         return !!(seo_thumbs!== check_berapa && viral_thumbs !== check_lagi_berapa && post_thumbs !== check_lagi_lagi_berapa);
      },
      50 // amout to wait between checks
   )(); // end waitUntil.

}

function onclick_thumbnail_meta(){
   // once click select POST thumbnail..
   var thumb_post = document.getElementsByClassName('post_pics');
   if(thumb_post){
      for (var ii = 0; ii < thumb_post.length; ii++) {
         if (typeof window.addEventListener === 'function'){
            (function (index) {
               thumb_post[ii].addEventListener('click', function(){
                  //remove selected..
                  remove_post_selected();
                  //add selected class, and set hidden input value: get path only
                  index.className = 'post_pics selected';
                  document.getElementById('post-thumbnails-path').value = index.getElementsByTagName('img')[0].getAttribute('src');
               });
            })(thumb_post[ii]);
         }
      }
   }
   // once click select SEO thumbnail..
   var thumb_seo = document.getElementsByClassName('seo_pics');
   if(thumb_seo){
      for (var ii = 0; ii < thumb_seo.length; ii++) {
         if (typeof window.addEventListener === 'function'){
            (function (index) {
               thumb_seo[ii].addEventListener('click', function(){
                  //remove selected..
                  remove_seo_selected();
                  //add selected class, and set hidden input value: get path only
                  index.className = 'seo_pics selected';
                  document.getElementById('seo-thumbnails-path').value = index.getElementsByTagName('img')[0].getAttribute('src');
               });
            })(thumb_seo[ii]);
         }
      }
   }

   // once click select Viral thumbnail..
   var thumb_viral = document.getElementsByClassName('viral_pics');
   if(thumb_viral){
      for (var ii = 0; ii < thumb_viral.length; ii++) {
         if (typeof window.addEventListener === 'function'){
            (function (index) {
               thumb_viral[ii].addEventListener('click', function(){
                  //remove selected..
                  remove_viral_selected();
                  //add selected class, and set hidden input value: get path only
                  index.className = 'viral_pics selected';
                  document.getElementById('viral-thumbnails-path').value = index.getElementsByTagName('img')[0].getAttribute('src');
               });
            })(thumb_viral[ii]);
         }
      }
   }
}

function remove_post_selected(){
   var postThumbnails = document.getElementById('post-thumnails');
   var post_selected = postThumbnails.querySelectorAll('.selected');
   if(post_selected[0]){
      post_selected[0].className = 'post_pics';
   }
}

function remove_seo_selected(){
   var seoThumbnails = document.getElementById('seo-thumbnails');
   var seo_selected = seoThumbnails.querySelectorAll('.selected');
   if(seo_selected[0]){
      seo_selected[0].className = 'seo_pics';
   }
}

function remove_viral_selected(){
   var viralThumbnails = document.getElementById('viral-thumbnails');
   var viral_selected = viralThumbnails.querySelectorAll('.selected');
   if(viral_selected[0]){
      viral_selected[0].className = 'viral_pics';
   }
}


document.getElementById('img-insert-btn').addEventListener('click', function(){
   var jumlah = document.getElementsByClassName('pics')[0].querySelectorAll('.selected img');
   if(jumlah){
      if(jumlah.length == 1){
         build_single_image(jumlah);
      } else {
         build_gallery_image(jumlah);
      }
   }
});

function build_single_image(jumlah){
   var title = jumlah[0].getAttribute('alt');
   var path = jumlah[0].getAttribute('src');
   var path = path.substr(path.lastIndexOf('/') + 1);
   var myhtml = '<a href="'+'//media.malayatimes.com/gambar/' + path + '" title="'+ title +'" class="single_image"><img alt="'+ title +'" src="'+'//media.malayatimes.com/gambar/' + path + '"/></a>';
   var selectPastedContent = myhtml;
   console.log('%cBUILD SINGLE IMAGE', 'color:brown;font-weight:600;');
   console.log(selRange.startContainer);
   pasteHtmlAtCaret('<div class="para">'+myhtml+'</div>','insert');
   return false;
}

function build_gallery_image(jumlah){
   var figure = document.createElement('figure');
   figure.className = 'gallery';
   for (var i = 0; i < jumlah.length; i++) {
      var title = jumlah[i].getAttribute('alt');
      var path = jumlah[i].getAttribute('src');
      var path = path.substr(path.lastIndexOf('/') + 1);
      var a = document.createElement('a');
      a.className = 'gallery_image';
      a.setAttribute('href', '//media.malayatimes.com/gambar/' + path);
      a.setAttribute('title', title);
      a.innerHTML = '<img alt="'+ title +'" src="'+'//media.malayatimes.com/image/' + path + '"/>';
      figure.appendChild(a);
   }
   pasteHtmlAtCaret('<div class="para">'+figure.outerHTML+'</div>','insert');
   return false;
}

//example path_gambar('http://media.malayatimes.com/picker/try-gambar-with-caption.jpg')
// function path_gambar(link_gambar){
// var filename = link_gambar.substring(link_gambar.lastIndexOf('/')+1);
// return filename;
// }
/* END GAMBAR AREA */






/* VIDEO AREA */
function videoid(x){
   var url = x.toString();
   var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
   if(videoid != null) {
      return videoid[1];
   } else {
      return false;
   }
}

var check_bedio;
document.getElementById('youtube-reader').addEventListener('input', function(e){
   //e.preventDefault();
   clearTimeout(check_bedio);
   check_bedio = setTimeout(function() {
      var ee = document.getElementById('youtube-reader');
      if(ee.value.length > 3 && videoid(ee.value)){
         document.getElementsByClassName('embed-video')[0].innerHTML = '<iframe src="https://www.youtube.com/embed/'+videoid(ee.value)+'?autohide=1&amp;rel=0&amp;showinfo=0" allowfullscreen="true" class="video"></iframe>';
         document.getElementById('a_a_video').querySelector('.insert').removeAttribute('disabled');
      } else {
         document.getElementById('a_a_video').querySelector('.insert').setAttribute('disabled','disabled');
         document.getElementsByClassName('embed-video')[0].innerHTML = '';
      }
   }, 1000);
});

document.querySelectorAll('#a_a_video .insert')[0].addEventListener('click', function(e){
   e.preventDefault();
   pasteHtmlAtCaret('<div class="para">'+document.getElementsByClassName('embed-video')[0].innerHTML+'</div>','insert');
   document.getElementById('a_a_video').querySelector('.insert').setAttribute('disabled','disabled');
   document.getElementsByClassName('embed-video')[0].innerHTML = '';
   document.getElementById('youtube-reader').value = '';
});
/* END VIDEO AREA */




/* SELECT CATEGORY AREA*/
document.getElementById('category-search').addEventListener('click', function(){
   document.getElementsByClassName('category-list')[0].style.display = 'block';
});
document.getElementsByClassName('category-list')[0].addEventListener('mouseleave',function(){
   document.getElementsByClassName('category-list')[0].style.display = 'none';
});
var li = document.getElementsByClassName('category-list')[0].querySelectorAll('li');
for (var i = 0; i < li.length; i++) {
   if (typeof window.addEventListener === 'function'){
      (function (index) {
         li[i].addEventListener('click', function(){
            document.getElementById('category-search').value = index.getAttribute('data-domain');
            //new_line();
         });
      })(li[i]);
   }
}




/* META BOX AREA */

// SEO meta title.
var seoTitle = document.getElementById('seo-title');
if(seoTitle){
   seoTitle.addEventListener('input', function(){
      document.getElementById('title-count').textContent = this.value.length;
   });
}

// SEO meta description.
var metaDescription = document.getElementById('meta-description');
if(metaDescription){
   metaDescription.addEventListener('input', function(){
      document.getElementById('description-count').textContent = this.value.length;
   });
}

// Viral meta title.
var viralTitle = document.getElementById('viral-title');
if(viralTitle){
   viralTitle.addEventListener('input', function(){
      document.getElementById('viral-title-count').textContent = this.value.length;
   });
}

// Viral meta description.
var viral_metaDescription = document.getElementById('viral-meta-description');
if(viral_metaDescription){
   viral_metaDescription.addEventListener('input', function(){
      document.getElementById('viral-description-count').textContent = this.value.length;
   });
}

// V_S_Thumb select..
var vsThumb = document.getElementsByClassName('v_s_thumb');
if(vsThumb){
   for (var ii = 0; ii < vsThumb.length; ii++) {
      var puc = vsThumb[ii];
      if(puc){
         var pic = puc.getElementsByTagName('div');
         if(pic){
            for (var i = 0; i < pic.length; i++) {
               document.getElementsByClassName(pic[i].className)[i].addEventListener('click', function(){
                  var klas = this.className.replace('selected', '').trim();
                  if(klas == 'seo_pics'){
                     remove_seo_selected();
                     document.getElementById('seo-thumbnails-path').value = this.getElementsByTagName('img')[0].getAttribute('src');
                  } else if(klas == 'viral_pics'){
                     remove_viral_selected();
                     document.getElementById('viral-thumbnails-path').value = this.getElementsByTagName('img')[0].getAttribute('src');
                  } else if(klas == 'post_pics'){
                     remove_post_selected();
                     document.getElementById('post-thumbnails-path').value = this.getElementsByTagName('img')[0].getAttribute('src');
                  } else {
                     alert('there\'s error to detect className of thumbnail.. Please report to Malayatimes Developer. Then you should use another browser (Google Chrome browser is higly recommended).');
                  }
                  this.className = klas + ' selected';
               })
            }
         }
      }
   }
}

/* END META BOX AREA */


/*  AUTO SAVE DRAFT AREA / AUTODRAFT */
function save_draft(path_last){
   var myForm = document.getElementById('draft-form');
   var formData = new FormData(myForm);
   var xhr      = new XMLHttpRequest();
   xhr.open('POST', 'http://media.malayatimes.com/save-draft/' + path_last, true);
   xhr.onreadystatechange = function() {
      if ( xhr.readyState === 4 && xhr.status === 200 ) {
         var data = xhr.responseText;
         if(data){
            console.log(data);
         }
      }
   }
   xhr.send( formData );
}

/* ENN AUTODRAFT */

})();//end
