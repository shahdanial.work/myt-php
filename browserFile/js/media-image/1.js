function run_js(){
   listen_search()
   setup_search()
   listen_slider()
}
run_js()

function setup_search(){
   var cock=rearray_cookies(get_cookie('searched'))
   var suggestBox=document.getElementById('search-suggest')
   var searchBox=document.getElementById('search-input')

   if(cock.length>0){
      searchBox.setAttribute('autocomplete','off')
      for (var i = 0; i < 3; i++) {
         var item =document.createElement('div')
         item.className = 'item recent'
         item.setAttribute('data-val', cock[i])
         item.textContent =  cock[i]
         suggestBox.appendChild(item)
      }
   }
}

var src_time;
function listen_search(){
   var body = document.getElementById('image-page')
   var searchBox = document.getElementById('search-input')
   searchBox.addEventListener('focus',function(){
         add_class(body, 'suggest_on')
   })

   searchBox.addEventListener('focusout',function(){
      src_time = setTimeout(function(){
         clearTimeout(src_time)
         remove_class(body, 'suggest_on')
      },800)
   })
}

function listen_slider(){
   var slider = document.getElementsByClassName('slider')
   if(slider){
      for (var i = 0; i < slider.length; i++) {
               var sliding = slider[i].getElementsByClassName('sliding')
               if(sliding.length > 1){
                  var width = parseFloat(sliding[1].getAttribute('style').replace(/px|\:|\;|\s|left/g,''))
                  if(!isNaN(width)) listen_slide(slider[i], sliding, width)
               }
      }
   }
}

function listen_slide(slider, sliding, width){
   var wrapperWidth = parseFloat(slider.clientWidth)
   if(!isNaN(wrapperWidth)){
      if( width > wrapperWidth ){
         var pergerakan = wrapperWidth
      } else {
         var pergerakan = 0
         var dapat = 0
         while(dapat === 0){
            pergerakan += width
            if(pergerakan > wrapperWidth){
               pergerakan = pergerakan - width;
               dapat = 1;
            }
         }
      }
      var left = slider.getElementsByClassName('slide_left')[0]
      if(left){
         left.addEventListener('click',function(){
            //penambahan left.
            for (var i = 0; i < sliding.length; i++) {
               gerak(sliding[i],pergerakan,false)
            }
         })
      }
      var right = slider.getElementsByClassName('slide_right')[0]
      if(right){
         right.addEventListener('click',function(){
            //penolakan left.
            for (var i = 0; i < sliding.length; i++) {
               gerak(sliding[i],pergerakan,true)
            }
         })
      }
   }
}

function gerak(sliding, pergerakan, nak_tolak){
   if(nak_tolak) var gerak = parseFloat(sliding.getAttribute('style').replace(/px|\:|\;|\s|left/g,'')) - parseFloat(pergerakan);
   else var gerak = parseFloat(sliding.getAttribute('style').replace(/px|\:|\;|\s|left/g,'')) + parseFloat(pergerakan);
   if(!isNaN(gerak)) sliding.setAttribute('style','left:'+gerak+'px;')
}