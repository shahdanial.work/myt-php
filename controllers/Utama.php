<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Utama extends CI_Controller {

  function __construct(){
    parent::__construct();
    $this->load->helper(array('h_user_helper'));
  }


  public function index(){
    if(current_user_id() > 0){
      $this->load->view('v_account/v_dashboard');
    } else {
      $this->load->view('v_account/v_google_login');
    }
  }
}
