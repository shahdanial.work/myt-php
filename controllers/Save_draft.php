<?php

class Save_draft extends CI_Controller {

     function __construct() {
          parent::__construct();
          $this->load->library(array('session', 'user_agent'));
          $this->l_login->cek_login(current_url());
          $this->load->helper(array('security', 'h_user_helper', 'h_link_helper', 'url'));
          $this->load->model(array('m_post', 'm_img', 'm_posts'));
     }

     public function index(){

          // if($this->agent->referrer()){ #mesti ada refferer.
               // $dari = $this->agent->referrer(); // full url..
               // parse_str(parse_url($dari, PHP_URL_QUERY), $queries);
               // $my_from_id = trim($queries['id']);
               // $status_query=  trim($queries['status']);
               // $from_tbl = $status_query . '_tbl';

               // $status = trim($this->input->post('status', FALSE));
               // $from_tbl = $status;
               $my_from_id = trim($this->input->post('id', FALSE));
               $where = array(
                   'id )( =' => $my_from_id
               );
               $from_tbl = query_select('status', 'post_tbl', $where)['0']['status'];

               $current_role = user_info(current_user_id())['user_role'];
               $admin_gang = array('special admin', 'special staff');

               $allowed_action = false;
               if($this->agent->referrer()){ #mesti ada refferer.
                    $dari = $this->agent->referrer();
                    if( allowed_domain($dari) ){
                       if( in_array($current_role, $admin_gang) ){ #admin
                            $allowed_from_tbl = array('draft','pending','publish', 'reviewed', 'banned');
                            // $to_tbl = 'publish';
                            $check_user_id = true;
                       } else { #author biasa
                            $allowed_from_tbl = array('draft','pending', 'reviewed', 'banned');
                            // $to_tbl = 'pending';
                            $check_user_id = current_user_id() == author_post_id($my_from_id, 'post_tbl');
                       }
                       if( in_array($from_tbl, $allowed_from_tbl) && $check_user_id ){
                          $allowed_action = true;
                       }
                    }
               }

               if( $allowed_action ){

                    if($this->input->is_ajax_request()){
                         $redirect = false;
                    } else {
                         $redirect = site_url('post');
                    }

                    date_default_timezone_set("Asia/Kuala_Lumpur");
                    $category = 'http://'. trim($this->security->xss_clean($this->input->post('category')));
                    if(allowed_domain($category)){
                         $category = trim($this->security->xss_clean($this->input->post('category')));
                    } else {
                         $category ='';
                    }

                    $final_first_thumb = '';
                    $semi_first_thumb = trim($this->security->xss_clean($this->input->post('post-thumbnails-path')));
                    if($semi_first_thumb){
                         if(allowed_domain($semi_first_thumb)){
                              $final_first_thumb = pathinfo($semi_first_thumb)['basename'];#ex my-image-path.png
                              if( !file_exists('./gambar/' . $final_first_thumb) ){
                                   $final_first_thumb = '';
                              }
                         }
                    }

                    $final_google_thumb = '';
                    $semi_google_thumb = trim($this->security->xss_clean($this->input->post('seo-thumbnails-path')));
                    if($semi_google_thumb){
                         if(allowed_domain($semi_google_thumb)){
                              $final_google_thumb = pathinfo($semi_google_thumb)['basename'];#ex my-image-path.png
                              if( !file_exists('./gambar/' . $final_google_thumb) ){
                                   $final_google_thumb = '';
                              }
                         }
                    }

                    $final_viral_thumb = '';
                    $semi_viral_thumb = trim($this->security->xss_clean($this->input->post('viral-thumbnails-path')));
                    if($semi_viral_thumb){
                         if(allowed_domain($semi_viral_thumb)){
                              $final_viral_thumb = pathinfo($semi_viral_thumb)['basename'];#ex my-image-path.png
                              if( !file_exists('./gambar/' . $final_viral_thumb) ){
                                   $final_viral_thumb = '';
                              }
                         }
                    }

                    #tulis section.
                    $data = array(
                         'title' => trim( str_replace("\"", "'", $this->security->xss_clean($this->input->post('title')) )),
                         'content' => trim($this->input->post('content', FALSE)),
                         'category' => $category,
                         'tag' => $this->input->post('tag[]', FALSE),
                         'gender' => $this->input->post('gender[]', FALSE),
                         'interest' => $this->input->post('interest[]', FALSE),
                         'meta_title' => trim($this->security->xss_clean($this->input->post('meta_title'))),
                         'from_tbl' => $from_tbl,
                         'to_tbl' => $from_tbl,//sbb save draft
                         'meta_description' => trim($this->security->xss_clean($this->input->post('meta_description'))),

                         'first_img' => $final_first_thumb,

                         #google
                         'meta_title' => trim($this->security->xss_clean($this->input->post('meta_title'))),
                         'meta_description' => trim($this->security->xss_clean($this->input->post('meta_description'))),
                         'google_thumb' => $final_google_thumb,

                         #facebook
                         'viral_title' => trim($this->security->xss_clean($this->input->post('viral_title'))),
                         'viral_description' => trim($this->security->xss_clean($this->input->post('viral_meta_description'))),
                         'viral_thumb' => $final_viral_thumb,

                         'from_id' => $my_from_id,
                         'redirect' => $redirect
                    );


                    $this->m_posts->post_proccess($data);


               } else {# $allowed_action
                    print_r('You action not allowed');
                    // print_r($my_from_id);
                    // print_r('<br>');
                    // print_r($from_tbl);
                    // if( in_array($current_role, $admin_gang) ){
                    //      print_r($my_from_id);
                    //      print_r('<br>');
                    //      print_r($from_tbl);
                    // }
               } # $allowed_action
          // }#mesti ada refferer.

     } // end index function

} //end class
