<?php

class Profile extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->library(array('form_validation', 'session'));
    $this->load->helper(array('url','form','h_user_helper', 'security', 'Whois', 'h_random_helper'));
    $this->load->model(array('m_account'));
    $this->l_login->cek_login(current_url());
  }

  public function index(){

    $this->form_validation->set_rules('nama', 'Nama','required|xss_clean|alpha_numeric_spaces', array(
      'required'      => '<b>%s</b> wajib diisi.',
      'alpha_numeric_spaces'     => '<i>huruf</i>, <i>nombor</i> atau <i>jarak</i> sahaja boleh.'
    ));

    $this->form_validation->set_rules('namapena', 'Nama Pena','required|xss_clean|alpha_numeric', array(
      'required'      => '<b>%s</b> wajib diisi.',
      'alpha_numeric'     => '<i>huruf</i> atau <i>nombor</i> sahaja boleh (tanpa jarak).'
    ));

    // $this->form_validation->set_rules('email','Email','required|valid_email|trim', array(
    //   'required'      => '<b>%s</b> wajib diisi.',
    //   'valid_email'     => '<b>%s</b> anda masukkan tidak valid.'
    // ));

    // if(!empty($this->input->post('password')) || !empty($this->input->post('password_conf'))){
    //   $this->form_validation->set_rules('password','Password','required|trim|alpha_numeric|max_length[15]|min_length[4]',array(
    //     'required'      => '<b>%s</b> wajib diisi jika Ulang Password ditetapkan.',
    //     'alpha_numeric'     => '<i>huruf</i> atau <i>nombor</i> sahaja boleh.',
    //     'max_length' => 'maximum 12 karakter sahaja.',
    //     'min_length' => 'sekurangnya ada 4 karakter.'
    //   ));
    //
    //   $this->form_validation->set_rules('password_conf','Password','required|trim|matches[password]|alpha_numeric', array(
    //     'required'      => '<b>%s</b> wajib diisi jika Password Baru ditetapkan.',
    //     'alpha_numeric'     => '<i>huruf</i> atau <i>nombor</i> sahaja boleh.',
    //     'matches' => 'ulangan <b>%s</b> tidak sepadan.'
    //   ));
    // }

    #Validiation Soalan..
    if(!empty($this->input->post('soalan')) || !empty($this->input->post('jawapan'))){
      $this->form_validation->set_rules('soalan', 'Soalan','required|xss_clean|alpha_numeric_spaces|max_length[60]', array(
        'required'      => '<b>%s</b> wajib diisi jika jawapan telah ditetapkan.',
        'alpha_numeric_spaces'     => '<i>huruf</i>, <i>nombor</i> atau <i>jarak</i> sahaja boleh.',
        'max_length' => 'maximum 60 karakter sahaja.'
      ));

      #Validiaton jawapan bg soalan diatas.
      $this->form_validation->set_rules('jawapan', 'Jawapan','required|xss_clean|alpha_numeric_spaces|max_length[30]', array(
        'required'      => '<b>%s</b> wajib diisi jika Soalan telah ditetapkan.',
        'alpha_numeric_spaces'     => '<i>huruf</i>, <i>nombor</i> atau <i>jarak</i> sahaja boleh.',
        'max_length' => 'maximum 30 karakter sahaja.'
      ));
    }

    if($this->form_validation->run() == FALSE) {

      $this->load->view('v_profile');

    }else{

      // $newpassword = $this->input->post('password', TRUE);
      // $ulangpassword = $this->input->post('password_conf', TRUE);
      // if(!empty($newpassword) && !empty($ulangpassword) && $newpassword == $ulangpassword){
      //   $data['password'] = $newpassword;
      // }
      $data['nama'] = $this->input->post('nama', TRUE);
      // $data['email'] = $this->input->post('email', TRUE);
      $data['namapena'] = $this->input->post('namapena', TRUE);
      $data['soalan'] = $this->input->post('soalan', TRUE);
      $data['jawapan'] = $this->input->post('jawapan', TRUE);

      $this->m_account->check_update_profile($data);
    }

  } // end index function
} // end class HAHAHA
