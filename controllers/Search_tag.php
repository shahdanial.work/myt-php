<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//used in wysiwyg-editor.

class Search_tag extends CI_Controller{

   public function __construct()
   {
      parent::__construct();
      $this->load->library(array('session', 'user_agent'));
      $this->l_login->cek_login(current_url());
      $this->load->helper(array('security', 'h_link_helper', 'url'));
      set_status_header('404');
   }

   function index() {
      $this->load->view('404');
   }

   function ajax(){

      if($this->agent->referrer()){ #mesti ada refferer.
         $dari = $this->agent->referrer();
         if(allowed_domain($dari)){
            $this->output->set_header('Content-type: text/plain; charset=utf-8');
            parse_str(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $queries);
            if ( isset($queries['cari']) && !empty($queries['cari']) ){
               set_status_header('200');
               $data = $queries;
               $this->load->view('v_search_tag/v_ajax', $data);
            } else {
               $this->load->view('404');
            }

         }#allowed_domain
      }#refferer

   }#end of function.


}
