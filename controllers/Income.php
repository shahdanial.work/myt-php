<?php

class Income extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->library('session');
    $this->l_login->cek_login(current_url());
    $this->output->set_header('X-Robots-Tag: noindex');
    set_status_header('403');
  }

  public function index(){
    $this->load->helper('h_user_helper');
    $this->load->view('v_income');
  }
}
