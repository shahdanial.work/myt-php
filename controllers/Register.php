<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

     function __construct(){
          parent::__construct();
          $this->load->library(array('form_validation', 'session'));
          $this->load->helper(array('url','form','security','Whois', 'h_random_helper'));
          $this->load->model('m_account');
     }

     public function index() {

          $this->load->view('v_account/v_google_register');

/*
          $this->form_validation->set_rules('nama', 'Nama','required|xss_clean|alpha_numeric_spaces',
         array(
              'required'      => '<b>%s</b> wajib diisi',
              'alpha_numeric_spaces'     => '<i>huruf</i>, <i>nombor</i> atau <i>jarak</i> sahaja boleh'
         )
   );//|xss_clean|alpha_numeric_spaces');
   $this->form_validation->set_rules('namapena', 'Nama Pena','required|xss_clean|alpha_numeric_spaces',
   array(
         'required'      => '<b>%s</b> wajib diisi',
         'alpha_numeric_spaces'     => '<i>huruf</i>, <i>nombor</i> atau <i>jarak</i> sahaja boleh'
   )
);//|xss_clean|alpha_numeric_spaces');//do not trim this thing, because we want to get Username[with_space], then url username[remove_space, texttransform_lower]
$this->form_validation->set_rules('email','Email','required|valid_email|trim',
array(
   'required'      => '<b>%s</b> wajib diisi',
   'valid_email'     => '<b>%s</b> anda masukkan tidak valid'
)
);//|valid_email|trim');
$this->form_validation->set_rules('password','Password','required|trim|alpha_numeric|max_length[15]|min_length[4]',
array(
   'required'      => '<b>%s</b> wajib diisi',
   'alpha_numeric'     => '<i>huruf</i> atau <i>nombor</i> sahaja boleh',
   'max_length' => 'maximum 12 karakter sahaja',
   'min_length' => 'sekurangnya ada 4 karakter'
)
);//|trim||alpha_numeric');
$this->form_validation->set_rules('password_conf','Password','required|trim|matches[password]|alpha_numeric',
array(
   'required'      => '<b>%s</b> wajib diisi',
   'alpha_numeric'     => '<i>huruf</i> atau <i>nombor</i> sahaja boleh',
   'matches' => 'ulangan <b>%s</b> tidak sepadan'
)
);//|trim|matches[password]');

if($this->session->flashdata('err')){
   $alert['err'] = $this->session->flashdata('err');
   $alert['data'] = $this->session->flashdata('data');
} else {
   $alert = '';
}

if($this->form_validation->run() == FALSE) {
   //print_r('form run flase');
   $this->load->view('v_account/v_register', $alert);//$view;
   //print_r(form_error());
}else{
   if(filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)){
         $data['nama']   =    $this->input->post('nama', TRUE);
         $data['namapena']   =    $this->input->post('namapena', TRUE);
         $data['email']  =    $this->input->post('email', TRUE);
         $data['password'] =    $this->input->post('password', TRUE);
         $this->m_account->check_daftar($data);
   } else {
         //print_r('email not valid');
         $this->load->view('v_account/v_register', $alert);
   }
}//run false else
*/


}


} //end class */
