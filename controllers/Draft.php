<?php

class Draft extends CI_Controller {

   function __construct() {
      parent::__construct();
      $this->load->library(array('session', 'user_agent'));
      $this->l_login->cek_login(current_url());
   }

   public function index(){
    $this->session->set_flashdata('from_tbl', 'draft');
    redirect(site_url('post'));
   }
}