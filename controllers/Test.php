<?php

/*
* http://www.zyxware.com/articles/5134/how-can-we-slimply-create-a-static-page-in-codeigniter
*/

class Test extends CI_Controller {

  function __construct(){
    parent::__construct();
    $this->load->helper('Whois', 'h_user_helper');
    $this->output->set_header('X-Robots-Tag: noindex');
    //set_status_header('403');
  }

  public function index() {

    //$this->load->view('templates/nav.php', $data);
    $this->load->view('v_private/v_test');
    //$this->load->view('templates/sidebar.php', $data);

  }

  function cuba(){
     print_r(is_admin());
  }

  function test(){
     $data['content'] = '<p class="para">something..</p><div class="para"><figure class="gallery"><a class="gallery_image" href="//media.malayatimes.com/gambar/imam-2017.png" title="try gambar with caption"><img alt="try gambar with caption" src="//media.malayatimes.com/image/imam-2017.png"></a><a class="gallery_image" href="//media.malayatimes.com/gambar/ada-jarak-2017.jpeg" title="try gambar with caption"><img alt="try gambar with caption" src="//media.malayatimes.com/image/ada-jarak-2017.jpeg"></a><a class="gallery_image" href="//media.malayatimes.com/gambar/shah-danial-programmer-square.jpg" title="try gambar with caption"><img alt="try gambar with caption" src="//media.malayatimes.com/image/shah-danial-programmer-square.jpg"></a></figure></div><p class="para">BBB</p><p class="para">It’s unclear whether an act of swatting has ever resulted in someone’s death before, but numerous people in the past have been severely injured in such situations over online feuds that often do not involve the victim in any way whatsoever. SWAT teams also have a disturbing history of killing innocent people in their own homes, regardless of the source of the threat. The alleged perpetuator of the swatting attack, who went by the Twitter handle “SWauTistic” before changing his handle and then deleting his account entirely, reportedly admitted to calling in a false bomb threat against the Federal Communications Commission over the net neutrality decision, according to Krebs.</p><p class="para">In this case, Wichita local Andrew Finch, whose family members say did not play video games and was a father of two young boys, answered his door only to face down a SWAT team-level response. Allegedly, one officer immediately fired upon Finch, who later died at a hospital. It’s unclear why Finch, who is said not to have had a weapon on him, was fired upon. The Wichita Eagle reports that the police department is investigating the issue, which occurred late Thursday night.</p><p class="para">Update at 5:47PM ET, 12/29: Added additional information from independent security journalist Brian Krebs’ report on the swatting and identity of the perpetrator.</p><p class="para">CCC</p>';

$dom = new domDocument;
               libxml_use_internal_errors(true);
               $dom->loadHTML($data['content']);
               libxml_clear_errors();
               $dom->preserveWhiteSpace = false;
               $images = $dom->getElementsByTagName('img');

               for ($i=0; $i < count($images); $i++) {
                  if($images[$i]->getAttribute('srch')){
                  print_r($images[$i]->getAttribute('srch'));
                  } else {
                     print_r('attribute xde');
                  }
               }

//print_r($images->length);
$finder = new DomXPath($dom);
$nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' para ')]");
print_r($nodes->length);
  }



} // end Class
