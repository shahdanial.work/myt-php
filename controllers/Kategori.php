<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kategori extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->library('session');
    $this->l_login->cek_login(current_url());
    $this->output->set_header('X-Robots-Tag: noindex');
  }

  public function index(){
    $this->load->view('v_kategori');
  } // end index function
} // end class HAHAHA
