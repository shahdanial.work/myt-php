<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

  function __construct(){
    parent::__construct();
    $this->load->helper(array('h_link_helper'));
    $this->output->set_header('X-Robots-Tag: noindex');
    //set_status_header('403');
  }

  public function index() {
    $hangpimana = site_url('dashboard');
    if(isset($_SERVER['HTTP_REFERER'])){
      if(allowed_domain($_SERVER['HTTP_REFERER'])){
        $hangpimana = $_SERVER['HTTP_REFERER'];
      }
    }
    $this->l_login->logout($hangpimana);
  }
}
