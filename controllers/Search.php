<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do Moree
    //$this->simple_login->cek_login(current_url());
    //$this->output->set_header(array());
    $this->load->library(array('user_agent'));
    $this->load->helper(array('security', 'h_link_helper', 'url'));
    //$this->load->model(array());
  }

  function index(){

     if($this->agent->referrer()){ #mesti ada refferer.
       $dari = $this->agent->referrer();
       if(allowed_domain($dari)){

          set_status_header('200');
          parse_str(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $queries);

          if ( isset($queries['q']) && !empty($queries['q']) ){
              $query =  $this->db->conn_id->prepare(
                 "SELECT category_tbl.id, category_tbl.meta_title as title, category_tbl.title as alt_title, 'categories' as from_tbl, 'categories' as type, category_tbl.domain as link FROM category_tbl WHERE MATCH(title) AGAINST(:keyword  IN BOOLEAN MODE)

                 UNION DISTINCT

                 SELECT tag_tbl.id, tag_tbl.meta_title as title, tag_tbl.title as alt_title, 'tags' as from_tbl, tag_tbl.type, tag_tbl.tag_path as link FROM tag_tbl WHERE MATCH(title) AGAINST(:keyword  IN BOOLEAN MODE) AND tag_tbl.status = 'publish'

                 UNION DISTINCT

                 SELECT post_tbl.id, post_tbl.meta_title as title, post_tbl.title as alt_title, 'posts' as from_tbl, category_manager.domain as type, post_tbl.path as link

                 FROM post_tbl

                 LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = post_tbl.id

                 WHERE MATCH(title) AGAINST(:keyword  IN BOOLEAN MODE) AND post_tbl.status = 'publish'

                 LIMIT 0,10"
               );

               $query->execute(array(
                  ':keyword' => urlencode($queries['q'])
               ));

               $result = $query->fetchAll(PDO::FETCH_ASSOC);

               if($result){
                  #var_dump($result);
                  $this->output->set_content_type('text/plain', 'UTF-8');
                  echo json_encode($result);
               } else {
                  echo '0';
               }
          } else {
              $this->load->view('404');
          }

       }#allowed_domain
       else {
          #not allowed domain of referrer
          set_status_header('203');
       }
     }#refferer

  }

}
