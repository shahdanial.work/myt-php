<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller{

     public function __construct(){
          parent::__construct();
          $this->load->helper(array('h_query_helper', 'h_random_helper', 'security', 'h_user_helper', 'h_link_helper', 'url'));
          $this->load->model(array('m_post', 'M_upload', 'm_img'));
          $this->load->library(array('upload','user_agent'));
          $this->output->set_header('X-Robots-Tag: noindex');
          set_status_header('200');
          $this->l_login->cek_login(current_url());
     }

     function index(){


        // $status = trim($this->input->post('status', FALSE));
        $my_from_id = trim($this->input->post('id', FALSE));
        $where = array(
            'id )( =' => $my_from_id
        );
        $from_tbl = query_select('status', 'tag_tbl', $where)['0']['status'];


        $current_role = user_info(current_user_id())['user_role'];
        $admin_gang = array('special admin', 'special staff');


        $allowed_action = true;
        if($this->agent->referrer()){ #mesti ada refferer.
             $dari = $this->agent->referrer();
             if( allowed_domain($dari) ){
                if( in_array($current_role, $admin_gang) ){ #admin
                     $allowed_from_tbl = array('draft','pending','publish', 'reviewed', 'banned');
                     $to_tbl = 'publish';
                     $check_user_id = true;
                } else { #author biasa
                     $allowed_from_tbl = array('draft','pending', 'reviewed', 'banned');
                     $to_tbl = 'pending';
                     $check_user_id = current_user_id() == author_post_id($my_from_id, 'post_tbl');
                }
                if( in_array($from_tbl, $allowed_from_tbl) && $check_user_id ){
                   $allowed_action = true;
                }
             }
        }

        if( isset($_FILES['usr_files']['name']) && $allowed_action){
                    set_status_header('200');
                    $res = array();
                    #setup custom field.
                    foreach ($this->input->post('usr_files') as $k => $val) {
                         foreach ($val as $key => $value) {
                              #k adalah key mcm text, tags, etc.
                              $res[$key][$k] = $value;
                         }
                    }


                    #setup file field.
                    foreach ($_FILES['usr_files']['name'] as $key => $name) {
                         #SET UP FILE DATA
                         $res[$key]['key'] = $key;
                         $res[$key]['type'] = $_FILES['usr_files']['type'][$key];
                         $res[$key]['name'] = $_FILES['usr_files']['name'][$key];
                         $res[$key]['tmp_name'] = $_FILES['usr_files']['tmp_name'][$key];
                         $res[$key]['size'] = $_FILES['usr_files']['size'][$key]; //size data. kb mb
                         $res[$key]['error'] = $_FILES['usr_files']['error'][$key];
                         $res[$key]['ext'] = strtolower(pathinfo($res[$key]['name'], PATHINFO_EXTENSION));


                         // $res[$key]['img_size'] = getimagesize($res[$key]['tmp_name']);
                         //
                         // $res[$key]['width'] = $res[$key]['img_size']['0'];
                         // $res[$key]['height'] = $res[$key]['img_size']['1'];

                         $res[$key]['from_id'] = $my_from_id;
                         $res[$key]['from_tbl'] = 'post_tbl';

                         $ext_array = array('png','gif','jpg','JPG', 'webp', 'jpeg');
                         $mime_array = array('image/gif', 'image/png', 'image/jpg', 'image/jpeg','image/GIF', 'image/PNG', 'image/JPG', 'image/jpeg', 'image/webp', 'webp', 'image/WEBP');


                         if(
                              in_array($res[$key]['ext'], $ext_array) &&
                              in_array($res[$key]['type'], $mime_array) &&
                              $res[$key]['error'] == 0 &&
                              $res[$key]['size'] < 1000000
                         ){
                              $res[$key]['security'] = 'safe';
                              $this->M_upload->post_image($res);
                         } else {
                              #return nothing. better to save server usage.
                              $res[$key]['security'] = 'warning';
                         }


                    } #end foreach.

                    // var_dump($res);
                    //
                    // var_dump(
                    //    in_array($res[$key]['ext'], $ext_array) ,
                    //    in_array($res[$key]['type'], $mime_array) ,
                    //    $res[$key]['error'] == 0 ,
                    //    $res[$key]['size'] < 1000000
                    //  );
                    #Stop using res, jadi x pening.



               } else {
                  // $this->output->set_header('Content-type: text/plain; charset=utf-8');
                  // echo 'error';
               }


     } #end function..

}
