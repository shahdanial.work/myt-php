<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

     function __construct(){
          parent::__construct();
          require_once(APPPATH .'vendor/autoload.php');
          putenv('GOOGLE_APPLICATION_CREDENTIALS=./mycredentials.json');
          $client = new Google_Client();
          $client->useApplicationDefaultCredentials();
          $client->setScopes('https://www.googleapis.com/auth/fusiontables');
          $service = new Google_Service_Fusiontables($client);
          $this->load->library(array('form_validation', 'session','user_agent'));
          $this->load->helper(array('h_link_helper','url','form','security', 'h_random_helper', 'h_function_helper'));
          $this->load->model('m_account');
     }

     public function index() {

          $redirect = $this->input->get('redirect');

          if($redirect){
               $this->session->set_flashdata('redirect', $redirect);
          }

          $this->load->view('v_account/v_google_login');


     } // END index

     function google(){

          // echo '<p>Dalam penyelengaraan, system akan up pada pukul 8 pagi, 13 July.</p>';

          #play with redirect
          if($this->session->flashdata('redirect')){
               $this->session->set_flashdata('redirect', $this->session->flashdata('redirect'));
          }


          // Fill CLIENT ID, CLIENT SECRET ID, REDIRECT URI from Google Developer Console
          $client_id = '420084995003-ekp8ctt44kkpeq31h4j0v4rhrfu4v90o.apps.googleusercontent.com';//'896445828432-bseksi0205a8rio0mro7d6082q4bja17.apps.googleusercontent.com';
          $client_secret = '0-X5cD57tokcs6Z4W5Ywn0Xr';//'q0uh7v_HnwY0ji5TfoMSrwfw';
          $redirect_uri = base_url('login/google_login'); #must be this

          //Create Client Request to access Google API
          $client = new Google_Client();
          $client->setApplicationName("Malaya Times Press");
          $client->setClientId($client_id);
          $client->setClientSecret($client_secret);
          $client->setRedirectUri($redirect_uri);
          $client->addScope("email");
          $client->addScope("profile");

          //Send Client Request
          $objOAuthService = new Google_Service_Oauth2($client);

          $authUrl = $client->createAuthUrl();

          header('Location: '.$authUrl);
     } // END GLOGIN


     function google_login(){

          // Fill CLIENT ID, CLIENT SECRET ID, REDIRECT URI from Google Developer Console
          $client_id = '420084995003-ekp8ctt44kkpeq31h4j0v4rhrfu4v90o.apps.googleusercontent.com';//'896445828432-bseksi0205a8rio0mro7d6082q4bja17.apps.googleusercontent.com';
          $client_secret = '0-X5cD57tokcs6Z4W5Ywn0Xr';//'q0uh7v_HnwY0ji5TfoMSrwfw';
          $redirect_uri = base_url('login/google_login');

          //Create Client Request to access Google API
          $client = new Google_Client();
          $client->setApplicationName("Malaya Times Press");
          $client->setClientId($client_id);
          $client->setClientSecret($client_secret);
          $client->setRedirectUri($redirect_uri);
          $client->addScope("email");
          $client->addScope("profile");

          //Send Client Request
          $service = new Google_Service_Oauth2($client);
          $client->authenticate($_GET['code']);
          $_SESSION['access_token'] = $client->getAccessToken();

          #setup redirect to $queries['redirect']
          #play with redirect
          if($this->session->flashdata('redirect')){
               $redirect = $this->session->flashdata('redirect');
               print_r('1\n\n');
          } else {
               $redirect = site_url('dashboard');
               print_r('2\n\n');
               if(isset($_SERVER['HTTP_REFERER'])) {
                    parse_str(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY), $queries); //echo $queries['redirect'];
                    if (isset($queries['redirect']) && !empty($queries['redirect'] && allowed_domain($queries['redirect']))){
                         $redirect = $queries['redirect'];
                         print_r('3\n\n');
                    } else {
                         $redirect = site_url('dashboard');
                         print_r('4\n\n');
                    }
               }
          }

          print_r($redirect);

          //sini

          // User information retrieval starts..............................

          $user = $service->userinfo->get(); //get user info as array, ex: $user->id

          #check if this google+ id or email registered as user
          $check_query = $this->db->conn_id->prepare("SELECT * FROM users WHERE (gplus_id = :gplus_id) OR (LOWER(email) = :email)");
          $check_query->execute(
               array(
                    ':gplus_id' => $user->id,
                    ':email' => strtolower($user->email)
               )
          );
          #LOGIC BEFORE IF check_query->rowCount.
          date_default_timezone_set("Asia/Kuala_Lumpur");
          $masa = date("Y-m-d");
          $USER_REGISTERED = 0; #VERY-VERY NEW USER
          $ROWNAMA = false;
          $ROWJANTINA = false;
          if($user->gender == 'male'){
               $user->gender = 'lelaki';
          } elseif ($user->gender = 'female'){
               $user->gender = 'perempuan';
          } else {
               $user->gender = '';
          }

          if($check_query){
               $re = $check_query->fetchAll(PDO::FETCH_ASSOC);
               foreach ($re as $row) {
                    if($row['verify'] == 'yes' && $row['gplus_id'] == $user->id){
                         $USER_REGISTERED = 1; #you can login now
                    }
                    if($row['email'] == $user->email  && $row['verify'] == 'yes' && empty($row['gplus_id'])){
                         $USER_REGISTERED = 2;# verified via email, let's update G+ data
                         if(empty($row['nama'])){
                              $ROWNAMA = true;
                         }
                         if(empty($row['jantina'])){
                              $ROWJANTINA = true;
                         }
                    }
                    if($row['email'] == $user->email  && $row['verify'] != 'yes'){
                         $USER_REGISTERED = 3; #Email exist, but not verified.
                         $therow = $row;
                    }
               } // end foreach.
          }

          if($USER_REGISTERED == 1) {
               #you can login now
               $gplus_id = $user->id;
               // if($user->email == 'shahdanial.work@gmail.com'){
               //    var_dump($user);
               // } else {
               $this->l_login->login_google($gplus_id, $redirect);
          } // end $USER_REGISTERED == 1

          if($USER_REGISTERED == 2) {
               // # verified via email, let's update G+ data
               // 1. Set data
               // 2. Send $data to model for update user profile action.
               // 3. Provide $redirect str.
               // 4. After action by model, send $gplus_id and $redirect to login library

               $data['email'] = $user->email; // select table where email
               if($ROWJANTINA){
                    $data['jantina'] = $user->gender;
               }
               $data['gplus_id'] = $user->id;
               if($ROWNAMA){
                    $data['nama'] = $user->name;
               }
               $data['redirect'] = $redirect;
               $this->m_account->update_gplus($data);
          } // end $USER_REGISTERED == 2


          if($USER_REGISTERED == 0) {
               #VERY-VERY NEW USER
               #set username from google.com/+JohnSmith
               if( strpos( $user->link, '/+' ) ){ //because profile link contain '+/' = already set username on it (not id or numeric)
                    $whatIWant = substr($user->link, strpos($user->link, "/+") + 1);
                    $user->username = str_replace( '+', '', $whatIWant );
               } else {
                    $user->username = $user->name;
               }
               #if myusername already exist, user google name: John Smith
               $check_username = $this->db->conn_id->prepare("SELECT namapena FROM users WHERE LOWER(namapena) = :namapena");
               $check_username->execute(array( ':namapena' => strtolower( preg_replace('/\s+/', '', $user->username) ) ));
               if($check_username) {
                    $user->username = $user->name;
               }
               #OMG, if John Smith also exist, generate random JohnR4nd123
               $check_username1 = $this->db->conn_id->prepare("SELECT namapena FROM users WHERE LOWER(namapena) = :namapena");
               $check_username1->execute(array( ':namapena' => strtolower( preg_replace('/\s+/', '', $user->username) ) ));
               if($check_username1) {
                    $user->username = random_generate($user->name);
                    $user->username = $user->username;
               }
               #username done

               $data = array(
                    'nama' => $user->name,
                    'namapena' => strtolower( preg_replace('/\s+/', '', $user->username) ),
                    'email' => $user->email,
                    'password' => random_password(),
                    'gplus_id' => $user->id,
                    'jantina' => $user->gender,
                    'reg_time' => $masa,
                    'verify' => 'yes'
               );
               #DON'T xss_clean
               # end user as $data setup.
               // echo '<pre>';
               // print_r($data);
               // echo '</pre><br>';
               // print_r($redirect);

               $this->m_account->daftar_google($data, $redirect);


          }// end $USER_REGISTERED == 0

          if($USER_REGISTERED == 3) {
               $data = array(
                    'gplus_id' => $user->id,
                    'redirect' => $redirect
               );
               if($therow['nama']){
                    $data['nama'] = $therow['nama'];
               } else {
                    $data['nama'] = $user->name;
               }
               if($therow['jantina']){
                    $data['jantina'] = $therow['jantina'];
               } else {
                    $data['jantina'] = $user->gender;
               }
               $data['id_user'] = $therow['id_user'];

               $b = explode('-', $therow['reg_time']);

               $masa = array_reverse($b);

               $masa['1'] = date("M", mktime(0, 0, 0, $masa['1'], 10));

               $therow['reg_time'] = implode(" ",$masa);
               $row['row'] = $therow;
               $this->load->view('v_account/v_google_verify', $row);

               $this->session->set_flashdata('gdata', $data);
          }// end $USER_REGISTERED == 3
          //sini

     } // END FUNCTION GOOGLE_LOGIN

     function google_verify(){ #check form

          if($this->session->flashdata('gdata') && $this->input->post('jawapan')){
               $jwpn = $this->input->post('jawapan');
               $data = $this->session->flashdata('gdata');
               if($jwpn == 'yes'){
                    #update data from model > then login via lib + redirect
                    $this->m_account->email_verify($data);
               } else {
                    #moved post by this old user to admin > remove old user from table > redirect new user login/google
                    $this->m_account->remove_user($data);
               }
          } else {
               redirect(site_url('login/google'));
          }
     }
} // END CLASS
