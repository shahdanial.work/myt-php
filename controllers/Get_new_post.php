<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Get_new_post extends CI_Controller{

     public function __construct()
     {
          parent::__construct();
          $this->load->library(array('session', 'user_agent'));
          $this->load->helper(array('security', 'h_user_helper', 'h_link_helper', 'url', 'h_query_helper'));
          $this->load->model(array('m_post', 'm_img', 'm_posts'));
     }

     function index(){
          if($this->agent->referrer()){ #mesti ada refferer.

               $dari = $this->agent->referrer(); // full url..

               if(allowed_domain($dari)){
                    set_status_header('200');
                    $this->output->set_header("Content-Type: text/plain charset=UTF-8");
                    $offset = $this->input->post('offset', FALSE); #18
                    $category = $this->input->post('category', FALSE); #artis.malayatimes.com

                    if(allowed_domain($category) && is_numeric($offset)){
                         $query = $this->db->conn_id->prepare(
                              "SELECT post_tbl.title, post_tbl.first_img, post_tbl.path, category_manager.domain as category, users.namapena, group_concat(tag_tbl.tag_path) as tag_path, group_concat(tag_tbl.title) as tag_title, group_concat(tag_tbl.type) as tag_type FROM post_tbl
                              LEFT JOIN category_manager ON category_manager.from_tbl = :post_tbl AND category_manager.from_id = post_tbl.id
                              LEFT JOIN users ON users.id_user = post_tbl.author_id
                              LEFT JOIN tag_manager ON tag_manager.from_tbl = :post_tbl_1 AND tag_manager.from_id = post_tbl.id
                              LEFT JOIN tag_tbl ON tag_tbl.tag_path = tag_manager.tag_path AND tag_tbl.status = 'publish' AND tag_tbl.type != ''
                              WHERE category_manager.domain = :category
                              GROUP BY post_tbl.id
                              ORDER BY post_tbl.id DESC
                              LIMIT  ".$offset." , 8"
                         );
                         $query->execute(array(
                              ':post_tbl' => 'post_tbl',
                              ':post_tbl_1' => 'post_tbl',
                              ':category' => $category
                         ));
                         $result = $query->fetchAll(PDO::FETCH_ASSOC);
                         #print_r($result);
                         if($result){

                              if($result[0] && sizeof($result) == 8){
                                   $data = $result;
                                   //echo '<section class="wrapper rows">';

                                   echo '<div class="recent-articles even big"><div class="content_group_inner_wrapper"> <article class="content-item"><a class="img" href="//'. $data[0]['category'] .'/'.$data[0]['path'].'" style="background:url(//media.malayatimes.com/photo/'.$data[0]['first_img'].') no-repeat"><img alt=" '.$data[0]['title'].' " src="//media.malayatimes.com/photo/'.$data[0]['first_img'].'"></a><div class="header"> <h3><a class="title" href="//'. $data[0]['category'] .'/' . $data[0]['path'] . '">' . $data[0]['title'] . '</a></h3>';
                                   if($data[0]['tag_path'] && $data[0]['tag_title']){
                                        echo '<a class="tag" href="//hub.malayatimes.com/'.$data[0]['tag_path'] .'">'.$data[0]['tag_title'].'</a>';
                                   }
                                   echo '</div></article> </div></div>';

                                        if(isset($data[1])){
                                             echo '<div class="recent-articles odd">';
                                             for ($i=1; $i < sizeof($data); $i++) {
                                                  if($i < 4){
                                                       echo '<article class="recent-article"> <a class="img" href="//'. $data[$i]['category'] .'/' . $data[$i]['path'] . '"> <img alt=" '.$data[$i]['title'].' " src="//media.malayatimes.com/picture/'.$data[$i]['first_img'].'"> </a> <h2><a class="title" href="//'. $data[$i]['category'] . '/' .$data[$i]['path'].'">'.$data[$i]['title'].'</a></h2> ';
                                                       if($data[$i]['tag_title'] && $data[$i]['tag_path']){
                                                            echo '<a class="tag" href="//hub.malayatimes.com/'.$data[$i]['tag_path'] .'">'.$data[$i]['tag_title'].'</a>';
                                                       }
                                                       echo' </article>';
                                                  }
                                             }
                                             echo '</div>';
                                        }

                                        if(isset($data[4])){
                                             echo '<div class="recent-articles even">';
                                             for ($i=4; $i < sizeof($data); $i++) {
                                                  if($i < 7){
                                                       echo '<article class="recent-article"> <a class="img" href="//'. $data[$i]['category'] .'/' . $data[$i]['path'] . '"> <img alt=" '.$data[$i]['title'].' " src="//media.malayatimes.com/picture/'.$data[$i]['first_img'].'"> </a> <h2><a class="title" href="//'. $data[$i]['category'] . '/' .$data[$i]['path'].'">'.$data[$i]['title'].'</a></h2> ';
                                                       if($data[$i]['tag_title'] && $data[$i]['tag_path']){
                                                            echo '<a class="tag" href="//hub.malayatimes.com/'.$data[$i]['tag_path'] .'">'.$data[$i]['tag_title'].'</a>';
                                                       }
                                                       echo' </article>';
                                                  }
                                             }
                                             echo '</div>';
                                        }

                                        if(isset($data[7])){
                                             echo '<div class="recent-articles odd big"><div class="content_group_inner_wrapper"> <article class="content-item"><a class="img" href="//'. $data[7]['category'] .'/'.$data[7]['path'].'" style="background:url(//media.malayatimes.com/photo/'.$data[7]['first_img'].') no-repeat"><img alt=" '.$data[7]['title'].' " src="//media.malayatimes.com/photo/'.$data[7]['first_img'].'"></a><div class="header"> <h3><a class="title" href="//'. $data[7]['category'] .'/' . $data[7]['path'] . '">' . $data[7]['title'] . '</a></h3>';
                                             if($data[7]['tag_path'] && $data[7]['tag_title']){
                                                  echo '<a class="tag" href="//hub.malayatimes.com/'.$data[7]['tag_path'] .'">'.$data[7]['tag_title'].'</a>';
                                             }
                                             echo '</div></article> </div></div>';
                                        }

                                   //echo '</section>';
                              }

                         }
                    } else {
                         set_status_header('304');
                    }

               } else {
                    set_status_header('304');
               }

          } else {
               set_status_header('304');
          }

     }

}
