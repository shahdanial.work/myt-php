<?php

class Submit extends CI_Controller {

     function __construct() {
          parent::__construct();
          $this->load->library(array('session', 'user_agent'));
          $this->l_login->cek_login(current_url());
          $this->load->helper(array('security', 'h_user_helper', 'h_link_helper', 'url', 'h_query_helper'));
          $this->load->model(array('m_post', 'm_img', 'm_posts'));
     }

     public function index(){

          // $status = trim($this->input->post('status', FALSE));
          // $from_tbl = $status;
          $my_from_id = trim($this->input->post('id', FALSE));
          $where = array(
             'id )( =' => $my_from_id
          );
          $from_tbl = query_select('status', 'post_tbl', $where)['0']['status'];

               $current_role = user_info(current_user_id())['user_role'];
               $admin_gang = array('special admin', 'special staff');


               $allowed_action = false;
               if($this->agent->referrer()){ #mesti ada refferer.
                    $dari = $this->agent->referrer();
                    if( allowed_domain($dari) ){
                       if( in_array($current_role, $admin_gang) ){ #admin
                            $allowed_from_tbl = array('draft','pending','publish', 'reviewed', 'banned');
                            $to_tbl = 'publish';
                            $check_user_id = true;
                       } else { #author biasa
                            $allowed_from_tbl = array('draft','pending', 'reviewed', 'banned');
                            $to_tbl = 'pending';
                            $check_user_id = current_user_id() == author_post_id($my_from_id, 'post_tbl');
                       }
                       if( in_array($from_tbl, $allowed_from_tbl) && $check_user_id ){
                          $allowed_action = true;
                       }
                    }
               }


               if( $allowed_action ){ #is_numeric($my_from_id)

                    if($this->input->is_ajax_request()){
                         #xhr request.
                         $redirect = '';
                    } else {
                         #manual submit.
                         $redirect = site_url('post');
                    }

                    date_default_timezone_set("Asia/Kuala_Lumpur");
                    $category = 'http://'. trim($this->security->xss_clean($this->input->post('category')));
                    if(allowed_domain($category)){
                         $category = trim($this->security->xss_clean($this->input->post('category')));
                    } else {
                         $category ='';
                    }

                    $final_first_thumb = '';
                    $semi_first_thumb = trim($this->security->xss_clean($this->input->post('post-thumbnails-path')));
                    if($semi_first_thumb){
                         if(allowed_domain($semi_first_thumb)){
                              $final_first_thumb = pathinfo($semi_first_thumb)['basename'];#ex my-image-path.png
                              if( !file_exists('./gambar/' . $final_first_thumb) ){
                                   $final_first_thumb = '';
                              }
                         }
                    }

                    $final_google_thumb = '';
                    $semi_google_thumb = trim($this->security->xss_clean($this->input->post('seo-thumbnails-path')));
                    if($semi_google_thumb){
                         if(allowed_domain($semi_google_thumb)){
                              $final_google_thumb = pathinfo($semi_google_thumb)['basename'];#ex my-image-path.png
                              if( !file_exists('./gambar/' . $final_google_thumb) ){
                                   $final_google_thumb = '';
                              }
                         }
                    }

                    $final_viral_thumb = '';
                    $semi_viral_thumb = trim($this->security->xss_clean($this->input->post('viral-thumbnails-path')));
                    if($semi_viral_thumb){
                         if(allowed_domain($semi_viral_thumb)){
                              $final_viral_thumb = pathinfo($semi_viral_thumb)['basename'];#ex my-image-path.png
                              if( !file_exists('./gambar/' . $final_viral_thumb) ){
                                   $final_viral_thumb = '';
                              }
                         }
                    }

                    #tulis section.
                    $data = array(
                         'title' => trim( str_replace("\"", "'", $this->security->xss_clean($this->input->post('title')) )),
                         'content' => trim($this->input->post('content', FALSE)),
                         'category' => $category,
                         'tag' => $this->input->post('tag[]'),
                         'gender' => $this->input->post('gender[]', FALSE),
                         'interest' => $this->input->post('interest[]', FALSE),
                         'meta_title' => trim($this->security->xss_clean($this->input->post('meta_title'))),
                         'from_tbl' => $from_tbl,
                         'to_tbl' => $to_tbl,
                         'meta_description' => trim($this->security->xss_clean($this->input->post('meta_description'))),

                         'first_img' => $final_first_thumb,

                         #google
                         'meta_title' => trim($this->security->xss_clean($this->input->post('meta_title'))),
                         'meta_description' => trim($this->security->xss_clean($this->input->post('meta_description'))),
                         'google_thumb' => $final_google_thumb,

                         #facebook
                         'viral_title' => trim($this->security->xss_clean($this->input->post('viral_title'))),
                         'viral_description' => trim($this->security->xss_clean($this->input->post('viral_meta_description'))),
                         'viral_thumb' => $final_viral_thumb,

                         'from_id' => $my_from_id,
                         'redirect' => $redirect
                    );



                    /* Start error_msg */
                    $error_msg = array();
                    if($data['content']){



                         if(in_array($current_role, $admin_gang)){ /* for admin only: publish purpose.. */
                              #CHECK TITLE
                              if(strlen($data['title']) < 3){
                                   array_push($error_msg, '<li>Minimum Title ada 3 aksara.</li>');
                              } elseif(strlen($data['title']) > 60 && empty($data['meta_title'])){
                                   array_push($error_msg, '<li>Memandangkan <span class="red">Title</span> post anda melebihi 60 aksara, anda hendaklah menetapkan <span class="red">SEO Title</span>.</li>');
                              } elseif(strlen($data['title']) > 100){
                                   array_push($error_msg, '<li><span class="red">Title</span> post tidak boleh melebihi 100 aksara.</li>');
                              }

                              if(strlen($data['meta_title']) > 60){
                                   array_push($error_msg, '<li><b>SEO Title</b> tidak boleh melebihi 60 aksara</li>');
                              } elseif (!empty($data['meta_title']) && strlen($data['meta_title']) < 3){
                                   array_push($error_msg, '<li><b>SEO Title</b> tidak boleh kurang dari 3 aksara</li>');
                              }


                              ////////////// PEMERIKSAAN TITLE UTAMA (META_TITLE/TITLE) ///////////////


                              if(!empty($data['category'])){

                                 if(!empty($data['meta_title'])){
                                      $title_utama = $data['meta_title'];
                                      $msg_duplicate_title = '<li><b>SEO Title</b> yang anda tetapkan telah digunakan di post lain. Sila ubah agar berlainan <span class="red">(disarankan)</span>, atau biarkan kosong.</li>';
                                 } else {
                                      $title_utama = $data['title'];
                                      $msg_duplicate_title = '<li><b>Title Post</b> yang anda tetapkan telah digunakan di post lain. Sila ubah <b>Title Post</b> atau tetapkan <b>SEO Title</b> yang unik <span class="red">(disarankan)</span>.</li>';
                                 }

                                 #checking title make sure not duplicate in post_tbl

                                 $check_title_record = false;

                                 if($data['from_tbl'] == 'publish' && $data['to_tbl'] == 'publish'){ #post_tbl TO post_tbl (already have ID and data)
                                      $check_title_record = $this->db->conn_id->prepare(
                                           "SELECT post_tbl.id FROM post_tbl

                                           LEFT JOIN category_manager ON category_manager.from_tbl = :from_tbl AND category_manager.from_id = post_tbl.id

                                           WHERE
                                           post_tbl.id != :id
                                           AND
                                           post_tbl.status = :status
                                           AND
                                           category_manager.domain = :category
                                           AND
                                           (LOWER(post_tbl.title) = :title_utama AND post_tbl.meta_title = :kosong)
                                           OR (LOWER(post_tbl.meta_title) = :title_utama_1)

                                           LIMIT 0,1"
                                      );
                                      $check_title_record->execute(array(
                                           ':from_tbl' => 'post_tbl',
                                           ':id' => $data['from_id'],
                                           ':status' => 'publish',
                                           ':category' => $data['category'],
                                           ':title_utama' => strtolower($title_utama),
                                           ':kosong' => '',
                                           ':title_utama_1' => strtolower($title_utama),
                                      ));
                                      $id_title_ada =  $check_title_record->fetch(PDO::FETCH_COLUMN);

                                 } elseif($data['from_tbl'] != 'publish' && $data['to_tbl'] == 'publish') { #draft_tbl OR pending_table to post_tbl .. no id no data in post_tbl
                                      $check_title_record = $this->db->conn_id->prepare(
                                           "SELECT post_tbl.id FROM post_tbl

                                           LEFT JOIN category_manager ON category_manager.from_tbl = :from_tbl AND category_manager.from_id = post_tbl.id

                                           WHERE

                                           post_tbl.status = :status
                                           AND
                                           category_manager.domain = :category
                                           AND
                                           (
                                              (LOWER(post_tbl.title) = :title_utama AND post_tbl.meta_title = :kosong)
                                              OR (LOWER(post_tbl.meta_title) = :title_utama_1)
                                           )

                                           LIMIT 0,1"
                                      );
                                      $check_title_record->execute(array(
                                           ':from_tbl' => 'post_tbl',

                                           //where
                                           ':status' => 'publish',
                                           ':category' => $data['category'],
                                           ':title_utama' => strtolower($title_utama),
                                           ':kosong' => '',
                                           ':title_utama_1' => strtolower($title_utama)
                                      ));

                                      $id_title_ada =  $check_title_record->fetch(PDO::FETCH_COLUMN);
                                 }

                                 if($id_title_ada){
                                    if($id_title_ada != $data['from_id'])
                                      array_push($error_msg, $msg_duplicate_title);
                                 }

                              }
                              ////////////// PEMERIKSAAN TITLE UTAMA (TITLE/META_TITLE) ///////////////

                              ////////////// PEMERIKSAAN [META_DESCRIPTION] ///////////////
                              if(!empty($data['meta_description']) && !empty($data['category'])){
                                   if($data['from_tbl'] == 'publish' && $data['to_tbl'] == 'publish'){
                                        #post_tbl TO post_tbl (already have ID and data)
                                        $check_description_record = $this->db->conn_id->prepare(
                                             'SELECT id FROM post_tbl WHERE meta_description = :description_kau AND id <> :id_sekarang AND status = :status'
                                        );
                                        $check_description_record->execute(array(
                                             ':description_kau' => $data['meta_description'],
                                             ':id_sekarang' => $data['from_id'],
                                             ':status' => 'publish'
                                        ));
                                   } elseif($data['from_tbl'] != 'publish' && $data['to_tbl'] = 'publish') {
                                        #draft_tbl OR pending_table to post_tbl .. no id no data in post_tbl
                                        $check_description_record = $this->db->conn_id->prepare(
                                             'SELECT id FROM post_tbl WHERE meta_description = :description_kau AND status = :status'
                                        );
                                        $check_description_record->execute(array(
                                             ':description_kau' => $data['meta_description'],
                                             ':status' => 'publish'
                                        ));
                                   }
                                   $id_description_ada = $check_description_record->fetch(PDO::FETCH_COLUMN, 0);

                                   if(is_numeric($id_description_ada)){
                                        #title ni ada digunakan sbg title utama post_tbl. mari check kalo category sama.
                                        $query_get_cat = $this->db->conn_id->prepare(
                                             'SELECT domain from category_manager WHERE from_tbl = :post_tbl AND from_id = :id_title_ada AND domain = :data_category'
                                        );
                                        $query_get_cat->execute(array(
                                             ':post_tbl' => 'post_tbl',
                                             ':id_title_ada' => $id_description_ada,
                                             ':data_category' => $data['category']
                                        ));
                                        $memang_x_unik = $query_get_cat->fetch(PDO::FETCH_COLUMN, 0);
                                        if($memang_x_unik){
                                             array_push($error_msg, '<li><b>SEO Description</b> ini telah digunakan di post lain. <span class="red">Sila olah sedikit agar ianya berlainan atau unik (lebih baik)</span>, atau biarkan ianya kosong.</li>');
                                        }
                                   }

                              }
                              ////////////// PEMERIKSAAN [META_DESCRIPTION] ///////////////

                              if(strlen($data['meta_description']) > 150){
                                   array_push($error_msg, '<li><b>SEO Description</b> tidak boleh melebihi 150 aksara</li>');
                              } elseif(!empty($data['meta_description']) && strlen($data['meta_description']) < 100){
                                   array_push($error_msg, '<li><b>SEO Description</b> tidak boleh kurang dari 100 aksara.</li>');
                              }

                              if(strlen($data['viral_title']) > 60){
                                   array_push($error_msg, '<li><b>Viral Title</b> tidak boleh melebihi 60 aksara</li>');
                              }
                              if(strlen($data['viral_description']) > 150){
                                   array_push($error_msg, '<li><b>Viral Description</b> tidak boleh melebihi 150 aksara</li>');
                              }


                              #CHECK CATEGORY
                              if(!$data['category'] || !allowed_domain('http://'.$data['category'])){
                                   array_push($error_msg, '<li>Mesti tetapkan <b>Category</b></li>');
                              }

                              #CHECK POST THUMBNAIL.
                              if(empty($data['first_img'])){
                                   array_push($error_msg, '<li>Wajib tetapkan <b>Post Thumbnail</b></li>');
                              }
                         }

                         #CHECK POST
                         $dom_content = new domDocument;
                         libxml_use_internal_errors(true);
                         $dom_content->loadHTML($data['content']);
                         libxml_clear_errors();
                         $dom_content->preserveWhiteSpace = false;
                         $finder = new DomXPath($dom_content);

                         #check content para.
                         $para = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' para ')]");
                         if($para->length < 3){
                              array_push($error_msg, '<li>Link ke website luar selain dari <i>malayatimes.com</i>, tidak boleh <b>melebihi 2</b>.</li>');
                         }

                         $dom_images = $dom_content->getElementsByTagName('img');
                         #check content external image.
                         for ($i=0; $i < $dom_images->length; $i++) {
                              $o = $i+1;
                              if($dom_images[$i]->getAttribute('src')){
                                   if( !allowed_domain($dom_images[$i]->getAttribute('src')) ){
                                        array_push($error_msg, '<li>Gambar ke <b>'. $o .'</b> tidak dibenarkan kerana menggunakan gambar yang bukan diupload dari media.malayatimes.com.</li>');
                                   }
                              } else {
                                   array_push($error_msg, '<li>Element gambar ke <b>'. $o .'</b> tidak mempunyai attribute <b>src</b>.</li>');
                              }
                         }

                         #check content image length. DISABLED
                         // if($dom_images->length < 1){
                         //    array_push($error_msg, '<li>Wajib sekurang-kurangnya ada satu gambar yang berkaitan di dalam post.</li>');
                         // }


                         $dom_link = $dom_content->getElementsByTagName('a');
                         $dom_external_link = array();
                         for ($i=0; $i < $dom_link->length; $i++) {
                              if( !allowed_domain($dom_link[$i]->getAttribute('href')) ){
                                   array_push($dom_external_link, $dom_link[$i]);
                              }
                         }
                         if(sizeof($dom_external_link) > 2){
                              array_push($error_msg, '<li>Link ke website luar selain dari <i>malayatimes.com</i>, tidak boleh <b>melebihi 2</b>.</li>');
                         }

                    } else { // ELSE $data['content']
                         array_push($error_msg, '<li>Content tidak boleh kosong tanpa sebarang perenggan.</li>');
                    } #END IF $data['content']
                    /* End error_msg */



                    if(sizeof($error_msg)>0){

                         #ada error.

                         #save draft lu..
                                   $this->session->set_flashdata('error_submit_post', $error_msg);
                                   $data['to_tbl'] = $data['from_tbl'];
                                   $data['redirect'] = site_url('post?id=') . $data['from_id'] . '&status=' . $data['from_tbl'];
                                   $this->m_posts->post_proccess($data);


                    } else {

                         #tiada error.
                         // if( in_array($current_role, $admin_gang) ){ #admin
                         //      $data['to_tbl'] = 'publish';
                         // } else {
                         //      $data['to_tbl'] = 'pending';
                         // }

                         // if(!$this->input->is_ajax_request()){
                         //    $data['redirect'] = site_url(str_replace('_tbl','',$data['to_tbl']));
                         // }

                         /* TESTING*/
                         // print_r('<textarea style="width:100%;height:500px;">');
                         // print_r($data);
                         // print_r('</textarea>');

                         $data['reset_gambar'] = true;

                         $this->m_posts->post_proccess($data);

                    }



               }#is_numeric($my_from_id)

          // }#mesti ada refferer.

     } // end index function

} //end class
