<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_image extends CI_Controller{

   public function __construct()
   {
      parent::__construct();
      $this->load->library(array('session', 'user_agent'));
      $this->load->helper(array('security', 'h_link_helper', 'url', 'h_user_helper'));
      // $this->l_login->cek_login(current_url());
      set_status_header('404');
   }

   function index() {
      $this->load->view('404');
   }

   function ajax(){

      // if($this->agent->referrer()){ #mesti ada refferer.
      //    $dari = $this->agent->referrer();
      //    if(allowed_domain($dari)){
      if( current_user_id() > 0 ){

         parse_str(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $queries);
         if ( isset($queries['cari']) && !empty($queries['cari']) ){
            set_status_header('200');
            $data = $queries;
            $this->load->view('v_search_image/v_search_image', $data);
         } else {
            $this->load->view('404');
         }

      } else {
         set_status_header('200');
         print_r('logout');
      }

      //    }#allowed_domain
      // }#refferer

   }#end of function.


}
