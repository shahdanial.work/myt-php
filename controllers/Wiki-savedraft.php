<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wiki_savedraft extends CI_Controller{

   function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'user_agent'));
        $this->l_login->cek_login(current_url());
        $this->load->helper(array('security', 'h_user_helper', 'h_link_helper', 'url'));
        $this->load->model(array('m_img', 'm_wikis'));
   }

   /* THIS AREA ONLY FOR AUTOSAVE (AJAX) */


  public function index(){

               $my_from_id = trim($this->input->post('id', FALSE));

               $current_role = user_info(current_user_id())['user_role'];
               $admin_gang = array('special admin', 'special staff');


               if( is_numeric($my_from_id) ){

                  if(in_array($current_role, $admin_gang)){
                     $query = $this->db->conn_id->prepare('SELECT status FROM tag_tbl WHERE id = :post_id');
                     $query->execute(array(
                          ':post_id' => $my_from_id
                     ));
                  } else {
                     $query = $this->db->conn_id->prepare('SELECT status FROM tag_tbl WHERE id = :post_id AND author_id = :author_id AND status != :status');
                     $query->execute(array(
                          ':post_id' => $my_from_id,
                          ':author_id' => current_user_id(),
                          ':status' => 'publish'
                     ));
                  }

                  $from_status = $query->fetch(PDO::FETCH_COLUMN);


                  if($from_status){


                     if($this->input->is_ajax_request()){
                          $redirect = false;
                     } else {
                          $redirect = site_url('wiki');
                     }

                     date_default_timezone_set("Asia/Kuala_Lumpur");


                     $final_first_thumb = '';
                     $semi_first_thumb = trim($this->security->xss_clean($this->input->post('post-thumbnails-path')));
                     if($semi_first_thumb){
                          if(allowed_domain($semi_first_thumb)){
                               $final_first_thumb = pathinfo($semi_first_thumb)['basename'];#ex my-image-path.png
                               if( !file_exists('./gambar/' . $final_first_thumb) ){
                                    $final_first_thumb = '';
                               }
                          }
                     }

                     $final_google_thumb = '';
                     $semi_google_thumb = trim($this->security->xss_clean($this->input->post('seo-thumbnails-path')));
                     if($semi_google_thumb){
                          if(allowed_domain($semi_google_thumb)){
                               $final_google_thumb = pathinfo($semi_google_thumb)['basename'];#ex my-image-path.png
                               if( !file_exists('./gambar/' . $final_google_thumb) ){
                                    $final_google_thumb = '';
                               }
                          }
                     }

                     $final_viral_thumb = '';
                     $semi_viral_thumb = trim($this->security->xss_clean($this->input->post('viral-thumbnails-path')));
                     if($semi_viral_thumb){
                          if(allowed_domain($semi_viral_thumb)){
                               $final_viral_thumb = pathinfo($semi_viral_thumb)['basename'];#ex my-image-path.png
                               if( !file_exists('./gambar/' . $final_viral_thumb) ){
                                    $final_viral_thumb = '';
                               }
                          }
                     }

                     #tulis section.
                     $data = array(
                          'title' => trim( str_replace("\"", "'", $this->security->xss_clean($this->input->post('title')) )),
                          'content' => trim($this->input->post('content', FALSE)),

                          'type' => trim($this->security->xss_clean($this->input->post('category'))),

                          'tag' => $this->input->post('tag[]'),
                          'gender' => $this->input->post('gender[]', FALSE),
                          'interest' => $this->input->post('interest[]', FALSE),
                          
                          'meta_title' => trim($this->security->xss_clean($this->input->post('meta_title'))),
                          'meta_description' => trim($this->security->xss_clean($this->input->post('meta_description'))),

                          'first_img' => $final_first_thumb,

                          #google
                          'meta_title' => trim($this->security->xss_clean($this->input->post('meta_title'))),
                          'meta_description' => trim($this->security->xss_clean($this->input->post('meta_description'))),
                          'google_thumb' => $final_google_thumb,

                          #facebook
                          'viral_title' => trim($this->security->xss_clean($this->input->post('viral_title'))),
                          'viral_description' => trim($this->security->xss_clean($this->input->post('viral_meta_description'))),
                          'viral_thumb' => $final_viral_thumb,

                          'from_id' => $my_from_id,
                          'redirect' => $redirect,

                          'status' => $from_status // save draft kan? so status guna status asal..
                     );


                     if($this->agent->referrer()){ #mesti ada refferer.
                          $dari = $this->agent->referrer();
                          if(allowed_domain($dari)){
                               //$this->m_post->update_draft($data);
                               $this->m_wikis->post_proccess($data);
                               // print_r($data);
                          }
                     }


                  }




         } else {#is_numeric($my_from_id)
                 print_r('You action not allowed');
                 print_r($my_from_id);
                 print_r('<br>');
                 if( in_array($current_role, $admin_gang) ){
                      print_r($my_from_id);
                      print_r('<br>');
                 }
         } #is_numeric($my_from_id)

  } // end index function

}
