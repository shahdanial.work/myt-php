<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kira extends CI_Controller{

  public function __construct(){
    parent::__construct();
    //Codeigniter : Write Less Do Moree
    $this->simple_login->cek_login(current_url());
    $this->load->helper(array('h_user_helper', 'h_query_helper'));
    //$this->output->set_header(array());
    //$this->load->library(array());
    $this->output->set_header('X-Robots-Tag: noindex');
    $this->load->model(array('m_posts', 'm_tags', 'm_img'));
  }

  function index(){

    #(GAMBAR ASAL, SILA UBAH2).
    $gw = 200;
    $gh = 500;
    #(TARGET, SILA UBAH2).
    $tw = 658;
    $th = 309;
    #result comparison.
    $rw = $gw - $tw;
    $rh = $gh - $th;

    if($rw < 0 || $rh < 0){ #PEMBESARAN START.
      $pw = str_replace('-', '', $rw) / $gw * 100;
      $ph = str_replace('-', '', $rh) / $gh * 100;
      if($pw > $ph){
        $fpw = ($pw + 100) / 100 * $gw ;
        $fph = ($pw + 100) / 100 * $gh ;
        $quality = 100;
      } else {
        $fpw = ($ph + 100) / 100 * $gw ;
        $fph = ($ph + 100) / 100 * $gh ;
        $quality = 100;
      }
    } else { #PENGECILAN START.
      $pw = str_replace('-', '', $rw) / $gw * 100;
      $ph = str_replace('-', '', $rh) / $gh * 100;
      if($pw < $ph){
        $fpw = (100 - $pw) / 100 * $gw;
        $fph = (100 - $pw) / 100 * $gh;
        $quality = 100 - $pw;
      } else {
        $fpw = (100 - $ph) / 100 * $gw;
        $fph = (100 - $ph) / 100 * $gh;
        $quality = 100 - $ph;
      }
    }

    if($fpw != $tw){ #dapatkan left before crop.
      $potong_atas = 0;
      $potong_kiri = ($fpw - $tw) / 2;
    } else{ #dapatkan atas before crop.
      $potong_kiri = 0;
      $potong_atas = ($fph - $th) / 2;
    }


    //  print_r('FPW = ' .    str_replace('-', '', $fpw)   );
    //  print_r('<br>');
    //  print_r('FPH = ' .    str_replace('-', '', $fph)    );
    //
    //  print_r('<hr>');
    //  print_r('<br>');
    //  print_r('$pw = ' .    $pw   );
    //  print_r('<br>');
    //  print_r('$ph = ' .    $ph   );
    //  print_r('<br>');
    //
    // print_r('<hr>');
    // print_r('$rw = ' .    $rw   );
    // print_r('<br>');
    // print_r('$rh = ' .    $rh   );
    // print_r('<br>');

  }

  function unik_keyword_manager(){
     $delete = $this->db->conn_id->prepare(
        'DELETE FROM keyword_manager WHERE from_tbl != "post_tbl"'
     );
     $delete->execute();
     $query = $this->db->conn_id->prepare(
        'SELECT DISTINCT id, from_tbl, from_id, mykey, myvalue FROM keyword_manager'
     );
     $query->execute();
     $result = $query->fetchAll(PDO::FETCH_ASSOC);
     if($result){
        var_dump($result);
        foreach ($result as $post) {
           $del = $this->db->conn_id->prepare(
             'DELETE FROM keyword_manager WHERE
             id != :id AND from_tbl = :from_tbl AND from_id = :from_id AND mykey = :mykey AND myvalue = :myvalue
             '
          );
          $del->execute(array(
             ':id' => $post['id'],
             ':from_tbl' => $post['from_tbl'],
             ':from_id' => $post['from_id'],
             ':mykey' => $post['mykey'],
             ':myvalue' => $post['myvalue']
          ));
        }
     }
  }

  function delete_tag_tbl(){

    #CHECK BY tag_tbl
    $query = $this->db->conn_id->prepare(
      'SELECT tag_path FROM tag_tbl'
    );
    $query->execute();
    $paths = $query->fetchAll(PDO::FETCH_COLUMN);
    if($paths){
      foreach ($paths as $path) {
          // $this->m_tags->delete_tag_tbl($tag_path);
          $where = array(
             'tag_path )( =' => $path
           );
           if(query_select('tag_path', 'tag_manager', $where)){
             echo '<b style="color:green">' . $path . '</b> ni ada<br>';
           } else {
             echo '<b style="color:red">' . $path . ' </b>tiada tag_manager tp ada d tag_tbl, kami delete skang<br>';
           $this->m_tags->delete_tag_tbl($path);
          }
      }
    }

    #CHECK BY tag_manager
    $myquery = $this->db->conn_id->prepare(
      'SELECT DISTINCT(tag_path) FROM tag_manager'
    );
    $myquery->execute();
    $mypaths = $myquery->fetchAll(PDO::FETCH_COLUMN);
    if($mypaths){
      foreach ($mypaths as $mypath) {
          // $this->m_tags->delete_tag_tbl($tag_path);
          $mywhere = array(
             'tag_path )( =' => $mypath
           );
           if(query_select('tag_path', 'tag_tbl', $mywhere)){
             echo '<b style="color:green">' . $mypath . '</b> ni ada<br>';
           } else {
             echo '<b style="color:red">' . $mypath . ' </b>tiada di tag_tbl tp ada di tag_manager, kami delete skang<br>';
             $thequery = $this->db->conn_id->prepare(
                 'DELETE FROM tag_manager WHERE tag_path = :tag_path'
              );
              $thequery->execute(array(
                 ':tag_path' => $mypath
              ));
          }
      }
    }

  }

  function delete_file_tbl(){

     $query = $this->db->conn_id->prepare(
        'SELECT filename FROM file_tbl'
     );
     $query->execute();
     $filenames = $query->fetchAll(PDO::FETCH_COLUMN);
     if($filenames){
        echo '<h1>Check dari file_tbl db</h1>';
        foreach ($filenames as $filename) {
           if( !is_dir($filename) ){

             // $this->m_tags->delete_tag_tbl($tag_path);
            $where = array(
               'filename )( =' => $filename
             );
            if(query_select('filename', 'file_tbl', $where) && query_select('filename', 'file_manager', $where) && file_exists('./gambar/' . $filename)){
              echo '<b style="color:green">' . $filename . '</b> ni ada<br>';
           } else {
              echo '<b style="color:red">' . $filename . ' </b>tiada, kami delete skang<br>';
              $this->m_img->delete($filename, 'wtf', '000');
            }

          }
        }
     }

     echo '<br><br><hr><br><br>';
     #loop dari file wujud tp xde record lak..
     $files = scandir('./gambar/');
     $i = 0;

      #delete gambar yang ada di directory tp x wujud di file_manager.
      foreach($files as $file) {
         if(!is_dir($file)){
            $i++;
            echo $i . '. ' . $file . '<br>';
            #check table manger delete('filename', from_tbl, from_id);
            # dibawah ni dia akan delete rows(file_tbl dan file_manager) where tiada_di_file_manager(filename != ? bukan from_tbl != ? and from_id != ?)
            $this->m_img->delete($file, 'wtf', '000');
         }
      }

  }

#function dibwh delete segala gambar yang bukan di upload dari 'post_tbl'
  // function sementara(){
  //
  //    $query = $this->db->conn_id->prepare(
  //      'SELECT * FROM file_manager WHERE from_tbl != "post_tbl"'
  //    );
  //    $query->execute();
  //    $array = $query->fetchAll(PDO::FETCH_ASSOC);
  //    if($array){
  //      foreach ($array as $arrays) {
  //         echo 'SELECT * FROM file_manager WHERE filename = ' . $arrays['filename'] . ' AND from_tbl = ' .  $arrays['from_tbl'] . ' AND from_id = ' . $arrays['from_id'] . '<br>';
  //
  //         $this->m_img->delete($arrays['filename'], $arrays['from_tbl'], $arrays['from_id']);
  //      }
  //    }
  //
  // }

  function matilanak(){
     var_dump(sys_get_temp_dir());
 }

  function today(){
       $check_title_record = $this->db->conn_id->prepare(
            "SELECT post_tbl.id FROM post_tbl

             LEFT JOIN category_manager ON category_manager.from_tbl = :from_tbl AND category_manager.from_id = post_tbl.id

             WHERE
             category_manager.domain = :category
             AND
             (LOWER(post_tbl.title) = :title_utama AND post_tbl.meta_title = :kosong)
             OR (LOWER(post_tbl.meta_title) = :title_utama_1)

             LIMIT 0,1"
       );
       $check_title_record->execute(array(
            ':from_tbl' => 'post_tbl',

            ':category' => 'artis.malayatimes.com',

            ':title_utama' => strtolower('RM 12 juta tidak dibayar kepada Abu Sayyaf'),
            ':kosong' => '',
            ':title_utama_1' => strtolower('RM 12 juta tidak dibayar kepada Abu Sayyaf')
       ));
       // $check_title_record->execute(array(
       //      ':from_tbl' => 'post_tbl',
       //      ':category' => 'artis.malayatimes.com',
       //      ':id' => '1',
       //      ':title_utama' => strtolower('RM 12 juta tidak dibayar kepada Abu Sayyaf'),
       //      ':kosong' => '',
       //      ':title_utama_1' => strtolower('RM 12 juta tidak dibayar kepada Abu Sayyaf')
       // ));

       $id_title_ada =  $check_title_record->fetch(PDO::FETCH_COLUMN);
       if($id_title_ada){
            print_r($id_title_ada);
       } else {
            print_r('tiada');
       }
  }

  function semalam(){
    $query = $this->db->conn_id->prepare("SELECT title FROM post_tbl WHERE title REGEXP '^[A-Za-z0-9]+$' = :title");
    $query->execute(array(
       ':title' => 'RM 12 juta tidak dibayar kepada Abu Sayyaf disalur kepada b'
    ));
    $result =  $query->fetch(PDO::FETCH_COLUMN);
    print_r($result);
  }

  function hmm(){
     $query = $this->db->conn_id->prepare(
     "SELECT
          (SELECT SUM(value) FROM keluar_masuk WHERE income_cost = 'income' AND currency = 'MYR')
          +
          ((SELECT SUM(value) FROM keluar_masuk WHERE income_cost = 'income' AND currency = 'USD') * (SELECT value FROM site_data WHERE what = 'usd_to_myr'))
          as income,
          (SELECT SUM(value) FROM keluar_masuk WHERE income_cost = 'cost' AND currency = 'MYR')
          +
          ((SELECT SUM(value) FROM keluar_masuk WHERE income_cost = 'cost' AND currency = 'USD') * (SELECT value FROM site_data WHERE what = 'usd_to_myr'))
          as cost"
   );
   $query->execute();

   $result = $query->fetchAll(PDO::FETCH_ASSOC);
   if(!$result){
      $result = array(
         'income' => 0,
         'cost' => 0
      );
   } else {
      if(array_key_exists('income', $result)) $result['income'] = 0;
      if(array_key_exists('cost', $result)) $result['cost'] = 0;
   }

   print_r($result);
 }

  function trying(){
     $final = array();
     $data = array(
    'title'=> 'my title',
    'content' => '<p class="para">my content</p>',
    'category' => 'viral.malayatiimes.com',
    'meta_title' => 'meta title gua',
    'from_tbl' => 'draft_tbl',
    'to_tbl' => 'draft_tbl',
    'meta_description' => '',
    'first_img' => '',
    'google_thumb' => '',
    'viral_title' => '',
    'viral_description' => '',
    'viral_thumb' => '',
    'post_id' => '3',
    'redirect' => 'http://media.malayatimes.com/post',
    'response_msg' => 'autodraft saved at 2018 Jan 07, 2:43AM'
);
$data['tag']['0'] = 'tag 1';
$data['tag']['1'] = 'tag 2';

$first = array();
$prepare = '';
$execute = array();
$selected = array('masa','title','content','first_img','meta_title','meta_description','google_thumb','viral_title','viral_description','viral_thumb');
foreach ($data as $key => $value) {
  if(in_array($key, $selected)){
     array_push($first,$key);
     $execute[':'.$key] = $value;
  }
}

foreach ($first as $key => $value) {
   if($key < sizeof($first)-1){
      $comma = ', ';
   } else {
      $comma = '';
   }
   #prepare.
   $damn = $value.' = :'.$value.$comma;
   $prepare = $prepare . $damn;
}


$final['prepare']  = $prepare;
$final['execute']  = $execute;
#execute

print_r('<textarea style="width:100%;height:100%">');
print_r($final['execute']);
print_r('</textarea>');
  }

  function test(){
    $thumbnails = array(
      array(
        'fake' => './A_fake/',
        'folder' => './picker/',
        'tw' => 110,
        'th' => 110
      ),
      array(
        'fake' => './B_fake/',
        'folder' => './foto/',
        'tw' => 658,
        'th' => 309
      ),
      array(
        'fake' => './C_fake/',
        'folder' => './photo/',
        'tw' => 300,
        'th' => 194
      ),
      array(
        'fake' => './D_fake/',
        'folder' => './picture/',
        'tw' => 100,
        'th' => 75
      ),
      array(
        'fake' => './E_fake/',
        'folder' => './image/',
        'tw' => 206,
        'th' => 206
      ),
      array(
        'fake' => './F_fake/',
        'folder' => './thumbnail/',
        'tw' => 72,
        'th' => 72
      )
    );
    echo "<pre>";
    foreach ($thumbnails as $value) {
      print_r($value);
    }
    echo "</pre>";
  }

  function insert(){

    $final = array();
    $data = array(
   'title'=> 'my title',
   'content' => '<p class="para">my content</p>',
   'category' => 'viral.malayatiimes.com',
   'meta_title' => 'meta title gua',
   'from_tbl' => 'draft_tbl',
   'to_tbl' => 'draft_tbl',
   'meta_description' => '',
   'first_img' => '',
   'google_thumb' => '',
   'viral_title' => '',
   'viral_description' => '',
   'viral_thumb' => '',
   'post_id' => '3',
   'redirect' => 'http://media.malayatimes.com/post',
   'response_msg' => 'autodraft saved at 2018 Jan 07, 2:43AM'
);
  $data['tag']['0'] = 'tag 1';
  $data['tag']['1'] = 'tag 2';


  $execute = array();
  $selected = array('masa','title','content','first_img','meta_title','meta_description','google_thumb','viral_title','viral_description','viral_thumb');
  $first = array();
  $execute = array();

  foreach ($data as $key => $value) {
    if(in_array($key, $selected)){
      array_push($first, $key);
      $execute[':'.$key] = $value;
    }
  }

  $set_prepare = '';
  $value_prepare = '';
  foreach($first as $key => $value){
    if($key < sizeof($first)-1){
      $comma = ', ';
    } else {
      $comma = '';
    }
    $set_prepare = $set_prepare.$value.$comma;
    $value_prepare = $set_prepare.':'.$value.$comma;
  }

  $final_prepare = '('.$set_prepare.') VALUES ('.$value_prepare.')';


  print_r('<textarea style="width:100%;height:100%">');
  print_r($execute);
  print_r('</textarea>');

  }

  function amput(){
    $ch = $this->db->conn_id->prepare(
      'SELECT tag_path FROM tag_manager WHERE tag_path = :tag_path AND from_tbl = :from_tbl AND from_id = :from_id'
  );
  $ch->execute(array(
      ':tag_path'=>'abbu-sayyaf',
      ':from_tbl'=>'post_tbl',
      ':from_id'=>1
  ));
  $reqorded = $ch->fetchAll(PDO::FETCH_COLUMN, 0);
  print_r($reqorded);
  }

  function check_rows(){
    $select = '*';
    $from_tbl = 'tag_manager';
    $where = array(
      'tag_path )( =' => 'abu-sayyaf',
      'from_tbl )( =' => $from_tbl,
      'from_id )( =' => '1'
  );
    $prepare = 'SELECT '. $select .' FROM '. $from_tbl .' WHERE ';
    $execute = array();

    $i = 0;
    foreach ($where as $keyy => $value) {
      $format_key = explode(')(', $keyy);
      $key = trim($format_key[0]);
      $condition = trim($format_key[1]);
      $i = $i + 1;
      if($i < sizeof($where)){
        $and = ' AND ';
      } else {
        $and = ' ';
      }
      //array_push(':'.$key => $value, $execute);

      $prepare = $prepare . $key . ' ' .$condition. ' :'. $key . $and;
      $execute[':'.$key] = $value;
    }
    $query = $this->db->conn_id->prepare($prepare);
    $query->execute($execute);
    $query_result = $query->fetchAll(PDO::FETCH_ASSOC);

    print_r($prepare);
    print_r('<br><textarea style="width:100%; height: 500px;">');

    print_r($execute);

    print_r('</textarea>');

    // if(!$query_result || sizeof(array_filter($query_result)) < 1){
    //   return false;
    // } else {
    //   return $query_result;
    // }
  }

  function join(){
    $backup = ("SELECT
      post_tbl.*, category_manager.domain as category, users.namapena, group_concat(tag_manager.tag_path) as tag_path, group_concat(tag_tbl.title) as tag_title
      FROM post_tbl
      LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = post_tbl.id
      LEFT JOIN users ON users.id_user = post_tbl.author_id
      LEFT JOIN tag_manager ON tag_manager.from_tbl = 'post_tbl' AND tag_manager.from_id = post_tbl.id
      WHERE category_manager.domain = 'berita.malayatimes.com'
      GROUP BY post_tbl.id
      ORDER BY post_tbl.id DESC
      LIMIT 0,8");

      //AND post_tbl.id NOT IN (100,200)

    $wtf = $this->db->conn_id->prepare(
      "SELECT
      post_tbl.*, category_manager.domain as category, users.namapena, group_concat(tag_manager.tag_path) as tag_path, group_concat(tag_tbl.title) as tag_title
      FROM post_tbl
      LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = post_tbl.id
      LEFT JOIN users ON users.id_user = post_tbl.author_id
      LEFT JOIN tag_manager ON tag_manager.from_tbl = 'post_tbl' AND tag_manager.from_id = post_tbl.id
      LEFT JOIN tag_tbl ON tag_tbl.tag_path = tag_manager.tag_path
      WHERE category_manager.domain = 'berita.malayatimes.com'
      GROUP BY post_tbl.id
      ORDER BY post_tbl.id DESC
      LIMIT 0,8
      "
    );
    $wtf->execute(array(
      // ':satu' => 'berita',
      // ':post_tbl' => 'post_tbl'
    ));
    $res = $wtf->fetchAll(PDO::FETCH_ASSOC);
    print_r('<textarea style="width:100%;height:100%;">');
    print_r($res);
    print_r('</textarea>');
  }

  function u2(){
    $str = 'AA)(BB';
    $str = explode(')(',$str);
    print_r($str);
  }

  function fuck(){
    $path_ext = $this->db->conn_id->prepare("SELECT _path, _ext FROM file_tbl WHERE id = :file_id");
    $path_ext->execute(array(':file_id' => '257'));
    $result =  $path_ext->fetch(PDO::FETCH_ASSOC);
    $path = $result['_path'];
    $ext = $result['_ext'];
    print_r($path);
    print_r('.');
    print_r($ext);
  }

  function lol(){
    $array1 = array("green", "red", "blue", "red", "WADEPAK", "WADEPIS");
    $array2 = array("green", "yellow", "red");
    $results = array_diff($array1, $array2);


    foreach($results as $result){
      print_r($result);
    }
  }

  function nak_kecing(){
    $tag_query = $this->db->conn_id->prepare('SELECT id FROM tag_tbl WHERE LOWER(title) = :title');
    $tag_query->execute( array(
      ':title' =>strtolower('satu')
    ) );
    $tag_result = $tag_query->fetch(PDO::FETCH_ASSOC);
    if($tag_result){
      $tag_id = $tag_result['id'];
      print_r($tag_id);
      print_r('aik');
    } else {
      print_r('xde la bhai');
    }
  }

  function masuk(){
    $ck_tag = $this->db->conn_id->prepare("SELECT tag_id FROM tag_manager WHERE from_id = :from_id");
    $ck_tag->execute(array(':from_id'=>'6'));

    $ttag_id = $ck_tag->fetchAll(PDO::FETCH_COLUMN);
    print_r($ck_tag->rowCount());
    echo '<hr>';
    foreach ($ttag_id as $ash) {
      print_r($ash);echo',';
    }
  }

  function pathinfo(){
    print_r( pathinfo('http://media.malayatimes.com/gambar/testing-2.jpg')['basename'] );
  }

}
