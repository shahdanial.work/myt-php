<?php

class Income_cost extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper(array('h_user_helper','security','form'));
    $this->load->model(array('m_money'));
    $this->l_login->cek_login(current_url());
    $this->output->set_header('X-Robots-Tag: noindex');
    date_default_timezone_set("Asia/Kuala_Lumpur");
    set_status_header('403');
  }


  public function index(){
    $pangkat = user_info(current_user_id())['user_role'];//$this->session->userdata('user_role');
    $allowed = array('special admin');
    if(in_array($pangkat, $allowed)){
      #siapkan data, nak pass ke main view..
      $query = $this->db->conn_id->prepare(
         "SELECT * FROM keluar_masuk WHERE MONTH(added_time) = MONTH(CURRENT_DATE()) AND YEAR(added_time) = YEAR(CURRENT_DATE())"
      );
      $query->execute();
      $datas['data'] = $query->fetchAll(PDO::FETCH_ASSOC);
      #load view
      $this->load->view('/v_income_cost/v_main', $datas);
    } else {
      $this->load->view('404');
    }
  } // end index function

  function income(){
    $id = $this->input->get('id', TRUE);
    $pangkat = user_info(current_user_id())['user_role'];
    $allowed = array('special admin');


    if(in_array($pangkat, $allowed)){
      $this->load->view('/v_income_cost/v_income');
    } else {
      $this->load->view('404');
    }
  }

  function cost(){
    $pangkat = user_info(current_user_id())['user_role'];
    $allowed = array('special admin');
    if(in_array($pangkat, $allowed)){
      $this->load->view('/v_income_cost/v_cost');
    } else {
      $this->load->view('404');
    }
  }

  function update_income(){

    $dari = parse_url($_SERVER['HTTP_REFERER']);
    $dari_canonical = $dari['scheme'] . '://' . $dari['host'] . $dari['path'];
    if($dari_canonical == site_url('income_cost/income')){
      $currency = $this->input->post('currency', TRUE);
      $value = $this->input->post('value', TRUE);
      $penerangan = $this->input->post('penerangan', TRUE);


      $time = date("Y-m-d H:i:s");//date("d M Y") . ', ' . date('H:i:s');

      if( is_numeric($value) && $currency && $value && $penerangan){
        $data['added_time'] = $time;
        $data['added_by_user_id'] = current_user_id();
        $data['currency'] = $currency;
        $data['value'] = $value;
        $data['penerangan'] = $penerangan;
        $data['income_cost'] = 'income';
        $this->m_money->update_income($data);
      } else {
        print_r('error, ada field x isi; so sila back to form, dan pastikan isi semua field.');
      }
    } else {
      $this->load->view('404');
    }

  }

  function update_cost(){

    $dari = parse_url($_SERVER['HTTP_REFERER']);
    $dari_canonical = $dari['scheme'] . '://' . $dari['host'] . $dari['path'];
    if($dari_canonical == site_url('income_cost/cost')){
      $currency = $this->input->post('currency');
      $value = $this->input->post('value');
      $penerangan = $this->input->post('penerangan');
      // date_default_timezone_set("Asia/Kuala_Lumpur");
      $time = date("Y-m-d H:i:s");

      if( $currency && $value && $penerangan ){
        $data['added_time'] = $time;
        $data['added_by_user_id'] = current_user_id();
        $data['currency'] = $currency;
        $data['value'] = $value;
        $data['penerangan'] = $penerangan;
        $data['income_cost'] = 'cost';
        $this->m_money->update_income($data);
      } else {
        print_r('error, ada field x isi; so sila back to form, dan pastikan isi semua field.');
      }
    } else {
      $this->load->view('404');
    }
  }

  function edit(){
     $id = $this->input->get('id', TRUE);
     $type = $this->input->get('type', TRUE);
     $pangkat = user_info(current_user_id())['user_role'];
     $allowed = array('special admin');

     if($id && $type && in_array($pangkat, $allowed)){
        set_status_header('200');
      #nak edit with id..
         $query = $this->db->conn_id->prepare(
             "SELECT * FROM keluar_masuk WHERE id = :id AND income_cost = :type"
         );
         $query->execute(
            array(
               ':id' => $id,
               ':type' => $type
            )
         );
         $datas['data'] = $query->fetch(PDO::FETCH_ASSOC);
         $this->load->view('/v_income_cost/v_'.$type, $datas);
         // if(current_user_id() == '1') var_dump($datas['data']);
         //$this->load->view('/v_income_cost/v_'.$type, $datas);
      } elseif(in_array($pangkat, $allowed)) {
         set_status_header('200');
         $this->load->view('/v_income_cost/v_'.$type);
      }
   }

   function edit_income(){
      set_status_header('200');
      $data = array(
         'id' => $this->input->post('id'),
         'currency' => $this->input->post('currency'),
         'value' => $this->input->post('value'),
         'penerangan' => $this->input->post('penerangan'),
         'added_time' => date("Y-m-d H:i:s"),

         'redirect' => site_url('income-cost')//site_url('income_cost/edit?type=income&id='. $this->input->post('id'))
      );
      $this->m_money->edit_income($data);
   }


   function edit_cost(){
      set_status_header('200');
      $data = array(
         'id' => $this->input->post('id'),
         'currency' => $this->input->post('currency'),
         'value' => $this->input->post('value'),
         'penerangan' => $this->input->post('penerangan'),
         'added_time' => date("Y-m-d H:i:s"),

         'redirect' => site_url('income-cost')//site_url('income_cost/edit?type=cost&id='. $this->input->post('id'))
      );
      $this->m_money->edit_income($data);
   }

} // end of index
