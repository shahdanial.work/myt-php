<?php

class Published extends CI_Controller {

   function __construct() {
      parent::__construct();
      $this->load->library(array('session', 'user_agent'));
      $this->l_login->cek_login(current_url());
   }

   public function index(){
    $this->session->set_flashdata('from_tbl', 'publish');
    redirect(site_url('post'));
   }
}