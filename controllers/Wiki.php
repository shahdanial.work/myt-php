<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wiki extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    $this->l_login->cek_login(current_url());
    $this->load->helper(array('security', 'h_user_helper', 'h_link_helper', 'url','h_html_helper'));
    // $this->load->model(array('M_upload'));
  }

  function index()
  {

     /* list status allowed in url query:
     1. created = tag yang di create but had do nothing with this..
     2. submitted = tag edited and clicked 'SUBMIT' (need approval by admin)
     3. published = tag submitted AND APPROVED by Admin..
     4. else { check by tag-path }
     */
     $status_query = $this->input->get('status', TRUE);

     $datas = array();
     $datas['table'] = array();

     if($status_query == 'created'){

     } elseif ($status_query == 'submitted'){

     } elseif ($status_query == 'published'){

     } else {

        $id = $this->input->get('id', TRUE);
        $status = $this->input->get('status', TRUE);
        // print_r($id);

        if(is_numeric($id) && $status){

           #come with path
           $query = $this->db->conn_id->prepare(
             "SELECT tag_tbl.*

             from tag_tbl

             #LEFT JOIN file_manager ON file_manager.from_tbl = 'tag_tbl' AND file_manager.from_id = tag_tbl.id

             WHERE tag_tbl.id = :id AND tag_tbl.status = :status AND status = :status"
           );
           #query above will return null on every rows once there's no result !
           $query->execute(array(
              ':id' => $id,
              ':status' => $status
           ));
           $result =  $query->fetch(PDO::FETCH_ASSOC);

              #setup gambar.
              $query = $this->db->conn_id->prepare("SELECT filename FROM file_manager WHERE from_id = :from_id AND from_tbl = :from_tbl");
              $query->execute( array(
                 ':from_id' => $id,
                 ':from_tbl' => 'tag_tbl'//$table['from_tbl']
              ) );
              $result['gambar'] = $query->fetchAll(PDO::FETCH_COLUMN);

              #setup type_tbl
              if(user_info(current_user_id())['user_role'] == 'special admin'){
                 $cat_query = $this->db->conn_id->prepare("SELECT type_path, title FROM type_tbl");
                 $cat_query->execute();
              } else {
                 $cat_query = $this->db->conn_id->prepare("SELECT type_path, title FROM type_tbl WHERE (status = :status)");
                 $cat_query->execute(array(
                    ':status' => 'publish'
                 ));
              }

              $result['type_list'] = $cat_query->fetchAll(PDO::FETCH_ASSOC);


              #setup tag.
              $tg = $this->db->conn_id->prepare(
                  'SELECT tag_tbl.title
                   FROM tag_tbl

                   LEFT JOIN tag_manager
                   ON tag_manager.tag_path = tag_tbl.tag_path

                   WHERE
                   tag_manager.from_id = :satu
                   AND tag_manager.from_tbl = :post_tbl'
              );
                $tg->execute(array(
                  ':satu' => $id,
                  ':post_tbl' => 'tag_tbl'
                ));

                $result['tag'] = $tg->fetchAll(PDO::FETCH_COLUMN);


                #setup keyword targetting.
               $kw = $this->db->conn_id->prepare(
                   'SELECT keyword_manager.mykey, keyword_manager.myvalue
                    FROM keyword_manager
                    WHERE from_tbl = :from_tbl AND from_id = :from_id'
               );
                 $kw->execute(array(
                   ':from_tbl' => 'tag_tbl',
                   ':from_id' => $id
                 ));
                 $keyword = $kw->fetchAll(PDO::FETCH_ASSOC);

                 $result['keyword']['gender'] = array();
                 $result['keyword']['interest'] = array();

                 if($keyword){

                   foreach ($keyword as $bbb) {
                      if($bbb['mykey'] == 'gender') array_push($result['keyword']['gender'], $bbb['myvalue']);
                      if($bbb['mykey'] == 'interest') array_push($result['keyword']['interest'], $bbb['myvalue']);
                   }

                 }



              $datas['table'] = $result;

              #inject role
              $current_role = user_info(current_user_id())['user_role'];
              $admin_gang = array('special admin', 'admin', 'special staff');
              if(in_array($current_role, $admin_gang)){
                $datas['table']['is_admin'] = 1;
              }else{
                $datas['table']['is_admin'] = '';
              }

              #you come after failed to publish post.. we push some message, you can use it in view!
             if($this->session->flashdata('error_submit_post')){
                $datas['table']['error_flash'] = $this->session->flashdata('error_submit_post');
             } else {
                $datas['table']['error_flash'] = false;
             }

              $this->load->view('v_wiki/v_tulis/v_main', $datas);


       } else {

           #come without url queries.
           $this->load->view('v_wiki/v_table');

       }


     }






  }


  function draft(){
     $this->session->set_flashdata('status', 'pending');
     redirect(site_url('wiki'));
  }


 function pending(){
    $this->session->set_flashdata('status', 'submit');
    redirect(site_url('wiki'));
 }

function published(){
   $this->session->set_flashdata('status', 'publish');
   redirect(site_url('wiki'));
 }




} // end class..