<?php

class Moderateuser extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->library('session');
    $this->l_login->cek_login(current_url());
    $this->load->helper(array('h_user_helper'));
    $this->output->set_header('X-Robots-Tag: noindex');
    set_status_header('403');
  }


  public function index(){

    $pangkat = user_info(current_user_id())['user_role'];

    $admin_gang = array('special admin', 'admin', 'special staff');

    if(in_array($pangkat, $admin_gang)){
    $this->load->view('v_user_table');
  } else {
    $this->load->view('404');
  }

  } // end index function

  function changerole(){

    $pangkat = user_info(current_user_id())['user_role'];

     $selected_emails = $this->input->post('user-email');
     $action = $this->input->post('role-to');

   if($pangkat == 'special admin'){ //shah
     $check = array('admin', 'special admin', 'author', 'special author', 'staff', 'special staff', 'banned');
   }

   if($pangkat == 'admin'){ //adam
     $check = array('author', 'special author', 'staff', 'special staff', 'banned');
   }

   if($pangkat == 'special staff') { //pekerja adam dan shah
     $check = array('staff', 'author', 'special author', 'banned');
   }

   if($pangkat == 'special admin' || $pangkat == 'admin' || $pangkat == 'special staff' && in_array($action, $check)){
     // you can

     $actions['user_role'] = $action;

     $this->m_account->moderate_user($actions, $selected_emails);


   } else {
     print_r('Pangkat anda sekarang tidak memadai untuk moderate user dari role ini.');
   }

  }




} // end class HAHAHA
