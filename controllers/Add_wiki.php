<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_wiki extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->l_login->cek_login(current_url());
    $this->load->helper(array('h_random_helper', 'security', 'h_user_helper', 'h_link_helper', 'url'));
    $this->load->model(array('M_add_wiki'));
  }

  public function index(){
     $this->M_add_wiki->new_wiki();
  }

}