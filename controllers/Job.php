<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Job extends CI_Controller {

   function __construct() {
      parent::__construct();
      $this->load->library('session');
      $this->l_login->cek_login(current_url());
      $this->load->helper(array('security', 'h_user_helper', 'h_link_helper', 'url','h_html_helper'));
      $this->load->model(array('M_upload'));
   }


   public function index(){
      #global variable.
      $post_id =  $this->input->get('id', TRUE);
      $status_query= $this->input->get('status', TRUE);
      $from_tbl = $status_query;

      $current_role = user_info(current_user_id())['user_role'];
      $admin_gang = array('special admin', 'admin', 'special staff');


      if( in_array($current_role, $admin_gang) ){ #admin
         $allowed_from_tbl = array('draft','pending','publish');
      } else { #author biasa
         $allowed_from_tbl = array('draft','pending');
						}

						print_r('hi');

      if( is_numeric($post_id) && in_array($from_tbl, $allowed_from_tbl) ){

         if( in_array($current_role, $admin_gang) ){

            #if admin gang.
            // $pdo = $this->db->conn_id->prepare(
            //    'SELECT * FROM post_tbl WHERE id = :post_id AND status = :status ORDER BY masa'
            // );
            // $pdo->execute(array(
            //    ':post_id'=>$post_id,
            //    ':status' => $from_tbl
            // ));
												// $datas['table'] = $pdo->fetch(PDO::FETCH_ASSOC);

         } else {

            #if normal user.
            // $abc = $this->db->conn_id->prepare(
            //    'SELECT * FROM post_tbl WHERE id = :id AND author_id = :author_id AND status = :status ORDER BY masa'
            // );
            // $abc->execute(array(
            //    ':id'=>$post_id,
            //    ':author_id'=>current_user_id(),
            //    ':status' => $from_tbl
            // ));
												// $datas['table'] = $abc->fetch(PDO::FETCH_ASSOC);

         } #end in_array admin gang or normal user.

         if($datas['table']){

            #id wujud dan user ni punya.

												#setup tag.
												/*
            $tg = $this->db->conn_id->prepare(
                'SELECT tag_tbl.title
                 FROM tag_tbl

                 RIGHT JOIN tag_manager
                 ON tag_manager.from_id = :satu
                 AND tag_manager.from_tbl = :post_tbl
                 AND tag_manager.tag_path = tag_tbl.tag_path'
            );
              $tg->execute(array(
                ':satu' => $post_id,
                ':post_tbl' => 'post_tbl'
              ));
              $tg_title = $tg->fetchAll(PDO::FETCH_COLUMN);
              $tg_title = array_filter($tg_title);
              if($tg_title){
                $datas['table']['tag'] = $tg_title;
              } else {
                $datas['table']['tag'] = '';
              }

              #setup keyword targetting.
              $kw = $this->db->conn_id->prepare(
                  'SELECT keyword_manager.mykey, keyword_manager.myvalue
                   FROM keyword_manager
                   WHERE from_tbl = :from_tbl AND from_id = :from_id'
              );
                $kw->execute(array(
                  ':from_tbl' => 'post_tbl',
                  ':from_id' => $post_id
                ));
                $keyword = $kw->fetchAll(PDO::FETCH_ASSOC);

                if($keyword){
                  //$datas['table']['keyword'] = $keyword;
                  $datas['table']['keyword']['gender'] = array();
                  $datas['table']['keyword']['interest'] = array();
                  foreach ($keyword as $bbb) {
                     if($bbb['mykey'] == 'gender') array_push($datas['table']['keyword']['gender'], $bbb['myvalue']);
                     if($bbb['mykey'] == 'interest') array_push($datas['table']['keyword']['interest'], $bbb['myvalue']);
                  }
                } else {
                   $datas['table']['keyword']['gender'] = array();
                   $datas['table']['keyword']['interest'] = array();
                }

            #setup is_admin
            if(in_array($current_role, $admin_gang)){
               $datas['table']['is_admin'] = 1;
            }else{
               $datas['table']['is_admin'] = '';
            }
            $ct = $this->db->conn_id->prepare(
               "SELECT domain from category_manager WHERE from_tbl = :from_tbl AND from_id = :from_id"
            );
            $ct->execute(array(
               ':from_tbl'=>'post_tbl',
               ':from_id'=>$post_id
            ));
            if($ct){
               $datas['table']['category']=$ct->fetch(PDO::FETCH_COLUMN);
            } else {
               $datas['table']['category'] = '';
            }
            #you come after failed to publish post.. we push some message, you can use it in view!
            if($this->session->flashdata('error_submit_post')){
               $datas['table']['error_flash'] = $this->session->flashdata('error_submit_post');
            } else {
               $datas['table']['error_flash'] = false;
            }
            $datas['table']['from_tbl'] = $from_tbl;
            $datas['table']['status'] = $status_query;
            $this->session->set_userdata('post_id', $post_id);
												$this->load->view('v_post/v_tulis/v_main', $datas);
												*/
												// var_dump($datas['table']['keyword']);

         } else {

            #normal user = this post not your own. || admin_gang = this post already publish or id not exists.
            // redirect(site_url('add-post'));
									}

      } else {

         #id not found or url did't have query.

         // $this->load->view('v_post/v_table');

      }



   }#end funciton.

}#end class
