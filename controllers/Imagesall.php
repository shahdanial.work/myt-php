<?php

/*
 * http://www.zyxware.com/articles/5134/how-can-we-slimply-create-a-static-page-in-codeigniter
 */

class Imagesall extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->l_login->cek_login(current_url());
    $this->output->set_header('X-Robots-Tag: noindex');
    //set_status_header('403');
  }
  public function index() {
    $this->load->view('v_api/v_imagesall.php');
  }
}
