<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do Moree
    //$this->simple_login->cek_login(current_url());
    //$this->load->helper(array());
    //$this->output->set_header(array());
    //$this->load->library(array());
    //$this->load->model(array());
    $this->load->helper(array('h_user_helper','h_link_helper','url','form','security', 'h_random_helper', 'h_function_helper'));
    set_status_header('404');
  }

 function _remap($method, $params=array()){
        $method_exists = method_exists($this, $method);
        $methodToCall = $method_exists ? $method : 'index';
        $this->$methodToCall($method_exists ? $params : $method);
 }

  function index($path) {
      $referer = false;
      if(isset($_SERVER['HTTP_REFERER'])) {
          if(allowed_domain($_SERVER['HTTP_REFERER'])){
             $referer = $_SERVER['HTTP_REFERER'] || false;
          }
       }

     if(isset($_SERVER['REQUEST_URI'])) if(sizeof(explode('?', $_SERVER['REQUEST_URI'])) != 1) redirect(current_url(),'location',301);

      if($path){

         $filename = str_replace('--', '.', str_replace('_', '-', $path));

         if(file_exists('./gambar/' . $filename) && !is_dir('./gambar/' . $filename)){
            $get_data = $this->db->conn_id->prepare(
                 'SELECT file_tbl.*, users.namapena
                 FROM file_tbl
                 LEFT JOIN users
                 ON file_tbl.uploader = users.id_user
                 WHERE file_tbl.filename = :filename
                 LIMIT 0,1'
            );
            $get_data->execute(array(
                 ':filename' => $filename
            ));

            $db_res = $get_data->fetch(PDO::FETCH_ASSOC);
            if($db_res){
               set_status_header('200');
               $data = $db_res;

               #back button.
               $data['referer'] = $referer;

                #get related image.
                $query = $this->db->conn_id->prepare(
                     'SELECT file_tbl.filename, file_tbl.caption FROM file_tbl WHERE MATCH(file_tbl.caption) AGAINST(:keyword  IN BOOLEAN MODE) AND file_tbl.id != :id LIMIT 0,10'
                );
                $query->execute(array(
                     ':keyword' => $data['caption'],
                     ':id' => $data['id']
                ));

                $r_res = $query->fetchAll(PDO::FETCH_ASSOC);

                if($r_res) $data['related'] = $r_res;

                $datas['data'] = $data;
                $this->load->view('v_image/v_index', $datas);

            }
         }

      }
  }




   // function follow($param){
   //    print_r($param);
   // }
}