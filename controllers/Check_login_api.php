<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Check_login_api extends CI_Controller{

     public function __construct()
     {
          parent::__construct();
          //Codeigniter : Write Less Do Moree
          //$this->simple_login->cek_login(current_url());
          $this->load->library(array('session', 'user_agent'));
          $this->load->helper(array('h_email_helper', 'h_user_helper', 'security', 'url'));
          //$this->output->set_header(array());
          //$this->load->library(array());
          //$this->load->model(array());
     }

     function index(){

          if( current_user_id() ){ #must login
               // if($this->agent->referrer()){ #mesti ada refferer.
               //    $dari = $this->agent->referrer(); // full url..
               //    parse_str(parse_url($dari, PHP_URL_QUERY), $queries);
               //    $the_id = trim($queries['id']);
               //
               //    $from_tbl =  trim($queries['status']) . '_tbl';
               //
               //    $status_query= $this->input->get('status', TRUE);
               //    $from_tbl = $status_query . '_tbl';

               $status = $this->input->post('status', FALSE);
               $from_tbl = $status . '_tbl';
               $id = $this->input->post('id' ,FALSE);

               $pangkat = user_info(current_user_id())['user_role'];
               $admin_gang = array('special admin', 'admin', 'special staff');



               if( in_array($pangkat, $admin_gang) ){

                    #admin bhai.. directly pass. print number
                    print_r($id);

               } else {

                  #bukan admin.

                    if ( is_numeric($id) ){


                         $query = $this->db->conn_id->prepare('SELECT author_id FROM post_tbl WHERE id = :post_id AND author_id = :author_id AND status != :status');
                         $query->execute(array(
                              ':post_id' => $id,
                              ':author_id' => current_user_id(),
                              ':status' => 'publish'
                         ));
                         $result = $query->fetch(PDO::FETCH_COLUMN);

                         if($result == current_user_id()){

                              #author id sepadan dengan id login sekang. pass. print number
                              print_r($result);

                         } else {
                            $aut_id = $this->db->conn_id->prepare(
                               'SELECT users.email FROM users
                               LEFT JOIN post_tbl ON post_tbl.author_id = users.id_user
                               WHERE post_tbl.id = :post_id'
                            );
                            $aut_id->execute(array(
                                 ':post_id' => $id
                            ));
                            $author_email = $aut_id->fetch(PDO::FETCH_COLUMN);

                            if($author_email){
                               print_r('Akaun tidak sepadan dengan post, sila login akaun '. show_safe_email($get_email));
                            } else {
                               print_r('there\'s error to validiate Post Author in our system, please report to shahdanial.work@gmail.com');
                            }

                            /*
                              // post id and author id not matching.. please login with email below..
                              $aut_id = $this->db->conn_id->prepare('SELECT author_id FROM post_tbl WHERE id = :post_id');
                              $aut_id->execute(array(
                                   ':post_id' => $id
                              ));
                              $author_id = $aut_id->fetch(PDO::FETCH_COLUMN);
                              if($author_id){
                                   $get_mail = $this->db->conn_id->prepare('SELECT email FROM users WHERE id_user = :author_id');
                                   $get_mail->execute(array(
                                        ':author_id' => $author_id
                                   ));
                                   $get_email = $get_mail->fetch(PDO::FETCH_COLUMN);
                                   if($get_email){
                                        print_r('Akaun tidak sepadan dengan post, sila login akaun '. show_safe_email($get_email));
                                   }
                              }
                              else {

                                   #print_r('aikkk... takde author id?untuk post id '. $id);
                                   print_r('error, please report to shahdanial.work@gmail.com, id not found');

                              }
                              */
                         }

                    } else {
                         print_r('error, please report to shahdanial.work@gmail.com, id not numberic');
                    }

               }

               // }
          } else {
               print_r('Session expired, please login again');
          }
     }#end function..

}
