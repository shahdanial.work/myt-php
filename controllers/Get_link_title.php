<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Get_link_title extends CI_Controller{

   public function __construct(){
      parent::__construct();
      $this->load->library(array('session', 'user_agent'));
      $this->l_login->cek_login(current_url());
      $this->load->helper(array('security', 'h_link_helper', 'url'));
      set_status_header('400');
   }

   function index(){

            parse_str(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $queries);
            if ( isset($queries['link']) && !empty($queries['link']) ){
               $URL = trim(urldecode($queries['link']));

               $ch=curl_init($URL);
               curl_setopt($ch, CURLOPT_VERBOSE, true);
               curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
               curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
               $data = curl_exec($ch);
               curl_close($ch);
               #$data is full HTML of page..
               $dom = new domDocument;
               libxml_use_internal_errors(true);
               $dom->loadHTML($data);
               libxml_clear_errors();


               $dom->preserveWhiteSpace = false;
               $title = $dom->getElementsByTagName('title')[0];
               if($title){
                  set_status_header('200');
                  print_r($title->textContent);
               } else {
                  set_status_header('400');
               }
            } else {
               $this->load->view('400');
            }
   }

}// end class..
