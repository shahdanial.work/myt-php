<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wiki_submit extends CI_Controller{

   function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'user_agent'));
        $this->l_login->cek_login(current_url());
        $this->load->helper(array('security', 'h_user_helper', 'h_link_helper', 'url'));
        $this->load->model(array('m_img', 'm_wikis'));
   }

   /* THIS AREA ONLY FOR AUTOSAVE (AJAX) */


  public function index(){

               $my_from_id = trim($this->input->post('id', FALSE));

               $current_role = user_info(current_user_id())['user_role'];
               $admin_gang = array('special admin', 'special staff');




               if( is_numeric($my_from_id) ){ // security also verify wiki is own by current user

                  if(in_array($current_role, $admin_gang)){
                     $query = $this->db->conn_id->prepare('SELECT status, author_id FROM tag_tbl WHERE id = :post_id');
                     $query->execute(array(
                          ':post_id' => $my_from_id
                     ));
                     $the_action = 'publish';
                  } else {
                     $query = $this->db->conn_id->prepare('SELECT status, author_id FROM tag_tbl WHERE id = :post_id AND author_id = :author_id');
                     $query->execute(array(
                          ':post_id' => $my_from_id,
                          ':author_id' => current_user_id()
                     ));
                     $the_action = 'submit';
                  }

                  $from_status = $query->fetch(PDO::FETCH_COLUMN);


                  if($from_status){


                     if($this->input->is_ajax_request()){
                          $redirect = false;
                     } else {
                          $redirect = site_url('wiki');
                     }

                     date_default_timezone_set("Asia/Kuala_Lumpur");


                     $final_first_thumb = '';
                     $semi_first_thumb = trim($this->security->xss_clean($this->input->post('post-thumbnails-path')));
                     if($semi_first_thumb){
                          if(allowed_domain($semi_first_thumb)){
                               $final_first_thumb = pathinfo($semi_first_thumb)['basename'];#ex my-image-path.png
                               if( !file_exists('./gambar/' . $final_first_thumb) ){
                                    $final_first_thumb = '';
                               }
                          }
                     }

                     $final_google_thumb = '';
                     $semi_google_thumb = trim($this->security->xss_clean($this->input->post('seo-thumbnails-path')));
                     if($semi_google_thumb){
                          if(allowed_domain($semi_google_thumb)){
                               $final_google_thumb = pathinfo($semi_google_thumb)['basename'];#ex my-image-path.png
                               if( !file_exists('./gambar/' . $final_google_thumb) ){
                                    $final_google_thumb = '';
                               }
                          }
                     }

                     $final_viral_thumb = '';
                     $semi_viral_thumb = trim($this->security->xss_clean($this->input->post('viral-thumbnails-path')));
                     if($semi_viral_thumb){
                          if(allowed_domain($semi_viral_thumb)){
                               $final_viral_thumb = pathinfo($semi_viral_thumb)['basename'];#ex my-image-path.png
                               if( !file_exists('./gambar/' . $final_viral_thumb) ){
                                    $final_viral_thumb = '';
                               }
                          }
                     }

                     #tulis section.
                     $data = array(
                          'title' => trim( str_replace("\"", "'", $this->security->xss_clean($this->input->post('title')) )),
                          'content' => trim($this->input->post('content', FALSE)),

                          'type' => trim($this->security->xss_clean($this->input->post('category'))),

                          'tag' => $this->input->post('tag[]'),
                          'gender' => $this->input->post('gender[]', FALSE),
                          'interest' => $this->input->post('interest[]', FALSE),

                          'meta_title' => trim($this->security->xss_clean($this->input->post('meta_title'))),
                          'meta_description' => trim($this->security->xss_clean($this->input->post('meta_description'))),

                          'first_img' => $final_first_thumb,

                          #google
                          'meta_title' => trim($this->security->xss_clean($this->input->post('meta_title'))),
                          'meta_description' => trim($this->security->xss_clean($this->input->post('meta_description'))),
                          'google_thumb' => $final_google_thumb,

                          #facebook
                          'viral_title' => trim($this->security->xss_clean($this->input->post('viral_title'))),
                          'viral_description' => trim($this->security->xss_clean($this->input->post('viral_meta_description'))),
                          'viral_thumb' => $final_viral_thumb,

                          'from_id' => $my_from_id,
                          'redirect' => $redirect,

                          'status' => $from_status // save draft kan? so status guna status asal..
                     );

                     ////////////////////////////// :CHECKING: //////////////////////////////////

                     $error_msg = array();
                    if($data['content']){



                         if(in_array($current_role, $admin_gang)){ /* for admin only: publish purpose.. */
                              #CHECK TITLE
                              if(strlen($data['title']) < 3){
                                   array_push($error_msg, '<li>Minimum Title ada 3 aksara.</li>');
                              } elseif(strlen($data['title']) > 60 && empty($data['meta_title'])){
                                   array_push($error_msg, '<li>Memandangkan <span class="red">Title</span> post anda melebihi 60 aksara, anda hendaklah menetapkan <span class="red">SEO Title</span>.</li>');
                              } elseif(strlen($data['title']) > 100){
                                   array_push($error_msg, '<li><span class="red">Title</span> post tidak boleh melebihi 100 aksara.</li>');
                              }

                              if(strlen($data['meta_title']) > 60){
                                   array_push($error_msg, '<li><b>SEO Title</b> tidak boleh melebihi 60 aksara</li>');
                              } elseif (!empty($data['meta_title']) && strlen($data['meta_title']) < 3){
                                   array_push($error_msg, '<li><b>SEO Title</b> tidak boleh kurang dari 3 aksara</li>');
                              }


                              ////////////// PEMERIKSAAN TITLE UTAMA (META_TITLE/TITLE) ///////////////
                              if(!empty($data['meta_title'])){
                                   $title_utama = $data['meta_title'];
                                   $msg_duplicate_title = '<li><b>SEO Title</b> yang anda tetapkan telah digunakan di wiki lain. Sila ubah agar berlainan <span class="red">(disarankan)</span>, atau biarkan kosong.</li>';
                              } else {
                                   $title_utama = $data['title'];
                                   $msg_duplicate_title = '<li><b>Wiki title</b> yang anda tetapkan telah digunakan di wiki lain. Sila ubah <b>Wiki title</b> atau tetapkan <b>SEO Title</b> yang unik <span class="red">(disarankan)</span>.</li>';
                              }

                              #checking title make sure not duplicate in post_tbl

                              $check_title_record = false;

                              if($data['status'] == 'publish'){ #hanya jika nak publish
                                   $check_title_record = $this->db->conn_id->prepare(
                                        "SELECT tag_tbl.id FROM tag_tbl

                                        #LEFT JOIN type_manager ON type_manager.from_tbl = :from_tbl AND type_manager.from_id = tag_tbl.id

                                        WHERE
                                        tag_tbl.id <> :id
                                        AND
                                        tag_tbl.tag_path = :tag_path
                                        AND
                                        (LOWER(tag_tbl.title) = :title_utama AND tag_tbl.meta_title = :kosong)
                                        OR (LOWER(tag_tbl.meta_title) = :title_utama_1)

                                        LIMIT 0,1"
                                   );
                                   $check_title_record->execute(array(
                                        ':id' => $data['from_id'],
                                        ':tag_path' => $data['category'],
                                        ':title_utama' => strtolower($title_utama),
                                        ':kosong' => '',
                                        ':title_utama_1' => strtolower($title_utama)
                                   ));

                                   $id_title_ada =  $check_title_record->fetch(PDO::FETCH_COLUMN);

                                   if($id_title_ada){
                                       array_push($error_msg, $msg_duplicate_title);
                                   }

                              }
                              ////////////// PEMERIKSAAN TITLE UTAMA (TITLE/META_TITLE) ///////////////

                              ////////////// PEMERIKSAAN [SEO META] ///////////////
                              if(!empty($data['meta_description'])){

                                   if($data['status'] == 'publish'){

                                        $check_description_record = $this->db->conn_id->prepare(
                                             'SELECT id FROM tag_tbl WHERE meta_description = :description_kau AND id <> :id'
                                        );

                                        $check_description_record->execute(array(
                                             ':description_kau' => $data['meta_description'],
                                             ':id' => $data['from_id']
                                        ));

                                        $id_description_ada = $check_description_record->fetch(PDO::FETCH_COLUMN, 0);

                                        if(is_numeric($id_description_ada)){

                                                  array_push($error_msg, '<li><b>SEO Description</b> ini telah digunakan di wiki lain. <span class="red">Sila olah sedikit agar ianya berlainan atau unik (lebih baik)</span>, atau biarkan ianya kosong.</li>');

                                        }

                                   }


                                   if(strlen($data['meta_description']) > 150){
                                       array_push($error_msg, '<li><b>SEO Description</b> tidak boleh melebihi 150 aksara</li>');
                                  } elseif(!empty($data['meta_description']) && strlen($data['meta_description']) < 100){
                                       array_push($error_msg, '<li><b>SEO Description</b> tidak boleh kurang dari 100 aksara.</li>');
                                  }

                              }
                              ////////////// PEMERIKSAAN VIRAL META] ///////////////

                              if(strlen($data['viral_title']) > 60){
                                   array_push($error_msg, '<li><b>Viral Title</b> tidak boleh melebihi 60 aksara</li>');
                              }
                              if(strlen($data['viral_description']) > 150){
                                   array_push($error_msg, '<li><b>Viral Description</b> tidak boleh melebihi 150 aksara</li>');
                              }


                              #CHECK CATEGORY
                              if(!$data['type']){
                                   array_push($error_msg, '<li>Mesti tetapkan <b>Wiki type</b></li>');
                              }

                              #CHECK POST THUMBNAIL.
                              if(empty($data['first_img'])){
                                   array_push($error_msg, '<li>Wajib tetapkan <b>Post Thumbnail</b></li>');
                              }
                         }

                         #CHECK POST
                         $dom_content = new domDocument;
                         libxml_use_internal_errors(true);
                         $dom_content->loadHTML($data['content']);
                         libxml_clear_errors();
                         $dom_content->preserveWhiteSpace = false;
                         $finder = new DomXPath($dom_content);

                         #check content para.
                         $para = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' para ')]");
                         if($para->length < 3){
                              array_push($error_msg, '<li>Link ke website luar selain dari <i>malayatimes.com</i>, tidak boleh <b>melebihi 2</b>.</li>');
                         }

                         $dom_images = $dom_content->getElementsByTagName('img');
                         #check content external image.
                         for ($i=0; $i < $dom_images->length; $i++) {
                              $o = $i+1;
                              if($dom_images[$i]->getAttribute('src')){
                                   if( !allowed_domain($dom_images[$i]->getAttribute('src')) ){
                                        array_push($error_msg, '<li>Gambar ke <b>'. $o .'</b> tidak dibenarkan kerana menggunakan gambar yang bukan diupload dari media.malayatimes.com.</li>');
                                   }
                              } else {
                                   array_push($error_msg, '<li>Element gambar ke <b>'. $o .'</b> tidak mempunyai attribute <b>src</b>.</li>');
                              }
                         }

                         #check content image length. DISABLED
                         // if($dom_images->length < 1){
                         //    array_push($error_msg, '<li>Wajib sekurang-kurangnya ada satu gambar yang berkaitan di dalam post.</li>');
                         // }


                         $dom_link = $dom_content->getElementsByTagName('a');
                         $dom_external_link = array();
                         for ($i=0; $i < $dom_link->length; $i++) {
                              if( !allowed_domain($dom_link[$i]->getAttribute('href')) ){
                                   array_push($dom_external_link, $dom_link[$i]);
                              }
                         }
                         if(sizeof($dom_external_link) > 2){
                              array_push($error_msg, '<li>Link ke website luar selain dari <i>malayatimes.com</i>, tidak boleh <b>melebihi 2</b>.</li>');
                         }

                    } else { // ELSE $data['content']
                         array_push($error_msg, '<li>Content tidak boleh kosong tanpa sebarang perenggan.</li>');
                    } #END IF $data['content']

                     ////////////////////////////// :CHECKING: //////////////////////////////////


                     if($this->agent->referrer()){ #mesti ada refferer.
                          $dari = $this->agent->referrer();
                          if(allowed_domain($dari)){

                             if(sizeof($error_msg)>0){

                                 #ada error.

                                 #save draft lu (status guna status lama)..
                                           $this->session->set_flashdata('error_submit_post', $error_msg);
                                           $data['redirect'] = site_url('wiki?id=') . $data['from_id'] . '&status=' . $data['status'];
                                           $this->m_wikis->post_proccess($data);


                            } else {

                               #takde error

                               #publish terus (if admin), submit terus (if user)...
                                         $data['status'] = $the_action;
                                         $this->m_wikis->post_proccess($data);

                            }
                          }
                     }


                  }




         } else {#is_numeric($my_from_id)
                 print_r('You action not allowed');
                 // print_r($my_from_id);
                 print_r('<br>');
                 if( in_array($current_role, $admin_gang) ){
                      print_r($my_from_id);
                      print_r('<br>');
                 }
         } #is_numeric($my_from_id)

  } // end index function

}
