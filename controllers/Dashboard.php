 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 class Dashboard extends CI_Controller {
     function __construct(){
         parent::__construct();
         $this->l_login->cek_login(current_url());
     }

     //Load Halaman dashboard
     public function index() {
         $this->load->view('v_account/v_dashboard');
     }
 }
