<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_link extends CI_Controller{

   public function __construct(){
      parent::__construct();
      $this->load->library(array('session', 'user_agent'));
      $this->l_login->cek_login(current_url());
      $this->load->helper(array('security', 'h_link_helper', 'url'));
      set_status_header('404');
   }

   function index(){
      $this->load->view('404');
   }

   function title(){
      if($this->agent->referrer()){ #mesti ada refferer.
         $dari = $this->agent->referrer();
         if(allowed_domain($dari)){

            parse_str(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $queries);
            if ( isset($queries['cari']) && !empty($queries['cari']) ){
               set_status_header('200');
               $data = $queries;
               $this->load->view('v_search_link/v_title', $data);
            } else {
               $this->load->view('404');
            }
         }#allowed_domain
      }#refferer
   }

   function link(){
      if($this->agent->referrer()){ #mesti ada refferer.
         $dari = $this->agent->referrer();
         if(allowed_domain($dari)){

            parse_str(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $queries);
            if ( isset($queries['cari']) && !empty($queries['cari']) ){
               set_status_header('200');
               $myurl = urldecode($queries['cari']);
               $myhtml = file_get_contents($myurl);
               $mydoc = new DOMDocument();
               @$mydoc->loadHTML($myhtml);
               $hmm = $mydoc->getElementsByTagName('title');
               if ($hmm->length > 0) {
                  $title = $hmm->item(0)->textContent;
                  echo $title;
               } else {
                  $this->load->view('404');
               }
            } else {
               $this->load->view('404');
            }
         }#allowed_domain
      }#refferer
   }

}
